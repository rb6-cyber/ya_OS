модуль TFSMTPServer;	(** AUTHOR "TF"; PURPOSE "Forward any emails received to one or more email addresses"; *)

использует
		ЛогЯдра, Modules, Потоки, Files, IP, TCP, TCPServices, Dates, Strings, SMTPClient, TFClasses,
		XML, XMLScanner, XMLParser, XMLObjects, Configuration;

тип
	DomainName = массив 128 из симв8;
	String = Strings.String;
	Line = окласс перем s :String кон Line;

	Message = окласс
	перем
		fromIP : IP.Adr;
		fromDomain : DomainName;
		timestamp : массив 64 из симв8;
		data : TFClasses.List;

		проц &Init*;
		нач
			нов(data);
			Strings.FormatDateTime("yyyy.mm.dd hh:nn:ss", Dates.Now(), timestamp);
		кон Init;

		проц AddLine(конст x : массив из симв8);
		перем l : размерМЗ; s : String; line : Line;
		нач
			l := Strings.Length(x) + 1; нов(s, l); копируйСтрокуДо0(x, s^); нов(line); line.s := s; data.Add(line);
		кон AddLine;
	кон Message;

	Account = окласс
	перем userName : String;
		mailAliases : TFClasses.List;
		forwardAddress : String;

		проц &Init*;
		нач
			нов(mailAliases);
		кон Init;

		проц AddAlias(s : String);
		перем l : Line;
		нач
			Strings.LowerCase(s^);
			нов(l); l.s := s; mailAliases.Add(l)
		кон AddAlias;

		проц IsAlias(конст s : массив из симв8) : булево;
		перем i : размерМЗ; p : динамическиТипизированныйУкль;
			isAlias : булево;
		нач
			mailAliases.Lock;
			isAlias := ложь;
			нцДля i := 0 до mailAliases.GetCount() - 1 делай
				p := mailAliases.GetItem(i);
				если (p # НУЛЬ) и (p суть Line) и (p(Line).s # НУЛЬ) и (Strings.Match(p(Line).s^, s)) то isAlias := истина всё
			кц;
			mailAliases.Unlock;
			возврат isAlias
		кон IsAlias;

		проц DumpAccount;
		перем i : размерМЗ; p : динамическиТипизированныйУкль;
		нач
			ЛогЯдра.пСтроку8("Account : "); ЛогЯдра.пСтроку8(userName^); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("Aliases :");
			mailAliases.Lock;
			нцДля i := 0 до mailAliases.GetCount() - 1 делай
				p := mailAliases.GetItem(i);
				если (p # НУЛЬ) и (p суть Line) и (p(Line).s # НУЛЬ)  то ЛогЯдра.пСтроку8(p(Line).s^); ЛогЯдра.пВК_ПС всё
			кц;
			mailAliases.Unlock;

			ЛогЯдра.пСтроку8("Forward address : ");
			если forwardAddress # НУЛЬ то ЛогЯдра.пСтроку8(forwardAddress^)
			иначе ЛогЯдра.пСтроку8("<not set>")
			всё;
			ЛогЯдра.пВК_ПС;
			ЛогЯдра.пВК_ПС;
		кон DumpAccount;

		проц SaveMessage(m : Message) : булево;
		перем path, filePath : массив 256 из симв8;
			f : Files.File; w : Files.Writer;
			i : размерМЗ; res: целМЗ; p : динамическиТипизированныйУкль;
		нач
			копируйСтрокуДо0(mailPath^, path);
			Strings.Append(path, "/"); Strings.Append(path, userName^);
			копируйСтрокуДо0(path, filePath);
			Strings.Append(filePath, "/mailbox.txt");
			f := Files.Old(filePath);
			если f = НУЛЬ то Files.CreateDirectory(path, res);
				f := Files.New(filePath);
			всё;
			если f # НУЛЬ то
				Files.OpenWriter(w, f, f.Length());
				(* append mail *)
				m.data.Lock;
				нцДля i := 0 до m.data.GetCount() - 1 делай
					p := m.data.GetItem(i);
					если (p # НУЛЬ) и (p суть Line) и (p(Line).s # НУЛЬ)  то w.пСтроку8(p(Line).s^); w.пВК_ПС всё
				кц;
				m.data.Unlock;
				w.ПротолкниБуферВПоток;
				Files.Register(f);
				возврат истина;
			иначе
				возврат ложь
			всё;
		кон SaveMessage;

		проц Forward(m : Message; конст sender : массив из симв8) : булево;
		перем smtpSession : SMTPClient.SMTPSession; p : динамическиТипизированныйУкль;
			res: целМЗ; i : размерМЗ;
		нач
			нов(smtpSession);
			smtpSession.Open(mailRelay, mailHost, 25, res);
			если res # 0 то
				ЛогЯдра.пСтроку8("Could not forward... SMTP - Relay is not available"); ЛогЯдра.пВК_ПС;
				возврат ложь
			всё;
			если smtpSession.StartMailFrom(sender) и smtpSession.SendTo(forwardAddress^) и smtpSession.StartData() то
				m.data.Lock;
				нцДля i := 0 до m.data.GetCount() - 1 делай
					p := m.data.GetItem(i);
					если (p # НУЛЬ) и (p суть Line) и (p(Line).s # НУЛЬ)  то smtpSession.SendRawLine(p(Line).s^) всё
				кц;
				m.data.Unlock;
				если ~smtpSession.FinishSendRaw() то
					ЛогЯдра.пСтроку8("could not forward message"); ЛогЯдра.пВК_ПС;
					smtpSession.Close;
					возврат ложь
				всё;
				smtpSession.Close;
			всё;
			возврат истина
		кон Forward;

		(* must be exclusive *)
		проц ReceiveMessage(m : Message; конст sender: массив из симв8) : булево;
		перем ok : булево;
		нач { единолично }
			ЛогЯдра.пСтроку8("====");
			ЛогЯдра.пСтроку8("Recieved message to account "); ЛогЯдра.пСтроку8(userName^); ЛогЯдра.пВК_ПС;
			ok := SaveMessage(m);
			если (forwardAddress # НУЛЬ) и (forwardAddress^ # "") то ok := ok и Forward(m, sender) всё;
			возврат ok
		кон ReceiveMessage;
	кон Account;

	SMTPAgent* = окласс (TCPServices.Agent)
	перем quit : булево;
			id : массив 1024 из симв8;
			domain, sender, token : массив 64 из симв8;
			in: Потоки.Чтец; out: Потоки.Писарь;
			isSpam: булево;
			recipients : TFClasses.List;
			message : Message;

		проц AddRecipient(перем x : массив из симв8);
		перем l, p : размерМЗ; s : String; line : Line;
		нач
			p := Strings.Pos("<", x); если p >= 0 то Strings.Delete(x, 0, p + 1) всё; Strings.TrimRight(x, ">");
			l := Strings.Length(x) + 1 ; если l = 1 то возврат всё;
			нов(s, l); копируйСтрокуДо0(x, s^); нов(line); line.s := s; recipients.Add(line)
		кон AddRecipient;

		проц Reply(code: цел32; конст text1, text2 : массив из симв8);
		нач
			out.пЦел64(code, 3);  out.пСтроку8(" "); out.пСтроку8(text1); out.пСтроку8(" "); out.пСтроку8(text2);
			out.пВК_ПС; out.ПротолкниБуферВПоток
		кон Reply;

		проц Init():булево;
		нач
			in.чЦепочкуСимв8ДоБелогоПоля(token);
			если EqualsI(token, "HELO") то in.ПропустиПробелыИТабуляции; in.чСтроку8ДоКонцаСтрокиТекстаВключительно(domain);
				Reply(250, id, "");
				возврат истина
			аесли EqualsI(token, "QUIT") то in.ПропустиДоКонцаСтрокиТекстаВключительно; quit := истина; возврат ложь
			аесли EqualsI(token, "NOOP") то in.ПропустиДоКонцаСтрокиТекстаВключительно; Reply(250, "ok", ""); возврат ложь
			всё;
			Reply(500,  "Command unrecognized", token); in.ПропустиДоКонцаСтрокиТекстаВключительно;
			возврат ложь
		кон Init;

		проц From():булево;
		нач
			in.чЦепочкуСимв8ДоБелогоПоля(token);
			если EqualsI(token, "MAIL") то
				in.ПропустиПробелыИТабуляции; in.чСтроку8ДоКонцаСтрокиТекстаВключительно(sender);
				Reply(250, sender, "...Sender ok");
				возврат истина
			аесли EqualsI(token, "QUIT") то in.ПропустиДоКонцаСтрокиТекстаВключительно; quit := истина; возврат ложь
			аесли EqualsI(token, "NOOP") то in.ПропустиДоКонцаСтрокиТекстаВключительно; Reply(250, "ok", ""); возврат ложь
			всё;
			Reply(500,  "Command unrecognized", token); in.ПропустиДоКонцаСтрокиТекстаВключительно;
			возврат ложь
		кон From;

		проц Rcpt():булево;
		перем recipient : массив 1024 из симв8; errcount : цел32;
		нач
			errcount := 0; isSpam:=истина;
			нц
				in.чЦепочкуСимв8ДоБелогоПоля(token);
				если in.кодВозвратаПоследнейОперации # 0 то quit := истина; возврат ложь всё;
				если EqualsI(token, "RCPT") то
					in.ПропустиПробелыИТабуляции; in.чСтроку8ДоКонцаСтрокиТекстаВключительно(recipient);
					Strings.LowerCase(recipient);
					AddRecipient(recipient);
					Reply(250, recipient, "...Recipient ok"); errcount := 0;
				аесли EqualsI(token, "QUIT") то in.ПропустиДоКонцаСтрокиТекстаВключительно; quit := истина; возврат ложь
				аесли EqualsI(token, "RSET") то in.ПропустиДоКонцаСтрокиТекстаВключительно; Reply(250, "Reset state", ""); возврат ложь
				аесли EqualsI(token, "NOOP") то in.ПропустиДоКонцаСтрокиТекстаВключительно; Reply(250, "ok", ""); errcount := 0;
				аесли EqualsI(token, "DATA") то in.ПропустиДоКонцаСтрокиТекстаВключительно; Reply(354, "Start mail input; end with <CRLF>.<CRLF>", "");
					возврат истина
				иначе Reply(500,  "Command unrecognized", token); in.ПропустиДоКонцаСтрокиТекстаВключительно;
					увел(errcount); если errcount = 5 то возврат ложь всё;
				всё
			кц
		кон Rcpt;

		проц Data():булево;
		перем line : массив 1024 из симв8;
		нач
			нов(message);
			нцДо
				in.чСтроку8ДоКонцаСтрокиТекстаВключительно(line);
				message.AddLine(line)
			кцПри (line = ".") или (in.кодВозвратаПоследнейОперации # 0);
			возврат in.кодВозвратаПоследнейОперации = 0
		кон Data;

		проц SaveLostMessage(m : Message) : булево;
		перем path, filePath : массив 256 из симв8;
			f : Files.File; w : Files.Writer;
			i : размерМЗ; res: целМЗ; p : динамическиТипизированныйУкль;
		нач
			копируйСтрокуДо0(mailPath^, path);
			Strings.Append(path, "/"); Strings.Append(path, "lost");
			копируйСтрокуДо0(path, filePath);
			Strings.Append(filePath, "/mailbox.txt");
			f := Files.Old(filePath);
			если f = НУЛЬ то Files.CreateDirectory(path, res);
				f := Files.New(filePath);
			всё;
			если f # НУЛЬ то
				Files.OpenWriter(w, f, f.Length());
				(* append mail *)
				m.data.Lock;
				нцДля i := 0 до m.data.GetCount() - 1 делай
					p := m.data.GetItem(i);
					если (p # НУЛЬ) и (p суть Line) и (p(Line).s # НУЛЬ)  то w.пСтроку8(p(Line).s^); w.пВК_ПС всё
				кц;
				m.data.Unlock;
				w.ПротолкниБуферВПоток;
				Files.Register(f);
				возврат истина;
			иначе
				возврат ложь
			всё;
		кон SaveLostMessage;

		проц DeliverMessage() : булево;
		перем i, j : размерМЗ; ap, sp : динамическиТипизированныйУкль;
			isReceiver : булево;
			ok : булево;
			lost : булево;
		нач
			ok := истина; lost := истина;
			recipients.Lock;
			accounts.Lock;
			нцДля i := 0 до accounts.GetCount() - 1 делай
				ap := accounts.GetItem(i);
				если (ap # НУЛЬ) и (ap суть Account) то
					isReceiver := ложь;
					нцДля j := 0 до recipients.GetCount() - 1 делай
						sp := recipients.GetItem(j);
						если (sp # НУЛЬ) и (sp суть Line) и (sp(Line).s # НУЛЬ) и (ap(Account).IsAlias(sp(Line).s^)) то isReceiver := истина всё
					кц;
					если isReceiver то
						если ~ap(Account).ReceiveMessage(message, sender) то ok := ложь иначе lost := ложь всё
					всё
				всё;
			кц;
			accounts.Unlock;
			recipients.Unlock;
			если lost то
				recipients.Lock;
				ЛогЯдра.пСтроку8("Lost message to "); ЛогЯдра.пВК_ПС;
				нцДля j := 0 до recipients.GetCount() - 1 делай
					sp := recipients.GetItem(j);
					если (sp # НУЛЬ) и (sp суть Line) и (sp(Line).s # НУЛЬ) то ЛогЯдра.пСтроку8(sp(Line).s^); ЛогЯдра.пВК_ПС всё;
				кц;
				если SaveLostMessage(message) то ЛогЯдра.пСтроку8("Saved. in lost messages"); ЛогЯдра.пВК_ПС всё;
				ЛогЯдра.пВК_ПС; ЛогЯдра.пВК_ПС;
				recipients.Unlock;
			всё;
			возврат ok
		кон DeliverMessage;

	нач {активное}
		id := "Bimbo SMPT Server";
		(* open streams *)
		Потоки.НастройЧтеца(in, client.ПрочтиИзПотока);
		Потоки.НастройПисаря(out, client.ЗапишиВПоток);

		(* read request *)
		quit := ложь;
		Reply(220, id, "Simple Mail Transfer Service Ready");
		нцДо кцПри Init() или quit или (in.кодВозвратаПоследнейОперации # 0) или (out.кодВозвратаПоследнейОперации # 0);
		нцДо
			если ~quit то нцДо кцПри From() или quit или (in.кодВозвратаПоследнейОперации # 0) или (out.кодВозвратаПоследнейОперации # 0) всё;
			если ~quit то
				message := НУЛЬ;
				нов(recipients);
				если Rcpt() то
					если Data() то
						если DeliverMessage() то Reply(250, id, "ok") иначе Reply(500, id, "internal failure"); quit := истина всё;
					иначе Reply(550, id, "Failure"); quit := истина
					всё
				всё
			всё
		кцПри quit;
		если quit то Reply(221, id, "Service closing transmission channel") всё;
		client.Закрой();
		Terminate
	кон SMTPAgent;

перем smtp : TCPServices.Service;
	accounts : TFClasses.List;
	mailPath : String;
	mailRelay, mailHost : массив 64 из симв8;
	mailConfig : XML.Document;
	errors : булево;

проц EqualsI(конст buf, with: массив из симв8): булево;
перем j: цел32;
нач
	j := 0; нцПока (with[j] # 0X) и (ASCII_вЗаглавную(buf[j]) = ASCII_вЗаглавную(with[j])) делай увел(j) кц;
	возврат ASCII_вЗаглавную(with[j]) = ASCII_вЗаглавную(buf[j])
кон EqualsI;

проц NewSMTPAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: SMTPAgent;
нач
	нов(a, c, s); возврат a
кон NewSMTPAgent;

проц TrapHandler(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
нач
	ЛогЯдра.пСтроку8("Mail config error at pos "); ЛогЯдра.пЦел64(pos, 0); ЛогЯдра.пВК_ПС;
	errors := истина
кон TrapHandler;

проц LoadSettings(конст filename : массив из симв8) : булево;
перем f: Files.File; scanner: XMLScanner.Scanner; parser: XMLParser.Parser;
	reader: Files.Reader;
нач {единолично}
	errors := ложь;
	f := Files.Old(filename);
	если f # НУЛЬ то
		нов(reader, f, 0);
		нов(scanner, reader); нов(parser, scanner); parser.reportError := TrapHandler; mailConfig := parser.Parse();
		если errors  то ЛогЯдра.пСтроку8("Mail config file contains errors."); ЛогЯдра.пВК_ПС; возврат ложь всё
	иначе ЛогЯдра.пСтроку8("Mail config file not found : "); ЛогЯдра.пСтроку8(filename); ЛогЯдра.пВК_ПС; возврат ложь
	всё;
	возврат истина
кон LoadSettings;

проц CreateAccounts;
перем acc, aliases, e : XML.Element;
		p : динамическиТипизированныйУкль; enum, enumAlias: XMLObjects.Enumerator;
		a : Account;
		acs : TFClasses.List;
нач
	нов(acs);
	acc := Configuration.GetSection("Accounts");
	если acc # НУЛЬ то
		enum := acc.GetContents();
		нцПока enum.HasMoreElements() делай
			p := enum.GetNext();
			если p суть XML.Element то
				e := p(XML.Element);
				нов(a);
				a.userName := e.GetAttributeValue("name");
				a.forwardAddress := e.GetAttributeValue("forward");
				aliases := Configuration.GetNamedElement(e, "Section", "Aliases");
				enumAlias := aliases.GetContents();
				нцПока enumAlias.HasMoreElements() делай
					p := enumAlias.GetNext();
					если (p # НУЛЬ) и (p суть XML.Element) то
						e := p(XML.Element);
						a.AddAlias(e.GetAttributeValue("name"));
					всё
				кц;
				a.DumpAccount;
				acs.Add(a);
			всё
		кц;
	всё;
	accounts := acs
кон CreateAccounts;

проц Start*;
перем e: XML.Element; a: XML.Attribute; s : String; res : целМЗ;
нач
	если smtp = НУЛЬ то
		если LoadSettings("TFMailConfig.XML") то
			CreateAccounts;
			e := Configuration.GetNamedElement(mailConfig.GetRoot(), "Setting", "path");
			если (e # НУЛЬ) то a := e.GetAttribute("value"); если a # НУЛЬ то mailPath := a.GetValue() всё всё;

			e := Configuration.GetNamedElement(mailConfig.GetRoot(), "Setting", "relay");
			если (e # НУЛЬ) то a := e.GetAttribute("value"); если a # НУЛЬ то s := a.GetValue() всё; если s # НУЛЬ то копируйСтрокуДо0(s^, mailRelay) всё всё;

			e := Configuration.GetNamedElement(mailConfig.GetRoot(), "Setting", "host");
			если (e # НУЛЬ) то a := e.GetAttribute("value"); если a # НУЛЬ то s := a.GetValue() всё; если s # НУЛЬ то копируйСтрокуДо0(s^, mailHost) всё всё;


			если mailPath = НУЛЬ то нов(mailPath, 2) всё;
			Strings.TrimRight(mailPath^, "/");
			нов(smtp, 25, NewSMTPAgent, res);
			ЛогЯдра.пСтроку8("TFSMTPServer started:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8(" mail directory ");ЛогЯдра.пСтроку8(mailPath^); ЛогЯдра.пВК_ПС;
		иначе
			ЛогЯдра.пСтроку8("TFMailConfig.XML not correct, not started"); ЛогЯдра.пВК_ПС
		всё
	иначе
		ЛогЯдра.пСтроку8("Already running."); ЛогЯдра.пВК_ПС
	всё;
кон Start;

проц Stop*;
нач
	если smtp # НУЛЬ то
		smtp.Stop(); smtp := НУЛЬ;
		ЛогЯдра.пСтроку8("Bimbo SMTP server stopped"); ЛогЯдра.пВК_ПС;
	всё;
кон Stop;

проц Cleanup;
нач
	Stop;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон TFSMTPServer.


EditTools.OpenAscii TFMailConfig.XML
System.Free TFSMTPServer ~
Aos.Call TFSMTPServer.Start
