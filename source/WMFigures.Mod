модуль WMFigures; (** AUTHOR "Patrick Hunziker, with inspirations from staubesv, gadgets source"; PURPOSE "Geometric shapes"; *)

использует
	ЛогЯдра, Потоки, Math, Strings, XML, WMRectangles, WMGraphics, WMGraphicUtilities, WMProperties, WMComponents;

конст
	(* Figure.state *)
	Filled* = 0;
	Closed* = 1;
	EditPoints* = 2;
	Reshape*=3;
	Arrow*=4;

	PointSize = 6; (* size of the control points. Currently not related to Effects.gravity *)

тип
	Point* = укль на запись
		x, y : размерМЗ;
		previous, next : Point;
		кон;

тип
	Figure* = окласс(WMComponents.VisualComponent)
	перем
		width- : WMProperties.Int32Property; (* in pixels. should rather be real. lineWidth=0 means hairline. *)
		color-, clHover- : WMProperties.ColorProperty;
		closed-: WMProperties.BooleanProperty;
		filled-: WMProperties.BooleanProperty;
		reshape-: WMProperties.BooleanProperty;
		arrow-: WMProperties.BooleanProperty;

		points- : Point;
		nofPoints : цел32;

		hover, selectedPoint:WMProperties.Property;
		selectedLine:булево;
		mouseOver:булево;

		lastKeys, state : мнвоНаБитахМЗ;

		oldx,oldy:размерМЗ;
		PArray: WMProperties.PropertyArray; (* PArray#NIL !*)

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrFigure);
			нов(width, PrototypeWidth, НУЛЬ, НУЛЬ); properties.Add(width);
			нов(color, PrototypeColor, НУЛЬ, НУЛЬ); properties.Add(color);
			нов(reshape, PrototypeReshape, НУЛЬ, НУЛЬ); properties.Add(reshape);
			нов(arrow, PrototypeArrow, НУЛЬ, НУЛЬ); properties.Add(arrow);
			нов(closed, PrototypeClosed, НУЛЬ, НУЛЬ); properties.Add(closed);
			нов(filled, PrototypeFilled, НУЛЬ, НУЛЬ); properties.Add(filled);
			нов(clHover, PrototypeclHover, НУЛЬ, НУЛЬ); properties.Add(clHover);
			state := {};
			если closed.Get() то включиВоМнвоНаБитах(state,Closed)  всё;
			если filled.Get() то включиВоМнвоНаБитах(state,Filled) всё;
			если reshape.Get() то включиВоМнвоНаБитах(state,Reshape) всё;
			если arrow.Get() то включиВоМнвоНаБитах(state,Arrow) всё;
			points := НУЛЬ;
			nofPoints := 0;
			hover := НУЛЬ;
			selectedPoint := НУЛЬ;
			lastKeys := {};
			нов(PArray,0);
		кон Init;

		проц AddPoint*(x, y : размерМЗ); (* in stable parent coordinates (because coordinates relative to the figure bound change with normalization) *)
		перем
			s: массив 16 из симв8;
			pp:WMProperties.PointProperty;
			(*a: XML.Attribute;*)
		нач
			Strings.IntToStr(nofPoints,s);
			Strings.Concat("Point",s,s);
			увел(nofPoints);
			нов(pp,НУЛЬ,Strings.NewString(s),НУЛЬ);
			pp.SetCoordinate(x,y);
			selectedPoint:=pp;
			Acquire;
			properties.Add(pp);
			Release;
		кон AddPoint;

		(* x,y in Figure coordinates, which may change when adding new points etc. Remember that PointProperty is in (stable) parent coordinates*)
		проц MovePoint*(point: WMProperties.Property; fx, fy: размерМЗ);
		нач
			point(WMProperties.PointProperty).SetCoordinate(fx+bounds.GetLeft(), fy+bounds.GetTop());
		кон MovePoint;

		(* remove point coordinates are currently in figure coordinates, in contrast to AddPoint() *)
		проц RemovePoint*(fx, fy : размерМЗ);
		перем
			pp:WMProperties.Property;
		нач
			pp:=ThisPoint(fx,fy);
			если pp#НУЛЬ то
				Acquire;
				properties.Remove(pp);
				Release;
			всё;
		кон RemovePoint;

		(** Return point located at mouse position fx. fy, which are in Figure coordinates that change, e.g. when points are added. (NIL if no point at location). *)
		проц ThisPoint*(fx, fy : размерМЗ): WMProperties.Property;
		перем  i:размерМЗ; px,py: размерМЗ;
		нач
			i:=0;
			нцПока i<длинаМассива(PArray) делай
				если (PArray[i]#НУЛЬ) и  (PArray[i] суть WMProperties.PointProperty) то
					PArray[i](WMProperties.PointProperty).GetCoordinate(px,py);
					если Invicinity(fx, fy, px-bounds.GetLeft(), py-bounds.GetTop()) то  возврат PArray[i] всё;
				всё;
				увел(i);
			кц;
			возврат НУЛЬ;
		кон ThisPoint;

		(** Return point Nr 'index' .  Negative numbers are counted top-down.  *)
		проц IndexedPoint*(index : цел32): WMProperties.Property;
		перем  i,j:размерМЗ;
		нач
			i:=0; j:=0;
			если index >=0 то
				нцПока i<длинаМассива(PArray) делай
					если (PArray[i]#НУЛЬ) и  (PArray[i] суть WMProperties.PointProperty) то
						если j=index то возврат PArray[i]
						иначе увел(j)
						всё;
					всё;
					увел(i);
				кц;
			иначе
				i:=длинаМассива(PArray)-1; j:=1; index:=-index;
				нцПока i>=0 делай
					если (PArray[i]#НУЛЬ) и  (PArray[i] суть WMProperties.PointProperty) то
						если j=index то возврат PArray[i]
						иначе увел(j)
						всё;
					всё;
					умень(i);
				кц;
			всё;
			возврат НУЛЬ;
		кон IndexedPoint;

		проц MoveFigure*(dx,dy:размерМЗ);
		перем i: размерМЗ; x,y:размерМЗ;
		нач
			Acquire;
			если PArray=НУЛЬ то PArray:=properties.Enumerate(); всё; (*?redundant*)
			если PArray=НУЛЬ то возврат всё;
			нцДля i:=0 до длинаМассива(PArray)-1 делай
				если (PArray[i]#НУЛЬ) и (PArray[i] суть WMProperties.PointProperty) то
					PArray[i](WMProperties.PointProperty).GetCoordinate(x, y);
					PArray[i](WMProperties.PointProperty).SetCoordinate(x+dx, y+dy);
				всё;
			кц;
			Release;
		кон MoveFigure;

		(* fx,fy in Figure coordinate system, NOT in parent coordinates*)
		проц {перекрыта}PointerDown*(fx, fy : размерМЗ; keys : мнвоНаБитахМЗ);
		нач
			lastKeys := keys;
			если (0 в keys) то
				oldx:=fx+bounds.GetLeft(); oldy:=fy+bounds.GetTop();
				selectedPoint := ThisPoint(fx, fy);
				если (selectedPoint # НУЛЬ) то (*Invalidate*) иначе selectedLine:=истина; mouseOver:=ложь всё;
				Invalidate;
			аесли (EditPoints в state) и (keys={1})  то
				AddPoint(bounds.GetLeft()+fx, bounds.GetTop()+fy);
				PropertyChanged(сам, properties);
			всё;
		кон PointerDown;

		проц {перекрыта}PointerUp*(fx, fy : размерМЗ; keys : мнвоНаБитахМЗ);
		перем dx,dy,bt,bl:размерМЗ;
		нач
			bl:=bounds.GetLeft();
			bt:=bounds.GetTop();
			если Reshape в state то
				если keys*{0,1}#{}  то
					если (selectedPoint # НУЛЬ) то
						MovePoint(selectedPoint, fx, fy);
					иначе
						dx:=fx+bl-oldx; oldx:=fx+bl;
						dy:=fy+bt-oldy; oldy:=fy+bt;
						MoveFigure(dx, dy);
					всё;
				аесли (EditPoints в state) то
					если (lastKeys={2}) и (keys#{2}) то
						RemovePoint(fx,fy);
						PropertyChanged(сам, properties);
					иначе
					всё;
				(*ELSIF (2 IN lastKeys) & ~(2 IN keys) THEN
					IF Reshape IN state THEN EXCL(state, Reshape); ELSE INCL(state, Reshape); END;
					Invalidate;
					*)
				иначе
				всё;
			иначе(* PointerUp^(x, y, keys);*)
			всё;
			selectedLine:=ложь;
			selectedPoint:=НУЛЬ;
		кон PointerUp;

		проц {перекрыта}PointerMove*(fx, fy : размерМЗ; keys : мнвоНаБитахМЗ);
		перем dx,dy,bl,bt:размерМЗ; pp:WMProperties.Property;
		нач
			если (Reshape в state)  и (keys*{0,1}#{})  и (selectedPoint # НУЛЬ) то
				MovePoint(selectedPoint, fx, fy);
			аесли (Reshape в state)  и (keys={0})  и selectedLine то
				bl:=bounds.GetLeft();
				bt:=bounds.GetTop();
				dx:=fx+bl-oldx; oldx:=fx+bl;
				dy:=fy+bt-oldy; oldy:=fy+bt;
				MoveFigure(dx, dy);
			(*ELSIF (Reshape IN state) & ~(0 IN keys) THEN
				mouseOver:=IsHit(fx,fy);
			*)
			иначе
				(*bl:=bounds.GetLeft();
				bt:=bounds.GetTop();
				PointerMove^(fx+bl, fy+bt, keys);
				pp := ThisPoint(fx+bl, fy+bt);
				hover:=pp;
				mouseOver := ~mouseOver; *)
			всё;
		кон PointerMove;

				(* Is X, Y somewhere inside the polygon defined by p ? *)
		проц Inside*(X, Y: размерМЗ): булево;(*Prototype*)
		кон Inside;

		(** Return if the line is hit at (x, y) in parent coordinates *)
		проц {перекрыта}IsHit*(x, y: размерМЗ): булево; (*Prototype*)
		кон IsHit;

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
		нач
			если (property = color) то Invalidate;
			аесли (property = width) то Invalidate;
			аесли (property = clHover) то Invalidate;
			аесли (property = closed) то если closed.Get() то включиВоМнвоНаБитах(state,Closed) иначе исключиИзМнваНаБитах(state,Closed);  всё; Invalidate;
			аесли (property = filled) то если filled.Get() то включиВоМнвоНаБитах(state,Filled) иначе исключиИзМнваНаБитах(state,Filled); всё; Invalidate;
			аесли (property = reshape) то если reshape.Get() то включиВоМнвоНаБитах(state,Reshape) иначе исключиИзМнваНаБитах(state,Reshape); всё; Invalidate;
			аесли (property = arrow) то если arrow.Get() то включиВоМнвоНаБитах(state,Arrow) иначе исключиИзМнваНаБитах(state,Arrow); всё; Invalidate;
			аесли (property суть WMProperties.PointProperty) или (property=properties) то
				RecachePoints;
				Normalize;
				Resized;(*implied Invalidate*)
			иначе  PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц AddDisplayPoint(x, y : размерМЗ);
		перем point, p : Point;
		нач
			Acquire;
			нов(point); point.x := x; point.y := y; point.previous := НУЛЬ; point.next := НУЛЬ;
			если (points = НУЛЬ) то points := point;
			иначе
				p := points;
				нцПока (p.next # НУЛЬ) делай p := p.next; кц;
				p.next := point; point.previous := p;
			всё;
			увел(nofPoints);
			Release;
		кон AddDisplayPoint;

		проц MoveDisplayPoints(dx, dy : цел32);
		перем p : Point;
		нач
			Acquire;
			p := points;
			нцПока (p # НУЛЬ) делай	p.x := p.x + dx; p.y := p.y + dy; p := p.next; кц;
			Release;
		кон MoveDisplayPoints;

		проц Normalize;
		перем p:Point; oldrect, rect: WMRectangles.Rectangle; dx,dy:размерМЗ;
		нач
			Acquire;
			rect.l := матМаксимум(размерМЗ); rect.t := матМаксимум(размерМЗ); rect.r := матМинимум(размерМЗ); rect.b := матМинимум(размерМЗ);
			p:=points;
			нцПока p#НУЛЬ делай (* adapt display point coordinates to new bounds *)
				rect.l:=матМинимум(rect.l, p.x-PointSize DIV 2 -1); rect.r:=матМаксимум(rect.r, p.x+PointSize DIV 2+1);  (*compute bounds*)
				rect.t:=матМинимум(rect.t, p.y-PointSize DIV 2 -1); rect.b:=матМаксимум(rect.b, p.y+PointSize DIV 2+1);
				p:=p.next;
			кц;
			p:=points;
			нцПока p#НУЛЬ делай (* adapt display point coordinates to new bounds *)
				p.x:=p.x-rect.l;
				p.y:=p.y-rect.t;
				p:=p.next;
			кц;
			bounds.Set(rect);
			Release;
		кон Normalize;

		проц Scale;
		кон Scale;

		проц GetBoundingBox() : WMRectangles.Rectangle; (*! will be eliminated*)
		перем rect : WMRectangles.Rectangle; i:размерМЗ;
		нач
			rect.l := матМаксимум(размерМЗ); rect.t := матМаксимум(размерМЗ);
			rect.r := матМинимум(размерМЗ); rect.b := матМинимум(размерМЗ);
			если PArray#НУЛЬ то PArray:=properties.Enumerate(); всё;
			если PArray=НУЛЬ то возврат rect всё;
			нцДля i:=0 до длинаМассива(PArray)-1 делай
				если PArray[i] суть WMProperties.PointProperty то
					rect.l:=матМинимум(rect.l, PArray[i](WMProperties.PointProperty).GetX());
					rect.r:=матМаксимум(rect.r, PArray[i](WMProperties.PointProperty).GetX());
					rect.t:=матМинимум(rect.t, PArray[i](WMProperties.PointProperty).GetY());
					rect.b:=матМаксимум(rect.b, PArray[i](WMProperties.PointProperty).GetY());
				всё;
			кц;
			возврат rect;
		кон GetBoundingBox;

		проц {перекрыта}RecacheProperties*;
		нач (* called by sequencer -> hierarchy is locked, see WMVisualComponentSkeleton.Mod *)
			если closed.Get() то включиВоМнвоНаБитах(state,Closed) иначе исключиИзМнваНаБитах(state,Closed) всё;
			если filled.Get() то включиВоМнвоНаБитах(state,Filled); иначе исключиИзМнваНаБитах(state, Filled) всё;
			если reshape.Get() то включиВоМнвоНаБитах(state,Reshape) иначе исключиИзМнваНаБитах(state, Reshape) всё;
			если arrow.Get() то включиВоМнвоНаБитах(state,Arrow) иначе исключиИзМнваНаБитах(state, Arrow) всё;
			RecacheProperties^;
			RecachePoints;
			Normalize;
			Resized;(* implied Invalidate - that is redundant*)
		кон RecacheProperties;

		проц RecachePoints; (*build point list in parent coordinates*)
		перем p:Point;	i:размерМЗ; x,y:размерМЗ;
		нач
			Acquire;
			PArray:=properties.Enumerate();
			points:=НУЛЬ; p:=НУЛЬ; nofPoints:=0;
			нцДля i:=0 до длинаМассива(PArray)-1 делай
				если (PArray[i]#НУЛЬ) и  (PArray[i] суть WMProperties.PointProperty) то
					PArray[i](WMProperties.PointProperty).GetCoordinate(x,y);
					AddDisplayPoint(x, y);
				всё;
			кц;
			Release;
		кон RecachePoints;

		проц DrawDisplayPoint(canvas : WMGraphics.Canvas; pp: WMProperties.PointProperty);
		перем rect : WMRectangles.Rectangle; color, x,y,fx,fy : размерМЗ;
		нач
			утв(pp # НУЛЬ);
			если (pp = selectedPoint) то color := WMGraphics.Yellow;
			аесли (pp = hover) то color := WMGraphics.Blue;
			иначе color := WMGraphics.White;
			всё;

			pp.GetCoordinate(x,y);
			fx:=x-bounds.GetLeft();
			fy:=y-bounds.GetTop();
			rect := WMRectangles.MakeRect(fx- PointSize DIV 2, fy - PointSize DIV 2, fx + PointSize DIV 2, fy + PointSize DIV 2);
			canvas.Fill(rect, WMGraphics.White, WMGraphics.ModeSrcOverDst);
			WMGraphicUtilities.DrawRect(canvas, rect, WMGraphics.Black, WMGraphics.ModeSrcOverDst);
		кон DrawDisplayPoint;

		проц {перекрыта}DrawForeground*(canvas : WMGraphics.Canvas);
		перем a: булево; i:размерМЗ;
		нач
			DrawForeground^(canvas);
			a:=arrow.Get();
			если reshape.Get() то
				нцДля i:=0 до длинаМассива(PArray)-1 делай
					если (PArray[i]#НУЛЬ) и  (PArray[i] суть WMProperties.PointProperty) то
						DrawDisplayPoint(canvas, PArray[i](WMProperties.PointProperty));
					всё;
				кц;
			всё;
		кон DrawForeground;

		проц DrawArrow*(canvas : WMGraphics.Canvas; p0,p1: Point);
		конст pi=3.1516;
		перем  alpha: вещ32;
			 head: вещ64;
			col: WMGraphics.Color;
		нач
				alpha:=arctan2(p1.x-p0.x, p1.y-p0.y);
				head:=матМаксимум( 4,  0.05 * матМаксимум(матМодуль(p1.x-p0.x), матМодуль(p1.y-p0.y))); (*avoid sqrt for performance reasons*)
				col:=color.Get();
				canvas.Line(p1.x,p1.y, p1.x - округлиВниз(0.5+head * Math.cos(alpha + pi/8)), p1.y - округлиВниз(0.5+head * Math.sin(alpha + pi/8)), col, WMGraphics.ModeSrcOverDst);
				canvas.Line(p1.x,p1.y, p1.x - округлиВниз(0.5+head * Math.cos(alpha - pi/8)), p1.y - округлиВниз(0.5+head * Math.sin(alpha - pi/8)), col, WMGraphics.ModeSrcOverDst);
		кон DrawArrow;

	кон Figure;

тип

	PointArray = укль на массив из WMGraphics.Point2d;

	Line* = окласс(Figure)
	перем
		pointArray : PointArray; (* {pointArray # NIL} *)

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMFigures.GenLine");
			SetNameAsString(StrLine);
			включиВоМнвоНаБитах(state, EditPoints);
			RecachePoints;
			нов(pointArray, nofPoints);
		кон Init;

		проц {перекрыта}Initialize*;
		перем pp:WMProperties.PointProperty;
		нач
			RecachePoints;
			если nofPoints=0 то (* prototype*)
				Acquire;
				нов(pp,НУЛЬ,Strings.NewString("Point0"),НУЛЬ); pp.SetCoordinate(5,20); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point1"),НУЛЬ); pp.SetCoordinate(20,0); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point2"),НУЛЬ); pp.SetCoordinate(20,20); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point3"),НУЛЬ); pp.SetCoordinate(30,30); properties.Add(pp); увел(nofPoints);
				Release;
				RecachePoints;
			всё;
			Normalize;
			Initialize^;
		кон Initialize;

		проц {перекрыта}Scale;
		перем p : Point; bounds, box : WMRectangles.Rectangle; oldWidth, oldHeight, newWidth, newHeight, n : размерМЗ;
		нач
			Acquire;
			bounds := сам.bounds.Get();
			box := GetBoundingBox();
			oldWidth := box.r - box.l;
			oldHeight := box.b - box.t;
			n := (PointSize DIV 2) + (width.Get() DIV 2) + 1;
			newWidth := bounds.r - bounds.l - 2*n;
			newHeight := bounds.b - bounds.t - 2*n;
			если (oldWidth # 0) и (oldHeight # 0) то
				p := points;
				нцПока (p # НУЛЬ) делай
					p.x := (p.x - box.l) * newWidth DIV oldWidth + box.l;
					p.y := (p.y - box.t) * newHeight DIV oldHeight + box.t;
					p := p.next;
				кц;
			всё;
			Release;
		кон Scale;

		(* Is X, Y somewhere inside the polygon defined by p ? *)
		проц {перекрыта}Inside*(X, Y: размерМЗ): булево;
		перем c: цел32; p, q: Point;
		нач
			c := 0;
			если (points # НУЛЬ) то
				p := points; q:=p.next;
				нцПока q#НУЛЬ делай
					если Intersect(X, Y, p.x, p.y, q.x, q.y) то увел(c) всё;
					p:=q; q:=q.next;
				кц;
				если (nofPoints > 1) и Intersect(X, Y, p.x, p.y, points.x, points.y) то увел(c) всё;
			всё;
			возврат нечётноеЛи¿(c);
		кон Inside;

		проц {перекрыта}IsHit*(mx, my: размерМЗ): булево;
		перем p, q: Point; i : цел32;
		нач
			если (points = НУЛЬ) или (points.next = НУЛЬ) то возврат ложь; всё;
			если ~ (Reshape в state) то возврат ложь
			аесли Filled в state то
				если Inside(mx, my) то возврат истина всё;
			всё;
			p := points; q := points.next;
			нцПока (q # НУЛЬ) делай
				если InLineVicinity(mx, my, p.x, p.y, q.x, q.y) то возврат истина всё;
				p:=q; q:=q.next; увел(i);
			кц;
			если (Closed в state) или (Filled в state) то
				если InLineVicinity(mx, my, p.x, p.y, points.x, points.y) то возврат истина всё;
			всё;
			возврат ложь
		кон IsHit;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем p, plast : Point; i : цел32;
		нач
			canvas.SetLineWidth(width.Get());
			DrawBackground^(canvas);
			если (nofPoints # длинаМассива(pointArray)) то нов(pointArray, nofPoints); всё;
			p := points; i := 0;
			нцПока (p # НУЛЬ) делай
				pointArray[i].x := p.x;
				pointArray[i].y := p.y;
				увел(i);
				plast:=p; p := p.next;
			кц;
			если (Arrow в state) и (plast#НУЛЬ) то DrawArrow(canvas, plast.previous, plast) всё;
			если Filled в state то    canvas.FillPolygonFlat(pointArray^, nofPoints, color.Get(), WMGraphics.ModeSrcOverDst);
			иначе canvas.PolyLine(pointArray^, nofPoints, closed.Get(), color.Get(), WMGraphics.ModeSrcOverDst);
			всё;
		кон DrawBackground;
	кон Line;

тип

	Circle* = окласс(Figure)

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMFigures.GenCircle");
			SetNameAsString(StrCircle);
			исключиИзМнваНаБитах(state, EditPoints);
		кон Init;

		проц {перекрыта}Initialize*;
		перем pp: WMProperties.PointProperty;
		нач
			RecachePoints;
			если nofPoints=0 то (* prototype*)
				нов(pp,НУЛЬ,Strings.NewString("Point0"),НУЛЬ); pp.SetCoordinate(15,15); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point1"),НУЛЬ); pp.SetCoordinate(30,30); properties.Add(pp); увел(nofPoints);
				bounds.Set(WMRectangles.MakeRect(0,0,40,40));
				RecachePoints;
			всё;
			Normalize;
			Initialize^;
		кон Initialize;

		проц {перекрыта}Normalize;
		перем p:Point; rect: WMRectangles.Rectangle; r,n:размерМЗ;
		нач
			rect.l := матМаксимум(размерМЗ); rect.t := матМаксимум(размерМЗ); rect.r := матМинимум(размерМЗ); rect.b := матМинимум(размерМЗ);
			p:=points;
			r := Distance(p.x, p.y, p.next.x, p.next.y);
			n := r + (PointSize DIV 2) + 1;
			(* adapt display point coordinates to new bounds *)
			rect.l:=p.x-n; rect.r:=p.x+n;  (*compute bounds*)
			rect.t:=p.y-n; rect.b:=p.y+n;
			p:=points;
			нцПока p#НУЛЬ делай (* adapt display point coordinates to new bounds *)
				p.x:=p.x-rect.l; p.y:=p.y-rect.t;
				p:=p.next;
			кц;
			bounds.Set(rect);
		кон Normalize;

		проц {перекрыта}IsHit*(mx, my: размерМЗ): булево;
		перем radius0, radius: размерМЗ;
		нач
			если ~ (Reshape в state) то возврат ложь всё;
			radius0:= Distance(points.x, points.y, points.next.x,points.next.y);
			radius:=Distance(mx,my, points.x,points.y);
			если (Filled в state) то возврат radius<=radius0 (*inside circle*)
			аесли radius < gravity то возврат истина (*center point hit*)
			аесли матМодуль(radius - radius0)<gravity то возврат истина (*boundary line hit*)
			иначе возврат ложь
			всё;
		кон IsHit;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем p, q : Point;
		нач
			DrawBackground^(canvas);
			RecachePoints;
			Normalize;
			если (points=НУЛЬ) или (points.next=НУЛЬ) то возврат всё;
			p := points;
			q := points.next;
			canvas.SetColor(color.Get());
			если (*Filled IN state*) ложь то (*canvas.FillPolygonFlat(pointArray^, nofPoints, color.Get(), WMGraphics.ModeSrcOverDst); *)(*! to be done: draw filled circle*)
			иначе WMGraphicUtilities.Circle(canvas, p.x, p.y, Distance(p.x, p.y, q.x, q.y));
			всё;
			если (Arrow в state) то DrawArrow(canvas, p,q); всё;
		кон DrawBackground;

	кон Circle;

тип

	Rectangle* = окласс(Figure)

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMFigures.GenRectangle");
			SetNameAsString(StrRectangle);
			исключиИзМнваНаБитах(state, EditPoints);
		кон Init;

		проц {перекрыта}Initialize*;
		перем pp: WMProperties.PointProperty;
		нач
			RecachePoints;
			если nofPoints=0 то (* prototype*)
				нов(pp,НУЛЬ,Strings.NewString("Point0"),НУЛЬ); pp.SetCoordinate(10,10); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point1"),НУЛЬ); pp.SetCoordinate(30,30); properties.Add(pp); увел(nofPoints);
				bounds.Set(WMRectangles.MakeRect(0,0,40,40));
				RecachePoints;
			всё;
			Normalize;
			Initialize^;
		кон Initialize;

		проц {перекрыта}IsHit*(mx, my: размерМЗ): булево;
		нач
			если ~ (Reshape в state) то возврат ложь
			аесли Filled в state то
				возврат ((mx-points.x)*(mx-points.next.x) <=0 ) и ((my-points.y)*(my-points.next.y) <=0 ) (* simple "in-between" test *)
			иначе возврат
				InLineVicinity(mx, my, points.x, points.y, points.x, points.next.y) или
				InLineVicinity(mx, my, points.x, points.y, points.next.x, points.y) или
				InLineVicinity(mx, my, points.x, points.next.y, points.next.x, points.next.y) или
				InLineVicinity(mx, my, points.next.x, points.y, points.next.x, points.next.y)
			всё;
		кон IsHit;


		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем p, q : Point; rect : WMRectangles.Rectangle;
		нач
			DrawBackground^(canvas);
			RecachePoints;
			Normalize;
			если (points=НУЛЬ) или (points.next=НУЛЬ) то возврат всё;
			p := points;
			q := points.next;
			rect.l := матМинимум(p.x, q.x);
			rect.r := матМаксимум(p.x, q.x);
			rect.t := матМинимум(p.y, q.y);
			rect.b := матМаксимум(p.y, q.y);
			если (*Filled IN state *) ложь то (*canvas.FillPolygonFlat(pointArray^, nofPoints, color.Get(), WMGraphics.ModeSrcOverDst);*) (*! to be done *)
			иначе WMGraphicUtilities.DrawRect(canvas, rect, color.Get(), WMGraphics.ModeSrcOverDst);
			всё;
		кон DrawBackground;

	кон Rectangle;

тип

	Spline* = окласс(Figure)
	перем
		pointArray : массив 2048 из WMGraphics.Point2d;
		nSegments:цел32;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetGenerator("WMFigures.GenSpline");
			SetNameAsString(StrSpline);
			включиВоМнвоНаБитах(state, EditPoints);
			RecachePoints;
		кон Init;

		проц {перекрыта}Initialize*;
		перем pp:WMProperties.PointProperty;
		нач
			если nofPoints=0 то (* default *)
				Acquire;
				нов(pp,НУЛЬ,Strings.NewString("Point0"),НУЛЬ); pp.SetCoordinate(5,20); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point1"),НУЛЬ); pp.SetCoordinate(20,0); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point2"),НУЛЬ); pp.SetCoordinate(20,20); properties.Add(pp); увел(nofPoints);
				нов(pp,НУЛЬ,Strings.NewString("Point3"),НУЛЬ); pp.SetCoordinate(30,30); properties.Add(pp); увел(nofPoints);
				Release;
			всё;
			RecachePoints;
			SplineToPoly(points, Closed в state, pointArray, nSegments);
			Normalize;
			Initialize^;
		кон Initialize;

			(* Is X, Y somewhere inside the polygon defined by p ? *) (*! to be implemented for pointarray*)
		проц {перекрыта}Inside*(X, Y: размерМЗ): булево;
		перем c: цел32; p,q: Point;
		нач
			c := 0;
			если (points # НУЛЬ) то
				p := points; q := p.next;
				нцПока q#НУЛЬ делай
					если Intersect(X, Y, p.x, p.y, q.x, q.y) то увел(c) всё;
					p:=q; q:=q.next;
				кц;
				если (nofPoints > 1) и Intersect(X, Y, p.x, p.y, points.x, points.y) то увел(c) всё;
			всё;
			возврат нечётноеЛи¿(c);
		кон Inside;

		проц {перекрыта}IsHit*(mx, my: размерМЗ): булево;
		перем p, q: Point; i : цел32;
		нач
			если (points = НУЛЬ) или (points.next = НУЛЬ) то возврат ложь; всё;
			если Filled в state то
				если Inside(mx, my) то возврат истина всё;
			всё;
			нцДля i:=0 до nSegments-1 делай
				если (матМодуль(mx-pointArray[i].x)<gravity) и (матМодуль(my-pointArray[i].y)<gravity) то возврат истина всё; (* here only testing segment points, can be refined*)
			кц;
			 (*!the following code is  for line hiting above. does not work so well for spline yet*)
			(*p := points; q := points.next;
			WHILE (q # NIL) DO
				IF InLineVicinity(mx, my, p.x, p.y, q.x, q.y) THEN RETURN TRUE END;
				p:=q; q:=q.next; INC(i);
			END;
			IF (Closed IN state) OR (Filled IN state) THEN
				IF InLineVicinity(mx, my, p.x, p.y, points.x, points.y) THEN RETURN TRUE END;
			END;*)
			возврат ложь
		кон IsHit;

		(*PROCEDURE PropertyChanged*(sender, property : ANY);
			BEGIN
				IF (property=closed) THEN
					IF closed.Get() THEN INCL(state,Closed) ELSE EXCL(state,Closed);  END;
					RecachePoints;
					SplineToPoly(points, Closed IN state, pointArray, nSegments);
					Normalize;
					Resized; (*implied Invalidate; *)
				ELSIF (property=properties) OR (property IS WMProperties.PointProperty) THEN
					RecachePoints;
					SplineToPoly(points, Closed IN state, pointArray, nSegments);
					Normalize;
					Resized;
					(*should call PropertyChanged^ in some cases here ?*)
				ELSE
					RecachePoints;
					SplineToPoly(points, Closed IN state, pointArray, nSegments);
					Normalize;
					Resized;
					PropertyChanged^(sender,property);
				END;
			END PropertyChanged;*)

		проц {перекрыта}PropertyChanged*(sender, property : динамическиТипизированныйУкль);
			нач
				RecacheProperties;
				SplineToPoly(points, Closed в state, pointArray, nSegments);
				Normalize;
				Resized;(* implied Invalidate - that is redundant*)
				если ~(property суть WMProperties.PointProperty) и ~(property=closed) то
					PropertyChanged^(sender,property);
				всё;
			кон PropertyChanged;

		проц {перекрыта}Normalize;
		перем i:цел32; oldrect, rect:WMRectangles.Rectangle; p:Point;
		нач
			oldrect:=bounds.Get();
			rect.l := матМаксимум(размерМЗ); rect.t := матМаксимум(размерМЗ); rect.r := матМинимум(размерМЗ); rect.b := матМинимум(размерМЗ);
			нцДля i:=0 до nSegments-1 делай
				rect.l:=матМинимум(rect.l, pointArray[i].x-PointSize DIV 2 -1); rect.r:=матМаксимум(rect.r, pointArray[i].x+PointSize DIV 2+1);  (*compute bounds*)
				rect.t:=матМинимум(rect.t, pointArray[i].y-PointSize DIV 2 -1); rect.b:=матМаксимум(rect.b, pointArray[i].y+PointSize DIV 2+1);
			кц;
			нцДля i:=0 до nSegments-1 делай
				pointArray[i].x:=pointArray[i].x-rect.l;
				pointArray[i].y:=pointArray[i].y-rect.t;
			кц;
			p:=points;
			нцПока p#НУЛЬ делай (* adapt display point coordinates to new bounds *)
				p.x:=p.x-rect.l; p.y:=p.y-rect.t;
				p:=p.next;
			кц;
			bounds.Set(rect);
		кон Normalize;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		перем p,pa: Point; col: WMGraphics.Color; d:вещ32;
		нач
			DrawBackground^(canvas);
			если mouseOver то col:=clHover.Get() иначе col:=color.Get() всё;
			если Filled в state то
				canvas.FillPolygonFlat(pointArray, nSegments, col, WMGraphics.ModeSrcOverDst);
			иначе
				canvas.PolyLine(pointArray, nSegments, closed.Get(), col, WMGraphics.ModeSrcOverDst);
			всё;
			если arrow.Get() (*(Arrow IN state)*) и (p#НУЛЬ) то
				(*
				d:=Math.sqrt( (p.x-p.previous.x)*(p.x-p.previous.x) + (p.y-p.previous.y)*(p.y-p.previous.y));
				d:=10/d;
				NEW(pa);
				pa.x:= p.x - ENTIER(d*(p.x-p.previous.x));
				pa.y:= p.y - ENTIER(d*(p.y-p.previous.y));
				DrawArrow(canvas, pa, p);
				*)
				DrawArrow(canvas, p.previous, p)
			всё;
		кон DrawBackground;
	кон Spline;

перем
	log: Потоки.Писарь;

	(* Size of gravity spot used for "snapping" the cursor *)
	gravity : цел32;

	PrototypeWidth : WMProperties.Int32Property;
	PrototypeColor, PrototypeclHover : WMProperties.ColorProperty;
	PrototypeClosed: WMProperties.BooleanProperty;
	PrototypeFilled: WMProperties.BooleanProperty;
	PrototypeReshape: WMProperties.BooleanProperty;
	PrototypeArrow: WMProperties.BooleanProperty;

	StrFigure, StrLine, StrCircle, StrRectangle, StrSpline : Strings.String;

проц arctan2(x,y: вещ32): вещ32; (*arctan in range 0..2pi*)
	нач
		если (x>0) и (y>=0) то возврат Math.arctan(y/x)
		аесли (x>0) и (y<0) то возврат Math.arctan(y/x)+2*Math.pi
		аесли x<0 то возврат Math.arctan(y/x)+Math.pi
		аесли (x=0) и (y>0) то возврат Math.pi/2
		аесли (x=0) и (y<0) то возврат 3*Math.pi/2
		иначе (*( x=0) & (y=0) *) возврат 0 (*or RETURN NaN ?*)
		всё
	кон arctan2;

(* start of Rege code *)

проц MakePoly(конст RX, RY, RXstrich, RYstrich, RS: массив из вещ32; n: цел32; перем points : массив из WMGraphics.Point2d; перем k: цел32);
 тип
	Polynom = запись A, B, C, D: вещ32 кон;
 перем
	i, cs, smax, k1: цел32; px, py: Polynom;
	x, dx1, dx2, dx3, y, dy1, dy2, dy3: вещ32; L, B, R, T,dW  : размерМЗ;

	проц GetPolynom((* VAR *) y1, y2, y1s, y2s: вещ32; перем p: Polynom);
		перем dx1, dyx: вещ32;
	нач
		если RS[i] # RS[i+1] то dx1 := 1.0/(RS[i + 1] - RS[i]) иначе dx1 := 1.0 всё;
		dyx := (y2 - y1)*dx1;
		p.A := dx1*dx1*(-2.0*dyx + y1s + y2s);
		p.B := dx1*(3.0*dyx - 2.0*y1s - y2s);
		p.C := y1s;
		p.D := y1
	кон GetPolynom;

нач
	points[0].x := устарПреобразуйКБолееУзкомуЦел(округлиВниз(RX[1])); points[0].y := устарПреобразуйКБолееУзкомуЦел(округлиВниз(RY[1]));
	L := матМаксимум(размерМЗ);  B := матМаксимум(размерМЗ); R := матМинимум(размерМЗ); T := матМинимум(размерМЗ);
	i := 1; нцПока i <= n делай
		L := матМинимум(L,устарПреобразуйКБолееУзкомуЦел(округлиВниз(RX[i]))); B := матМинимум(B,устарПреобразуйКБолееУзкомуЦел(округлиВниз(RY[i])));
		R := матМаксимум(R,устарПреобразуйКБолееУзкомуЦел(округлиВниз(RX[i]))); T := матМаксимум(T,устарПреобразуйКБолееУзкомуЦел(округлиВниз(RY[i])));
		увел(i);
	кц;

	dW := матМаксимум(1,матМинимум((матМаксимум(R-L ,T-B)  * 3 DIV n DIV 20),4));
	i := 1; k := 1;
	нцПока i < n делай
		GetPolynom(RX[i], RX[i+1], RXstrich[i], RXstrich[i+1], px);
		x := px.D;
		dx1 := px.A + px.B + px.C;
		dx3 := 6.0*px.A;
		dx2 := dx3 + 2.0*px.B;
		GetPolynom(RY[i], RY[i+1], RYstrich[i], RYstrich[i+1], py);
		y := py.D;
		dy1 := py.A + py.B + py.C;
		dy3 := 6.0*py.A;
		dy2 := dy3 + 2.0*py.B;
		smax := устарПреобразуйКБолееУзкомуЦел(округлиВниз(RS[i+1]-RS[i]));
		cs := 0;
		нцПока cs <= smax делай
			points[k].x := устарПреобразуйКБолееУзкомуЦел(округлиВниз(x)); points[k].y := устарПреобразуйКБолееУзкомуЦел(округлиВниз(y));
			k1 := k-1;
			если (матМодуль(points[k].x - points[k1].x) > dW) или (матМодуль(points[k].y - points[k1].y) > dW) то увел(k) всё;
			x   := x + dx1;    y   := y + dy1;
			dx1 := dx1 + dx2;  dy1 := dy1 + dy2;
			dx2 := dx2 + dx3;  dy2 := dy2 + dy3;
			увел(cs);
		кц;
		увел(i);
	кц; (* FOR i *)
	points[k].x := устарПреобразуйКБолееУзкомуЦел(округлиВниз(RX[n])); points[k].y := устарПреобразуйКБолееУзкомуЦел(округлиВниз(RY[n])); увел(k);
кон MakePoly;

проц SplineToPoly(c: Point; closed: булево; перем points : массив из WMGraphics.Point2d; перем k: цел32);
тип
	RealVect = массив 256 из вещ32;
перем
	n, i: цел32; RS, RX, RY ,RXstrich, RYstrich : RealVect; dx, dy: вещ32;
	helpR: вещ32;

	проц NatSplineDerivates(перем x, y, d: массив из вещ32; n: цел32);
	перем i: цел32; d1, d2: вещ32; a, b, c: RealVect;

		проц SolveTriDiag(перем a, b, c: массив из вещ32; n: цел32; перем y: массив из вещ32);
		перем i: цел32; t: вещ32;
		нач i := 1;
			нцПока i < n делай t := a[i]; c[i] := c[i]/t; helpR := c[i]*b[i]; a[i+1] := a[i+1] -  helpR; увел(i); кц;
			i := 2;
			нцПока i <= n делай helpR := c[i-1]*y[i-1]; y[i] := y[i] - helpR; увел(i); кц;
			t := a[n]; y[n] := y[n]/t; i := n-1;
			нцПока i > 0 делай  t := y[i+1]; helpR :=y[i] - b[i]*t; y[i] := helpR/a[i]; умень(i) кц
		кон SolveTriDiag;

		нач  (* NatSplineDerivates *)
			если x[1] # x[2] то b[1] := 1.0/(x[2] - x[1]); иначе b[1] := 1.0 всё;
			a[1] := 2.0*b[1]; c[1] := b[1];
			d1 := (y[2] - y[1])*3.0*b[1]*b[1];
			d[1] := d1;
			i :=2;
			нцПока i < n делай
				если x[i] # x[i+1] то b[i] := 1.0 /(x[i+1] - x[i]) иначе b[i] := 1.0 всё;
				a[i] := 2.0*(c[i-1] + b[i]); c[i] := b[i];
				d2 := (y[i+1] - y[i])*3.0*b[i]*b[i];
				d[i] := d1 + d2; d1 := d2;
				увел(i);
			кц;
			a[n] := 2.0*b[n-1]; d[n] := d1;
			SolveTriDiag(a, b, c, n, d)
		кон NatSplineDerivates;

	проц ClSplineDerivates(перем x, y, d: массив из вещ32; n: цел32);
		перем i: цел32; hn1, dn1, d1, d2: вещ32; a, b, c, u: RealVect;

		проц SolveTriDiag2(перем a, b, c: массив из вещ32; n:цел32; перем y1, y2: массив из вещ32);
		перем i: цел32; t: вещ32;
		нач
			i := 1;
			нцПока i < n делай
				t := a[i]; c[i] := c[i]/t;
				helpR := c[i]*b[i]; a[i+1] := a[i+1] - helpR;
				увел(i)
			кц;
			i :=2;
			нцПока i <= n делай
				helpR := c[i-1]*y1[i-1];  y1[i] := y1[i] - helpR;
				helpR :=  c[i-1]*y2[i-1]; y2[i] := y2[i] - helpR;
				увел(i);
			кц;
			t := a[n]; y1[n] := y1[n]/t; t := a[n]; y2[n] := y2[n]/t;
			i := n-1;
			нцПока i > 0 делай
				t := y1[i+1]; helpR := y1[i] - b[i]* t; y1[i] := helpR/a[i];
				t := y2[i+1]; helpR :=y2[i] - b[i]*t; y2[i] := helpR/a[i];
				умень(i)
			кц
		кон SolveTriDiag2;

	нач  (* ClSplineDerivates *)
		hn1 := 1.0/(x[n] - x[n-1]);
		dn1 := (y[n] - y[n-1])*3.0*hn1*hn1;
		если x[2] # x[1] то
			b[1] := 1.0/(x[2] - x[1]);
		иначе
			b[1] := 0
		всё;
		a[1] := hn1 + 2.0*b[1];
		c[1] := b[1];
		d1 := (y[2] - y[1])*3.0*b[1]*b[1];
		d[1] := dn1 + d1;
		u[1] := 1.0;
		i := 2;
		нцПока i < n-1 делай
			если x[i+1] # x[i] то b[i] := 1.0/(x[i+1] - x[i]) иначе b[i] := 0 всё;
			a[i] := 2.0*(c[i-1] + b[i]);
			c[i] := b[i];
			d2 := (y[i+1] - y[i])*3.0*b[i]*b[i];
			d[i] := d1 + d2;
			d1 := d2;
			u[i] := 0.0;
			увел(i)
		кц;
		a[n-1] := 2.0*b[n-2] + hn1;
		d[n-1] := d1 + dn1;
		u[n-1] := 1.0;
		SolveTriDiag2(a, b, c, n-1, u, d);
		helpR := u[1] + u[n-1] + x[n] - x[n-1];
		d1 := (d[1] + d[n-1])/helpR;
		i := 1;
		нцПока i < n делай
			d[i] := d[i] - d1*u[i];
			увел(i)
		кц;
		d[n] := d[1]
	кон ClSplineDerivates;

нач
	если c=НУЛЬ то k:=0; возврат всё;

	n := 0; нцПока c # НУЛЬ делай RX[n+1] := c.x ; RY[n+1] := c.y; увел(n); c := c.next кц;
	если closed то RX[n+1] := RX[1]; RY[n+1] := RY[1]; увел(n) ; всё;
	RS[1] := 0.0; i := 2;
	нцПока i <= n делай
		dx := RX[i] - RX[i-1];  dy := RY[i] - RY[i-1];
		RS[i] := RS[i-1] + Math.sqrt(dx*dx + dy*dy);
		увел(i);
	кц;
	если ~closed то
		NatSplineDerivates(RS, RX, RXstrich, n);
		NatSplineDerivates(RS, RY, RYstrich, n);
	иначе
		ClSplineDerivates(RS, RX, RXstrich, n);
		ClSplineDerivates(RS, RY, RYstrich, n)
	всё;
	MakePoly(RX, RY, RXstrich, RYstrich, RS, n, points, k);
кон SplineToPoly;

(* end of Rege code *)


(** Returns TRUE if mx, my is within gravity pixels from X, Y. *)
проц Invicinity(mx, my, X, Y: размерМЗ): булево;
нач возврат (mx - X) * (mx - X) + (my - Y) * (my - Y) < gravity * gravity
кон Invicinity;

(** Returns TRUE if mx, my is within gravity pixels of the line from X, Y to X1, Y1. *)
проц InLineVicinity(mx, my, X, Y, X1, Y1: размерМЗ): булево;
перем  w, h, pw, ph, det,len : размерМЗ;

	проц Between(x, a, b: размерМЗ): булево;
	перем min, max: размерМЗ;
	нач
		min := матМинимум(a, b); max := матМаксимум(a, b);
		возврат (min - gravity <= x) и (x <= max + gravity);
	кон Between;

нач
	если матМодуль(X - X1) > gravity  то
		если матМодуль(Y - Y1) > gravity то
			если Invicinity(mx, my,X, Y) или Invicinity(mx, my,X1, Y1) то возврат истина всё;
			pw := mx - X; ph := my - Y; w := X1 -X;  h := Y1 - Y;
			det := pw * h - ph * w; len := w * w + h * h;
			возврат  Between(mx, X, X1) и Between(my, Y, Y1) и (det / len * det < gravity * gravity)
		иначе
			возврат Between(mx, X, X1) и (матМодуль(my - Y) < gravity)
		всё
	иначе
		возврат Between(my, Y, Y1) и (матМодуль(mx - X) < gravity)
	всё
кон InLineVicinity;

проц Intersect(X, Y, x0,y0,x1,y1 : размерМЗ) : булево;
нач
	если ((Y >= y0) и (Y < y1)) или ((Y >= y1) и (Y < y0)) то
		если y1 > y0 то возврат x0 + (Y - y0) * (x1 -x0) DIV (y1 - y0) - X >= 0
		аесли y1 <  y0 то возврат x0 + (Y - y0) * (x0 -x1) DIV (y0 - y1) - X >= 0
		иначе возврат (x0 > X) или (x1 > X)
		всё
	иначе возврат ложь
	всё
кон Intersect;

проц Distance(x, y, x0, y0: размерМЗ): размерМЗ;
перем dx, dy: размерМЗ;
нач dx := x - x0; dy := y - y0;
	возврат округлиВниз(Math.sqrt(dx * dx + dy * dy))
кон Distance;

проц GenLine*() : XML.Element; (* needs AddPoint(); AddPoint(), before a prototype becomes visible  *)
перем line : Line;
нач
	нов(line); возврат line;
кон GenLine;

проц GenCircle*() : XML.Element;
перем circle : Circle;
нач
	нов(circle); возврат circle;
кон GenCircle;

проц GenRectangle*() : XML.Element;
перем rectangle : Rectangle;
нач
	нов(rectangle); возврат rectangle;
кон GenRectangle;

проц GenSpline*() : XML.Element;
перем spline : Spline;
нач
	нов(spline); возврат spline;
кон GenSpline;

проц InitPrototypes;
нач
	нов(PrototypeWidth, НУЛЬ, Strings.NewString("width"), Strings.NewString("Width of stroke")); PrototypeWidth.Set(1);
	нов(PrototypeColor, НУЛЬ, Strings.NewString("color"), Strings.NewString("Color"));	PrototypeColor.Set(WMGraphics.Gray);
	нов(PrototypeclHover, НУЛЬ, Strings.NewString("clHover"), Strings.NewString("Color HOver"));	PrototypeclHover.Set(WMGraphics.Yellow);
	нов(PrototypeClosed, НУЛЬ, Strings.NewString("closed"), Strings.NewString("Figure is closed")); PrototypeClosed.Set(ложь);
	нов(PrototypeFilled, НУЛЬ, Strings.NewString("filled"), Strings.NewString("Figure is filled")); PrototypeFilled.Set(ложь);
	нов(PrototypeReshape, НУЛЬ, Strings.NewString("reshape"), Strings.NewString("Control Points can be individually moved"));	PrototypeReshape.Set(истина);
	нов(PrototypeArrow, НУЛЬ, Strings.NewString("arrow"), Strings.NewString("Draw arrow at end of line")); PrototypeArrow.Set(ложь);
кон InitPrototypes;

проц InitStrings;
нач
	StrFigure := Strings.NewString("Figure");
	StrLine := Strings.NewString("Line");
	StrCircle := Strings.NewString("Circle");
	StrRectangle := Strings.NewString("Rectangle");
	StrSpline := Strings.NewString("Spline");
кон InitStrings;

нач
	gravity := 6;
	InitStrings;
	InitPrototypes;
	Потоки.НастройПисаря(log, ЛогЯдра.ЗапишиВПоток);
кон WMFigures.

System.FreeDownTo WMFigures ~
----------------
Example commands for interactive figures (left click for editing and moving; middle clicking for adding points, right-left interclick for deleting points).

WMComponents.Open FigureExample.Cwd ~
ComponentViewer.Open WMFigures.GenSpline ~
ComponentViewer.Open WMStandardComponents.GenPanel ~

------------------------
Example application for programmable figures:

MODULE TestFigures;

IMPORT WMFigures, WMWindowManager, WMGraphics, WMComponents, WMStandardComponents;

PROCEDURE Do*;
VAR
	r:WMFigures.Rectangle;
	s:WMFigures.Spline;
	l:WMFigures.Line;
	c:WMFigures.Circle;
	window: WMComponents.FormWindow;
	background: WMStandardComponents.Panel;
BEGIN
	NEW(window, 400,400, FALSE);

	NEW(background);
	background.bounds.SetExtents(400,400);
	background.fillColor.Set(WMGraphics.Green);

	NEW(l); l.AddPoint(10,20); l.AddPoint(100,200); l.AddPoint(80, 20);
	background.AddContent(l);

	NEW(r); r.AddPoint(30,40); r.AddPoint(120,200);
	background.AddContent(r)

	NEW(c); c.AddPoint(100,100); c.AddPoint(150,150);
	background.AddContent(c);

	NEW(s); (*spline with default points*)
	background.AddContent(s);
	s.color.Set(WMGraphics.Blue);

	NEW(r); r.AddPoint(70,80); r.AddPoint(110,110);
	r.reshape.Set(FALSE); (* non-reshapable *)
	r.color.Set(WMGraphics.Blue);
	background.AddContent(r);

	window.SetContent(background); (* calls window.form.Reset(NIL,NIL) implicitly *)
	WMWindowManager.AddWindow(window, 200,200);
	INCL(window.flags, WMWindowManager.FlagStorable); (* allow interactive storage of window through context menu *)

(* add content after window insertion to display *)
	NEW(l); l.AddPoint(20,30); l.AddPoint(110,210); l.AddPoint(90, 40);
	background.AddContent(l);
END Do;

PROCEDURE Do1*;
VAR
	r:WMFigures.Rectangle;
	s:WMFigures.Spline;
	l:WMFigures.Line;
	window: WMComponents.FormWindow;
BEGIN
	NEW(window, 400,400, FALSE);
	WMWindowManager.AddWindow(window, 200,200);
	INCL(window.flags, WMWindowManager.FlagStorable);

	NEW(l); l.AddPoint(10,20); l.AddPoint(100,200); l.AddPoint(80, 20);
	window.SetContent(l);
END Do1;
END TestFigures.

TestFigures.Do
TestFigures.Do1

System.FreeDownTo WMFigures TestFigures ~
