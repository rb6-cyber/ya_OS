(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль VecRe;   (** AUTHOR "fof"; PURPOSE "Vector objects of type Real."; *)

использует НИЗКОУР, NbrInt, ArrayXdBytes, ArrayXd := ArrayXdRe, Array1d := Array1dRe, NbrRat, NbrRe, DataErrors, DataIO;

конст
	(** The version used when reading/writing a vector to file. *)
	VERSION* = 1;

тип
	Value* = ArrayXd.Value;  Index* = цел32;  IntValue = ArrayXd.IntValue;  RatValue = NbrRat.Rational;
	Array* = ArrayXd.Array1;  Map* = ArrayXd.Map;

	(** Class Vector has been DataIO registered. *)
	Vector* = окласс (ArrayXd.Array)
	перем lenx-: цел32;
		ox-: цел32;
		Get-: проц {делегат} ( x: Index ): Value;

		(** overrride *)
		проц {перекрыта}AlikeX*( ): ArrayXdBytes.Array;
		перем copy: Vector;
		нач
			нов( copy, origin[0], len[0] );  возврат copy;
		кон AlikeX;

		проц {перекрыта}NewRangeX*( neworigin, newlen: ArrayXdBytes.IndexArray;  copydata: булево );
		нач
			если длинаМассива( newlen ) # 1 то СТОП( 1001 ) всё;
			NewRangeX^( neworigin, newlen, copydata );
		кон NewRangeX;

		проц {перекрыта}ValidateCache*;
		нач
			ValidateCache^;
			если dim # 1 то СТОП( 100 ) всё;
			lenx := len[0];  ox := origin[0];
		кон ValidateCache;

		проц {перекрыта}SetBoundaryCondition*( c: цел8 );   (* called by new, load and directly *)
		нач
			SetBoundaryCondition^( c );
			просей c из
			ArrayXd.StrictBoundaryC:
					Get := Get1;
			| ArrayXd.AbsorbingBoundaryC:
					Get := Get1BAbsorbing;
			| ArrayXd.PeriodicBoundaryC:
					Get := Get1BPeriodic;
			| ArrayXd.SymmetricOnBoundaryC:
					Get := Get1BSymmetricOnB
			| ArrayXd.SymmetricOffBoundaryC:
					Get := Get1BSymmetricOffB
			| ArrayXd.AntisymmetricOnBoundaryC:
					Get := Get1BAntisymmetricOnB
			| ArrayXd.AntisymmetricOffBoundaryC:
					Get := Get1BAntisymmetricOffB
			всё;
		кон SetBoundaryCondition;

	(** new *)
		проц & New*( ox, w: цел32 );   (** ox is the offset and w is the width (or length) of the array. *)
		нач
			NewXdB( ArrayXdBytes.Array1( ox ), ArrayXdBytes.Array1( w ) );
		кон New;

		проц NewRange*( ox, w: цел32;  copydata: булево );
		нач
			если (w # len[0]) или (ox # origin[0]) то
				NewRangeX^( ArrayXdBytes.Array1( ox ), ArrayXdBytes.Array1( w ), copydata );
			всё;
		кон NewRange;

		проц Alike*( ): Vector;
		перем copy: ArrayXdBytes.Array;
		нач
			copy := AlikeX();  возврат copy( Vector );
		кон Alike;

		проц Copy*( ): Vector;
		перем res: ArrayXdBytes.Array;
		нач
			res := CopyX();  возврат res( Vector );
		кон Copy;

		проц Set*( x: Index;  v: Value );
		нач
			ArrayXdBytes.Set1( сам, x, v );
		кон Set;

	(** vec[beginAtRow] := array[0] ... vec[beginAtRow+LEN(array)-1] := array[LEN(array)-1] *)
		проц SetArray*( beginAtRow: Index;  перем array: массив из Value );
		нач
			ArrayXdBytes.CopyMemoryToArrayPart( адресОт( array[0] ), сам, длинаМассива( array ), ArrayXdBytes.Index1( beginAtRow ),
																			 НУЛЬ );
		кон SetArray;

		проц GetArray*( beginAtRow: Index;  перем array: массив из Value );
		нач
			ArrayXdBytes.CopyArrayPartToMemory( сам, адресОт( array[0] ), ArrayXdBytes.Index1( beginAtRow ), НУЛЬ ,
																			 длинаМассива( array ) );
		кон GetArray;

	(** Exchanges values held by vec[row1] and vec[row2] *)
		проц Swap*( row1, row2: Index );
		нач
			ToggleElements( 0, row1, row2 );
		кон Swap;

	(** Rearranges entries so that vec[0] # vec[1] # ... # vec[len-1] *)
		проц Sort*;
		нач {единолично}
			Array1d.Sort( data^, 0, len[0] );
		кон Sort;

		проц Insert*( x: Index;  v: Value );
		нач
			InsertElements( 0, x, 1 );  ArrayXdBytes.Set1( сам, x, v );
		кон Insert;

		проц Delete*( x: Index );
		нач
			DeleteElements( 0, x, 1 );
		кон Delete;

	(** copy methods using the current boundary condition SELF.bc*)
		проц CopyToVec*( dest: ArrayXd.Array;  srcx, destx, len: Index );
		нач
			если (dest.dim # 1) то СТОП( 1001 ) всё;
			CopyToArray( dest, ArrayXdBytes.Index1( srcx ), ArrayXdBytes.Index1( len ), ArrayXdBytes.Index1( destx ),
								   ArrayXdBytes.Index1( len ) );
		кон CopyToVec;

		проц CopyToMtx*( dest: ArrayXd.Array;  dim: Index;  srcx, destx, desty, len: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 2) то СТОП( 1002 ) всё;
			slen := ArrayXdBytes.Index2( 1, 1 );  slen[dim] := len;
			CopyToArray( dest, ArrayXdBytes.Index1( srcx ), ArrayXdBytes.Index1( len ), ArrayXdBytes.Index2( destx, desty ),
								   slen );
		кон CopyToMtx;

		проц CopyToCube*( dest: ArrayXd.Array;  dim: Index;  srcx, destx, desty, destz, len: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 3) то СТОП( 1002 ) всё;
			slen := ArrayXdBytes.Index3( 1, 1, 1 );  slen[dim] := len;
			CopyToArray( dest, ArrayXdBytes.Index1( srcx ), ArrayXdBytes.Index1( len ),
								   ArrayXdBytes.Index3( destx, desty, destz ), slen );
		кон CopyToCube;

		проц CopyToHCube*( dest: ArrayXd.Array;  dim: Index;  srcx, destx, desty, destz, destt, len: Index );
		перем slen: ArrayXdBytes.IndexArray;
		нач
			если (dest.dim # 4) то СТОП( 1002 ) всё;
			slen := ArrayXdBytes.Index4( 1, 1, 1, 1 );  slen[dim] := len;
			CopyToArray( dest, ArrayXdBytes.Index1( srcx ), ArrayXdBytes.Index1( len ),
								   ArrayXdBytes.Index4( destx, desty, destz, destt ), slen );
		кон CopyToHCube;

		проц CopyTo1dArray*( перем dest: массив из Value;  srcpos, srclen: Index;  dpos, dlen: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( dest ) ), размер16_от( Value ),
																			  адресОт( dest[0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index1( srcpos ), ArrayXdBytes.Index1( srclen ),
																  ArrayXdBytes.Index1( dpos ), ArrayXdBytes.Index1( dlen ) );
		кон CopyTo1dArray;

		проц CopyTo2dArray*( перем dest: массив из массив из Value;  srcpos, srclen: Index;  dposx, dposy, dlenx, dleny: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ),
																			  размер16_от( Value ), адресОт( dest[0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index1( srcpos ), ArrayXdBytes.Index1( srclen ),
																  ArrayXdBytes.Index2( dposx, dposy ), ArrayXdBytes.Index2( dlenx, dleny ) );
		кон CopyTo2dArray;

		проц CopyTo3dArray*( перем dest: массив из массив из массив из Value;  srcpos, srclen: Index;
													   dposx, dposy, dposz, dlenx, dleny, dlenz: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index1( srcpos ), ArrayXdBytes.Index1( srclen ),
																  ArrayXdBytes.Index3( dposx, dposy, dposz ),
																  ArrayXdBytes.Index3( dlenx, dleny, dlenz ) );
		кон CopyTo3dArray;

		проц CopyTo4dArray*( перем dest: массив из массив из массив из массив из Value;  srcpos, srclen: Index;
													   dposx, dposy, dposz, dpost, dlenx, dleny, dlenz, dlent: цел32 );
		перем destm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			destm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( dest, 3 ), длинаМассива( dest, 2 ), длинаМассива( dest, 1 ), длинаМассива( dest, 0 ) ), размер16_от( Value ),
																			  адресОт( dest[0, 0, 0, 0] ) );
			ArrayXd.CopyArrayToArrayPartB( сам, destm, bc, ArrayXdBytes.Index1( srcpos ), ArrayXdBytes.Index1( srclen ),
																  ArrayXdBytes.Index4( dposx, dposy, dposz, dpost ),
																  ArrayXdBytes.Index4( dlenx, dleny, dlenz, dlent ) );
		кон CopyTo4dArray;

	(** copy from without boundary conditions *)
		проц CopyFrom1dArray*( перем src: массив из Value;  spos, slen: Index;  destpos, destlen: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 1, ArrayXdBytes.Index1( 0 ), ArrayXdBytes.Index1( длинаМассива( src ) ), размер16_от( Value ),
																			  адресОт( src[0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index1( spos ), ArrayXdBytes.Index1( slen ),
																			   ArrayXdBytes.Index1( destpos ), ArrayXdBytes.Index1( destlen ) );
		кон CopyFrom1dArray;

		проц CopyFrom2dArray*( перем src: массив из массив из Value;  sposx, spoxy, slenx, sleny: Index;
														    destpos, destlen: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 2, ArrayXdBytes.Index2( 0, 0 ), ArrayXdBytes.Index2( длинаМассива( src, 1 ), длинаМассива( src, 0 ) ),
																			  размер16_от( Value ), адресОт( src[0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index2( sposx, spoxy ),
																			   ArrayXdBytes.Index2( slenx, sleny ), ArrayXdBytes.Index1( destpos ),
																			   ArrayXdBytes.Index1( destlen ) );
		кон CopyFrom2dArray;

		проц CopyFrom3dArray*( перем src: массив из массив из массив из Value;  sposx, spoxy, sposz, slenx, sleny, slenz: Index;
														    destpos, destlen: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 3, ArrayXdBytes.Index3( 0, 0, 0 ),
																			  ArrayXdBytes.Index3( длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index3( sposx, spoxy, sposz ),
																			   ArrayXdBytes.Index3( slenx, sleny, slenz ), ArrayXdBytes.Index1( destpos ),
																			   ArrayXdBytes.Index1( destlen ) );
		кон CopyFrom3dArray;

		проц CopyFrom4dArray*( перем src: массив из массив из массив из массив из Value;
														    sposx, spoxy, sposz, spost, slenx, sleny, slenz, slent: Index;  destpos, destlen: Index );
		перем srcm: ArrayXdBytes.ArrayMemoryStructure;
		нач
			srcm :=
				ArrayXdBytes.MakeMemoryStructure( 4, ArrayXdBytes.Index4( 0, 0, 0, 0 ),
																			  ArrayXdBytes.Index4( длинаМассива( src, 3 ), длинаМассива( src, 2 ), длинаМассива( src, 1 ), длинаМассива( src, 0 ) ), размер16_от( Value ),
																			  адресОт( src[0, 0, 0, 0] ) );
			ArrayXdBytes.CopyArrayPartToArrayPart( srcm, сам, ArrayXdBytes.Index4( sposx, spoxy, sposz, spost ),
																			   ArrayXdBytes.Index4( slenx, sleny, slenz, slent ),
																			   ArrayXdBytes.Index1( destpos ), ArrayXdBytes.Index1( destlen ) );
		кон CopyFrom4dArray;

	кон Vector;

	операция ":="*( перем l: Vector;  перем r: массив из Value );
	нач
		(*		IF r = NIL THEN l := NIL;  RETURN END;  *)
		если l = НУЛЬ то нов( l, 0, длинаМассива( r, 0 ) ) иначе l.NewRange( 0, длинаМассива( r, 0 ), ложь );  всё;
		ArrayXdBytes.CopyMemoryToArray( адресОт( r[0] ), l, длинаМассива( r, 0 ) );
	кон ":=";

	операция ":="*( перем l: Vector;  r: Value );
	нач
		если l # НУЛЬ то ArrayXd.Fill( l, r );  всё;
	кон ":=";

	операция ":="*( перем l: Vector;  r: RatValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция ":="*( перем l: Vector;  r: IntValue );
	перем r1: Value;
	нач
		r1 := r;  l := r1;
	кон ":=";

	операция "+"*( l, r: Vector ): Vector;
	перем res: Vector;
	нач
		res := l.Alike();  ArrayXd.Add( l, r, res );  возврат res;
	кон "+";

	операция "-"*( l, r: Vector ): Vector;
	перем res: Vector;
	нач
		res := l.Alike();  ArrayXd.Sub( l, r, res );  возврат res;
	кон "-";

	операция "+"*( l: Vector;  r: Value ): Vector;
	перем res: Vector;
	нач
		res := l.Alike();  ArrayXd.AddAV( l, r, res );  возврат res;
	кон "+";

	операция "+"*( l: Vector;  r: RatValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Vector;  r: IntValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.AddAV( l, r1, res );  возврат res;
	кон "+";

	операция "+"*( l: Value;  r: Vector ): Vector;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: RatValue;  r: Vector ): Vector;
	нач
		возврат r + l
	кон "+";

	операция "+"*( l: IntValue;  r: Vector ): Vector;
	нач
		возврат r + l
	кон "+";

	операция "-"*( l: Vector;  r: Value ): Vector;
	перем res: Vector;
	нач
		res := l.Alike();  ArrayXd.SubAV( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: Vector;  r: RatValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Vector;  r: IntValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.SubAV( l, r1, res );  возврат res;
	кон "-";

	операция "-"*( l: Value;  r: Vector ): Vector;
	перем res: Vector;
	нач
		res := r.Alike();  ArrayXd.SubVA( l, r, res );  возврат res;
	кон "-";

	операция "-"*( l: RatValue;  r: Vector ): Vector;
	перем res: Vector;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: IntValue;  r: Vector ): Vector;
	перем res: Vector;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.SubVA( l1, r, res );  возврат res;
	кон "-";

	операция "-"*( l: Vector ): Vector;
	нач
		возврат 0 - l;
	кон "-";

	операция "*"*( l: Vector;  r: Value ): Vector;
	перем res: Vector;
	нач
		res := l.Alike();  ArrayXd.MulAV( l, r, res );  возврат res;
	кон "*";

	операция "*"*( l: Vector;  r: RatValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Vector;  r: IntValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.MulAV( l, r1, res );  возврат res;
	кон "*";

	операция "*"*( l: Value;  r: Vector ): Vector;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: RatValue;  r: Vector ): Vector;
	нач
		возврат r * l;
	кон "*";

	операция "*"*( l: IntValue;  r: Vector ): Vector;
	нач
		возврат r * l;
	кон "*";

	операция "/"*( l: Vector;  r: Value ): Vector;
	перем res: Vector;
	нач
		res := l.Alike();  ArrayXd.DivAV( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: Vector;  r: RatValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Vector;  r: IntValue ): Vector;
	перем res: Vector;  r1: Value;
	нач
		res := l.Alike();  r1 := r;  ArrayXd.DivAV( l, r1, res );  возврат res;
	кон "/";

	операция "/"*( l: Value;  r: Vector ): Vector;
	перем res: Vector;
	нач
		res := r.Alike();  ArrayXd.DivVA( l, r, res );  возврат res;
	кон "/";

	операция "/"*( l: RatValue;  r: Vector ): Vector;
	перем res: Vector;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";

	операция "/"*( l: IntValue;  r: Vector ): Vector;
	перем res: Vector;  l1: Value;
	нач
		res := r.Alike();  l1 := l;  ArrayXd.DivVA( l1, r, res );  возврат res;
	кон "/";

(*

	OPERATOR "MOD"*( l: Vector;  r: Value ): Vector;
	VAR res: Vector;
	BEGIN
		res := l.Alike();  ArrayXd.ModAV( l, r, res );  RETURN res;
	END "MOD";

	OPERATOR "MOD"*( l: Value;  r: Vector ): Vector;
	VAR res: Vector;
	BEGIN
		res := r.Alike();  ArrayXd.ModVA( l, r, res );  RETURN res;
	END "MOD";
*)

	операция "*"*( l, r: Vector ): Value;   (* scalar product *)
	(*! replace by operation on memory *)
	перем res: Value;  i: цел32;
	нач
		ArrayXdBytes.CheckEqDimensions( l, r );  res := 0;
		нцДля i := l.ox до l.ox + l.lenx - 1 делай res := res + l.Get( i ) * r.Get( i );  кц;
		возврат res;
	кон "*";

	проц L1Norm*( l: Vector ): Value;
	(*! todo: replace by operation on memory *)
	перем norm: Value;  i: цел32;
	нач
		norm := 0;
		нцДля i := l.ox до l.ox + l.lenx - 1 делай norm := norm + матМодуль( l.Get( i ) );  кц;
		возврат norm;
	кон L1Norm;

	проц L2Norm*( l: Vector ): Value;
	(*! todo: replace by operation on memory *)
	перем norm,cur: Value;  i: цел32;
	нач
		norm := 0;
		нцДля i := l.ox до l.ox + l.lenx - 1 делай cur := l.Get( i ); norm := norm + cur*cur;  кц;
		возврат NbrRe.Sqrt(norm);
	кон L2Norm;


	проц LInftyNorm*( l: Vector ): Value;
	(*! todo: replace by operation on memory *)
	перем norm, abs: Value;  i: цел32;
	нач
		norm := матМодуль( l.Get( l.ox ) );
		нцДля i := l.ox + 1 до l.ox + l.lenx - 1 делай
			abs := матМодуль( l.Get( i ) );
			если abs > norm то norm := abs всё;
		кц;
		возврат norm;
	кон LInftyNorm;

(* The procedures needed to register type Vector so that its instances can be made persistent. *)
	проц LoadVector( R: DataIO.Reader;  перем obj: окласс );
	перем a: Vector;  version: цел8;  ver: NbrInt.Integer;
	нач
		R.чЦел8_мз( version );
		если version = -1 то
			obj := НУЛЬ  (* Version tag is -1 for NIL. *)
		иначе
			если version = VERSION то нов( a, 0, 0 );  a.Read( R );  obj := a
					иначе  (* Encountered an unknown version number. *)
				ver := version;  DataErrors.IntError( ver, "Alien version number encountered." );  СТОП( 1000 )
			всё
		всё
	кон LoadVector;

	проц StoreVector( W: DataIO.Writer;  obj: окласс );
	перем a: Vector;
	нач
		если obj = НУЛЬ то W.пЦел8_мз( -1 ) иначе W.пЦел8_мз( VERSION );  a := obj( Vector );  a.Write( W ) всё
	кон StoreVector;

	проц Register;
	перем a: Vector;
	нач
		нов( a, 0, 0 );  DataIO.PlugIn( a, LoadVector, StoreVector )
	кон Register;

(** Load and Store are procedures for external use that read/write an instance of Vector from/to a file. *)
	проц Load*( R: DataIO.Reader;  перем obj: Vector );
	перем ptr: окласс;
	нач
		R.Object( ptr );  obj := ptr( Vector )
	кон Load;

	проц Store*( W: DataIO.Writer;  obj: Vector );
	нач
		W.Object( obj )
	кон Store;

нач
	Register
кон VecRe.
