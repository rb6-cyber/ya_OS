модуль UCS32u; (* Куски UCS32, которые ещё не устоялись. Цель существования данного модуля - не пересобирать постоянно всю систему *)

(** Нельзя импортировать UTF8Strings, нужно копировать из него. 
   Данный код пойдёт на очень ранний этап загрузки *)
использует UCS32	;

(* Временная станция для новых объектов, конечным пунктом которых является UCS32.Mod  *)

(* Названия букв даны в яролите, а не в юникоде! *)
конст
	(* Sm takzhe UCS32.CyrillicSmallLetterA i izhe s nimi *)
	StrochnajaA* = UCS32.CyrillicSmallLetterA;
	StrochnajaB* = 0431H;
	StrochnajaV* = 0432H;
	StrochnajaG* = 0433H;
	StrochnajaD* = 0434H;
	StrochnajaE* = 0435H;
	
	StrochnajaZH* = 0436H;
	StrochnajaZ* = 0437H;
	StrochnajaI* = 0438H;
	StrochnajaJJ* = 0439H;
	StrochnajaK* = 043AH;
	StrochnajaL* = 043BH;
	StrochnajaM* = 043CH;
	StrochnajaN* = 043DH;
	StrochnajaO* = 043EH;
	StrochnajaP* = 043FH;
	StrochnajaR* = 0440H;
	StrochnajaS* = 0441H;
	StrochnajaT* = 0442H;
	StrochnajaU* = 0443H;
	StrochnajaF* = 0444H;
	StrochnajaKH* = 0445H;
	StrochnajaC* = 0446H;
	StrochnajaCH* = 0447H;
	StrochnajaSH* = 0448H;
	StrochnajaSHH* = 0449H;
	StrochnajaJQ* = 044AH;
	StrochnajaY* = 044BH;
	StrochnajaQ* = 044CH;
	StrochnajaEH* = 044DH;
	StrochnajaJU* = 044EH;
	StrochnajaJA* = 044FH;
	
	PropisnajaA* = UCS32.CyrillicCapitalLetterA;
	PropisnajaN* = PropisnajaA + StrochnajaN - StrochnajaA;
	
	KodParagraf* = 0A7H; (* § *)
	
(* Сюда же перенести пр-ру WMGraphics.ОсновнаяЛатиницаЛи *)
	

кон UCS32u. 
