(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль WebDefaultSSMP; (** AUTHOR "TF"; PURPOSE "Default SSMP plugin (system info)"; *)

использует
	Machine, Потоки, Modules, WebSSMPPlugin, Clock, WebHTTP,
	WebHTTPServer, Performance, Reflection, Kernel;

конст Samples = 60;

тип
	TimeSampleQueryMethod = проц {делегат} () : цел32;

	TimeSampler = окласс
	перем
		hits : укль на массив из цел32;
		pos : цел32;
		timer : Kernel.Timer;
		query : TimeSampleQueryMethod;
		interval, nofSamples : цел32;
		alive : булево;

		проц &Init*(queryMethod : TimeSampleQueryMethod; interval, samples : цел32);
		нач
			нов(timer); сам.interval := interval; сам.nofSamples := samples; сам.query := queryMethod; нов(hits, samples)
		кон Init;

		проц Step;
		нач {единолично}
			hits^[pos] := query();
			увел(pos); pos := pos остОтДеленияНа nofSamples
		кон Step;

		проц QuerySamples(перем x : массив из цел32);
		перем i : цел32;
		нач {единолично}
			нцДля i := 0 до nofSamples - 1 делай
				x[i] := hits[(pos + i) остОтДеленияНа nofSamples]
			кц
		кон QuerySamples;

		проц Kill;
		нач
			alive := ложь; timer.Wakeup; timer.Wakeup
		кон Kill;

	нач {активное}
		alive := истина; pos := 0;

		нцПока alive делай
			Step; если alive то timer.Sleep(interval) всё
		кц
	кон TimeSampler;

перем hitSampler : TimeSampler;
(** samples are values 0..100*)
проц HTMLBarChartVertical(перем out : Потоки.Писарь; samples : массив из цел32; start, end : цел32;
							chartheight, barwidth, border, color: цел32);
перем i, v : цел32;
нач
	если chartheight <= 0 то chartheight := 100 всё;
	если barwidth <= 0 то barwidth := 20 всё;
	out.пСтроку8('<table border="'); out.пЦел64(border, 0);
	out.пСтроку8('" height="'); out.пЦел64(chartheight, 0); out.пСтроку8('">'); out.пВК_ПС;

	out.пСтроку8('<tr valign="bottom">'); out.пВК_ПС;

	нцДля i := start до end делай
		out.пСтроку8('<td><table border="0" cellspacing="0" width="');
		out.пЦел64(barwidth, 0);
		out.пСтроку8('" height="');
		v := (samples[i] * chartheight) DIV 100; (*IF v = 0 THEN v := 1 END;*)
		out.пЦел64(v, 0);
		out.пСтроку8('" bgcolor="#'); out.п16ричное(color, 6);
		out.пСтроку8('" ><tr><td></td></tr></table></td>');
		out.пВК_ПС;
	кц;
	out.пСтроку8('</tr>'); out.пВК_ПС;
	out.пСтроку8('</table>'); out.пВК_ПС

кон HTMLBarChartVertical;

проц ServerNofRequests(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
нач
	out.пЦел64(WebHTTPServer.GetRequests(), 5)
кон ServerNofRequests;

проц ServerRPMChart(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
перем values : массив 60 из цел32; max: цел32; i : цел32;
нач
	hitSampler.QuerySamples(values);
	нцДля i := 0 до 60 - 1 делай если max < values[i] то max := values[i] всё кц;
	если max = 0 то max := 1 всё;
	нцДля i := 0 до 60 - 1 делай values[i] := values[i] * 100 DIV max кц;
	HTMLBarChartVertical(out, values, 0, 60-1, 100, 5, 0, 0FF0000H)
кон ServerRPMChart;

проц ServerNofRequestsPerMinute(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
нач
	out.пЦел64(WebHTTPServer.requestsPerMinute, 5)
кон ServerNofRequestsPerMinute;

проц SystemTime(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
перем time, date: цел32;
нач
	Clock.Get(time, date); out.пДатуОберона822(time, date, Clock.tz);
кон SystemTime;

проц SystemStartTime(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
нач
	out.пДатуОберона822(Clock.starttime, Clock.startdate, Clock.tz)
кон SystemStartTime;

проц SystemLoad(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
перем x, i: цел32;
нач
	нцДля i := 0 до 2 делай
		x := округлиВниз(Performance.load[i]*100 + 0.5);
		out.пЦел64(x DIV 100, 3); out.пСимв8(".");
		out.пЦел64(x DIV 10 остОтДеленияНа 10, 1); out.пЦел64(x остОтДеленияНа 10, 1)
	кц
кон SystemLoad;

проц SystemIdle(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
перем i: цел32;
нач
	i := 0;
	нцПока (i < длинаМассива(Performance.idle)) и (Performance.idle[i] > - 1) делай
		out.пЦел64(Performance.idle[i], 3); out.пСимв8("%"); out.пСимв8(" ");
		увел(i)
	кц
кон SystemIdle;

проц SystemVersion(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
нач
	out.пСтроку8(Machine.version)
кон SystemVersion;

проц ReadName(перем b: Потоки.Чтец; перем s: массив из симв8);
перем j, max: размерМЗ; ch: симв8;
нач
	j := 0; max := длинаМассива(s)-1;
	нц
		ch := b.ПодглядиСимв8();
		если ~(((ASCII_вЗаглавную(ch) >= "A") и (ASCII_вЗаглавную(ch) <= "Z")) или ((ch >= "0") и (ch <= "9"))) или (b.кодВозвратаПоследнейОперации # Потоки.Успех) то прервиЦикл всё;
		если j < max то s[j] := ch; увел(j) всё;
		ch := b.чИДайСимв8()
	кц;
	s[j] := 0X
кон ReadName;

проц SystemState(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
перем name: массив 32 из симв8;
нач
	in.ПропустиБелоеПоле; ReadName(in, name);
	(*out.String("System.State ");
	out.String(name); out.Ln;*)
	Reflection.ModuleState(out, Modules.ModuleByName(name))	(* no error message *)
кон SystemState;

проц SystemGet(перем request : WebHTTP.RequestHeader; перем in : Потоки.Чтец; перем out : Потоки.Писарь);
перем v: Reflection.Variable; col: цел32; mod, var: массив 32 из симв8;
нач
	in.ПропустиБелоеПоле;
	ReadName(in, mod);
	если in.ПодглядиСимв8() = "." то
		in.чСимв8(var[0]);	(* skip *)
		ReadName(in, var);
		если Reflection.FindVar(Modules.ModuleByName(mod), var, v) то
			Reflection.WriteVar(out, v, col)
		иначе
			out.пСтроку8("System.Get "); out.пСтроку8(mod); out.пСимв8(".");
			out.пСтроку8(var); out.пСтроку8(": variable not found")
		всё
	иначе
		out.пСтроку8("usage: System.Get module.variable")
	всё
кон SystemGet;

проц Install*;
нач
	WebSSMPPlugin.RegisterMethod("Server.NofRequests", ServerNofRequests);
	WebSSMPPlugin.RegisterMethod("Server.NofRequestsPerMinute", ServerNofRequestsPerMinute);
	WebSSMPPlugin.RegisterMethod("System.Time", SystemTime);
	WebSSMPPlugin.RegisterMethod("System.StartTime", SystemStartTime);
	WebSSMPPlugin.RegisterMethod("System.Load", SystemLoad);
	WebSSMPPlugin.RegisterMethod("System.Idle", SystemIdle);
	WebSSMPPlugin.RegisterMethod("System.Version", SystemVersion);
	WebSSMPPlugin.RegisterMethod("Server.RPMChart", ServerRPMChart);
	WebSSMPPlugin.RegisterMethod("System.State", SystemState);
	WebSSMPPlugin.RegisterMethod("System.Get", SystemGet);
кон Install;

проц Cleanup;
нач
	hitSampler.Kill;
	WebSSMPPlugin.UnregisterMethod("Server.NofRequests");
	WebSSMPPlugin.UnregisterMethod("Server.NofRequestsPerMinute");
	WebSSMPPlugin.UnregisterMethod("System.Time");
	WebSSMPPlugin.UnregisterMethod("System.StartTime");
	WebSSMPPlugin.UnregisterMethod("System.Load");
	WebSSMPPlugin.UnregisterMethod("System.Idle");
	WebSSMPPlugin.UnregisterMethod("System.Version");
	WebSSMPPlugin.UnregisterMethod("Server.RPMChart");
	WebSSMPPlugin.UnregisterMethod("System.State");
	WebSSMPPlugin.UnregisterMethod("System.Get")
кон Cleanup;

проц QueryRPM():цел32;
нач
	возврат WebHTTPServer.requestsPerMinute
кон QueryRPM;

нач
	нов(hitSampler, QueryRPM, 10000, 60);
	Modules.InstallTermHandler(Cleanup);
кон WebDefaultSSMP.


EditTools.OpenUnix public.info.ssmp
EditTools.OpenUnix public.test.ssmp

System.Free WebDefaultSSMP~

Aos.Call WebDefaultSSMP.Install~
