модуль DiffLib; (** AUTHOR "negelef"; PURPOSE "Simple text diff tool"; *)

использует
	Потоки, Texts, TextUtilities, Commands, Strings;

конст
	lineBufferSize = 1000;
	maxLineSize = 256;

	dirNone = 0;
	dirLeft = 1;
	dirUp = 2;
	dirRight = 4;
	dirDown = 5;
	dirDiag = 6;

перем
	separator: булево;

тип
	LineBuffer = укль на запись
		lines: массив lineBufferSize из размерМЗ;
		next: LineBuffer;
		size: размерМЗ;
	кон;

	Element = запись
		val: размерМЗ;
		dir: целМЗ;
	кон;

	Handler* = проц {делегат} (pos, line: размерМЗ; string: Strings.String; out : Потоки.Писарь);
	EmptyHandler* = проц {делегат};
	SetupHandler* = проц {делегат} (nofLines: размерМЗ);

	проц GetLinePos (lineBuffer: LineBuffer; offset: размерМЗ): размерМЗ;
	нач
		нцПока offset >= lineBuffer.size делай
			умень (offset, lineBuffer.size);
			lineBuffer := lineBuffer.next;
		кц;
		возврат lineBuffer.lines[offset];
	кон GetLinePos;

	проц GetLineBuffer (reader: Texts.TextReader; перем size: размерМЗ): LineBuffer;
	перем
		first, current: LineBuffer;
		ch: цел32;
	нач
		нов (first);
		current := first;
		current.size := 0;
		size := 0;

		нцДо
			если (current.size = lineBufferSize) то
				нов (current.next);
				current := current.next;
				current.size := 0;
			всё;
			current.lines[current.size] := reader.GetPosition ();
			увел (current.size);
			увел (size);
			нцДо
				reader.ReadCh (ch);
			кцПри reader.eot или (ch = Texts.NewLineChar);
		кцПри reader.eot;
		возврат first;
	кон GetLineBuffer;

	проц ReadLine (pos: размерМЗ; reader: Texts.TextReader): Strings.String;
	перем
		ch: цел32; i: размерМЗ;
		string: Strings.String;
	нач
		reader.SetPosition (pos);
		i := 0;
		нов (string, maxLineSize + 1);
		нц
			reader.ReadCh (ch);
			если reader.eot или (ch = Texts.NewLineChar) или (i = maxLineSize) то
				прервиЦикл
			иначе
				string[i] := симв8ИзКода (ch);
				увел (i);
			всё;
		кц;
		string[i] := 0X;
		возврат string;
	кон ReadLine;

	проц Diff* (
		leftFile, rightFile: массив из симв8;
		setup: SetupHandler; leftDiff, rightDiff, leftEqual, rightEqual: Handler; emptyLeft, emptyRight: EmptyHandler;
		out : Потоки.Писарь);
	перем
		leftText, rightText: Texts.Text;
		leftReader, rightReader: Texts.TextReader;
		format, res: целМЗ;
		leftBuffer, rightBuffer, left, right: LineBuffer;
		width, height : размерМЗ;
		table: укль на массив из массив из Element;
		x, y, xv, yv: размерМЗ;

		проц CompareLines (left, right: размерМЗ): булево;
		перем
			leftCh, rightCh: цел32;
		нач
			leftReader.SetPosition (GetLinePos (leftBuffer, left));
			rightReader.SetPosition (GetLinePos (rightBuffer, right));

			нц
				leftReader.ReadCh (leftCh);
				rightReader.ReadCh (rightCh);

				если leftReader.eot и rightReader.eot то возврат истина всё;

				если leftCh # rightCh то возврат ложь всё;

				если (leftCh = Texts.NewLineChar) или (rightCh = Texts.NewLineChar) то
					возврат leftCh = rightCh;
				всё;
			кц;
		кон CompareLines;

	нач
		нов (leftText);
		TextUtilities.LoadAuto(leftText, leftFile, format, res);
		leftText.AcquireRead;
		нов (leftReader, leftText);
		leftReader.SetPosition (0);

		нов (rightText);
		TextUtilities.LoadAuto(rightText, rightFile, format, res);
		rightText.AcquireRead;
		нов (rightReader, rightText);
		rightReader.SetPosition (0);

		leftBuffer := GetLineBuffer (leftReader, width);
		rightBuffer := GetLineBuffer (rightReader, height);
		если setup # НУЛЬ то setup(width + height); всё;

		нов (table, width + 1, height + 1);

		table[0, 0].val := 0;
		table[0, 0].dir := 0;

		нцДля x := 1 до width делай
			table[x, 0].val := 0;
			table[x, 0].dir := dirLeft;
		кц;

		нцДля y := 1 до height делай
			table[0, y].val := 0;
			table[0, y].dir := dirUp;
		кц;

		left := leftBuffer;
		right := rightBuffer;

		нцДля y := 1 до height делай
			нцДля x := 1 до width делай
				если CompareLines (x - 1, y - 1) то
					table[x, y].val := table[x - 1, y - 1].val + 1;
					table[x, y].dir := dirDiag;
				иначе
					xv := table[x - 1, y].val;
					yv := table[x, y - 1].val;
					если xv > yv то
						table[x, y].val := xv;
						table[x, y].dir := dirLeft;
					иначе
						table[x, y].val := yv;
						table[x, y].dir := dirUp;
					всё;
				всё;
			кц;
		кц;

		(* DEC (x); DEC (y); *)
		x := width; y := height;

		нцПока (x # 0) или (y # 0) делай
			просей table[x, y].dir из
			dirUp:
				умень (y); table[x, y].val := dirDown;
			| dirLeft:
				умень (x); table[x, y].val := dirRight;
			| dirDiag:
				умень (x); умень (y); table[x, y].val := dirDiag;
			всё
		кц;

		нцПока (x # width) или (y # height) делай
			просей table[x, y].val из
			dirDown:
				увел (y); Handle (y, rightReader, rightBuffer, rightDiff, out); если emptyLeft # НУЛЬ то emptyLeft; всё;
			| dirRight:
				увел (x); Handle (x, leftReader, leftBuffer, leftDiff, out); если emptyRight # НУЛЬ то emptyRight; всё;
			| dirDiag:
				увел (x); Handle (x, leftReader, leftBuffer, leftEqual, out);
				увел (y); Handle (y, rightReader, rightBuffer, rightEqual, out);
			всё
		кц;
	кон Diff;

	проц Handle (line: размерМЗ; reader: Texts.TextReader; buffer: LineBuffer; handler: Handler; out : Потоки.Писарь);
	перем
		pos: размерМЗ;
	нач
		если handler # НУЛЬ то
			pos := GetLinePos (buffer, line - 1);
			handler (pos, line, ReadLine (pos, reader), out);
		всё
	кон Handle;

	проц Left (pos, line: размерМЗ; string: Strings.String; out : Потоки.Писарь);
	нач
		out.пСтроку8 (		"< ("); out.пЦел64 (line, 0); out.пСимв8 (':');
		out.пЦел64 (pos, 0); out.пСтроку8 (") "); out.пСтроку8 (string^); out.пВК_ПС;
		separator := истина;
	кон Left;

	проц Right (pos, line: размерМЗ; string: Strings.String; out : Потоки.Писарь);
	нач
		out.пСтроку8 ("> ("); out.пЦел64 (line, 0); out.пСимв8 (':');
		out.пЦел64 (pos, 0); out.пСтроку8 (") "); out.пСтроку8 (string^); out.пВК_ПС;
		separator := истина;
	кон Right;

	проц Equal (pos, line: размерМЗ; string: Strings.String; out : Потоки.Писарь);
	нач
		если separator то out.пВК_ПС; separator := ложь всё
	кон Equal;

	проц Compare* (context : Commands.Context);
	перем
		left, right: массив 64 из симв8;
	нач
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(left);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(right);

		context.out.пСтроку8 ("< "); context.out.пСтроку8 (left); context.out.пВК_ПС;
		context.out.пСтроку8 ("> "); context.out.пСтроку8 (right); context.out.пВК_ПС;
		context.out.пВК_ПС;

		separator := ложь;

		Diff (left, right, НУЛЬ, Left, Right, Equal, Equal, НУЛЬ, НУЛЬ, context.out);
	кон Compare;

кон DiffLib.

System.Free DiffLib~
DiffLib.Compare DiffTest1.Text DiffTest2.Text~

DiffLib.Compare Configuration.XML Configuration.XML.Bk ~
