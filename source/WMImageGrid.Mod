(*Author: Stephan Koster; Purpose: Components to display multiple images arranged in a grid*)
модуль WMImageGrid;
использует WMWindowManager, Graphics:=WMGraphics, Raster, Messages:=WMMessages, Rectangles:=WMRectangles;

тип Message=Messages.Message;
тип Rectangle=Rectangles.Rectangle;
тип Grid=массив из массив из Graphics.Image;


тип GridWindow* = окласс(WMWindowManager.Window)
	перем
		imgs*: укль на Grid;
		background*: Graphics.Image;
		canvas* : Graphics.BufferCanvas;
		canvasGen-: Graphics.CanvasGenerator;
		pointerThreshold*,
		maxInterpolation* : целМЗ; (* allows limiting the interpolation degree on Draw *)
		hs,ws: укль на массив из размерМЗ;
		totalW, totalH: размерМЗ;
		gap: размерМЗ;

		проц &Init1*( конст Ws, Hs: массив из размерМЗ; alpha : булево);
		перем
			i,j: размерМЗ;
			rm: Raster.Mode;
			pix: Raster.Pixel;
		нач
			gap:=10;
			нов(сам.ws, длинаМассива(Ws));
			нов(сам.hs, длинаМассива(Hs));
			нцДля i:=0 до длинаМассива(Ws,0)-1 делай
				утв(Ws[i]>0);
				сам.ws[i]:=Ws[i];
				totalW:=totalW+Ws[i];
			кц;
			totalW:=totalW+gap*(длинаМассива(Ws,0)-1);

			нцДля i:=0 до длинаМассива(Hs,0)-1 делай
				утв(Hs[i]>0);
				сам.hs[i]:=Hs[i];
				totalH:=totalH+Hs[i];
			кц;
			totalH:=totalH+gap*(длинаМассива(Hs,0)-1);

			Init(totalW, totalH, alpha); (*parent constructor*)

			нов(imgs,длинаМассива(Ws,0),длинаМассива(Hs,0));
			нцДля i:=0 до длинаМассива(Ws,0)-1 делай
				нцДля j:=0 до длинаМассива(Hs,0)-1 делай
					нов(imgs[i,j]);
					если alpha то Raster.Create(imgs[i,j], Ws[i], Hs[j], Raster.BGRA8888) иначе Raster.Create(imgs[i,j], Ws[i], Hs[j], WMWindowManager.format) всё;
				кц
			кц;
			нов(background);
			если alpha то Raster.Create(background, totalW, totalH, Raster.BGRA8888) иначе Raster.Create(background, totalW, totalH, WMWindowManager.format) всё;
			Raster.InitMode(rm, Raster.srcOverDst);
			Raster.SetRGBA(pix, 0,0,0,255);
			Raster.Fill(background, 0,0, totalW,totalH,pix, rm);
			SetCanvasGenerator(Graphics.GenCanvas);
			pointerThreshold := 1; (* invisible pixels are treated as invisible *)
			maxInterpolation := Graphics.ScaleBilinear;
		кон Init1;

		проц SetCanvasGenerator*(canvasGen:Graphics.CanvasGenerator);
		нач{единолично}
			сам.canvasGen:=canvasGen; если background # НУЛЬ то canvas:=canvasGen(background); всё;
			если manager # НУЛЬ то manager.AddVisibleDirty(сам, bounds) всё
		кон SetCanvasGenerator;

		проц {перекрыта}IsHit(x, y  : размерМЗ) : булево;
		перем w, h : размерМЗ; fx, fy : вещ32;
		нач
			w := GetWidth(); h := GetHeight();
			если (w > 0) и (h > 0) и ((w # totalW) или (h # totalH)) то
				fx := totalW/ w;
				fy := totalH/ h;
				возврат Graphics.IsBitmapHit(округлиВниз(x * fx), округлиВниз(y * fy), pointerThreshold, background)
			иначе возврат Graphics.IsBitmapHit(x, y, pointerThreshold, background)
			всё
		кон IsHit;

		проц {перекрыта}Draw*(canvas : Graphics.Canvas; w, h : размерМЗ; q : целМЗ);
		перем
			mode: целМЗ;
			isScaled: булево;
			i,j: размерМЗ;
			x,y: размерМЗ;

			проц DrawSingle(конст  img: Graphics.Image; offX,offY: размерМЗ; isScaled: булево; mode: целМЗ);
			перем
				wscaled,hscaled: размерМЗ;
			нач
				если img # НУЛЬ то
				если ~isScaled то
					canvas.DrawImage(offX,offY,img, mode);
				иначе
					offX:=offX*w DIV totalW;  (*these are not pixel perfect, but I can't think of anything better*)
					offY:=offY*h DIV totalH;
					wscaled:=img.width*w DIV totalW; (*this means if the image is not actually the size suggested by ws[i],hs[j], then it won't be blown up to fill the space.*)
					hscaled:=img.height*h DIV  totalH;
					canvas.ScaleImage(img, Rectangles.MakeRect(0, 0, img.width, img.height), Rectangles.MakeRect(offX,offY, offX+wscaled, offY+hscaled), mode, матМинимум(q,maxInterpolation));
				всё
			всё;
			кон DrawSingle;

		нач
			если useAlpha то
				mode:=Graphics.ModeSrcOverDst;
			иначе
				mode:=Graphics.ModeCopy;
			всё;
			isScaled:=~((w = totalW) и (h = totalH));

			если reduceQuality то q := 0 всё;
			DrawSingle(background, 0,0, isScaled, mode);
			x:=0;
			нцДля i:=0 до длинаМассива(imgs,0)-1 делай
				y:=0;
				нцДля j:=0 до длинаМассива(imgs,1)-1 делай
					DrawSingle(imgs[i,j], x,y, isScaled,mode);
					y:=y+hs[j]+gap;
				кц;
				x:=x+ws[i]+gap;
			кц;


			увел(timestamp);
		кон Draw;

		проц {перекрыта}Invalidate*(rect : Rectangle);
		перем w, h : размерМЗ; fx, fy : вещ64;
		нач
			w := GetWidth(); h := GetHeight();
			если (w > 0) и (h > 0) и ((w # totalW) или (h # totalH))  то
				fx := w / totalW; fy := h / totalH;
				rect.l := округлиВниз(rect.l * fx); rect.t := округлиВниз(rect.t * fy);
				rect.r := округлиВниз(rect.r * fx + 0.5); rect.b := округлиВниз(rect.b * fy + 0.5)
			всё;
			Invalidate^(rect)
		кон Invalidate;

		проц {перекрыта}Handle*(перем m : Message);
		перем w, h : размерМЗ; fx, fy : вещ32;
		нач
			w := GetWidth(); h := GetHeight();
			если (w > 0) и (h > 0) и ((w # totalW) или (h # totalH)) и (m.msgType = Messages.MsgPointer) то
				m.x := m.x-bounds.l; m.y := m.y-bounds.t;
				fx := totalW/ w;
				fy := totalH/ h;
				m.x := округлиВниз(m.x * fx); m.y := округлиВниз(m.y * fy);
				m.x := m.x + bounds.l; m.y := m.y+bounds.l;
			аесли m.msgType = Messages.MsgInvalidate то
				если m.msgSubType = Messages.MsgSubAll то
					Invalidate(Rectangles.MakeRect(0, 0, totalW, totalH));
				иначе
					СТОП(200)
				всё;
			иначе
				Handle^(m)
			всё;
		кон Handle;

	кон GridWindow;



	проц Test*();
	перем
		w: GridWindow;
		Ws,Hs: массив 3 из размерМЗ;
		i: размерМЗ;
		rm: Raster.Mode;
		pix: Raster.Pixel;
	нач

		нцДля i:=0 до 2 делай
			Ws[i]:=100*i+30;
			Hs[i]:=20+200*i;
		кц;
		нов(w,Ws,Hs, истина);


		Raster.InitMode(rm, Raster.srcOverDst);
		Raster.SetRGBA(pix, 255,0,0,255);
		Raster.Fill(w.imgs[0,0], 0,0, Ws[0],Hs[0],pix, rm);
		Raster.SetRGBA(pix, 0,255,0,255);
		Raster.Fill(w.imgs[1,1], 0,0, Ws[1],Hs[1],pix, rm);
		Raster.SetRGBA(pix, 0,0,255,255);
		Raster.Fill(w.imgs[2,2], 0,0, Ws[2],Hs[2],pix, rm);

		WMWindowManager.DefaultAddWindow(w);


	кон Test;


кон WMImageGrid.


System.FreeDownTo WMImageGrid~
WMImageGrid.Test~
