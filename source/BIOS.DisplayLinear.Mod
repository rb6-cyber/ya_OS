(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль DisplayLinear; (** AUTHOR "pjm"; PURPOSE "Linear framebuffer display driver"; *)

(*
Config strings:
	DWidth=1024	Display width
	DHeight=768	Display height
	DDepth=16	Display depth
	DMem=?	Display memory size in bytes
	Init=?	Init program.

The Init program is a 8086 machine code program in hexadecimal.  It has to initialize the specified display mode, possibly by making display BIOS calls, and leave the 32-bit physical address of the frame buffer in DX:CX.
*)

использует НИЗКОУР, Machine, ЛогЯдра, MemCache, Displays, Strings, Commands, Options, Modules;

перем
	d: Displays.Display;

проц GetVal(name: массив из симв8;  default: цел32): цел32;
перем v: цел32;  s: массив 10 из симв8;  p: размерМЗ;
нач
	Machine.GetConfig(name, s);
	если s[0] = 0X то
		v := default
	иначе
		p := 0;  v := Machine.StrToInt(p, s)
	всё;
	возврат v
кон GetVal;

проц Install*(context: Commands.Context);
перем options: Options.Options;
нач
	нов(options);
	options.Add("r", "reverse", Options.Flag);
	если options.Parse(context.arg, context.error) то
		если options.GetFlag("r") то Displays.Reverse() всё;
	всё;
кон Install;

проц InitPalette;
перем col: цел32; ch: симв8;
нач
	Machine.Portin8(3DAH, ch);
	Machine.Portout8(3C0H, 11X);
	Machine.Portout8(3C0H, 0X);	(* palette entry 0 is black *)
	Machine.Portout8(3C0H, 20X);
	нцДля col := 0 до 255 делай
		Machine.Portout8(3C8H, симв8ИзКода(col));
		Machine.Portout8(3C9H, симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, col) * {5..7}) DIV 4));
		Machine.Portout8(3C9H, симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(col, 7-4)) * {5..7}) DIV 4));
		Machine.Portout8(3C9H, симв8ИзКода(НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, арифмСдвиг(col, 7-1)) * {6..7}) DIV 4))
	кц
кон InitPalette;

проц Init;
перем w, h, f, mem: цел32; res: целМЗ; ts : массив 16 из симв8; padr, vadr: адресВПамяти;
нач
	w := GetVal("DWidth", 1024); h := GetVal("DHeight", 768);
	просей GetVal("DDepth", 16) DIV 8 из
		1: f := Displays.index8; InitPalette
		|2: f := Displays.color565
		|3: f := Displays.color888
		|4: f := Displays.color8888
	всё;
	mem := GetVal("DMem", 0)*1024;
	если mem = 0 то	(* compute default *)
		mem := 512*1024;
		нцПока w*h*f >= mem делай mem := mem*2 кц
	всё;
	Machine.GetInit(1, НИЗКОУР.подмениТипЗначения(цел32,padr));	(* DX:CX from Init code *)
	утв((padr # 0) и (padr остОтДеленияНа 4096 = 0));
	если GetVal("DCache", 1) = 1 то
		MemCache.GlobalSetCacheProperties(padr, mem, MemCache.WC, res);
		если res # 0 то
			ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("DisplayLinear: GlobalSetCacheProperties = ");
			ЛогЯдра.пЦел64(res, 1); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё
	всё;
	(* KernelLog.ScreenOff; *)
	Machine.MapPhysical(padr, mem, vadr);
	Machine.Fill32(vadr, mem, 0);	(* clear video memory *)
	нов(d);
	d.width := w; d.height := h; d.offscreen := mem DIV (w*f) - h;
	d.format := f; d.unit := 10000;
	d.InitFrameBuffer(vadr, mem, w * f);
	d.desc := "Generic linear framebuffer driver (";
	Strings.IntToStr(d.width, ts); Strings.Append(d.desc, ts);
	Strings.Append(d.desc, "x");
	Strings.IntToStr(d.height, ts);  Strings.Append(d.desc, ts);
	Strings.Append(d.desc, "x");
	Strings.IntToStr(d.format, ts);  Strings.Append(d.desc, ts);
	Strings.Append(d.desc, ")");
	Displays.registry.Add(d, res);
	утв(res = 0)
кон Init;

проц CleanUp;
нач
	утв (d # НУЛЬ);
	Machine.Fill32(d.fbadr, d.fbsize, 0);	(* clear video memory *)
кон CleanUp;

нач
	Init;
	Modules.InstallTermHandler(CleanUp);
кон DisplayLinear.

