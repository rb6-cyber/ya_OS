модуль Clipboard;	(** AUTHOR "G.F."; PUROSE "X11 clipboard interface";  *)

(* Clipboard.Install ~ *)

использует	НИЗКОУР, Unix, Machine, X11, Displays, XDisplay, Plugins, Log := ЛогЯдра,
		Modules, Texts, TextUtilities, Strings, HostClipboard, Commands, Options;

конст
	(* Текст размера больше определённого будет обрезан. А может быть, 
	и ошибка будет *)
	BufferSize = 200000;  

тип   
	Buffer = укль на массив BufferSize из симв8;

перем
	sendBuffer, recBuffer, utf8Buffer: Buffer;
	slen, rlen, ulen	: размерМЗ;
	received	: булево;

	UTF8_STRING_atom	: X11.Atom;
	xdisp		: X11.DisplayPtr;
	primary		: X11.Window;
	secondary	: X11.Window;
	clipboard: X11.Atom;
	TARGETS: X11.Atom;
	myProperty: X11.Atom;
	supportedTargets: массив 2 из X11.Atom;

	проц UnixToA2;
	нач
		utf8Buffer^ := recBuffer^;
		если rlen>0 то
			utf8Buffer[rlen-1] := 0X всё;
		ulen := rlen;
	кон UnixToA2; 	
	
	проц ClearSelection;
	нач
	(*	Texts.ClearLastSelection	*)
	кон ClearSelection;

	проц ClipboardChanged( sender, data : динамическиТипизированныйУкль );
	нач
		Texts.clipboard.AcquireRead;
		PutToClipboard( Texts.clipboard );
		Texts.clipboard.ReleaseRead;
	кон ClipboardChanged;

	(** Copy text to X11 clipboard *)
	проц PutToClipboard( text : Texts.Text );
	нач
		утв((text # НУЛЬ) и (text.HasReadLock()));
		TextUtilities.TextToStr( text, sendBuffer^ );  slen := Strings.Length( sendBuffer^ );
		Machine.Acquire( Machine.X11 );
		X11.SetSelectionOwner( xdisp, clipboard, primary, X11.CurrentTime );
		Machine.Release( Machine.X11 );
	кон PutToClipboard;

	проц SendSelection( перем event: X11.SelectionRequestEvent );
	перем ev: X11.SelectionEvent;
	нач
		ev.typ := X11.SelectionNotify;
		ev.requestor := event.requestor;
		ev.selection := event.selection;
		ev.target := event.target;
		ev.time := event.time;
		если (event.selection = clipboard)  то
			(* ShowAtom(event.target); *)
			если (event.target = TARGETS) то
				(* руководство говорит, что TARGETS обязательно *)
				ev.property := event.property;
				Machine.Acquire(Machine.X11);
				X11.ChangeProperty(xdisp, ev.requestor, ev.property, X11.XA_ATOM, 32, X11.PropModeReplace,
						адресОт(supportedTargets[0]), длинаМассива(supportedTargets));
				Machine.Release(Machine.X11);
			аесли (event.target = UTF8_STRING_atom) то
				ev.property := event.property;
				Machine.Acquire(Machine.X11);
				X11.ChangeProperty(xdisp, ev.requestor, ev.property, ev.target, 8, X11.PropModeReplace,
						адресОт( sendBuffer[0] ), целМЗ(slen));
				Machine.Release( Machine.X11);
			иначе
				ev.property := X11.None всё
		иначе
			ev.property := X11.None
		всё;
		Machine.Acquire( Machine.X11 );
		X11.SendEvent( xdisp, ev.requestor, X11.False, 0, адресОт(ev) );
		Machine.Release( Machine.X11 );
	кон SendSelection;

	(** Copy text of X11 clipboard to text *)
	проц GetFromClipboard( text : Texts.Text );
	нач
		утв((text # НУЛЬ) и (text.HasWriteLock()));
		GetX11Selection;
		UnixToA2;
		TextUtilities.StrToText( text, 0, utf8Buffer^ );
	кон GetFromClipboard;

	проц GetX11Selection;
	нач{единолично}
		received := ложь;
		Machine.Acquire( Machine.X11 );
		X11.ConvertSelection( xdisp, clipboard, UTF8_STRING_atom, myProperty, primary, X11.lastEventTime ); 
		Machine.Release( Machine.X11 );
		дождись( received ); 
	кон GetX11Selection;  
	
	(* проц ShowAtom(a:X11.Atom);
	перем имяАтома : АДРЕС; имяАтомаКакСтрока: укль {не_отсл_уборщиком, опасный_доступ_к_памяти} на ряд 512 из симв8 ;
	тело
	имяАтома := X11.XGetAtomName(xdisp, a);
	если имяАтома = НУЛЬ то
		TRACE("имяАтома = НУЛЬ")
	иначе
		имяАтомаКакСтрока := НИЗКОУР.VAL(Strings.String,имяАтома);
		TRACE(имяАтомаКакСтрока^) кн кн ShowAtom;	 *)
	

	проц ReceiveSelection( перем event: X11.SelectionEvent );
	перем type: X11.Atom;  format: целМЗ; len, after: длинноеЦелМЗ;  prop, adr: адресВПамяти; ch: симв8;
			нач {единолично}
		rlen := 0; recBuffer[0] := 0X;
		если (event.selection = clipboard) то
			(* ShowAtom(event.property); *)
			всё;
		если (event.selection = clipboard) и (event.property = myProperty) то
			Machine.Acquire( Machine.X11 );
			X11.GetWindowProperty( xdisp, event.requestor, event.property, 0, BufferSize, X11.False,
									event.target, type, format, len, after, prop );
			Machine.Release( Machine.X11 );
			adr := prop;
			если len >= BufferSize то len := BufferSize - 2  всё;
			нцПока len > 0 делай
				НИЗКОУР.прочтиОбъектПоАдресу( adr, ch ); увел( adr ); умень( len );
				если ch # 0X то  recBuffer[rlen] := ch;  увел( rlen )  всё
			кц;
			recBuffer[rlen] := 0X;  увел( rlen );
			Machine.Acquire( Machine.X11 );
			X11.Free( prop );  X11.DeleteProperty( xdisp, event.requestor, event.property );
			Machine.Release( Machine.X11 );
		всё;
		received := истина;
	кон ReceiveSelection;

	проц GetXDisplay;
	перем p: Plugins.Plugin;  disp: XDisplay.Display; 
	нач
		p := Displays.registry.Await("XDisplay");
		disp := p(XDisplay.Display);
		xdisp := disp.xdisp;
		primary := disp.primary;
		secondary := disp.secondary;
	кон GetXDisplay;

	(* set Selection handlers to NIL *)
	проц Cleanup;
	нач
		X11.SendSelection := НУЛЬ;
		X11.ReceiveSelection := НУЛЬ;
		X11.ClearSelection := НУЛЬ;

		Texts.clipboard.onTextChanged.Remove( ClipboardChanged );
		HostClipboard.SetHandlers( НУЛЬ, НУЛЬ );

		Log.ЗахватВЕдиноличноеПользование; Log.пСтроку8( "X11 clipboard unregistered at host clipboard interface." );  Log.ОсвобождениеИзЕдиноличногоПользования;
	кон Cleanup;

	проц Install*(context: Commands.Context);
	перем options: Options.Options;
	нач
		нов(options);
		options.Add("p","primary", Options.Flag);
		если Unix.Version = "Darwin" то
			Log.пСтроку8( "Can't register the X11 clipboard in the Darwin port (ABI incompatiblity)" ); Log.пВК_ПС
		иначе
			если options.Parse(context.arg, context.error) то
				GetXDisplay;
				X11.SendSelection := SendSelection;
				X11.ReceiveSelection := ReceiveSelection;
				X11.ClearSelection := ClearSelection;

				Machine.Acquire( Machine.X11 );
				UTF8_STRING_atom := X11.InternAtom( xdisp, "UTF8_STRING", X11.False );
				clipboard := X11.InternAtom(xdisp,"CLIPBOARD",1);
				TARGETS := X11.InternAtom(xdisp,"TARGETS",1);
				myProperty := X11.InternAtom(xdisp,"JAOS",X11.False);
				supportedTargets[0] := TARGETS;
				supportedTargets[1] := UTF8_STRING_atom;
				Machine.Release( Machine.X11 );

				если options.GetFlag("primary") то clipboard := X11.XAPRIMARY всё;

				нов( sendBuffer );  нов( recBuffer );  slen := 0;  rlen := 0;
				нов( utf8Buffer );  ulen := 0;

				(* register with AosText clipboard *)
				Texts.clipboard.onTextChanged.Add( ClipboardChanged );
				HostClipboard.SetHandlers( GetFromClipboard, PutToClipboard );
				Modules.InstallTermHandler( Cleanup );
				Log.ЗахватВЕдиноличноеПользование; Log.пСтроку8("X11 clipboard registered at host clipboard interface."); Log.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		всё
	кон Install;

нач
кон Clipboard.

 System.Free Clipboard ~
 Clipboard.Install ~

	---------------------------------------------
	Cut & paste between X11 applications and UnixAos.

	After performing 'Clipboard.Install ~' the X11 clipboard and the
	Aos clipboard get synchronized. Every new X11 selection modifies
	the Aos clipboard and the copy operation with the pie menu in
	Aos alters the primary X11 selection. The selection can then be
	inserted into an X11 application (e.g. the mail tool Thunderbird)
	by clicking the MM button.

	To stop the clipboard synchronization perform:
		'System.Free Clipboard ~'.

	---------------------------------------------
