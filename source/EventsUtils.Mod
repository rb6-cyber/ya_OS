модуль EventsUtils; (** AUTHOR "staubesv"; PURPOSE "System events utilities"; *)
(**
 * History:
 *
 *	07.03.2007	First release (staubesv)
 *)

использует
	Commands, Events, Потоки, Files, Dates, Strings;

конст

	(** Result codes for system event operations *)
	Ok* = 0;
	Error* = 1;
	Uncomplete* = 3;

	EOF = 4;

	DateTimeFormat = "dd.mm.yyyy hh:nn:ss"; (* don't change or adapt DateTimeFromStream *)

тип

	EventWrapper* = укль на запись
		nextIndex- : размерМЗ; (* index of next free place in events array *)
		events- : укль на массив из Events.Event;
		next- : EventWrapper;
	кон;

тип

	EventContainer* = окласс(Events.Sink)
	перем
		nofWrappers, nofEvents : размерМЗ;

		(* stamps *)
		lastCleared, lastAdded : размерМЗ;

		events, current : EventWrapper;
		maxNofWrappers, eventsPerWrapper : размерМЗ;

		(* for polling *)
		проц GetStamp*() : размерМЗ;
		нач
			возврат lastAdded;
		кон GetStamp;

		проц GetEvents*(перем nofEvents : размерМЗ; перем full : булево; перем lastCleared : размерМЗ) : EventWrapper;
		нач {единолично}
			nofEvents := сам.nofEvents;
			full := nofEvents = maxNofWrappers * eventsPerWrapper;
			lastCleared := сам.lastCleared;
			возврат events;
		кон GetEvents;

		проц IsFull*() : булево;
		нач {единолично}
			возврат nofEvents = maxNofWrappers * eventsPerWrapper;
		кон IsFull;

		проц Clear*;
		нач {единолично}
			events.next := НУЛЬ; events.nextIndex := 0;
			current := events;
			nofWrappers := 1; nofEvents := 0;
			увел(lastCleared); увел(lastAdded);
		кон Clear;

		(** Returns the maximum number of event records this container can hold *)
		проц GetSize*() : размерМЗ;
		нач
			возврат maxNofWrappers * eventsPerWrapper;
		кон GetSize;

		проц {перекрыта}Handle*(event : Events.Event);
		перем wrapper : EventWrapper;
		нач {единолично}
			если nofEvents = maxNofWrappers * eventsPerWrapper то возврат; всё;

			если (current.nextIndex >= длинаМассива(current.events)) то
				нов(wrapper); нов(wrapper.events, eventsPerWrapper); wrapper.nextIndex := 0;
				current.next := wrapper;
				current := wrapper;
				увел(nofWrappers);
			всё;

			current.events[current.nextIndex] := event;
			увел(current.nextIndex);
			увел(nofEvents);
			увел(lastAdded);
		кон Handle;

		проц &Init*(maxNofWrappers, eventsPerWrapper : размерМЗ);
		нач
			сам.maxNofWrappers := maxNofWrappers;
			сам.eventsPerWrapper:= eventsPerWrapper;
			нов(events); нов(events.events, eventsPerWrapper); events.nextIndex := 0;
			current := events;
			nofWrappers := 1; nofEvents := 0;
		кон Init;

	кон EventContainer;

проц LoadFromFile*(конст filename : массив из симв8; перем events : EventContainer; перем msg : массив из симв8; перем res : целМЗ);
перем file : Files.File; r : Files.Reader; event : Events.Event; nofEvents : размерМЗ;
нач
	file := Files.Old(filename);
	если file # НУЛЬ то
		Files.OpenReader(r, file, 0);
		нов(events, 1024, 1024);
		nofEvents := 0;
		нцПока (r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0) и (r.кодВозвратаПоследнейОперации = Потоки.Успех) делай
			FromStream(r, event, msg, res);
			если (res = Ok) то
				увел(nofEvents);
				events.Handle(event);
			аесли (res = EOF) то
				(* all done *)
			иначе
				если (nofEvents = 0) то
					res := Error;
				иначе
					res := Uncomplete;
				всё;
				возврат;
			всё;
		кц;
		res := Ok;
	иначе
		msg := "File not found"; res := Error;
	всё;
кон LoadFromFile;

проц StoreToFile*(конст filename : массив из симв8; events : EventContainer; перем msg : массив из симв8; перем res : целМЗ);
перем
	file : Files.File; w : Files.Writer; wrapper : EventWrapper;
	nofEvents, lastCleared, i, idx : размерМЗ; full : булево;
нач
	file := Files.New(filename);
	если file # НУЛЬ то
		Files.OpenWriter(w, file, 0);
		wrapper := events.GetEvents(nofEvents, full, lastCleared);
		если nofEvents > 0 то
			i := 0;
			нцПока (i < nofEvents) делай
				если i >= длинаМассива(wrapper.events) то wrapper := wrapper.next; всё;
				idx := i остОтДеленияНа длинаМассива(wrapper.events);
				ToStream(w, wrapper.events[idx]);
				увел(i);
			кц;
			Files.Register(file);
			res := Ok;
		иначе
			msg := "Number of events must be greater than zero"; res := Error;
		всё;
	иначе
		msg := "Could not create file"; res := Error;
	всё;
кон StoreToFile;

проц ToStream*(w : Потоки.Писарь; event : Events.Event);
перем dt : Dates.DateTime; str : массив 64 из симв8;
нач
	утв(w # НУЛЬ);
	dt := Dates.OberonToDateTime(event.date, event.time);
	Strings.FormatDateTime(DateTimeFormat, dt, str);
	w.пСтроку8(str); w.пСтроку8(" ");
	GetTypeString(event.type, str); w.пСтроку8(str); w.пСтроку8(" ");
	w.пСтроку8('"'); w.пСтроку8(event.originator); w.пСтроку8('"');
	w.пСтроку8(" ["); w.пЦел64(event.class, 0); w.пСтроку8(","); w.пЦел64(event.subclass, 0); w.пСтроку8(","); w.пЦел64(event.code, 0); w.пСтроку8('] "');
	w.пСтроку8(event.message); w.пСтроку8('"'); w.пВК_ПС;
	w.ПротолкниБуферВПоток;
кон ToStream;

проц FromStream*(r : Потоки.Чтец; перем event : Events.Event; перем msg : массив из симв8; перем res : целМЗ);
перем dt : Dates.DateTime; str : Events.Message; ch : симв8; class, subclass, code : цел32;

	проц IsValid(value : цел32) : булево;
	нач
		возврат (0 <= value) и (value <= матМаксимум(цел8));
	кон IsValid;

нач
	утв(r # НУЛЬ);
	res := Error;
	r.ПропустиБелоеПоле;
	если r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() = 0 то res := EOF; возврат; всё;
	(* date & time *)
	если ~DateTimeFromStream(r, dt) то
		ch := r.ПодглядиСимв8();
		если r.кодВозвратаПоследнейОперации = Потоки.КонецФайла то res := Ok; возврат;
		иначе
			msg := "Could not read datetime string"; возврат;
		всё;
	всё;
	Dates.DateTimeToOberon(dt, event.date, event.time);
	(* type *)
	r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то msg := "Could not read type string"; возврат; всё;
	event.type := GetType(str);
	(* originator *)
	r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(event.originator); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то msg := "Could not read originator string"; возврат; всё;
	(* class, subclass & code *)
	r.ПропустиБелоеПоле;
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # "[") то msg := "Expected opening bracket"; возврат; всё;
	r.чЦел32(class, ложь); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то	msg := "Could not parse event class"; возврат; всё;
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # ",") то msg := "Expected ,"; возврат; всё;
	r.чЦел32(subclass, ложь); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то msg := "Could not parse event subclass"; возврат; всё;
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # ",") то msg := "Expected ,"; возврат; всё;
	r.чЦел32(code, ложь); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то msg := "Could not parse event code"; возврат; всё;
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # "]") то msg := "Expected closing bracket"; возврат; всё;
	(* check validity of class/subclass/code *)
	если ~IsValid(class) то msg := "Class must be in [0, 127]"; возврат; всё;
	если ~IsValid(subclass) то msg := "Subclass must be in [0, 127]"; возврат; всё;
	если ~IsValid(code) то msg := "Code must be in [0, 127]"; возврат; всё;
	event.class := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(class));
	event.subclass := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(subclass));
	event.code := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(code));
	(* message *)
	r.ПропустиБелоеПоле; r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(event.message);
	если (r.кодВозвратаПоследнейОперации # Потоки.КонецФайла) и (~r.ВКонцеСтрокиТекстаИлиФайлаЛи¿()) то msg := "Expected end of line"; возврат; всё;
	res := Ok;
кон FromStream;

проц DateTimeFromStream(r : Потоки.Чтец; перем dt : Dates.DateTime) : булево;
перем ch : симв8;
нач
	утв(r # НУЛЬ);
	r.ПропустиБелоеПоле;
	r.чЦел32(dt.day, ложь);
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # ".") то возврат ложь; всё;
	r.чЦел32(dt.month, ложь);
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # ".") то возврат ложь; всё;
	r.чЦел32(dt.year, ложь);
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # " ") то возврат ложь; всё;
	r.чЦел32(dt.hour, ложь);
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # ":") то возврат ложь; всё;
	r.чЦел32(dt.minute, ложь);
	r.чСимв8(ch); если (r.кодВозвратаПоследнейОперации # Потоки.Успех) или (ch # ":") то возврат ложь; всё;
	r.чЦел32(dt.second, ложь);
	если (r.кодВозвратаПоследнейОперации # Потоки.Успех) то возврат ложь; всё;
	возврат Dates.ValidDateTime(dt);
кон DateTimeFromStream;

проц GetTypeString*(type : цел32; перем string: массив из симв8);
перем nbr : массив 16 из симв8;
нач
	просей type из
		|Events.Unknown: string := "Unknown";
		|Events.Undefined: string := "Undefined";
		|Events.Information: string := "Information";
		|Events.Warning: string := "Warning";
		|Events.Error: string := "Error";
		|Events.Critical: string := "Critical";
		|Events.Alert: string := "Alert";
		|Events.Failure: string := "Failure";
	иначе
		string := "Unknown ("; Strings.IntToStr(type, nbr); Strings.Append(string, nbr); Strings.Append(string, ")");
	всё;
кон GetTypeString;

проц GetType*(конст string : массив из симв8) : цел8;
перем type : цел8;
нач
	если string = "Unknown" то type := Events.Unknown;
	аесли string = "Undefined" то type := Events.Undefined;
	аесли string = "Information" то type := Events.Information;
	аесли string = "Warning" то type := Events.Warning;
	аесли string = "Error" то type := Events.Error;
	аесли string = "Critical" то type := Events.Critical;
	аесли string = "Alert" то type := Events.Alert;
	аесли string = "Failure" то type := Events.Failure;
	иначе
		type := Events.Unknown;
	всё;
	возврат type;
кон GetType;

проц GenerateEvent*(context : Commands.Context); (** originator type class subclass code message ~ *)
перем event : Events.Event; value : цел32;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(event.originator);
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(value, ложь); event.type := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(value));
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(value, ложь); event.class := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(value));
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(value, ложь); event.subclass := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(value));
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(value, ложь); event.code := устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(value));
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(event.message);
	Events.Add(event, ложь);
кон GenerateEvent;

кон EventsUtils.

