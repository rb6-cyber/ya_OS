модуль ZipFS; (** AUTHOR "ejz"; PURPOSE "mount a zipped file as a file-system"; *)
	использует Modules, Потоки, Files, Unzip, Dates;

	тип
		FileSystem = окласс (Files.FileSystem)
			перем zip: Unzip.ZipFile;

			проц &Init*(zip: Unzip.ZipFile);
			нач
				сам.zip := zip
			кон Init;

			проц {перекрыта}Old0*(конст name: массив из симв8): Files.File;
				перем E: Unzip.Entry; key: цел32; res: целМЗ; F: File; F0: Files.File; W: Files.Writer;
			нач {единолично}
				key := 0;
				E := zip.GetFirst();
				нцПока E # НУЛЬ делай
					увел(key);
					если E.name^ = name то
						F0 := localFS.New0("");
						Files.OpenWriter(W, F0, 0);
						zip.Extract(E, W, res);
						W.ПротолкниБуферВПоток();
						нов(F);
						F.fs := сам; F.key := key; F.E := E; F.F := F0;
						возврат F
					всё;
					E := zip.GetNext(E)
				кц;
				возврат НУЛЬ
			кон Old0;

			проц {перекрыта}Enumerate0*(конст mask: массив из симв8; flags: мнвоНаБитахМЗ; enum: Files.Enumerator);
				перем E: Unzip.Entry; name: Files.FileName; d, t: цел32;
			нач {единолично}
				E := zip.GetFirst();
				нцПока E # НУЛЬ делай
					если Match(mask, E.name^) то
						Files.JoinName(prefix, E.name^, name);
						если Files.EnumTime в flags то
							Dates.DateTimeToOberon(E.td, d, t)
						всё;
						enum.PutEntry(name, {}, t, d, E.size)
					всё;
					E := zip.GetNext(E)
				кц
			кон Enumerate0;

			проц {перекрыта}FileKey*(конст name: массив из симв8): цел32;
				перем E: Unzip.Entry; key: цел32;
			нач {единолично}
				key := 0;
				E := zip.GetFirst();
				нцПока E # НУЛЬ делай
					увел(key);
					если E.name^ = name то возврат key всё;
					E := zip.GetNext(E)
				кц;
				возврат 0
			кон FileKey;

			проц {перекрыта}Finalize*;
			нач {единолично}
				Finalize^()
			кон Finalize;

		кон FileSystem;

		File = окласс (Files.File)
			перем F: Files.File; E: Unzip.Entry;

			проц {перекрыта}Set*(перем r: Files.Rider; pos: Files.Position);
			нач
				F.Set(r, pos); r.file := сам
			кон Set;

			проц {перекрыта}Pos*(перем r: Files.Rider): Files.Position;
			нач
				возврат F.Pos(r)
			кон Pos;

			проц {перекрыта}Read*(перем r: Files.Rider; перем x: симв8);
			нач
				F.Read(r, x)
			кон Read;

			проц {перекрыта}ReadBytes*(перем r: Files.Rider; перем x: массив из симв8; ofs, len: размерМЗ);
			нач
				F.ReadBytes(r, x, ofs, len)
			кон ReadBytes;

			проц {перекрыта}Length*(): Files.Size;
			нач
				возврат F.Length()
			кон Length;

			проц {перекрыта}GetDate*(перем t, d: цел32);
			нач
				Dates.DateTimeToOberon(E.td, d, t)
			кон GetDate;

			проц {перекрыта}GetName*(перем name: массив из симв8);
			нач
				Files.JoinName(fs.prefix, E.name^, name)
			кон GetName;

			проц {перекрыта}Update*;
			нач
				F.Update кон
			Update;

		кон File;

	перем
		localFS: Files.FileSystem;

	(* Match - check if pattern matches file name; copied from DiskFS.Match and MatchPrefix *)
	проц Match(pat, name: массив из симв8): булево;
		перем pos, i0, i1, j0, j1: цел32; f: булево;
	нач
		f := истина;
		нц
			если pat[pos] = 0X то
				pos := -1; прервиЦикл
			аесли pat[pos] = "*" то
				если pat[pos+1] = 0X то pos := -1 всё;
				прервиЦикл
			аесли pat[pos] # name[pos] то
				f := ложь; прервиЦикл
			всё;
			увел(pos)
		кц;
		если pos # -1 то
			i0 := pos; j0 := pos;
			нц
				если pat[i0] = "*" то
					увел(i0);
					если pat[i0] = 0X то прервиЦикл всё
				иначе
					если name[j0] # 0X то f := ложь всё;
					прервиЦикл
				всё;
				f := ложь;
				нц
					если name[j0] = 0X то прервиЦикл всё;
					i1 := i0; j1 := j0;
					нц
						если (pat[i1] = 0X) или (pat[i1] = "*") то f := истина; прервиЦикл всё;
						если pat[i1] # name[j1] то прервиЦикл всё;
						увел(i1); увел(j1)
					кц;
					если f то j0 := j1; i0 := i1; прервиЦикл всё;
					увел(j0)
				кц;
				если ~f то прервиЦикл всё
			кц
		всё;
		возврат f и (name[0] # 0X)
	кон Match;

	проц NewFS*(context : Files.Parameters);
	перем
		name: Files.FileName;
		F: Files.File; zip: Unzip.ZipFile; fs: FileSystem; res: целМЗ;
	нач
		если (Files.This(context.prefix) = НУЛЬ) то
			context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(name);
			F := Files.Old(name);
			если F # НУЛЬ то
				нов(zip, F, res);
				если res = Потоки.Успех то
					нов(fs, zip);
					Files.Add(fs, context.prefix)
				иначе
					context.error.пСтроку8("ZipFS: "); context.error.пСтроку8(name); context.error.пСтроку8(" not a valid zip file");
					context.error.пВК_ПС;
				всё
			иначе
				context.error.пСтроку8("ZipFS: "); context.error.пСтроку8(name); context.error.пСтроку8(" not found");
				context.error.пВК_ПС;
			всё
		иначе
			context.error.пСтроку8("ZipFS: "); context.error.пСтроку8(context.prefix); context.error.пСтроку8(" already in use");
			context.error.пВК_ПС;
		всё;
	кон NewFS;

	проц Finalization;
		перем ft: Files.FileSystemTable; i: размерМЗ;
	нач
		если Modules.shutdown = Modules.None то
			Files.GetList(ft);
			если ft # НУЛЬ то
				нцДля i := 0 до длинаМассива(ft^)-1 делай
					если ft[i] суть FileSystem то Files.Remove(ft[i]) всё
				кц
			всё
		всё
	кон Finalization;

	проц Init;
		перем fs: Files.FileSystemTable; i: размерМЗ;
	нач
		i := 0;
		Files.GetList(fs);
		нцПока (i < длинаМассива(fs)) и ((fs[i].vol = НУЛЬ) или (Files.ReadOnly в fs[i].vol.flags)) делай
			увел(i)	(* find a writable file system *)
		кц;
		если (i < длинаМассива(fs)) то localFS := fs[i] всё;
		Modules.InstallTermHandler(Finalization)
	кон Init;

нач
	Init()
кон ZipFS.

System.Free ZipFS ~

OFSTools.Mount Test ZipFS ARM.Backup.zip ~
OFSTools.Unmount Test

System.Directory Test:*\d
