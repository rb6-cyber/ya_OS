(**
	AUTHOR: Alexey Morozov, HighDim GmbH, 2018
	PURPOSE: A2 clock with a plugable RTC get/set interface
*)

(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Clock;

тип

	(*
		Function for getting time from an RTC device

		second: seconds \in [0,59]
		minute: minutes \in [0,59]
		hour: hours \in [0,23]
		day: days \in [1,31]
		month: months \in [1,12]
		year: the actual year minus 1900
	*)
	GetRtcTimeFunc = проц{делегат}(перем second, minute, hour, day, month: цел8; перем year: цел16): булево;

	(*
		Function for setting up time on an RTC device

		second: seconds \in [0,59]
		minute: minutes \in [0,59]
		hour: hours \in [0,23]
		day: days \in [1,31]
		month: months \in [1,12]
		year: the actual year minus 1900
	*)
	SetRtcTimeFunc =проц{делегат}(second, minute, hour, day, month: цел8; year: цел16): булево;

перем
	getRtcTime: GetRtcTimeFunc;
	setRtcTime: SetRtcTimeFunc;

	tz*: цел32;	(** system time zone offset in minutes (from -720 to 720) *)
	starttime*, startdate*: цел32;	(** time this module was loaded (usually boot time) *)

(** Return the current time and date in Oberon format. *)

проц Get*(перем time, date: цел32);
перем
	second, minute, hour, day, month: цел8;
	year: цел16;
нач{единолично}
	если getRtcTime # НУЛЬ то
		если getRtcTime(second, minute, hour, day, month, year) то
			time := цел32(hour)*4096 + цел32(minute)*64 + second;
			date := цел32(year)*512 + цел32(month)*32 + day;
			возврат;
		всё;
	всё;
	time := 0;
	date := 0;
кон Get;

(** Set the current time and date in Oberon format. *)

проц Set*(time, date: цел32);
перем
	second, minute, hour, day, month: цел8;
	year: цел16;
нач{единолично}
	если setRtcTime # НУЛЬ то
		second := цел8(time остОтДеленияНа 64);
		minute := цел8(time DIV 64 остОтДеленияНа 64);
		hour := цел8(time DIV 4096 остОтДеленияНа 32);
		day := цел8(date остОтДеленияНа 32);
		month := цел8(date DIV 32 остОтДеленияНа 16);
		year := цел16(date DIV 512);
		если setRtcTime(second, minute, hour, day, month, year) то
		всё;
	всё;
кон Set;

проц Install*(get: GetRtcTimeFunc; set: SetRtcTimeFunc);
нач
	нач{единолично}
		getRtcTime := get;
		setRtcTime := set;
	кон;
	Get(starttime, startdate);
кон Install;

нач
	tz := 2*60;	(* fixme: configurable *)
кон Clock.

(*
23.08.1999	pjm	Split from Aos.Kernel
*)

(**
Notes

The time and date are that of the real-time clock of the system, which may be set to universal time, or to some local time zone.

The tz variable indicates the system time zone offset from universal time in minutes.  It may be updated at any time due to daylight savings time.  E.g. MET DST is 2 * 60 = 120.

The time and date are each represented in an encoded SIGNED32.

Converting from year, month, day, hour, minute, second to time, date:
	time := hour*4096 + minute*64 + second;
	date := (year-1900)*512 + month*32 + day;

Converting from time to hour, minute, second:
	hour := time DIV 4096 MOD 32;
	minute := time DIV 64 MOD 64;
	second := time MOD 64;

Converting from date to year, month, day:
	year = 1900+date DIV 512;
	month = date DIV 32 MOD 16;
	day = date MOD 32;

All years in the current millenium can be represented.  The 1900 offset is a historical artefact from the Oberon system.

Time and date values (respectively) can be compared with the normal Oberon operators <, <=, =, >=, >, #.  Overflow at midnight has to be handled separately.
*)
