(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль SymbiosDisks;	(** non-portable *)
(** AUTHOR "ryser"; PURPOSE "NCR/Symbios SCSI driver"; *)

(* Symbios/NCR SCSI Disk driver for Aos, Q&D port from the Native Oberon version by Peter Ryser *)

	использует НИЗКОУР, ЛогЯдра, Machine, ASPI := SymbiosASPI, Disks, Plugins;

	конст
		PageSize = 4096;

		BS = 512;	(* disk block size *)
		MaxRanges = ASPI.MaxRanges;
		MaxTransfer = (MaxRanges-1) * PageSize;

		TraceVerbose = ложь;

	тип
		Device = окласс (Disks.Device)
			перем
				ha, target, lun: симв8;
				writePreComp: цел32;
				capacity: цел32;

			проц {перекрыта}Transfer* (op, block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
			перем
				srb: ASPI.ExecIOCmdSRB; size, n, i, copyAdr, orgadr, orgsize: цел32; phys: массив MaxRanges из Machine.Range;
			нач
				orgsize := num*BS;
				orgadr := адресОт(data[ofs])(Machine.Address32);
				если TraceVerbose то
					ЛогЯдра.ЗахватВЕдиноличноеПользование;
					ЛогЯдра.пСтроку8("NCR: Transfer "); ЛогЯдра.пЦел64(op, 1); ЛогЯдра.пСимв8(" ");
					ЛогЯдра.пЦел64(block, 1); ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(num, 1);
(*
					IF op = Disks.Write THEN
						KernelLog.Ln; KernelLog.Memory(orgadr, orgsize)
					END;
*)
					ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё;
				если (op = Disks.Read) или (op = Disks.Write) то
					если (block >= 0) и (num > 0) то
						NewSRB(srb);
						нцДо
							copyAdr := 0;
							size := BS*num;	(* number of bytes to transfer now *)
							если size > MaxTransfer то
								size := MaxTransfer; утв(MaxTransfer остОтДеленияНа BS = 0)
							всё;
							если size > 256*256*BS то size := 256*256*BS всё;	(* max 64K * 512B blocks *)
							Machine.TranslateVirtual(адресОт(data[ofs]), size, n, phys);
							i := 0; size := 0;
							нцПока (i < n) и (phys[0].adr + size = phys[i].adr) делай	(* find contiguous memory range *)
								увел(size, phys[i].size(Machine.Address32)); увел(i)
							кц;
							если size остОтДеленияНа BS = 0 то
								srb.BufPointer := адресОт(data[ofs])(Machine.Address32)	(* the virtual address *)
							иначе
								AcquireBuffer(copyAdr);
								size := BS; srb.BufPointer := copyAdr;
								если op = Disks.Write то
									НИЗКОУР.копируйПамять(адресОт(data[ofs]), copyAdr, size)
								всё
							всё;
							srb.BufLen := size;
							n := size DIV BS;	(* n is number of blocks to transfer now *)
							утв((n > 0) и (n < 10000H));
							srb.HaId := ha; srb.Flags := {ASPI.FlagsDirIn}; srb.Target := target; srb.Lun := lun;
							srb.SenseLen := 0X;
							srb.CDBLen := 0AX;
							просей op из
								Disks.Read: srb.CDB[0] := 28X
								|Disks.Write: srb.CDB[0] := 2AX
							всё;
							srb.CDB[1] := логСдвиг(lun, 5);
							srb.CDB[2] := симв8ИзКода(block DIV 1000000H); srb.CDB[3] := симв8ИзКода((block DIV 10000H) остОтДеленияНа 100H);
							srb.CDB[4] := симв8ИзКода((block DIV 100H) остОтДеленияНа 100H); srb.CDB[5] := симв8ИзКода(block остОтДеленияНа 100H);
							srb.CDB[6] := 0X;
							srb.CDB[7] := симв8ИзКода(n DIV 100H); srb.CDB[8] := симв8ИзКода(n остОтДеленияНа 100H);
							srb.CDB[9] := 0X;
							srb.meas := НУЛЬ;
							srb.Status := 55X;
							если TraceVerbose то
								ЛогЯдра.ЗахватВЕдиноличноеПользование;
								ЛогЯдра.пСтроку8("SRB: "); ЛогЯдра.пЦел64(op, 1); ЛогЯдра.пСимв8(" ");
								ЛогЯдра.пЦел64(block, 1); ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(n, 1);
								ЛогЯдра.п16ричное(srb.BufPointer, 9); ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(srb.BufLen, 1);
								ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
							всё;
							ASPI.SendASPICommand(srb, истина);
							утв(srb.Status = ASPI.SSComp);
							если copyAdr # 0 то
								если op = Disks.Read то
									НИЗКОУР.копируйПамять(copyAdr, адресОт(data[ofs]), size);
									i := 0; нцПока (i < size) и (НИЗКОУР.прочти32битаПоАдресу(copyAdr+i) # 0DEADDEADH) делай увел(i, 4) кц;
									если i < size то
										ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("DEAD"); ЛогЯдра.пВК_ПС;
										ЛогЯдра.пБлокПамяти16рично(copyAdr, size); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
									всё
								всё;
								ReleaseBuffer(copyAdr)
							всё;
							увел(block, n); умень(num, n); увел(ofs, size)
						кцПри num <= 0;
						DisposeSRB(srb);
						res := Disks.Ok
					иначе
						если num = 0 то res := Disks.Ok иначе res := 4001 (* blocks out of range *) всё
					всё
				иначе
					res := Disks.Unsupported
				всё;
(*
				IF Trace & (op = Disks.Read) THEN
					KernelLog.Enter;
					KernelLog.String("Read "); KernelLog.Int(res, 1); KernelLog.Ln;
					KernelLog.Memory(orgadr, orgsize);
					KernelLog.Exit
				END
*)
			кон Transfer;

			проц {перекрыта}GetSize* (перем size: цел32; перем res: целМЗ);
			нач
				size := capacity; res := Disks.Ok;
				если TraceVerbose то
					ЛогЯдра.ЗахватВЕдиноличноеПользование;
					ЛогЯдра.пСтроку8("NCR GetSize "); ЛогЯдра.пЦел64(size, 1); ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(res, 1);
					ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё
			кон GetSize;

		кон Device;

перем
	fallbackBufAdr: цел32;
	fallbackBuf: укль на массив из симв8;
	Ncopies: цел32;
	freeSRB: ASPI.ExecIOCmdSRB;

	проц AcquireBuffer(перем adr: цел32);
	нач {единолично}
		увел(Ncopies);
		дождись(fallbackBufAdr # 0);
		adr := fallbackBufAdr;
		fallbackBufAdr := 0
	кон AcquireBuffer;

	проц ReleaseBuffer(adr: цел32);
	нач {единолично}
		fallbackBufAdr := adr
	кон ReleaseBuffer;

	проц NewSRB(перем srb: ASPI.ExecIOCmdSRB);
	нач {единолично}
		если freeSRB = НУЛЬ то
			нов(srb)
		иначе
			srb := freeSRB; freeSRB := srb.next
		всё
	кон NewSRB;

	проц DisposeSRB(srb: ASPI.ExecIOCmdSRB);
	нач {единолично}
		srb.next := freeSRB; freeSRB := srb
	кон DisposeSRB;

(* ---- Get disk capacity and block size ---- *)

	проц GetCapacity(ha, target, lun: симв8; перем capacity, blocksize: цел32);
	перем srb: ASPI.ExecIOCmdSRB; buf: массив 8 из симв8; i: цел32;
	нач
		нов(srb);
		srb.HaId := ha; srb.Flags := {ASPI.FlagsDirIn}; srb.Target := target; srb.Lun := lun;
		srb.BufLen := 8;
		srb.BufPointer := адресОт(buf[0])(Machine.Address32);
		srb.SenseLen := 0X;
		srb.CDBLen := 0AX;
		srb.CDB[0] := 25X; srb.CDB[1] := логСдвиг(lun, 5); srb.CDB[2] := 0X; srb.CDB[3] := 0X;
		srb.CDB[4] := 0X; srb.CDB[5] := 0X; srb.CDB[6] := 0X; srb.CDB[7] := 0X; srb.CDB[8] := 0X; srb.CDB[9] := 0X;
		srb.meas := НУЛЬ;
		srb.Status := 55X;
		ASPI.SendASPICommand(srb, истина);
		утв(srb.Status = ASPI.SSComp);
		capacity := 0; blocksize := 0;
		нцДля i := 0 до 3 делай
			capacity := capacity*100H + кодСимв8(buf[i]);
			blocksize := blocksize*100H + кодСимв8(buf[i+4])
		кц
	кон GetCapacity;

	проц Init;
	перем res, ha, targ, lun, num: цел32; regres: целМЗ; srb: ASPI.GetDevTypeSRB; dev: Device; name: Plugins.Name;
	нач
		freeSRB := НУЛЬ;
		нов(fallbackBuf, 2*BS);
		fallbackBufAdr := адресОт(fallbackBuf[0])(Machine.Address32);
		res := PageSize - fallbackBufAdr остОтДеленияНа PageSize;
		если res < BS то увел(fallbackBufAdr, res) всё;
		num := 0;
		res := ASPI.GetASPISupportInfo();
		если симв8ИзКода(логСдвиг(res, -8)) = ASPI.SSComp то
			res := res остОтДеленияНа 100H;
			нов(srb); ha := 0;
			нцПока ha < res делай
				targ := 0;
				нцПока targ < 7 делай
					lun := 0; srb.Status := ASPI.SSComp;
					нцПока (lun < 8) и (srb.Status = ASPI.SSComp) делай
						(*KernelLog.String("Init: ha:"); LogInt(ha); KernelLog.String("  target:"); LogInt(targ);
						KernelLog.String("  lun:"); LogInt(lun); KernelLog.Ln;*)
						srb.HaId := симв8ИзКода(ha); srb.Flags := {};
						srb.Target := симв8ИзКода(targ); srb.Lun := симв8ИзКода(lun);
						ASPI.SendASPICommand(srb, ложь);
						(*KernelLog.String("DevType: "); LogInt(ORD(srb.DevType)); KernelLog.Ln;*)
						если (srb.Status = ASPI.SSComp) и (srb.DevType = 0X) и (num < 10) то (* only support direct access devices *)
							нов(dev);
							name := "Symbios0";  name[7] := симв8ИзКода(48 + num); увел(num);
							dev.SetName(name);
							dev.flags := {};
							(*IF RemovableBit IN dev.id.type THEN INCL(dev.flags, Disks.Removable) END;*)
							копируйСтрокуДо0(dev.name, dev.desc);
							dev.ha := симв8ИзКода(ha); dev.target := симв8ИзКода(targ);
							dev.lun := симв8ИзКода(lun);
							dev.writePreComp := 0;
							GetCapacity(dev.ha, dev.target, dev.lun, dev.capacity, dev.blockSize);
							Disks.registry.Add(dev, regres);
							утв(regres = Plugins.Ok)
						всё;
						увел(lun)
					кц;
					увел(targ)
				кц;
				увел(ha)
			кц
		иначе
			ЛогЯдра.пСтроку8("SymbiosDisk: no host adapter found"); ЛогЯдра.пВК_ПС
		всё
	кон Init;

	(** Install - Install the driver in the Disk module. *)
	(** The install command has no effect, as all disk devices are installed when the module is loaded. *)

	проц Install*;
	кон Install;

нач
	Init
кон SymbiosDisks.

ASPI.Stop
SCSIDisk.ShowPartitions
System.Free SCSIDisk ASPI ~

		xferPhysAdr: ARRAY MaxPRD OF Machine.Range;
	Machine.TranslateVirtual(bufAdr, num*BS, n, c.xferPhysAdr); ASSERT(n > 0);
