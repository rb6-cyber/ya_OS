модуль SVGMatrix;

использует Math;

конст
	Eps = 1.0E-5;

тип
	Number*=вещ64;

	Point*=запись
		x*, y* : Number;
	кон;

	Matrix*=окласс
		перем a*, b*, c*, d*, e*, f*: Number;

		(*
		[a c e]
		[b d f]
		[0 0 1]
		*)

		проц SetIdentity*;
		нач
			a := 1.0; c := 0.0; e := 0.0;
			b := 0.0; d := 1.0; f := 0.0;
		кон SetIdentity;

		проц Set*(newa, newb, newc, newd, newe, newf: Number);
		нач
			a := newa; c := newc; e := newe;
			b := newb; d := newd; f := newf;
		кон Set;


		проц TransformBy*(othera, otherb, otherc, otherd, othere, otherf: Number):Matrix;
		перем other: Matrix;
		нач
			нов(other);
			other.Set(othera, otherb, otherc, otherd, othere, otherf);
			возврат Multiply(other)
		кон TransformBy;

		проц Translate*(x, y: Number):Matrix;
		перем other: Matrix;
		нач
			нов(other);
			other.Set(1.0, 0.0, 0.0, 1.0, x, y);
			возврат Multiply(other)
		кон Translate;

		проц Scale*(x, y: Number):Matrix;
		перем other: Matrix;
		нач
			нов(other);
			other.Set(x, 0.0, 0.0, y, 0.0, 0.0);
			возврат Multiply(other)
		кон Scale;

		проц Rotate*(angle, x, y: Number):Matrix;
		перем other: Matrix;
			s, c: Number;
		нач
			s := Math.sin(устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
			c := Math.cos(устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
			нов(other);
			other.Set(c, s, -s, c, -c*x+s*y+x, -s*x-c*y+y);
			возврат Multiply(other)
		кон Rotate;

		проц SkewX*(angle: Number):Matrix;
		перем other: Matrix;
			s, c: Number;
		нач
			s := Math.sin(устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
			c := Math.cos(устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
			нов(other);
			other.Set(1.0, 0.0, s / c, 1.0, 0.0, 0.0);
			возврат Multiply(other)
		кон SkewX;

		проц SkewY*(angle: Number):Matrix;
		перем other: Matrix;
			s, c: Number;
		нач
			s := Math.sin(устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
			c := Math.cos(устарПреобразуйКБолееУзкомуЦел(angle)/180.0*Math.pi);
			нов(other);
			other.Set(1.0, s / c, 0.0, 1.0, 0.0, 0.0);
			возврат Multiply(other)
		кон SkewY;

		проц Multiply*(other: Matrix):Matrix;
		перем result: Matrix;
		нач
			нов(result);
			result.a := a * other.a + c * other.b;
			result.b := b * other.a + d * other.b;
			result.c := a * other.c + c * other.d;
			result.d := b * other.c + d * other.d;
			result.e := a * other.e + c * other.f + e;
			result.f := b * other.e + d * other.f + f;
			возврат result
		кон Multiply;

		проц Invert*():Matrix;
		перем result: Matrix;
			det, inv: Number;
		нач
			нов(result);

			det := a * d - b * c;
			если матМодуль(det) >= Eps то	(* matrix can be inverted; use Cramer's rule *)
				inv := 1/det;
				result.a := +inv * d;
				result.b := -inv * b;
				result.c := -inv * c;
				result.d := +inv * a;
				result.e := +inv * (c * f - d * e);
				result.f := +inv * (b * e - a * f)
			иначе
				result.Set(0, 0, 0, 0, 0, 0)
			всё;
			возврат result
		кон Invert;

		проц Transform*(перем p: Point):Point;
		перем result: Point;
		нач
			result.x := p.x * a + p.y * c + e;
			result.y := p.x * b + p.y * d + f;
			возврат result
		кон Transform;

		проц TransformLength*(перем p: Number):Number;
		перем x, y: Number;
		нач
			x := p * a; y := p * b;
			возврат Math.sqrt(устарПреобразуйКБолееУзкомуЦел(x * x + y * y))
		кон TransformLength;

	кон Matrix;

кон SVGMatrix.
