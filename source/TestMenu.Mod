модуль TestMenu; (** AUTHOR "TF/staubesv" PURPOSE "Testbed for WMMenus.Mod" *)

использует
	ЛогЯдра, Потоки,
	Modules, Commands,Strings, Files, XML, XMLObjects, XMLScanner, XMLParser,
	WMGraphics, WMMessages, WM := WMWindowManager,
	WMComponents, WMStandardComponents, WMTrees, WMMenus;

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс (WMComponents.FormWindow)
	перем
		menu : WMTrees.Tree;
		menuPanel : WMMenus.MenuPanel;
		hasErrors : булево;

		проц CreateForm() : WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			root : WMTrees.TreeNode;
		нач
			нов(panel);
			panel.bounds.SetExtents(800, 700);
			panel.fillColor.Set(цел32(0FFFFFFFFH));
			panel.takesFocus.Set(истина);

			нов(menu); нов(root);

			нов(menuPanel);
			menuPanel.fillColor.Set(цел32(WMGraphics.White));
			menuPanel.bounds.SetHeight(20);
			menuPanel.alignment.Set(WMComponents.AlignTop);
			menuPanel.horizontal.Set(истина);
			menuPanel.openDirection.Set(WMMenus.OpenDownRight);
			menuPanel.SetMenu(menu, root);
			menuPanel.onSelect.Add(Selected);
			panel.AddContent(menuPanel);

			возврат panel
		кон CreateForm;

		проц &New*;
		перем vc : WMComponents.VisualComponent;
		нач
			IncCount;
			(* To create a multi language app, try loading the respective XML instead of CreateForm()
			if the XML was not found or does not contain all needed elements, use CreateForm as fallback *)
			vc := CreateForm();

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);

			 WM.DefaultAddWindow(сам);
			SetTitle(Strings.NewString("Test Window"));
		кон New;

		проц AddMenuItem(node : WMTrees.TreeNode; xml : XML.Element);
		перем newNode : WMTrees.TreeNode;
		нач
			нов(newNode);
			menu.AddChildNode(node, newNode);
			menu.SetNodeData(newNode, xml);
			menu.SetNodeCaption(newNode, xml.GetAttributeValue("caption"));
		кон AddMenuItem;

		проц Selected(sender, data : динамическиТипизированныйУкль);
		перем s : Strings.String;
		нач
			если ~sequencer.IsCallFromSequencer() то
				sequencer.ScheduleEvent(сам.Selected, sender, data)
			иначе
				если (data # НУЛЬ) и (data суть WMTrees.TreeNode) то
					menu.Acquire;
					s := menu.GetNodeCaption(data(WMTrees.TreeNode));
					если s # НУЛЬ то ЛогЯдра.пСтроку8(s^); ЛогЯдра.пВК_ПС; всё;
					menu.Release;
				всё
			всё
		кон Selected;

		проц AddSubMenu(node : WMTrees.TreeNode; xml : XML.Element );
		перем en : XMLObjects.Enumerator;
			p : динамическиТипизированныйУкль; s : Strings.String;
			newNode : WMTrees.TreeNode;
		нач
			нов(newNode);
			menu.AddChildNode(node, newNode);
			menu.SetNodeData(newNode, xml);
			menu.SetNodeCaption(newNode, xml.GetAttributeValue("caption"));

			en := xml.GetContents();
			нцПока en.HasMoreElements() делай
				p := en.GetNext();
				если p суть XML.Element то
					s := p(XML.Element).GetName();
					если s # НУЛЬ то
						если s^ = "MenuItem" то AddMenuItem(newNode, p(XML.Element))
						аесли s^ = "SubMenu" то AddSubMenu(newNode, p(XML.Element))
						всё
					всё
				всё
			кц;
		кон AddSubMenu;

		проц SetDocument(xml : XML.Element);
		перем en : XMLObjects.Enumerator;
			p : динамическиТипизированныйУкль; s : Strings.String;
			node : WMTrees.TreeNode;
		нач
			нов(node);
			menu.Acquire;
			menu.SetRoot(node);
			menu.SetNodeState(node, {WMTrees.NodeAlwaysExpanded});
			menu.SetNodeData(node, xml);
			en := xml.GetContents();
			нцПока en.HasMoreElements() делай
				p := en.GetNext();
				если p суть XML.Element то
					s := p(XML.Element).GetName();
					если s # НУЛЬ то
						если s^ = "SubMenu" то AddSubMenu(node, p(XML.Element))
						аесли s^ = "MenuItem" то AddMenuItem(node, p(XML.Element))
						всё
					всё
				всё
			кц;
			menu.Release;
			menuPanel.SetMenu(menu, node)
		кон SetDocument;

		проц Error(pos, line, row : Потоки.ТипМестоВПотоке; конст msg : массив из симв8);
		нач
			ЛогЯдра.пСтроку8("Parse error at pos "); ЛогЯдра.пЦел64(pos, 5); ЛогЯдра.пСтроку8(" in line "); ЛогЯдра.пЦел64(line, 5);
			ЛогЯдра.пСтроку8(" row "); ЛогЯдра.пЦел64(row, 5); ЛогЯдра.пСтроку8(" - "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
			hasErrors := истина
		кон Error;

		проц Read(конст name : массив из симв8);
		перем f : Files.File;
			r : Files.Reader;
			scanner : XMLScanner.Scanner;
			parser : XMLParser.Parser;
			doc : XML.Document;
		нач
			hasErrors := ложь;
			f := Files.Old(name);
			если f # НУЛЬ то
				Files.OpenReader(r, f, 0);
				нов(scanner, r); scanner.reportError := Error;
				нов(parser, scanner); parser.reportError := Error;
				doc := parser.Parse();
				если hasErrors то ЛогЯдра.пСтроку8("menu not loaded"); ЛогЯдра.пВК_ПС
				иначе SetDocument(doc.GetRoot());
				всё
			иначе
				ЛогЯдра.пСтроку8("name = "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" not found"); ЛогЯдра.пВК_ПС
			всё
		кон Read;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

тип

	Command = окласс
	перем
		commandString : массив 256 из симв8;

		проц &Init(конст commandString : массив из симв8);
		нач
			копируйСтрокуДо0(commandString, сам.commandString);
		кон Init;

		проц Execute;
		перем ignoreRes : целМЗ; ignoreMsg : массив 1 из симв8;
		нач
			Commands.Call(commandString, {}, ignoreRes, ignoreMsg);
		кон Execute;

	кон Command;

перем
	nofWindows : цел32;
	lastMenu : WMTrees.Tree; (* not thread-safe *)

проц Open*;
перем winstance : Window;
нач
	нов(winstance);
	winstance.Read("Menu.XML");
кон Open;

проц HandleItemSelected(sender, data : динамическиТипизированныйУкль);
перем caption : Strings.String; menu : WMTrees.Tree;
нач
	если (data # НУЛЬ) то
		если (data суть Command) то
			data(Command).Execute;
		иначе
			menu := lastMenu;
			если (menu # НУЛЬ) то
				caption := WMMenus.GetCaption(data, menu);
				ЛогЯдра.пСтроку8("Selected node: ");
				если (caption # НУЛЬ) то
					ЛогЯдра.пСтроку8(caption^);
				иначе
					ЛогЯдра.пСтроку8("NIL");
				всё;
				ЛогЯдра.пВК_ПС;
			иначе
				ЛогЯдра.пСтроку8("Test error: Menu not available"); ЛогЯдра.пВК_ПС;
			всё;
		всё;
	всё;
кон HandleItemSelected;

проц OpenPopup*(context : Commands.Context);
перем
	path, commandString : массив 256 из симв8;
	menu : WMTrees.Tree; node : WMTrees.TreeNode;
	command : Command;
нач
	нов(menu); lastMenu := menu;
	нцПока context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(path) делай
		если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(commandString) то
			нов(command, commandString);
			node := WMMenus.AddItemNode(path, menu);
			menu.Acquire;
			menu.SetNodeData(node, command);
			menu.Release;
		всё;
	кц;
	WMMenus.Show(menu, 100, 100, HandleItemSelected);
кон OpenPopup;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup)
кон TestMenu.

System.Free TestMenu WMMenus ~
TestMenu.Open ~

TestMenu.OpenPopup
	Inspect.Performance WMPerfMon.Open
	Inspect.Profiler WMProfiler.Open
	Inspect.Events WMEvents.Open
	Inspect.Components WMInspector.Open
	--- NoCommand
	Tools.Search ГПИ_поискИ_заменаВ_файлах.Явись
	Tools.Archiver WMArchives.Open
	Tools.Console WMShell.Open
	"Tools.Partition Manager" WMPartitions.Open
	"Tools.Partition Editor" WMPartitionEditor.Open
	--- NoCommand
	Commands.Files.ShowFS FSTools.Watch
	Commands.Files.Directory "FSTools.Directory *"
	Commands.Partitions.Show Partitions.Show
~~
