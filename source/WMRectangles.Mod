модуль WMRectangles; (** AUTHOR "TF"; PURPOSE "Basic rectangles that are used by the WM and visual components"; *)

тип
	Rectangle* = запись l*, t*, r*, b*: размерМЗ кон;
		(* It is important to understand and respect that the point (r,b) is not included in the rectangle !
			This is to ensure consistency between the continuous and discrete case
		*)

	(** move the rectangle by deltaX, deltaY *)
	проц MoveRel*(перем x:Rectangle; deltaX, deltaY:размерМЗ);
	нач
		x.l:=x.l+deltaX; x.t:=x.t+deltaY; x.r:=x.r+deltaX; x.b:=x.b+deltaY
	кон MoveRel;

	проц Bound(перем x:размерМЗ; min, max:размерМЗ);
	нач
		если x<min то x:=min иначе если x>max то x:=max всё всё
	кон Bound;

	(** Clip rectangle rect at the boundary bounds *)
	проц ClipRect*(перем rect, bounds : Rectangle);
	нач
		Bound(rect.l, bounds.l, bounds.r);Bound(rect.r, bounds.l, bounds.r);
		Bound(rect.t, bounds.t, bounds.b);Bound(rect.b, bounds.t, bounds.b)
	кон ClipRect;

	(** return true if rectangle x is empty *)
	проц RectEmpty*(x : Rectangle) : булево;
	нач
		возврат (x.t >= x.b) или (x.l >= x.r)
	кон RectEmpty;

	(** return true if x and y are inside rect *)
	проц PointInRect*(x, y : размерМЗ; rect : Rectangle) : булево;
	нач
		возврат (x >= rect.l) и (x < rect.r) и (y >= rect.t) и (y < rect.b)
	кон PointInRect;

	(** return true if the inner rectangle is completely inside the outer *)
	проц IsContained*(перем outer, inner : Rectangle) : булево;
	нач
		возврат (outer.l <= inner.l) и (outer.r >= inner.r) и
					 (outer.t <= inner.t) и (outer.b >= inner.b)
	кон IsContained;

	проц IsEqual*(конст a, b : Rectangle) : булево;
	нач
		возврат (a.l = b.l) и (a.r = b.r) и (a.t = b.t) и (a.b = b.b)
	кон IsEqual;

	(** return whether a and be intersect and not only touche *)
	проц Intersect*(перем a, b : Rectangle) : булево;
	нач
		возврат (a.l < b.r) и (a.r > b.l) и (a.t < b.b) и (a.b > b.t)
	кон Intersect;

	(** Set rect to (l, t, r, b) *)
	проц SetRect*(перем rect : Rectangle; l, t, r, b : размерМЗ);
	нач
		rect.l := l; rect.t := t; rect.r := r; rect.b := b
	кон SetRect;

	(** return the area of r. Overflow if r w * h > MAX(SIZE) *)
	проц Area*(перем r : Rectangle) : размерМЗ;
	нач
		возврат (r.r - r.l) * (r.b - r.t)
	кон Area;

	(** Extend old to contain addition *)
	проц ExtendRect*(перем old, addition : Rectangle);
	нач
		old.l := матМинимум(old.l, addition.l); old.r := матМаксимум(old.r,addition.r);
		old.t := матМинимум(old.t, addition.t); old.b := матМаксимум(old.b, addition.b)
	кон ExtendRect;

	(** return the Rectangle (l, t, r, b) *)
	проц MakeRect*(l, t, r, b: размерМЗ) : Rectangle;
	перем result : Rectangle;
	нач
		result.l := l; result.t := t; result.r := r; result.b := b; возврат result
	кон MakeRect;

	(** extend the rectangle by units in each direction *)
	проц ResizeRect*(x : Rectangle; units : размерМЗ) : Rectangle;
	перем t : Rectangle;
	нач
		t.l := x.l - units; t.t := x.t - units; t.r := x.r + units; t.b := x.b + units;
		возврат t
	кон ResizeRect;

	проц Normalize*(перем rect: Rectangle);
	перем
		temp: размерМЗ;
	нач
		если rect.l > rect.r то
			temp := rect.l;
			rect.l := rect.r;
			rect.r := temp;
		всё;

		если rect.t > rect.b то
			temp := rect.t;
			rect.t := rect.b;
			rect.b := temp;
		всё;
	кон Normalize;

кон WMRectangles.
