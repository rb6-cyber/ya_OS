модуль ComponentViewer;	(** AUTHOR "TF"; PURPOSE "Testbed for the component system"; *)

использует
	Modules, Commands, Options, XML, Repositories, WMMessages, WMWindowManager, WMComponents,
	WMRestorable, Потоки, D:= Debugging, Files, WMGraphicsSmooth;

конст
	DefaultWidth = 320;
	DefaultHeight = 240;
	InvalidPosition* =матМинимум(цел32);
	FlagMoveable* = 20;
	FlagSmoothGraphics* = 21;

тип

	KillerMsg = окласс
	кон KillerMsg;

	Window* = окласс(WMComponents.FormWindow)
	перем dragging: булево; lastX, lastY: размерМЗ;

		проц RestoreWindow*(c : WMRestorable.Context);
		нач
			ReInit(c.r-c.l, c.b-c.t);
			(*
			Init(c.r - c.l, c.b - c.t, FALSE);
			*)
			если c.appData # НУЛЬ то
				DisableUpdate;
				LoadComponents(c.appData(XML.Element));
				EnableUpdate;
			всё;
			WMRestorable.AddByContext(сам, c);
			Resized(c.r-c.l,c.b-c.t);
		кон RestoreWindow;

		проц &InitWindow(width, height : размерМЗ; alpha : булево);
		нач
			IncCount;
			Init(width, height, alpha);
		кон InitWindow;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		проц {перекрыта}Handle*(перем m : WMMessages.Message);
		перем data: XML.Element;
		нач
			если (m.msgType = WMMessages.MsgExt) и (m.ext # НУЛЬ) то
				если  (m.ext суть KillerMsg) то Close
				аесли (m.ext суть WMRestorable.Storage) то
					data := StoreComponents();
					m.ext(WMRestorable.Storage).Add("ComponentViewer", "ComponentViewer.Restore", сам, data)
				иначе Handle^(m);
				всё;
			иначе Handle^(m);
			всё;
		кон Handle;

		проц {перекрыта}PointerDown*(x, y:размерМЗ; keys:мнвоНаБитахМЗ);
		нач
			lastX := bounds.l + x; lastY:=bounds.t + y;
			если (keys = {0}) и (FlagMoveable в flags) то
				dragging := истина;
				PointerDown^(x,y,keys);
			иначе
				PointerDown^(x,y,keys);
			всё;
		кон PointerDown;

		проц {перекрыта}PointerMove*(x,y:размерМЗ; keys:мнвоНаБитахМЗ);
		перем dx, dy : размерМЗ;
		нач
			если dragging то
				x := bounds.l + x; y := bounds.t + y; dx := x - lastX; dy := y - lastY;
				lastX := lastX + dx; lastY := lastY + dy;
				если (dx # 0) или (dy # 0) то
					manager.SetWindowPos(сам, bounds.l + dx, bounds.t + dy);
				всё;
			всё;
		кон PointerMove;

		проц {перекрыта}PointerUp*(x, y:размерМЗ; keys:мнвоНаБитахМЗ);
		нач
			dragging := ложь;
			PointerDown^(x,y,keys);
		кон PointerUp;

	кон Window;

перем
	nofWindows : размерМЗ;

проц DoShow*( vc: WMComponents.VisualComponent; перем window: Window; x,y,width, height: размерМЗ; client, alpha, fullscreen: булево;flags: мнвоНаБитахМЗ);
перем
	fx,fy,fw,fh: размерМЗ;
	viewPort: WMWindowManager.ViewPort;
	manager: WMWindowManager.WindowManager;
нач
	если vc = НУЛЬ то возврат всё;
	если width # 0 то
		vc.bounds.SetWidth(width);
	иначе
		width := vc.bounds.GetWidth();
		если (width <= 0) то width := DefaultWidth; vc.bounds.SetWidth(width) всё;
	всё;
	если height # 0 то
		vc.bounds.SetHeight(height);
	иначе
		height := vc.bounds.GetHeight();
		если (height <= 0) то height := DefaultHeight; vc.bounds.SetHeight(height) всё;
	всё;
	если client то vc.alignment.Set(WMComponents.AlignClient) всё;
	если fullscreen то
		viewPort := WMWindowManager.GetDefaultView();
		fx := 0; fy := 0; fw := 1; fh := 1; (* full screen on screen number 4 *)
		x := fx * viewPort.width0;
		y := fy * viewPort.height0;
		width := fw* viewPort.width0;
		height := fh * viewPort.height0;
	всё;

	если window = НУЛЬ то
		нов(window, width, height, alpha);
		если FlagSmoothGraphics в flags то
			window.SetCanvasGenerator(WMGraphicsSmooth.GenCanvas);
		всё;
		window.SetTitle(vc.GetName());
		window.SetContent(vc);
		window.flags := window.flags + flags;
		manager := WMWindowManager.GetDefaultManager();
		если (x = InvalidPosition) или (y = InvalidPosition) то
			WMWindowManager.GetNextPosition(window, manager, WMWindowManager.GetDefaultView(),x,y);
		аесли fullscreen то
			x := 0; y := 0
		всё;
		manager := WMWindowManager.GetDefaultManager();
		если vc.sequencer # НУЛЬ то vc.sequencer.WaitFree() всё;
		manager.Add(x, y, window, flags);
	иначе
		window.SetContent(vc);
	всё;

кон DoShow;

проц DoLoad*(конст filename: массив из симв8; error: Потоки.Писарь): WMComponents.VisualComponent;
перем
	repositoryName, componentName : массив 128 из симв8;
	moduleName, procedureName : Modules.Name;
	ignoreMsg : массив 1 из симв8;
	generatorProc : XML.GeneratorProcedure;
	c : XML.Content; component : Repositories.Component;
	id: размерМЗ; res: целМЗ;
нач
	если Repositories.SplitName(filename, repositoryName, componentName, id) и (repositoryName # "") то
		(* Retrieve component from repository *)
		Repositories.GetComponentByString(filename, component, res);
		если (res = Repositories.Ok) то
			c := component;
		аесли error # НУЛЬ то
			error.пСтроку8("Could not load "); error.пСтроку8(filename);
			error.пСтроку8(" from repository, res: "); error.пЦел64(res, 0); error.пВК_ПС;
		всё;
	иначе
		Commands.Split(filename, moduleName, procedureName, res, ignoreMsg);
		если (res = Commands.Ok) то
			(* Assume argument is a generator procedure *)
			дайПроцПоИмени(moduleName, procedureName, generatorProc);
			если (generatorProc # НУЛЬ) то
				c := generatorProc();
			иначе
				(* Maybe argument is a filename *)
				c := WMComponents.Load(filename);
			всё;
		иначе
			(* Load component from XML file *)
			c := WMComponents.Load(filename);
		всё;
	всё;
	если ( c # НУЛЬ ) и (c суть WMComponents.VisualComponent) то возврат c(WMComponents.VisualComponent) иначе возврат НУЛЬ всё;
кон DoLoad;

проц DoOpen*(конст filename: массив из симв8; error: Потоки.Писарь; x,y,width, height: размерМЗ; client, alpha, fullscreen: булево; flags:мнвоНаБитахМЗ): WMComponents.VisualComponent;
перем
	window : Window;
	c : WMComponents.VisualComponent;
нач
	c := DoLoad(filename, error);

	если (c # НУЛЬ)  то
		DoShow(c(WMComponents.VisualComponent), window, x,y,width,height, client, alpha, fullscreen, flags);
	аесли error # НУЛЬ то
		если (c = НУЛЬ) то error.пСтроку8("Could not load/generate component "); error.пСтроку8(filename);
		иначе error.пСтроку8(filename); error.пСтроку8(" is not a VisualComponent.");
		всё;
		error.пВК_ПС;
	всё;
	если (c # НУЛЬ) и (c суть WMComponents.VisualComponent) то возврат c(WMComponents.VisualComponent)
	иначе возврат НУЛЬ
	всё
кон DoOpen;


проц SetProperties(c:WMComponents.Component; конст attr: массив из симв8);
перем property, value: массив 32 из симв8;
перем r: Потоки.ЧтецИзСтроки;
нач
	нов(r, длинаМассива(attr));
	r.ПримиСтроку8ДляЧтения(attr);
	нцПока r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(property) и r.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(value) делай
		если ~c.properties.SetPropertyValue(property, value) то всё;
	кц;
кон SetProperties;


проц Open*(context : Commands.Context); (** [Options] <RepositoryName:ComponentName:ID> | <ModuleName.ProcedureName> | <Filename> ~ *)
перем
	options : Options.Options;
	filename : массив 128 из симв8;
	x,y, width, height: цел32;
	flags: мнвоНаБитахМЗ;
	c: WMComponents.Component;
	properties: массив 256 из симв8;
нач
	нов(options);
	options.Add("x", "xPosition", Options.Integer);
	options.Add("y", "yPosition", Options.Integer);
	options.Add("h", "height", Options.Integer);
	options.Add("w", "width", Options.Integer);
	options.Add("c", "client", Options.Flag);
	options.Add("a","alpha", Options.Flag);
	options.Add("f","fullscreen", Options.Flag);
	options.Add("n","noFocus", Options.Flag);
	options.Add("t","onTop", Options.Flag);
	options.Add("F","noFrame",Options.Flag);
	options.Add("m","moveable",Options.Flag);
	options.Add("s","smoothGraphics",Options.Flag);
	options.Add("p","properties",Options.String);
	если options.Parse(context.arg, context.error) и context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то
		если ~options.GetInteger("width",width) то width := 0 всё;
		если ~options.GetInteger("height",height) то height := 0 всё;
		если ~options.GetInteger("x",x) то x := InvalidPosition всё;
		если ~options.GetInteger("y",y) то y := InvalidPosition всё;
		если options.GetFlag("fullscreen") то flags := {} иначе flags := {WMWindowManager.FlagFrame, WMWindowManager.FlagClose, WMWindowManager.FlagMinimize} всё;
		если options.GetFlag("noFrame") то flags := {} иначе flags := {WMWindowManager.FlagFrame, WMWindowManager.FlagClose, WMWindowManager.FlagMinimize} всё;
		если options.GetFlag("moveable") то flags := {FlagMoveable} иначе flags := {WMWindowManager.FlagFrame, WMWindowManager.FlagClose, WMWindowManager.FlagMinimize} всё;
		если options.GetFlag("noFocus") то включиВоМнвоНаБитах(flags, WMWindowManager.FlagNoFocus) всё;
		если options.GetFlag("onTop") то включиВоМнвоНаБитах(flags, WMWindowManager.FlagStayOnTop) всё;
		если options.GetFlag("smoothGraphics") то включиВоМнвоНаБитах(flags,FlagSmoothGraphics); всё;
		c := DoOpen(filename, context.error, x , y, width, height, options.GetFlag("client"), options.GetFlag("alpha"), options.GetFlag("fullscreen"), flags);
		если options.GetString("properties",properties) и (c # НУЛЬ) то SetProperties(c,properties) всё;
	иначе
		context.error.пСтроку8("Usage: ComponentViewer.Open [Options] <string> ~"); context.error.пВК_ПС;
	всё;
кон Open;

проц Store*(context: Commands.Context);
перем
	filename, name, ext, formName : массив 256 из симв8;
	form: WMComponents.Component;
	id: размерМЗ; res: целМЗ;
	originator: WMComponents.Component;
	parent: XML.Element;
нач{единолично}
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename); D.String(filename); D.Ln;

	если (context # НУЛЬ) и (context суть WMComponents.EventContext) то
		originator := context(WMComponents.EventContext).originator;
		parent := originator.GetParent();
		нцПока (parent # НУЛЬ) и (parent суть WMComponents.Component) и ~(parent суть WMComponents.Form) делай
			originator := parent(WMComponents.Component);
			parent := originator.GetParent();
		кц;
	всё;
	form := originator;
	(*form := GetForm(current);*)
	если (form # НУЛЬ) и (filename # "") то
		Repositories.CreateRepository(filename,res);
		утв(res = Repositories.Ok);
		Files.SplitExtension(filename, name, ext);
		id:= 1;
		копируйСтрокуДо0(form.GetName()^,formName);
		Repositories.PutComponent(form,name,form.GetName()^,id,res);
		утв(res = Repositories.Ok);
		Repositories.StoreRepository(name,res);
		утв(res = Repositories.Ok);
		Repositories.UnloadRepository(name,res);
		утв(res = Repositories.Ok);
		context.out.пСтроку8("stored component in repository "); context.out.пСтроку8(filename); context.out.пВК_ПС;
	всё;
выходя
кон Store;


проц Restore*(context : WMRestorable.Context);
перем w : Window;
нач
	если context # НУЛЬ то
		нов(w, 100,100,ложь);
		w.RestoreWindow(context);
	всё;
кон Restore;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем
	die : KillerMsg;
	msg : WMMessages.Message;
	m : WMWindowManager.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WMWindowManager.GetDefaultManager();
	m.Broadcast(msg);
	(*AWAIT(nofWindows = 0)*)
кон Cleanup;

нач
	nofWindows := 0;
	Modules.InstallTermHandler(Cleanup)
кон ComponentViewer.

System.FreeDownTo  ComponentViewer ~
ComponentViewer.Open FractalDemo.XML ~

ComponentViewer.Open ComponentHelper:Panel:1 ~
ComponentViewer.Open --moveable --width=100 --height=100 WMStandardComponents.GenButton  ~




1) Create a window containing an ImagePanel or an Image (both variants work, but are slighly different in term of zooming behaviour)
ComponentViewer.Open WMStandardComponents.GenImagePanel ~
or
ComponentViewer.Open WMStandardComponents.GenImage ~

2) open an image into the window:
variant a: Open the ComponentViewer, and set the "ImagePanel:Form:ImagePanel" extended property "Image" to "Yellow_Duck.jpg"~
variant b: Open the ComponentViewer, and set the "ImagePanel:Form:Image" extended property "ImageName" to "Yellow_Duck.jpg"~
This can also be done more directly by using WMComponents.
variant c: Use the ComponentInspector to drag an image from somewhere to somewher
variang d: drag an image name to a Image window created above (not implemented yet)
