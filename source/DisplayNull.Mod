(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль DisplayNull; (** AUTHOR "pjm"; PURPOSE "Null display driver"; *)

использует Displays, ЛогЯдра;

конст
	Trace = истина;

тип
	Display* = окласс (Displays.Display)

		проц &Init*;
		нач
			width := 1024; height := 768; offscreen := 0;
			format := Displays.color565; unit := 10000
		кон Init;

		проц {перекрыта}Transfer*(перем buf: массив из симв8; ofs, stride, x, y, w, h: размерМЗ; op: целМЗ);
		кон Transfer;

		проц {перекрыта}Fill*(col, x, y, w, h: цел32);
		кон Fill;

		проц {перекрыта}Dot*(col, x, y: цел32);
		кон Dot;

		проц {перекрыта}Mask*(перем buf: массив из симв8; bitofs, stride, fg, bg, x, y, w, h: цел32);
		кон Mask;

		проц {перекрыта}Copy*(sx, sy, w, h, dx, dy: цел32);
		кон Copy;

	кон Display;

проц Install*;
перем d: Display; res: целМЗ;
нач
	нов(d); d.desc := "Null display driver";
	Displays.registry.Add(d, res);
	утв(res = 0);
	если Trace то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Null display driver"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё
кон Install;

нач
	Install
кон DisplayNull.
