модуль WMStringGrids; (** AUTHOR "TF"; PURPOSE "String grid  component"; *)

использует
	Objects, Strings, XML, WMComponents, WMGraphics, WMGraphicUtilities,
	WMProperties, WMEvents, WMRectangles, WMGrids;

конст
	(* Cell.flags *)
	UsePerCellColors = 0;
	UseInternalBuffer = 1;

тип
	String = Strings.String;

	Cell* = окласс
	перем
		caption : String;
		color, textColor : WMGraphics.Color;
		align: целМЗ;
		img : WMGraphics.Image;
		data : динамическиТипизированныйУкль;
		flags : мнвоНаБитахМЗ;

		проц &Init;
		нач
			caption := НУЛЬ;
			color := 0; textColor := 0; align := 0;
			img := НУЛЬ;
			data := НУЛЬ;
			flags := {};
		кон Init;

	кон Cell;

	CellArray = укль на массив из Cell;

	Row = укль на запись
		cells : CellArray;
	кон;

	RowArray = укль на массив из Row;

тип

	StringGridModel* = окласс
	перем
		lockedBy : динамическиТипизированныйУкль;
		lockLevel : размерМЗ;
		viewChanged : булево;
		onChanged* : WMEvents.EventSource; (** does not hold the lock, if called *)
		rows : RowArray;
		nofRows, nofCols : размерМЗ;

		проц &Init*;
		нач
			нов(onChanged, сам, WMComponents.NewString("TreeModelChanged"), НУЛЬ, НУЛЬ);
			нов(rows, 4);
			lockLevel :=0;
		кон Init;

		(** acquire a read/write lock on the object *)
		проц Acquire*;
		перем me : динамическиТипизированныйУкль;
		нач {единолично}
			me := Objects.ActiveObject();
			если lockedBy = me то
				утв(lockLevel # -1);	(* overflow *)
				увел(lockLevel)
			иначе
				дождись(lockedBy = НУЛЬ); viewChanged := ложь;
				lockedBy := me; lockLevel := 1
			всё
		кон Acquire;

		(** release the read/write lock on the object *)
		проц Release*;
		перем hasChanged : булево;
		нач
			нач {единолично}
				утв(lockedBy = Objects.ActiveObject(), 3000);
				hasChanged := ложь;
				умень(lockLevel);
				если lockLevel = 0 то lockedBy := НУЛЬ; hasChanged := viewChanged всё
			кон;
			если hasChanged то onChanged.Call(НУЛЬ) всё
		кон Release;

		проц AdjustRows(newSize : размерМЗ);
		перем i : размерМЗ; newRows : RowArray;
		нач
			нов(newRows, newSize);
			нцДля i := 0 до матМинимум(nofRows, newSize) - 1 делай
				newRows[i] := rows[i]
			кц;
			нцДля i := матМинимум(nofRows, newSize)  до newSize - 1 делай
				нов(newRows[i]);
				AdjustRow(newRows[i])
			кц;
			rows := newRows
		кон AdjustRows;

		проц AdjustRow(row : Row);
		перем i : размерМЗ; newCells : CellArray;
		нач
			если row.cells = НУЛЬ то нов(row.cells, nofCols) всё;
			если длинаМассива(row.cells) # nofCols то
				нов(newCells, nofCols);
				нцДля i := 0 до матМинимум(nofCols, длинаМассива(row.cells)) - 1 делай
					newCells[i] := row.cells[i]
				кц;
				row.cells := newCells
			всё
		кон AdjustRow;

		проц SetNofRows*(newNofRows : размерМЗ);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (newNofRows > nofRows) или (newNofRows < nofRows DIV 2)  то AdjustRows(newNofRows) всё;
			nofRows := newNofRows;
			viewChanged := истина
		кон SetNofRows;

		проц SetNofCols*(newNofCols : размерМЗ);
		перем i : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			nofCols := newNofCols;
			нцДля i := 0 до nofRows - 1 делай AdjustRow(rows[i]) кц;
			viewChanged := истина
		кон SetNofCols;

		проц GetNofRows*() : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			возврат nofRows
		кон GetNofRows;

		проц GetNofCols*() : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			возврат nofCols
		кон GetNofCols;

		проц SetCellText*(col, row : размерМЗ; caption : String);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то нов(rows[row].cells[col]) всё;
				исключиИзМнваНаБитах(rows[row].cells[col].flags, UseInternalBuffer);
				если rows[row].cells[col].caption # caption то
					rows[row].cells[col].caption := caption;
					viewChanged := истина
				всё
			всё
		кон SetCellText;

		проц GetCellText*(col, row : размерМЗ ) : String;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то возврат НУЛЬ всё;
				возврат rows[row].cells[col].caption
			иначе возврат НУЛЬ
			всё
		кон GetCellText;

		проц SetCellTextAOC*(col, row, minBufferSize : размерМЗ; конст caption : массив из симв8);
		перем cell : Cell; length : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то нов(rows[row].cells[col]) всё;
				length := матМаксимум(minBufferSize, Strings.Length(caption) + 1); (* 0X *)
				cell := rows[row].cells[col];
				если (cell.caption = НУЛЬ) или ~(UseInternalBuffer в cell.flags) или (длинаМассива(cell.caption) < length) то
					нов(cell.caption, length);
					включиВоМнвоНаБитах(cell.flags, UseInternalBuffer);
				всё;
				если (cell.caption^ # caption) то
					копируйСтрокуДо0(caption, rows[row].cells[col].caption^);
					viewChanged := истина
				всё
			всё
		кон SetCellTextAOC;

		проц GetCellTextAOC*(col, row : размерМЗ; перем caption : массив из симв8);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] # НУЛЬ то
					копируйСтрокуДо0(rows[row].cells[col].caption^, caption);
				иначе
					caption := "";
				всё;
			иначе
				caption := "";
			всё
		кон GetCellTextAOC;

		проц SetCellColors*(col, row : размерМЗ; color, textColor : WMGraphics.Color);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то нов(rows[row].cells[col]) всё;
				включиВоМнвоНаБитах(rows[row].cells[col].flags, UsePerCellColors);
				если rows[row].cells[col].color # color то
					rows[row].cells[col].color := color;
					viewChanged := истина;
				всё;
				если rows[row].cells[col].textColor # textColor то
					rows[row].cells[col].textColor := textColor;
					viewChanged := истина;
				всё;
			всё;
		кон SetCellColors;

		проц GetCellColors*(col, row : размерМЗ; перем color, textColor : WMGraphics.Color; перем valid : булево);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			valid := ложь;
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если (rows[row].cells[col] # НУЛЬ) и (UsePerCellColors в rows[row].cells[col].flags) то
					valid := истина;
					color := rows[row].cells[col].color;
					textColor := rows[row].cells[col].textColor;
				всё;
			всё;
		кон GetCellColors;

		проц SetCellData*(col, row : размерМЗ; data : динамическиТипизированныйУкль);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то нов(rows[row].cells[col]) всё;
				если rows[row].cells[col].data # data то
					rows[row].cells[col].data:= data;
					viewChanged := истина
				всё
			всё
		кон SetCellData;

		проц GetCellData*(col, row : размерМЗ) : динамическиТипизированныйУкль;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то возврат НУЛЬ всё;
				возврат rows[row].cells[col].data
			иначе возврат НУЛЬ
			всё
		кон GetCellData;

		проц SetCellImage*(col, row : размерМЗ; img : WMGraphics.Image);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то нов(rows[row].cells[col]) всё;
				если rows[row].cells[col].img # img то
					rows[row].cells[col].img := img;
					viewChanged := истина
				всё
			всё
		кон SetCellImage;

		проц GetCellImage*(col, row : размерМЗ) : WMGraphics.Image;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то возврат НУЛЬ всё;
				возврат rows[row].cells[col].img
			иначе возврат НУЛЬ
			всё
		кон GetCellImage;

		проц SetTextAlign*(col, row : размерМЗ; align : целМЗ);
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то нов(rows[row].cells[col]) всё;
				если rows[row].cells[col].align # align то
					rows[row].cells[col].align:= align;
					viewChanged := истина
				всё
			всё
		кон SetTextAlign;

		проц GetTextAlign*(col, row : размерМЗ) : целМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (col >= 0) и (row >= 0) и (col < nofCols) и (row < nofRows) то
				если rows[row].cells[col] = НУЛЬ то возврат 0 всё;
				возврат rows[row].cells[col].align
			иначе возврат 0
			всё
		кон GetTextAlign;

		проц DeleteRow*(rowNo : размерМЗ; viewChanged : булево);
		перем i : размерМЗ;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (rowNo >=  0) и (rowNo < nofRows) то
				нцДля i := rowNo до nofRows - 2 делай
					rows[i] := rows[i + 1]
				кц;
				умень(nofRows);
				сам.viewChanged := viewChanged
			всё
		кон DeleteRow;

		проц InsertEmptyRow*(atRowNo : размерМЗ);
		перем i : размерМЗ;
			newRows : RowArray;
		нач
			утв(Objects.ActiveObject() = lockedBy, 3000);
			если (atRowNo >= 0) и (atRowNo <= nofRows) то
				нов(newRows, nofRows + 1);
				нцДля i := 0 до atRowNo - 1 делай
					newRows[i] := rows[i]
				кц;
				нов(newRows[atRowNo]);
				AdjustRow(newRows[atRowNo]);
				нцДля i := atRowNo + 1 до nofRows делай
					newRows[i] := rows[i - 1]
				кц
			всё;
			увел(nofRows);
			rows := newRows;
			viewChanged := истина
		кон InsertEmptyRow;

	кон StringGridModel;

тип

	StringGrid* = окласс(WMGrids.GenericGrid)
	перем
		model- : StringGridModel;
		cellColor, hoverColor, selectedColor, fixedColor, textHoverColor, textColor, textSelectedColor : WMGraphics.Color;
		clCell-, clFixed-, clHover-, clSelected-, clTextDefault-, clTextHover-, clTextSelected- : WMProperties.ColorProperty;

		проц &{перекрыта}Init*;
		нач
			Init^;
			SetNameAsString(StrStringGrid);
			SetGenerator("WMStringGrids.GenStringGrid");
			нов(clCell, PrototypeTclCell, НУЛЬ, НУЛЬ); properties.Add(clCell);
			нов(clHover, PrototypeTclHover, НУЛЬ, НУЛЬ); properties.Add(clHover);
			нов(clSelected, PrototypeTclSelected, НУЛЬ, НУЛЬ); properties.Add(clSelected);
			нов(clFixed, PrototypeTclFixed, НУЛЬ, НУЛЬ); properties.Add(clFixed);

			нов(clTextDefault, PrototypeTclTextDefault, НУЛЬ, НУЛЬ); properties.Add(clTextDefault);
			нов(clTextHover, PrototypeTclTextHover, НУЛЬ, НУЛЬ); properties.Add(clTextHover);
			нов(clTextSelected, PrototypeTclTextSelected,  НУЛЬ, НУЛЬ); properties.Add(clTextSelected);
		(*	NEW(fontHeight, PrototypeTfontHeight, NIL, NIL); properties.Add(fontHeight);	*)
			takesFocus.Set(истина);
			нов(model);
			model.onChanged.Add(ModelChanged);
			ModelChanged(НУЛЬ,НУЛЬ);
		кон Init;

		проц ModelChanged(sender, data : динамическиТипизированныйУкль);
		нач
			Acquire;
			nofCols.Set(model.nofCols);
			nofRows.Set(model.nofRows);
			Invalidate;
			SetDrawCellProc(DrawCell);
			Release
		кон ModelChanged;

		проц {перекрыта}DrawBackground*(canvas : WMGraphics.Canvas);
		нач
			cellColor := clCell.Get();
			hoverColor := clHover.Get();
			selectedColor := clSelected.Get();
			fixedColor := clFixed.Get();
			textColor := clTextDefault.Get();
			textHoverColor := clTextHover.Get();
			textSelectedColor := clTextSelected.Get();
			model.Acquire;
			DrawBackground^(canvas);
			model.Release
		кон DrawBackground;

		проц {перекрыта}GetCellData*(col, row : размерМЗ) : динамическиТипизированныйУкль;
		перем data : динамическиТипизированныйУкль;
		нач
			model.Acquire;
			data := model.GetCellData(col, row);
			model.Release;
			возврат data
		кон GetCellData;

(*		PROCEDURE CellClicked*(col, row : SIZE); (** PROTECTED *)
		BEGIN
			model.Acquire;
			data := model.GetCellData(col, row);
			model.Release;
			CellClicked^(col, row);
	(*		onClick.Call(data);
			IF wasSelected  & onClickSelected.HasListeners() THEN
				onClickSelected.Call(data)
			END; *)
		END CellClicked; *)

		проц DrawCell(canvas : WMGraphics.Canvas; w, h : размерМЗ; state : мнвоНаБитахМЗ; x, y : размерМЗ);
		перем
			s : String; font : WMGraphics.Font; left: размерМЗ; c, tc: WMGraphics.Color; img : WMGraphics.Image; dispW, dispH: размерМЗ;
			valid : булево;
		нач
			s := model.GetCellText(x, y);

			model.GetCellColors(x, y, c, tc, valid);
			если ~valid то
				c := cellColor;
				tc := textColor;
			всё;

			если WMGrids.CellFixed в state то
				c := fixedColor;
				если WMGrids.CellSelected в state то
					c := WMGraphicUtilities.InterpolateColorLinear(c, selectedColor, 128)
				аесли  WMGrids.CellHighlighted в state то
					c := WMGraphicUtilities.InterpolateColorLinear(c, hoverColor, 128)
				всё;
				canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), c, WMGraphics.ModeCopy)
			аесли WMGrids.CellSelected в state то
				canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), selectedColor, WMGraphics.ModeSrcOverDst);
				tc := textSelectedColor
			аесли WMGrids.CellHighlighted в state то
				canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), hoverColor, WMGraphics.ModeSrcOverDst);
				tc := textHoverColor
			иначе
				canvas.Fill(WMRectangles.MakeRect(0, 0, w, h), c, WMGraphics.ModeSrcOverDst)
			всё;

			font := GetFont();
			canvas.SetColor(tc);
			left := 1; img := model.GetCellImage(x, y);
			если img # НУЛЬ то увел(left, img.width + 1) всё;

			если s # НУЛЬ то
				если img # НУЛЬ то
					dispW := img.width;
					dispH := img.height;
					если dispW > w-2 то dispW := w-2 всё;
					если dispH > h-2 то dispH := h-2 всё;
					если (dispW # img.width) или (dispH # img.height) то
						canvas.ScaleImage(img, WMRectangles.MakeRect(0, 0, img.width, img.height),	WMRectangles.MakeRect(1, 1, dispW, dispH), WMGraphics.ModeSrcOverDst, 10);
					иначе
						canvas.DrawImage(1, 1, img, WMGraphics.ModeSrcOverDst);
					всё
				всё;
				WMGraphics.DrawStringInRect(canvas, WMRectangles.MakeRect(left, 1, w - 2, h - 2), ложь,
					model.GetTextAlign(x, y), WMGraphics.AlignCenter, s^)
			всё;
			если WMGrids.CellSelected в state то
				WMGraphicUtilities.ExtRectGlassShade(canvas, WMRectangles.MakeRect(0, 0, w, h), {1, 3}, 5, ложь);
			всё
			(* IF s # NIL THEN canvas.DrawString(0, h - font.descent, s^) END *)
		кон DrawCell;

	кон StringGrid;

перем
	 PrototypeTclCell*, PrototypeTclHover*, PrototypeTclSelected*, PrototypeTclTextDefault*,
	 PrototypeTclTextHover*, PrototypeTclTextSelected*, PrototypeTclFixed* : WMProperties.ColorProperty;
	 PrototypeTfontHeight* : WMProperties.Int32Property;

	 StrStringGrid : Strings.String;

проц GenStringGrid*() : XML.Element;
перем stringGrid : StringGrid;
нач
	нов(stringGrid); возврат stringGrid;
кон GenStringGrid;

проц InitStrings;
нач
	StrStringGrid := Strings.NewString("StringGrid");
кон InitStrings;

проц InitPrototypes;
перем plStringGrid : WMProperties.PropertyList;
нач
	нов(plStringGrid);
	нов(PrototypeTclCell, НУЛЬ, Strings.NewString("ClCell"), Strings.NewString("color of the cell"));
	plStringGrid.Add(PrototypeTclCell);
	нов(PrototypeTclFixed, НУЛЬ, Strings.NewString("ClFixed"), Strings.NewString("color of a fixed cell"));
	plStringGrid.Add(PrototypeTclFixed);
	нов(PrototypeTclHover, НУЛЬ, Strings.NewString("ClHover"), Strings.NewString("color of the tree item, if the mouse is over it"));
	plStringGrid.Add(PrototypeTclHover);
	нов(PrototypeTclSelected, НУЛЬ, Strings.NewString("ClSelected"), Strings.NewString("color of the the tree item, if it is selected"));
	plStringGrid.Add(PrototypeTclSelected);
	нов(PrototypeTclTextDefault, НУЛЬ, Strings.NewString("ClTextDefault"), Strings.NewString("default text color of the tree item"));
	plStringGrid.Add(PrototypeTclTextDefault);
	нов(PrototypeTclTextHover, НУЛЬ, Strings.NewString("ClTextHover"), Strings.NewString("text color of the tree item, if the mouse is over it"));
	plStringGrid.Add(PrototypeTclTextHover);
	нов(PrototypeTclTextSelected, НУЛЬ, Strings.NewString("ClTextSelected"), Strings.NewString("text color of the tree item, when selected"));
	plStringGrid.Add(PrototypeTclTextSelected);
	нов(PrototypeTfontHeight, НУЛЬ, Strings.NewString("FontHeight"), Strings.NewString("height of the tree item text"));
	plStringGrid.Add(PrototypeTfontHeight);
	PrototypeTclCell.Set(цел32(0FFFFFFFFH));
	PrototypeTclFixed.Set(цел32(0CCCCCCFFH));
	PrototypeTclHover.Set(цел32(0FFFF00FFH));
	PrototypeTclSelected.Set(00000FFFFH);
	PrototypeTclTextDefault.Set(0000000FFH);
	PrototypeTclTextHover.Set(00000FFFFH);
	PrototypeTclTextSelected.Set(цел32(0FFFFFFFFH));

	PrototypeTfontHeight.Set(12);
	WMComponents.propertyListList.Add("StringGrid", plStringGrid);
	WMComponents.propertyListList.UpdateStyle;
кон InitPrototypes;

нач
	InitStrings;
	InitPrototypes;
кон WMStringGrids.

System.Free WMStringGrids ~

