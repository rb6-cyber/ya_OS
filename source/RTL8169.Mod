модуль RTL8169; (** AUTHOR "Roger Keller"; PURPOSE "Driver for RealTek RTL8169 Ethernet Controllers"; *)
(*
Reference: RealTek, "RealTek Gigabit Ethernet Media Access Controller
	with Power Management RTL8169S/RTL8110S Registers"
Revision 1.0, March 2003
*)
использует
	НИЗКОУР, Kernel, Machine, PCI,
	Objects, Modules, Plugins, Network, ЛогЯдра;

конст
	Name = "RTL8169#";
	Description = "RealTek 8169 Gigabit ethernet driver";

	MaxETHFrameSize = 1514;
	TxMaxSize = 1600; (* Max size of tx buffers *)
	RxMaxSize = 600H; (* Max size for received frames = 1536 bytes *)
	RxRingSize = 1024; (* Rx Ring of 116 buffers *)
	TxRingSize = 1024; (* Tx Ring of 116 buffers *)

	SizeOfRxTxFDHdr = 16; (* size of Rx / Tx Descriptor Header *)

	InterruptMask = {0, 1, 2, 3, 4, 5, 6, 7, 15}; (* interrupts to handle *)
	Promisc = ложь; (* enable Promiscuous mode *)

	UnknownHW = 0;
	RTL8169 = 1;
	RTL8169S = 2;

	DebugFind = 0;
	DebugInit = 1;
	DebugConfigs = 2;
	DebugHWVer = 3;
	DebugMAC = 4;
	DebugStatus = 5;
	DebugRxRing = 6;
	DebugTxRing = 7;
	DebugReceive = 8;
	DebugTransmit = 9;
	DebugInterrupt = 10;
	DebugCleanup = 31;
	Debug = {DebugFind, DebugInit, DebugTxRing, DebugCleanup};

перем
	installed: цел32; (* number of installed devices *)

тип
	(* base Rx/Tx descriptor, as described in RTL8169 specs *)
	RxTxDescriptor = запись
		flags: мнвоНаБитахМЗ;
		vLanTag: цел32;
		bufAdrLo, bufAdrHi: цел32;
	кон;

	(* buffer for transmission *)
	TxBuffer = укль на запись
		data: массив TxMaxSize из симв8;
		next: TxBuffer;
	кон;

	(* wrapper for Network.Buffer to be able to form rings *)
	RxBuffer = укль на запись
		buf: Network.Buffer;
		next: RxBuffer;
	кон;

	(* LinkDevice: interface to Bluebottle *)
	LinkDevice = окласс (Network.LinkDevice)
		перем
			ctrl: Controller;
			hw: цел32;

		проц {перекрыта}Linked*(): цел32;
		нач
			возврат ctrl.linkStatus;
		кон Linked;

		проц {перекрыта}DoSend*(dst: Network.LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8;  h3len, h4len, dofs, dlen: цел32);
		нач
			ctrl.SendFrame(dst, type, l3hdr, l4hdr, data, h3len, h4len, dofs, dlen);
		кон DoSend;

		проц {перекрыта}Finalize(connected: булево);
		нач
			ctrl.Finalize;
			Finalize^(connected);
		кон Finalize;
	кон LinkDevice;

	(* Controller: interface to the RTL8169 hardware *)
	Controller = окласс
		перем
			next: Controller; (* next controller in list *)
			base: адресВПамяти; irq: цел32;
			dev: LinkDevice;
			rds: массив RxRingSize из RxTxDescriptor;
			tds: массив TxRingSize из RxTxDescriptor;
			curRD, curTD: цел32;
			firstRD, firstTD: цел32;
			lastRD, lastTD: цел32;
			(*rxBuffer, rxLast: TxBuffer;*)
			rxBuffer, rxLast: RxBuffer;
			txBuffer, txLast: TxBuffer;

			nofFreeTx: цел32; (* number of free tx descriptors *)

			nRxOverflow: цел64;
			nTxOverflow: цел64;
			nRxFrames, nTxFrames: цел32;
			nRxErrorFrames: цел32;
			nTxErrorFrames: цел32;

			linkStatus: цел32;

		проц &Init*(dev: LinkDevice; base: адресВПамяти; irq: цел32);
		перем
			res: целМЗ; i: цел32;
			s: мнвоНаБитахМЗ;
		нач
			(* update list of installed controllers, insert at head *)
			сам.next := installedControllers;
			installedControllers := сам;

			сам.base := base;
			сам.dev := dev;
			сам.irq := irq;
			dev.ctrl := сам;
			nRxOverflow := 0;
			nTxOverflow := 0;
			nRxFrames := 0;
			nTxFrames := 0;
			nRxErrorFrames := 0;
			nTxErrorFrames := 0;

			(* tell the system that the nic calculates the checksums for tcp, udp and ip packets *)
			dev.calcChecksum := {Network.ChecksumIP, Network.ChecksumTCP, Network.ChecksumUDP};

			(* set ethernet broadcast address: FF-FF-FF-FF-FF-FF *)
			нцДля i := 0 до 5 делай
				dev.broadcast[i] := 0FFX
			кц;

			(* make sure PIO and MMIO are enabled*)
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(52H));
			если ~ (2 в s) то
				ЛогЯдра.пСтроку8("I/O Mapping is disabled!");
				СТОП(1000);
			всё;
			если ~ (3 в s) то
				ЛогЯдра.пСтроку8("MMIO is disabled!");
				СТОП(1000);
			всё;

			(* find out if we're on 1GBps *)
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(6CH));
			(* if not, try to enable 1GBps ... *)
			если ~ (4 в s) то
				(* reset the hardware and enable 1000baseTx *)
				HwReset;
				EnableTBI;
			всё;

			(* read and store MAC address *)
			ReadMACAddress;

			(* find out hardware version *)
			GetHardwareVersion;

			(* soft reset the chip *)
			ResetNIC;

			(* enable Tx and Rx *)
			EnableTxRx(истина, истина);

			(* set Max Transmit Packet Size (MTPS):
			register counts in 32 byte units -> 32H * 32 bytes = 1600 bytes *)
			Write8(0ECH, 32H);

			(* set Receive Packet Maximum Size (RMS) *)
			Write16(0DAH, RxMaxSize);

			(* set Tx config register:
			let the nic compute CRCs of frames in Tx *)
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read32(40H));
			s := (s  - {17..18, 19}) + {8..10, 25};
			Write32(40H, НИЗКОУР.подмениТипЗначения(цел32, s));

			(* config c+ command register:
			PCI multiple read/write enable;
			Receive Checksum Offload enable *)
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read16(0E0H));
			s := s + {3, 5};
			Write16(0E0H, НИЗКОУР.подмениТипЗначения(цел32, s));

			(* setup Tx (normal priority) ring *)
			res := SetupTxRing();
			(* set Transmit Normal Priority Descriptor Start Address (TNPDS) *)
			Write32(20H, res);
			Write32(20H + 4, 0);

			(* setup Rx ring *)
			res := SetupRxRing();
			(* set Receive Descriptor Start Address (RDSAR) *)
			Write32(0E4H, res);
			Write32(0E4H + 4, 0);

			(* reset Rx missed packet counter *)
			Write32(04CH, 0);

			(* configure receiver *)
			ConfigRx;

			(* install interrupt handler *)
			если (irq >= 1) и (irq <= 15) то
				Objects.InstallHandler(сам.HandleInterrupt, Machine.IRQ0 + irq)
			всё;

			(* enable interrupts *)
			Write16(3CH, НИЗКОУР.подмениТипЗначения(цел32, InterruptMask));

			UpdateLinkStatus;

			(* register device with Network *)
			Network.registry.Add(dev, res);
			утв(res = Plugins.Ok);
			увел(installed);

			если DebugConfigs в Debug то
				DebugConfig;
			всё;
		кон Init;

		проц SendFrame(dst: Network.LinkAdr; type: цел32; конст l3hdr, l4hdr, data: массив из симв8;  h3len, h4len, dofs, dlen: цел32);
		перем
			txLen, offset, type4: цел32;
			bufBase: адресВПамяти;
			chksums: мнвоНаБитахМЗ;
		нач {единолично}
			если nofFreeTx <= 0 то
				ЛогЯдра.пСтроку8("no tx buffers"); ЛогЯдра.пВК_ПС;
				увел(nTxOverflow);
			всё;
			дождись(nofFreeTx > 0);

			txLen := 14 + h3len + h4len + dlen;
			bufBase := адресОт(txBuffer.data);

			(* generate ethernet frame: setup eth header, move data *)
			(* set destination mac address (first 6 bytes of eth frame) *)
			НИЗКОУР.копируйПамять(адресОт(dst[0]), bufBase, 6);
			(* set source mac address (6 bytes @ offset 6 of eth frame) *)
			НИЗКОУР.копируйПамять(адресОт(dev.local[0]), bufBase + 6, 6);
			(* set upper layer type, bring type from host to network byte order *)
			НИЗКОУР.запиши16битПоАдресу(bufBase + 12, вращБит(НИЗКОУР.подмениТипЗначения(цел16, устарПреобразуйКБолееУзкомуЦел(type)), 8));

			offset := 14; (* eth header has 14 bytes *)
			(* move layer 3 and layer 4 headers, data *)
			если h3len > 0 то
				НИЗКОУР.копируйПамять(адресОт(l3hdr[0]), bufBase + offset, h3len);
				увел(offset, h3len);
			всё;
			если h4len > 0 то
				НИЗКОУР.копируйПамять(адресОт(l4hdr[0]), bufBase + offset, h4len);
				увел(offset, h4len);
			всё;
			если offset + dlen < MaxETHFrameSize то
				НИЗКОУР.копируйПамять(адресОт(data[0]) + dofs, bufBase + offset, dlen);
				увел(offset, dlen);
			всё;

			(* make the frame at least 64 bytes long *)
			нцПока offset < 60 делай
				txBuffer.data[offset] := симв8ИзКода(0);
				увел(offset);
				увел(txLen);
			кц;

			если DebugTransmit в Debug то
				ЛогЯдра.пСтроку8("Sending frame of length ");
				ЛогЯдра.пЦел64(txLen, 0);
				ЛогЯдра.пВК_ПС;
				(*KernelLog.Memory(bufBase, txLen);*)
			всё;

			(* find out which protocols are used;
			let the NIC calc the checksums for IP, TCP and UCP headers *)
			chksums := {};
			если type = 0800H то
				включиВоМнвоНаБитах(chksums, 18); (* offload IP checksum *)
				type4 := НИЗКОУР.подмениТипЗначения(цел8, l3hdr[9]); (* get type if IP data *)
				если type4 = 6 то (* TCP/IP *)
					включиВоМнвоНаБитах(chksums, 16); (* offload TCP checksum *)
				аесли type4 = 17 то
					включиВоМнвоНаБитах(chksums, 17); (* offload UDP checksum *)
				всё;
			всё;

			(* update Tx Descriptor:
			set OWN=1, FS=1, LS=1, checksum offloads;
			set size of packet to be transmitted *)
			tds[curTD].flags := tds[curTD].flags * {30}; (* only keep EOR bit *)
			tds[curTD].flags := tds[curTD].flags + {31, 29, 28} + chksums;
			tds[curTD].flags := tds[curTD].flags + (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, txLen) * {0..15});

			(* move to next Tx Descriptor, Tx Buffer *)
			увел(curTD);
			если curTD = TxRingSize то
				curTD := firstTD;
			всё;
			txBuffer := txBuffer.next;

			умень(nofFreeTx);

			(* tell the nic that there's some eth frame waiting to be transmitted (set NPQ=1) *)
			Write8(38H, НИЗКОУР.подмениТипЗначения(цел32, {6}));
		кон SendFrame;

		проц ConfigRx;
		перем s: мнвоНаБитахМЗ;
		нач
			(* set Rx config register:
			let the nic check CRCs of frames in Rx;
			accept broadcast, multicast, phys match, all packets with dest addr (IFF Promiscuos mode enabled)
			set no Rx FIFO threshold, set unlimited DMA burst size*)
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read32(44H));
			s := (s * {7, 11..12, 17..31}) + {1..3, 9, 14..15, 16};
			если Promisc то
				включиВоМнвоНаБитах(s, 0);
			всё;
			Write32(44H, НИЗКОУР.подмениТипЗначения(цел32, s));

			(* set multicast filter: receive everything *)
			Write32(08H, цел32(0FFFFFFFFH));
			Write32(08H + 4, цел32(0FFFFFFFFH));
		кон ConfigRx;

		проц SetTimer(val: цел32);
		нач
			Write32(58H, val);
		кон SetTimer;

		проц GetHardwareVersion;
		перем s: мнвоНаБитахМЗ;
		нач
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read32(40H));
			s := s * {23, 26..30};

			если DebugHWVer в Debug то
				ЛогЯдра.пСтроку8("Hardware Version: ");
			всё;
			если s = {} то
				dev.hw := RTL8169;
				если DebugHWVer в Debug то
					ЛогЯдра.пСтроку8("RTL8169");
				всё;
			аесли ((s * {23, 26}) # {}) и ((s * {27..30}) = {}) то
				dev.hw := RTL8169S;
				если DebugHWVer в Debug то
					ЛогЯдра.пСтроку8("RTL8169S/RTL8110S");
				всё;
			иначе
				dev.hw := UnknownHW;
				если DebugHWVer в Debug то
					ЛогЯдра.пСтроку8("Hardware Version is unknown");
				всё;
			всё;
			если DebugHWVer в Debug то
				ЛогЯдра.пВК_ПС;
			всё;
		кон GetHardwareVersion;

		проц ResetNIC;
		перем s: мнвоНаБитахМЗ;
		нач
			Write8(37H, НИЗКОУР.подмениТипЗначения(цел32, {4}));

			(* wait until reset has finished *)
			нцДо
				Delay(10);
				s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(37H));
			кцПри ~(4 в s);
		кон ResetNIC;

		проц EnableTxRx(tx, rx: булево);
		перем s: мнвоНаБитахМЗ;
		нач
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(37H));
			s := s * {0..1, 5..7};
			если tx то
				включиВоМнвоНаБитах(s, 2);
			всё;
			если rx то
				включиВоМнвоНаБитах(s, 3);
			всё;
			Write8(37H, НИЗКОУР.подмениТипЗначения(цел32, s));
		кон EnableTxRx;

		проц ReadMACAddress;
		перем
			i: цел16;
			res: целМЗ;
		нач
			(* MAC address is in registers 00H - 05H *)
			если DebugMAC в Debug то
				ЛогЯдра.пСтроку8("MAC address is: ");
			всё;
			нцДля i := 0 до 5 делай
				res := Read8(i);
				НИЗКОУР.запиши8битПоАдресу(адресОт(dev.local[i]), res);
				если DebugMAC в Debug то
					если i > 0 то
						ЛогЯдра.пСтроку8("-");
					всё;
					ЛогЯдра.п16ричное(кодСимв8(dev.local[i]), -2);
				всё;
			кц;
			если DebugMAC в Debug то
				ЛогЯдра.пВК_ПС;
			всё;
			dev.adrSize := 6;
		кон ReadMACAddress;

		проц Read8(reg: цел32): цел8;
		нач
			возврат НИЗКОУР.прочти8битПоАдресу(base + reg);
		кон Read8;

		проц Write8(reg: цел32; val: цел32);
		нач
			НИЗКОУР.запиши8битПоАдресу(base + reg, устарПреобразуйКБолееУзкомуЦел(устарПреобразуйКБолееУзкомуЦел(val)));
		кон Write8;

		проц Read16(reg: цел32): цел16;
		нач
			возврат НИЗКОУР.прочти16битПоАдресу(base + reg);
		кон Read16;

		проц Write16(reg: цел32; val: цел32);
		нач
			НИЗКОУР.запиши16битПоАдресу(base + reg, устарПреобразуйКБолееУзкомуЦел(val));
		кон Write16;

		проц Read32(reg: цел32): цел32;
		нач
			возврат НИЗКОУР.прочти32битаПоАдресу(base + reg);
		кон Read32;

		проц Write32(reg: цел32; val: цел32);
		нач
			НИЗКОУР.запиши32битаПоАдресу(base + reg, val);
		кон Write32;

		проц EnableTBI;
		перем
			s: мнвоНаБитахМЗ;
			i: цел32;
		нач
			если 7 в НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(6CH)) то возврат всё;

			s := PHYRead(04H) + {5..8};	(* advertise 10 full/half, 100 full/half *)
			PHYWrite(04H, s);
			PHYWrite(09H, {9}); (* advertise 1000 full *)

			(* enable and restart auto negotiation *)
			PHYWrite(00H, {9, 12});
			Delay(100);

			нцДля i := 1 до 1000 делай
				s := PHYRead(01H);
				если 5 в s то (* auto negotiation complete *)
					Delay(100);
					если DebugStatus в Debug то
						PrintStatus;
					всё;
					возврат;
				иначе
					Delay(100);
				всё;
			кц;
		кон EnableTBI;

		проц HwReset;
		перем
			s: мнвоНаБитахМЗ;
			i: цел32;
		нач
			s := PHYRead(00H) + {15};
			PHYWrite(00H, s);

			(* wait until reset has been completet *)
			нцДля i := 1 до 50 делай
				если ~(15 в PHYRead(00H)) то
					возврат;
				всё;
			кц;
		кон HwReset;

		проц PHYWrite(regAdr: цел32; data: мнвоНаБитахМЗ);
		перем
			s: мнвоНаБитахМЗ;
			i: цел32;
		нач
			s := {31};
			s := s + (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, regAdr * 010000H) * {16..20});
			s := s + (data * {0..15});
			Write32(60H, НИЗКОУР.подмениТипЗначения(цел32, s));
			Delay(100);

			(* wait until write has been completet *)
			нцДля i := 1 до 2000 делай
				если НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read32(60H)) * {31} = {} то
					возврат;
				всё;
				Delay(100);
			кц;
		кон PHYWrite;

		проц PHYRead(regAdr: цел32): мнвоНаБитахМЗ;
		перем
			s: мнвоНаБитахМЗ;
			i: цел32;
		нач
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, regAdr * 010000H) * {16..20};
			Write32(60H, НИЗКОУР.подмениТипЗначения(цел32, s));
			Delay(100);

			(* wait until read has been completed *)
			нцДля i := 1 до 2000 делай
				s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read32(60H));
				если 31 в s то
					возврат (s * {0..15});
				всё;
				Delay(100);
			кц;
			возврат {};
		кон PHYRead;

		проц AllocBuffer(перем buf: TxBuffer);
		нач
			нов(buf); (* edit: no more alignment necessary, since PTR TO RECORD is already 32 byte aligned *)
		кон AllocBuffer;

		проц SetupRxRing(): Machine.Address32;
		перем
			r: цел32;
			adr, physAdr: адресВПамяти;
			buf, prev: RxBuffer;
		нач
			(* make sure the descriptor ring is 256 byte aligned in physical memory *)
			adr := адресОт(rds[0]);
			adr := Machine.PhysicalAdr(adr, SizeOfRxTxFDHdr);
			если adr остОтДеленияНа 256 = 0 то
				firstRD := 0;
			иначе
				firstRD := 16 - (цел32 (adr остОтДеленияНа 256) DIV 16);
			всё;

			если DebugRxRing в Debug то
				ЛогЯдра.пСтроку8("Rx descriptor start = ");
				ЛогЯдра.п16ричное(adr, 8);
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("first Rx descriptor id = ");
				ЛогЯдра.пЦел64(firstRD, 0);
				ЛогЯдра.пВК_ПС;
			всё;

			нцДля r := firstRD до RxRingSize - 1 делай
				нов(buf);
				buf.buf := Network.GetNewBuffer();
				утв(buf.buf # НУЛЬ);

				adr := адресОт(buf.buf.data[0]);
				physAdr := Machine.PhysicalAdr(adr, Network.MaxPacketSize);
				утв(physAdr # Machine.NilAdr);

				rds[r].flags := {31};
				rds[r].flags := rds[r].flags + (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Network.MaxPacketSize) * {0..13});
				rds[r].vLanTag := 0;
				rds[r].bufAdrLo := physAdr(Machine.Address32);
				rds[r].bufAdrHi := 0;

				если prev # НУЛЬ то
					prev.next := buf;
				иначе
					(* set first Rx Buffer *)
					rxBuffer := buf;
				всё;
				prev := buf;
			кц;
			rxLast := buf;
			rxLast.next := rxBuffer;

			(* mark last descriptor as EOR (end of descriptor ring) *)
			включиВоМнвоНаБитах(rds[RxRingSize - 1].flags, 30);

			curRD := firstRD;
			adr := адресОт(rds[firstRD]);
			(* return physical address of first rx descriptor *)
			возврат Machine.PhysicalAdr(adr, SizeOfRxTxFDHdr)(Machine.Address32);
		кон SetupRxRing;

		проц SetupTxRing(): Machine.Address32;
		перем
			r: цел32;
			adr, physAdr: адресВПамяти;
			buf, prev: TxBuffer;
		нач
			(* make sure the descriptor ring is 256 byte aligned in physical memory *)
			adr := адресОт(tds[0]);
			adr := Machine.PhysicalAdr(adr, SizeOfRxTxFDHdr);
			если adr остОтДеленияНа 256 = 0 то
				firstTD := 0;
			иначе
				firstTD := 16 - (цел32 (adr остОтДеленияНа 256) DIV 16);
			всё;
			lastTD := firstTD;
			nofFreeTx := TxRingSize - firstTD;

			если DebugTxRing в Debug то
				ЛогЯдра.пСтроку8("Tx descriptor start = ");
				ЛогЯдра.п16ричное(adr, -8);
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("first Tx descriptor id = ");
				ЛогЯдра.пЦел64(firstTD, 0);
				ЛогЯдра.пВК_ПС;
				ЛогЯдра.пСтроку8("nofFreeTx = ");
				ЛогЯдра.пЦел64(nofFreeTx, 0);
				ЛогЯдра.пВК_ПС;
			всё;

			нцДля r := firstTD до TxRingSize - 1 делай
				AllocBuffer(buf);

				(* configure TFD *)
				adr := адресОт(buf.data[0]);
				physAdr := Machine.PhysicalAdr(adr, TxMaxSize);
				утв(physAdr # Machine.NilAdr);

				tds[r].flags := {};
				tds[r].vLanTag := 0;
				tds[r].bufAdrLo := physAdr(Machine.Address32);
				tds[r].bufAdrHi := 0;

				если prev # НУЛЬ то
					prev.next := buf;
				иначе
					(* set first Tx Buffer *)
					txBuffer := buf;
				всё;
				prev := buf;
			кц;
			txLast := buf;
			txLast.next := txBuffer;

			(* mark last descriptor as EOR (end of descriptor ring) *)
			включиВоМнвоНаБитах(tds[TxRingSize - 1].flags, 30);

			curTD := firstTD;
			adr := адресОт(tds[firstTD]);
			(* return physical address of first tx descriptor *)
			возврат Machine.PhysicalAdr(adr, SizeOfRxTxFDHdr)(Machine.Address32);
		кон SetupTxRing;

		проц ReadFrames;
		перем
			adr: адресВПамяти; type, size: цел32;
			dstAdr: Network.LinkAdr;
			buf: Network.Buffer;
			s: мнвоНаБитахМЗ;
		нач
			(* read all frames that are marked with OWN = 0*)
			нцПока ~(31 в rds[curRD].flags) делай
				(* skip error frames *)
				если (21 в rds[curRD].flags) то
					увел(nRxErrorFrames);
				аесли CheckChecksumErrors(rds[curRD]) то
					(* find out how many bytes have been received, including CRC *)
					size := НИЗКОУР.подмениТипЗначения(цел32, rds[curRD].flags * {0..13});

					если DebugReceive в Debug то
						ЛогЯдра.пСтроку8("Received a frame of length ");
						ЛогЯдра.пЦел64(size, 0);
						ЛогЯдра.пВК_ПС;
					всё;

					adr := адресОт(rxBuffer.buf.data[0]);
					(* copy destination and source addresses, type of packet *)
					dstAdr := НИЗКОУР.подмениТипЗначения(Network.LinkAdr, rxBuffer.buf.data[0]);
					rxBuffer.buf.src := НИЗКОУР.подмениТипЗначения(Network.LinkAdr, rxBuffer.buf.data[6]);
					type := Network.GetNet2(rxBuffer.buf.data, 12);

					buf := rxBuffer.buf;
					buf.ofs := 14;
					buf.len := size - 14;
					buf.calcChecksum := { Network.ChecksumIP, Network.ChecksumUDP, Network.ChecksumTCP };
					buf.next := НУЛЬ;
					buf.prev := НУЛЬ;
					если type = 0DEADH то
						(* make sure the frame doesn't bounce between the two cards by adding 1 to the type *)
						SendFrame(buf.src, type + 1, buf.data, buf.data, buf.data, 0, 0, 0, buf.len);
					аесли type = 0DEADH + 1 то
						(* discard this frame *)
					иначе
					dev.QueueBuffer(buf, type);
					всё;

					увел(nRxFrames);

					если (type # 0DEADH) и (type # 0DEADH + 1) то
					rxBuffer.buf := Network.GetNewBuffer();
					buf := rxBuffer.buf;
					утв(rxBuffer.buf # НУЛЬ);
					если buf # НУЛЬ то
						rds[curRD].bufAdrLo := Machine.PhysicalAdr(адресОт(rxBuffer.buf.data[0]), Network.MaxPacketSize)(Machine.Address32);
					всё;
					всё;
				иначе
					если DebugReceive в Debug то
						ЛогЯдра.пСтроку8("Checksum error detected!"); ЛогЯдра.пВК_ПС;
					всё;
					увел(nRxErrorFrames);
				всё;

				(* mark the buffer to be able to receive again *)
				rds[curRD].flags := {31} + (rds[curRD].flags * {30}) + (НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Network.MaxPacketSize) * {0..13});
				rds[curRD].vLanTag := 0;
				s := rds[curRD].flags;

				(* advance Rx descriptor, Rx buffer *)
				rxBuffer := rxBuffer.next;
				увел(curRD);
				если curRD = RxRingSize то
					curRD := firstRD;
				всё;
			кц;
		кон ReadFrames;

		проц CheckChecksumErrors(d: RxTxDescriptor): булево;
		перем proto: мнвоНаБитахМЗ;
		нач
			proto := d.flags * {17..18};
			если proto = {} то
				возврат истина;	(* no checksum errors since non-ip packet *)
			аесли proto = {17} то
				(* protocol is TCP/IP so check IP and TCP checksum failures *)
				возврат d.flags * {14, 16} = {};
			аесли proto = {18} то
				(* protocol is UDP/IP so check IP and UDP checksum failures *)
				возврат d.flags * {15, 16} = {};
			иначе
				(* protocol is IP so check IP checksum failures *)
				возврат d.flags * {16} = {};
			всё;
		кон CheckChecksumErrors;

		проц HandleInterrupt;
		перем
			status, ack: мнвоНаБитахМЗ;
		нач
			(* get current interrupt mask, disable all interrupts *)
			Write16(3CH, 0);

			ack := {0};
			(* read interrupt status, @ offset 3EH - 3FH *)
			status := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read16(3EH));

			(* System Error (SERR) *)
			если (15 в InterruptMask) и (15 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("System Error Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				включиВоМнвоНаБитах(ack, 15);
			всё;

			(* Time Out (TimeOut) *)
			если (14 в InterruptMask) и (14 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Timeout Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				включиВоМнвоНаБитах(ack, 14);
			всё;

			если (8 в InterruptMask) и (8 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Software Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				включиВоМнвоНаБитах(ack, 8);
			всё;

			если (7 в InterruptMask) и (7 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Tx Descriptor Unavailable Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				включиВоМнвоНаБитах(ack, 7);
				(*UpdateTxRing;*)
				(*INCL(status, 2); (* let the tx ring be updated *)*)
			всё;

			(* Rx FIFO Overflow (FOVW) *)
			если (6 в InterruptMask) и (6 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Rx FIFO Overflow Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				увел(nRxOverflow);
				включиВоМнвоНаБитах(ack, 6);
				(*INCL(ack, 4);*)
				(*INCL(status, 0); (* read the frames *)*)
			всё;

			(* Link Change (LinkChg) *)
			если (5 в InterruptMask) и (5 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Link Change Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				UpdateLinkStatus;
				включиВоМнвоНаБитах(ack, 5);
			всё;

			(* Rx Descriptor Unavailable (RDU) *)
			если (4 в InterruptMask) и (4 в status) то
				если DebugInterrupt в Debug то
					(* CAREFUL: UN-COMMENTING THE NEXT LINE CAN CRASH THE OS *)
					(*KernelLog.String("Rx Descriptor Unavailable Interrupt"); KernelLog.Ln;*)
				всё;
				включиВоМнвоНаБитах(ack, 4);
				(*INCL(status, 0); (* read the frames *)*)
			всё;

			(* Transmit (Tx) Error (TER) *)
			если (3 в InterruptMask) и (3 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Transmit Error Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				включиВоМнвоНаБитах(ack, 3);
				увел(nTxErrorFrames);
				включиВоМнвоНаБитах(status, 2); (* let the tx ring be updated *)
			всё;

			(* Transmit (Tx) OK (TOK) *)
			если (2 в InterruptMask) и (2 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Transmit OK Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				UpdateTxRing;
				включиВоМнвоНаБитах(ack, 2);
			всё;

			(* Receive (Rx) Error (RER) *)
			если (1 в InterruptMask) и (1 в status) то
				если DebugInterrupt в Debug то
					ЛогЯдра.пСтроку8("Receive Error Interrupt"); ЛогЯдра.пВК_ПС;
				всё;
				включиВоМнвоНаБитах(ack, 1);
				(*ReadFrames;*)
				включиВоМнвоНаБитах(status, 0); (* let the rx ring be updated *)
			всё;

			(* Receive (Rx) OK (ROK) *)
			если (0 в InterruptMask) и (0 в status) то
				если DebugInterrupt в Debug то
					(* CAREFUL: UN-COMMENTING THE NEXT LINE CAN CRASH THE OS *)
					(*KernelLog.String("Receive Ok Interrupt"); KernelLog.Ln;*)
				всё;
				ReadFrames;
				включиВоМнвоНаБитах(ack, 0); (* read the frames *)
			всё;

			ack := status;
			(* reset interrupt status *)
			Write16(3EH, НИЗКОУР.подмениТипЗначения(цел32, ack));

			(* re-enable interrupts *)
			Write16(3CH, НИЗКОУР.подмениТипЗначения(цел32, InterruptMask));
		кон HandleInterrupt;

		проц UpdateLinkStatus;
		нач
			если 1 в НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(6CH)) то
				linkStatus := Network.LinkLinked;
			иначе
				linkStatus := Network.LinkNotLinked;
			всё;
		кон UpdateLinkStatus;

		проц UpdateTxRing;
		перем i: цел32;
		нач { единолично }
			i := lastTD;
			нцПока (i # curTD) делай
				если DebugTransmit в Debug то
					ЛогЯдра.пСтроку8("*** Tx OK ***"); ЛогЯдра.пВК_ПС;
				всё;
				увел(i);
				увел(nTxFrames);
				увел(nofFreeTx);
				если i = TxRingSize то
					i := firstTD;
				всё;
				txLast := txLast.next;
			кц;
			lastTD := i;
		кон UpdateTxRing;

		проц Finalize;
		перем
			s: мнвоНаБитахМЗ;
		нач
			(* cleanup Network registry *)
			Network.registry.Remove(dev);

			(* disable Tx and Rx *)
			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(37H));
			Write8(37H, НИЗКОУР.подмениТипЗначения(цел8, s - {2, 3}));

			(* soft reset *)
			Write8(37H, НИЗКОУР.подмениТипЗначения(цел8, {4}));

			(* disable all interrupts *)
			Write16(3CH, 0);

			нцПока (rxBuffer # НУЛЬ) и (rxBuffer.buf # НУЛЬ) делай
				Network.ReturnBuffer(rxBuffer.buf);
				rxBuffer.buf := НУЛЬ;
				rxBuffer := rxBuffer.next;
			кц;

			если DebugCleanup в Debug то
				ЛогЯдра.пСтроку8("Removing IRQ Handler.");
				ЛогЯдра.пВК_ПС
			всё;
			Objects.RemoveHandler(сам.HandleInterrupt, Machine.IRQ0 + irq);
		кон Finalize;

		проц DebugConfig;
		перем
			s: мнвоНаБитахМЗ;
			res: целМЗ;
		нач
			ЛогЯдра.пСтроку8("*** BEGIN OF NIC CONFIGURATION ***"); ЛогЯдра.пВК_ПС;

			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read16(0E0H));
			ЛогЯдра.пСтроку8("C+ Command:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  "); ЛогЯдра.пМнвоНаБитахМЗКакБиты(s, 0, 16); ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("Rx Descriptor base address:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  ");
			res := Read32(0E4H + 4H);
			ЛогЯдра.п16ричное(res, 8);
			res := Read32(0E4H);
			ЛогЯдра.п16ричное(res, 8);
			ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("Tx Normal Priority Descriptor base address:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  ");
			res := Read32(020H + 4H);
			ЛогЯдра.п16ричное(res, 8);
			res := Read32(020H);
			ЛогЯдра.п16ричное(res, 8);
			ЛогЯдра.пВК_ПС;

			res := Read16(0DAH);
			ЛогЯдра.пСтроку8("Receive Packet Max Size:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пВК_ПС;

			res := Read8(0ECH);
			ЛогЯдра.пСтроку8("Max Transmit Packet Size:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  "); ЛогЯдра.пЦел64(res * 32, 0); ЛогЯдра.пВК_ПС;

			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read32(40H));
			ЛогЯдра.пСтроку8("Transmit Configuration:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  "); ЛогЯдра.пМнвоНаБитахМЗКакБиты(s, 0, 32); ЛогЯдра.пВК_ПС;

			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read32(44H));
			ЛогЯдра.пСтроку8("Receive Configuration:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  "); ЛогЯдра.пМнвоНаБитахМЗКакБиты(s, 0, 32); ЛогЯдра.пВК_ПС;

			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read16(3CH));
			ЛогЯдра.пСтроку8("interrupt mask:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  "); ЛогЯдра.пМнвоНаБитахМЗКакБиты(s, 0, 16); ЛогЯдра.пВК_ПС;

			s := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(37H));
			ЛогЯдра.пСтроку8("command bits:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("  "); ЛогЯдра.пМнвоНаБитахМЗКакБиты(s, 0, 8); ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("*** END OF NIC CONFIGURATION ***"); ЛогЯдра.пВК_ПС;
		кон DebugConfig;

		проц PrintStatus;
		перем
			phyStatus: мнвоНаБитахМЗ;
		нач
			phyStatus := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, Read8(6CH));

			если 1 в phyStatus то
				ЛогЯдра.пСтроку8("  Device is linked");
				ЛогЯдра.пВК_ПС;
			иначе
				ЛогЯдра.пСтроку8("  Device is NOT linked");
				ЛогЯдра.пВК_ПС;
			всё;

			если 4 в phyStatus то
				ЛогЯдра.пСтроку8("  Linkspeed is 1GBps Full-Duplex");
				ЛогЯдра.пВК_ПС;
			иначе
				если 3 в phyStatus то
					ЛогЯдра.пСтроку8("  Linkspeed is 100MBps");
					ЛогЯдра.пВК_ПС;
				аесли 2 в phyStatus то
					ЛогЯдра.пСтроку8("  Linkspeed is 10MBps");
					ЛогЯдра.пВК_ПС;
				всё;
				если 0 в phyStatus то
					ЛогЯдра.пСтроку8("  Device is in FULL-DUPLEX MODE");
					ЛогЯдра.пВК_ПС;
				иначе
					ЛогЯдра.пСтроку8("  Device is in Half-Duplex Mode");
					ЛогЯдра.пВК_ПС;
				всё;
			всё;

			если 6 в phyStatus то
				ЛогЯдра.пСтроку8("  Transmit Flow Control enabled");
				ЛогЯдра.пВК_ПС;
			всё;

			если 5 в phyStatus то
				ЛогЯдра.пСтроку8("  Receive Flow Control enabled");
				ЛогЯдра.пВК_ПС;
			всё;

			ЛогЯдра.пСтроку8("  nRxOverflow = ");
			ЛогЯдра.п16ричноеДубликат(nRxOverflow, 16);
			ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("  nTxOverflow = ");
			ЛогЯдра.п16ричноеДубликат(nTxOverflow, 16);
			ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("  Rx Missed Packet Counter = ");
			ЛогЯдра.пЦел64(Read32(4CH), 0);
			ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("  nRxFrames = ");
			ЛогЯдра.пЦел64(nRxFrames, 0);
			ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("  nTxFrames = ");
			ЛогЯдра.пЦел64(nTxFrames, 0);
			ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("  nRxErrorFrames = ");
			ЛогЯдра.пЦел64(nRxErrorFrames, 0);
			ЛогЯдра.пВК_ПС;

			ЛогЯдра.пСтроку8("  nTxErrorFrames = ");
			ЛогЯдра.пЦел64(nTxErrorFrames, 0);
			ЛогЯдра.пВК_ПС;
		кон PrintStatus;
	кон Controller;

 перем
	installedControllers: Controller;

(* Scan the PCI bus for the specified card. *)
проц ScanPCI(vendor, device: цел32);
перем index, bus, dev, fct, irq, i: цел32; res: целМЗ; base: адресВПамяти; d: LinkDevice; c: Controller; name: Plugins.Name;
нач
	index := 0;
	нцПока (PCI.FindPCIDevice(device, vendor, index, bus, dev, fct) = PCI.Done) и (installed < 16) делай
		res := PCI.ReadConfigDword(bus, dev, fct, PCI.Adr1Reg, i); утв(res = PCI.Done);
		base := i; утв(~нечётноеЛи¿(base)); (* memory mapped *)
		умень(base, base остОтДеленияНа 16);
		Machine.MapPhysical(base, 0FFH, base);

		res := PCI.ReadConfigByte(bus, dev, fct, PCI.IntlReg, irq); утв(res = PCI.Done);
		нов(d, Network.TypeEthernet, MaxETHFrameSize - 14, 6);
		name := Name;
		i := 0; нцПока name[i] # 0X делай увел(i) кц;
		если installed > 9 то
			name[i] := симв8ИзКода(кодСимв8("A") + installed - 10);
		иначе
			name[i] := симв8ИзКода(кодСимв8("0") + installed);
		всё;
		name[i+1] := 0X;
		если DebugFind в Debug то
			ЛогЯдра.пСтроку8("Found device: ");
			ЛогЯдра.пСтроку8(name);
			ЛогЯдра.пСтроку8("; IRQ = ");
			ЛогЯдра.пЦел64(irq, 0);
			ЛогЯдра.пВК_ПС;
		всё;
		d.SetName(name);
		d.desc := Description;

		нов(c, d, base, irq);	 (* increments "installed" when successful *)
		если DebugStatus в Debug то
			c.PrintStatus;
		всё;
		увел(index)
	кц
кон ScanPCI;

проц Install*;
нач {единолично}
	если DebugFind в Debug то
		ЛогЯдра.пСтроку8("Searching devices...");
		ЛогЯдра.пВК_ПС
	всё;
	если installed = 0 то
		ScanPCI(10ECH, 8169H);	(* Vendor = RealTek, Device = RTL8169 *)
	всё;
	если DebugFind в Debug то
		ЛогЯдра.пСтроку8("Find finished.");
		ЛогЯдра.пВК_ПС
	всё;
кон Install;

проц DebugStati*;
перем c: Controller;
нач
	c := installedControllers;
	нцПока c # НУЛЬ делай
		c.PrintStatus;
		c := c.next;
	кц;
кон DebugStati;

проц TestDevices*;
перем c: Controller;
нач
	c := installedControllers;
	нцПока c # НУЛЬ делай
		TestDevice(c);
		c := c.next;
	кц;
кон TestDevices;

проц TestDevice(ctrl: Controller);
перем
	i, diff, bytes, times: цел32;
	milliTimer : Kernel.MilliTimer;
	data: массив 1024 из симв8;
	bw: вещ32;
	dst: Network.LinkAdr;
нач
	dst[0] := 000X;
	dst[1] := 030X;
	dst[2] := 04FX;
	dst[3] := 025X;
	dst[4] := 0BBX;
	dst[5] := 0DBX;
	если ctrl # НУЛЬ то
		ctrl.nRxFrames := 0;
		ctrl.nTxFrames := 0;
		(* fill the buffer *)
		нцДля i := 0 до длинаМассива(data)-1 делай
			data[i] := симв8ИзКода(i остОтДеленияНа 100H)
		кц;

		Kernel.SetTimer(milliTimer, 0);
		times := 1024 * 1024 * 2;
		нцДля i := 1 до times делай
			ctrl.SendFrame(dst, 0DEADH, data, data, data, 0, 0, 0, длинаМассива(data));
			если i остОтДеленияНа 1024 = 0 то
				Delay(1);
			всё;
		кц;
		diff := Kernel.Elapsed(milliTimer);
		times := ctrl.nRxFrames * 2;
		bytes := (длинаМассива(data));
		ЛогЯдра.пСтроку8("stats:"); ЛогЯдра.пВК_ПС;
		ЛогЯдра.пСтроку8("frame size = ");
		ЛогЯдра.пЦел64(bytes, 0);
		ЛогЯдра.пСтроку8("; num frames = ");
		ЛогЯдра.пЦел64(times, 0);
		ЛогЯдра.пСтроку8("; time = ");
		ЛогЯдра.пЦел64(diff, 0); ЛогЯдра.пСтроку8("ms");
		ЛогЯдра.пСтроку8("; bandwidth = ");
		bw := bytes * 1.0 * times / (diff / 1000.0);
		ЛогЯдра.пЦел64(округлиВниз(bw / 1024), 0); ЛогЯдра.пСтроку8("KB/s, ");
		ЛогЯдра.пЦел64(округлиВниз(bw * 8 / 1000 / 1000), 0); ЛогЯдра.пСтроку8("Mbps"); ЛогЯдра.пВК_ПС;
	всё
кон TestDevice;


проц Cleanup;
нач
	нцПока installedControllers # НУЛЬ делай
		если DebugCleanup в Debug то
			ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("Removing ");
			ЛогЯдра.пСтроку8(installedControllers.dev.name);
			ЛогЯдра.пВК_ПС;
			installedControllers.PrintStatus;
		всё;
		installedControllers.Finalize;
		installedControllers := installedControllers.next;
		если DebugCleanup в Debug то
			ЛогЯдра.пСтроку8("Success!");
			ЛогЯдра.пВК_ПС;
		всё;
	кц;
	installedControllers := НУЛЬ;
кон Cleanup;

проц Delay(ms: цел32);
перем
	t: Kernel.MilliTimer;
нач
	Kernel.SetTimer(t, ms);
	нцДо кцПри Kernel.Expired(t);
кон Delay;

нач
	Modules.InstallTermHandler(Cleanup);
кон RTL8169.

(*
MAC address 00-30-4F-25-BB-DB
MAC address 00-08-A1-3C-06-CB

local IP 129.132.134.209

System.Free RTL8169 ~
RTL8169.Install ~
WMPerfMon.Open ~
RTL8169.DebugStati ~

IP.IPConfig ~

RTL8169.TestDevices~

OFSTools.Mount RAM RamFS 300000 4096 ~

TestNet.SetDevice "RTL8169#0" ~
TestNet.ShowDevices ~
TestNet.SendBroadcast ~
TestNet.SendBroadcastVar 1499 ~
TestNet.SendTest ^ 1 10 100 1000 ~
*)
