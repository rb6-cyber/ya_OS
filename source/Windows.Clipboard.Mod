модуль Clipboard; (** AUTHOR "ALEX"; PURPOSE "Windows clipboard interface"; *)

использует Kernel32, User32, ЛогЯдра, Modules, Texts, 
	HostClipboard, UCS32, UCS2, HostStrings;

(** Copy text of Windows clipboard to text *)
проц GetFromClipboard( text: Texts.Text );
перем
	clipWinString: HostStrings.StrokaWindowsUTF16DoNolja;
	clipA2UCS2String: UCS2.PString;
	textBuffer: UCS32.PString;
нач
	утв(( text # НУЛЬ ) и ( text.HasWriteLock( )));
	если User32.OpenClipboard(Kernel32.NULL) # Kernel32.False то
		clipWinString := User32.GetClipboardData( User32.CFUnicodeText );
		если clipWinString # Kernel32.NULL то
		(* copy clipboard data into internal buffer and unlock clipboard data *)
			clipWinString := Kernel32.GlobalLock( clipWinString );
			если clipWinString = Kernel32.NULL то
				ЛогЯдра.пСтроку8("Otkaz pri blokirovke bufera obmena");
				ЛогЯдра.пВК_ПС;
				возврат всё;
			clipA2UCS2String := HostStrings.ZagruziStrokuIzWindows( clipWinString );
			если Kernel32.GlobalUnlock( clipWinString ) = Kernel32.False то
				ЛогЯдра.пСтроку8("Otkaz v razblokirovanii clipWinString"); 
				ЛогЯдра.пВК_ПС всё;
			если User32.CloseClipboard( ) = Kernel32.False то
				ЛогЯдра.пСтроку8("Otkaz pri zakrytii bufera obmena"); 
				ЛогЯдра.пВК_ПС всё;
				
			textBuffer := UCS32.JQvVTotZhePString(UCS2.U2vNovJQ(clipA2UCS2String^));
			если textBuffer # НУЛЬ то
				text.Delete( 0, text.GetLength( ));
				text.InsertUCS32( text.GetLength( ), textBuffer^ ) всё всё всё кон GetFromClipboard;


(** Copy text to Windows clipboard *)
проц PutToClipboard( text: Texts.Text );
перем
	clipboardData: HostStrings.StrokaWindowsUTF16DoNolja;
	textBuffer: UCS2.PString;
	textLen: размерМЗ;
нач
	утв(( text # НУЛЬ ) и ( text.HasReadLock( )));
	textLen := text.GetLength( );
	если textLen > 0 то
		textBuffer := Texts.IzTextsTextVUTF16(text);
		textLen := длинаМассива(textBuffer) + 1;

		если User32.OpenClipboard( Kernel32.NULL ) # Kernel32.False то
			неважно User32.EmptyClipboard( );
			неважно HostStrings.OtpravqStrokuVWindowsDljaPutClipboard(textBuffer, clipboardData);
			неважно User32.SetClipboardData(User32.CFUnicodeText, clipboardData );
		всё;
		неважно User32.CloseClipboard( );
	всё;
кон PutToClipboard;

проц ClipboardChanged(sender, data : динамическиТипизированныйУкль);
нач
	Texts.clipboard.AcquireRead;
	PutToClipboard(Texts.clipboard);
	Texts.clipboard.ReleaseRead;
кон ClipboardChanged;

проц Install*;
нач
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("WindowsClipboard: Registered clipboard at host clipboard interface."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
кон Install;

проц Cleanup;
нач
	Texts.clipboard.onTextChanged.Remove(ClipboardChanged);
	HostClipboard.SetHandlers(НУЛЬ, НУЛЬ);
	ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("WindowsClipboard: Unregistered clipboard at host clipboard interface."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
кон Cleanup;


нач
	(* register with AosText clipboard *)
	Texts.clipboard.onTextChanged.Add( ClipboardChanged );
	HostClipboard.SetHandlers( GetFromClipboard, PutToClipboard );
	Modules.InstallTermHandler( Cleanup );
кон Clipboard.

Clipboard.Install ~

System.Free Clipboard ~
