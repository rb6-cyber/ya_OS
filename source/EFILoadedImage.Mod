модуль EFILoadedImage; (** AUTHOR "Matthias Frei"; PURPOSE "EFI Loaded Image Protocol"; *)

использует
	EFI, НИЗКОУР;

конст
	Revision* = 00010000H;

перем
	GUID-: EFI.GUID;

тип Protocol* = укль на ProtocolDescription;

тип PtrToArrayOfByte* = укль на массив 2048 из НИЗКОУР.октет;
тип LIUnload* = проц{WINAPI}(ImageHandle : EFI.Handle) : EFI.Status;
тип ProtocolDescription* = запись(EFI.ProtocolDescription)
	Revision-: EFI.Int32;
	ParentHandle-: EFI.Handle;
	SystemTable-{неОтслСборщиком}: укль на EFI.SystemTable;

	(* source location of the image *)
	DeviceHandle-: EFI.Handle;
	FilePath : адресВПамяти; (* not implemented *)
	Reserved : адресВПамяти;

	(* image's load options *)
	LoadOptionsSize-: EFI.Int32;
	LoadOptions-{неОтслСборщиком} : PtrToArrayOfByte;

	(* location where image was loaded *)
	ImageBase-: адресВПамяти;
	ImageSize-: EFI.Int64;
	ImageCodeType-: EFI.Int; (* memory types: see EFI.MT... *)
	ImageDataType-: EFI.Int;

	Unload-: LIUnload;
кон;

нач
	GUID.Data1 := 5B1B31A1H;
	GUID.Data2 := -6A9EH; (*9562H;*)
	GUID.Data3 := 11D2H;
	GUID.Data4[0] :=-72H; (* 8EH;*)
	GUID.Data4[1] := 3FH;
	GUID.Data4[2] := 00H;
	GUID.Data4[3] := -60H; (*0A0H*)
	GUID.Data4[4] := -37H; (*0C9H;*)
	GUID.Data4[5] := 69H;
	GUID.Data4[6] := 72H;
	GUID.Data4[7] := 3BH;
кон EFILoadedImage.
