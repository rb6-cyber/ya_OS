(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль MouseSerial; (** AUTHOR "pjm"; PURPOSE "Serial mouse driver"; *)
(**
 * Aos serial mouse driver (quick and dirty port from Native Input.Mod and ConfigInput.Mod).
 * Mouse protocol information from XFree in X11R6 distribution (Thomas Roell & David Dawes)
 *
 * Usage:
 *
 *	Load mouse driver: MouseSerial.Install ~
 *
 *	To unload the driver, use System.Free MouseSerial ~
 *
 * Note:
 *
 * Be sure that the serial port driver is loaded before this module. Use V24.Install to load the serial port driver.
 *
 * History:
 *
 *	12.03.2003	procedure Configure modified to support 8 serial ports i.o. 4 (AFI)
 *	03.07.2007	Quick and dirty port to Shell (staubesv)
 *)

использует НИЗКОУР, Machine, ЛогЯдра, Modules, Kernel, Commands, Inputs, Serials;

конст
	Trace = истина;
	Debug = ложь;

	(* If TRUE, call the V24.Install command when loading this module *)
	LoadV24 = истина;

	(* mouse types *)
	MinType = 0;  MaxType = 9;
	MS = 0;  MSC1 = 1;  MM = 2;  Logi = 3;  MSC2 = 4;  LogiMan = 5;  PS2 = 6;  MSI = 7;  MSC3 = 8;  MSC4 = 9;
(*
	0  Microsoft serial (2-button)
	1  Mouse Systems Corp serial type a (dtr on, rts on)
	2  Logitech serial Type a (old models)
	3  Logitech serial Type b (old models)
	4  Mouse Systems Corp serial type b (dtr off, rts off)
	5  Logitech serial Type c (new models)
	6  PS/2 mouse (default)
	7  Microsoft serial IntelliMouse
	8  Mouse Systems Corp serial type c (dtr off, rts on)
	9  Mouse Systems Corp serial type d (dtr on, rts off)

	MT=PS2  PS/2 or built-in
	MT=LM1  Logitech 1
	MT=LM2  Logitech 2
	MT=LM3  Logitech 3
	MT=MS1  Mouse Systems 1
	MT=MS2  Mouse Systems 2
	MT=MS3  Mouse Systems 3
	MT=MS4  Mouse Systems 4
	MT=MSM  Microsoft (2-button)
	MT=MSI  Microsoft IntelliMouse
	MP=1
	MP=2
*)

	Rate = 100;	(* Sampling rate *)
	BPS = 1200;	(* Speed *)

	DetectOffTime = 250;	(* ms *)
	DetectOnTime = 250;	(* ms *)
	DetectMaxIdent = 256;

тип

	Mouse = окласс
	перем
		type: цел32; (* mouse type *)
		mbufp, numb: цел8; (* buffer pointer & protocol bytes *)
		mbuf: массив 5 из мнвоНаБитахМЗ; (* protocol buffer *)
		mask0, val0, mask1, val1, lastkeys: мнвоНаБитахМЗ; (* protocol parameters *)

		errors : цел32;
		res : целМЗ;

		port : Serials.Port;
		m : Inputs.MouseMsg; keys: мнвоНаБитахМЗ;

		dead : булево;

		(* Read a mouse event *)
		проц GetMouseEvent(перем keys: мнвоНаБитахМЗ;  перем dx, dy: цел32): целМЗ;
		перем ch : симв8; b : мнвоНаБитахМЗ; res : целМЗ;
		нач
			b := {};

			port.ReceiveChar(ch, res);
			если res # Serials.Ok то возврат res; всё;
			b := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ch);

			(* check for resync *)
			если (mbufp # 0) и ((b * mask1 # val1) или (b = {7})) то mbufp := 0 всё;
			если (mbufp = 0) и (b * mask0 # val0) то
				(* skip package, unless it is a LogiMan middle button... *)
				если ((type = MS) или (type = LogiMan)) и (b * {2..4,6,7} = {}) то
					keys := lastkeys * {0,2};
					если 5 в b то включиВоМнвоНаБитах(keys, 1) всё;
					dx := 0;  dy := 0;
					возврат Serials.Ok;
				иначе
					увел(errors);
				всё
			иначе
				mbuf[mbufp] := b;  увел(mbufp);
				если mbufp = numb то
					просей type из
						|MS, LogiMan:
							keys := lastkeys * {1};
							если 5 в mbuf[0] то включиВоМнвоНаБитах(keys, 2) всё;
							если 4 в mbuf[0] то включиВоМнвоНаБитах(keys, 0) всё;
							dx := устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, логСдвиг(mbuf[0] * {0,1}, 6) + mbuf[1] * {0..5}));
							dy := устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, логСдвиг(mbuf[0] * {2,3}, 4) + mbuf[2] * {0..5}));
						|MSC1, MSC2, MSC3, MSC4:
							keys := {0..2} - (mbuf[0] * {0..2});
							dx := устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, mbuf[1])) + устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, mbuf[3]));
							dy := -(устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, mbuf[2])) + устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, mbuf[4])));
						|MM, Logi:
							keys := mbuf[0] * {0..2};
							dx := НИЗКОУР.подмениТипЗначения(цел16, mbuf[1]);
							если ~(4 в mbuf[0]) то dx := -dx всё;
							dy := НИЗКОУР.подмениТипЗначения(цел16, mbuf[2]);
							если 3 в mbuf[0] то dy := -dy всё;
						|MSI:
							keys := {};
							если 4 в mbuf[0] то включиВоМнвоНаБитах(keys, 0) всё;
							если 5 в mbuf[0] то включиВоМнвоНаБитах(keys, 2) всё;
							если 3 в mbuf[3] то включиВоМнвоНаБитах(keys, 3) всё;
							если 4 в mbuf[3] то включиВоМнвоНаБитах(keys, 1) всё;
							если ~(3 в mbuf[3]) и (mbuf[3] * {0..2} # {}) то включиВоМнвоНаБитах(keys, 4) всё;
							dx := устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, логСдвиг(mbuf[0] * {0,1}, 6) + mbuf[1] * {0..7}));
							dy := устарПреобразуйКБолееШирокомуЦел(НИЗКОУР.подмениТипЗначения(цел8, логСдвиг(mbuf[0] * {2,3}, 4) + mbuf[2] * {0..7}));
					иначе
						(* ignore *)
					всё;
					mbufp := 0;
					возврат Serials.Ok;
				всё;
			всё;
			keys := lastkeys;  dx := 0;  dy := 0;
			возврат 99; (* don't sent mouse message *)
		кон GetMouseEvent;

		проц Close;
		нач
			port.Закрой();
			нач {единолично} дождись(dead); кон;
		кон Close;

		(* Initialise mouse.
			"type" - mouse type from list
			"port" - V24.COM[12]
			"bps" - V24.BPS*
			"rate" - sample rate (not all mice support this) *)
		проц &Init*(port : Serials.Port; type : цел32);
		перем c: симв8; res: целМЗ; n: цел32;
		нач
			(* ASSERT: Port is already open *)
			утв(port # НУЛЬ);
			утв((MinType <= type) и (type  <= MaxType));
			dead := ложь;
			сам.port := port;
			сам.type := type;
			если type # PS2 то
				errors := 0;
				если type = LogiMan то
					SetSpeed(port, type, 1200, 1200);
					port.SendChar("*", res);
					port.SendChar("X", res);
					SetSpeed(port, type, 1200, BPS)
				иначе
					SetSpeed(port, type, 9600, BPS);
					SetSpeed(port, type, 4800, BPS);
					SetSpeed(port, type, 2400, BPS);
					SetSpeed(port, type, 1200, BPS);
					если type = Logi то
						port.SendChar("S", res);
						SetSpeed(port, MM, BPS, BPS);
					всё;
					(* set sample rate *)
					если Rate <= 0 то c := "O";	(* continuous - don't use *)
					аесли Rate <= 15 то c := "J";	(* 10 Hz *)
					аесли Rate <= 27 то c := "K";	(* 20 *)
					аесли Rate <= 42 то c := "L";	(* 35 *)
					аесли Rate <= 60 то c := "R";	(* 50 *)
					аесли Rate <= 85 то c := "M";	(* 70 *)
					аесли Rate <= 125 то c := "Q"; (* 100 *)
					иначе c := "N"; (* 150 *)
					всё;
					port.SendChar(c, res);
					если type = MSC2 то port.ClearMC({Serials.DTR, Serials.RTS});
					аесли type = MSC3 то port.ClearMC( {Serials.DTR});
					аесли type = MSC4 то port.ClearMC( {Serials.RTS});
					всё;
				всё;
				mbufp := 0;  lastkeys := {};
				(* protocol parameters *)
				просей type из
					|MS:  numb := 3;  mask0 := {6};  val0 := {6};  mask1 := {6};  val1 := {};
					|MSC1, MSC2, MSC3, MSC4:  numb := 5;  mask0 := {3..7};  val0 := {7};  mask1 := {};  val1 := {};
					|MM:  numb := 3;  mask0 := {5..7};  val0 := {7};  mask1 := {7};  val1 := {};
					|Logi:  numb := 3;  mask0 := {5..7};  val0 := {7};  mask1 := {7};  val1 := {};
					|LogiMan:  numb := 3;  mask0 := {6};  val0 := {6};  mask1 := {6};  val1 := {};
					|MSI: numb := 4;  mask0 := {6};  val0 := {6};  mask1 := {6};  val1 := {};
				иначе
					(* ignore *)
				всё;

				(* ignore the first few bytes from the mouse (e.g. Logitech MouseMan Sensa) *)
				n := 4;
				нцДо
					нцПока port.Available() # 0 делай port.ReceiveChar(c, res) кц;
					Wait(1000 DIV n); (* wait 1/4s, 1/3s, 1/2s, 1s *)
					умень(n);
				кцПри (port.Available() = 0) или (n = 0)
			всё;

			(* Lower/Raise DTR/RTS for autodetection, and to start an Intellimouse *)
			port.ClearMC({Serials.DTR, Serials.RTS});
			Wait(DetectOffTime);
			port.SetMC({Serials.DTR, Serials.RTS});
			Wait(DetectOnTime);
		кон Init;

	нач {активное}
		нц
			res := GetMouseEvent(keys, m.dx, m.dy);
			если res = Serials.Ok то
				m.dz := 0; m.keys := {};
				если 0 в keys то включиВоМнвоНаБитах(m.keys, 2); всё;
				если 1 в keys то включиВоМнвоНаБитах(m.keys, 1); всё;
				если 2 в keys то включиВоМнвоНаБитах(m.keys, 0); всё;
				Inputs.mouse.Handle(m);
			аесли res = Serials.Closed то
				прервиЦикл;
			всё;
		кц;
		нач {единолично} dead := истина; кон;
	кон Mouse;

перем
	mouse : массив Serials.MaxPorts + 1 из Mouse; (* index 0 not used *)
	timer : Kernel.Timer;

(* Set mouse speed *)
проц SetSpeed(port : Serials.Port; mouseType, oldBPS, newBPS: цел32);
перем word, stop, par : цел16; c : симв8;  res : целМЗ;
нач
	утв(port # НУЛЬ);
	port.Закрой();
	просей mouseType из
		MS:  word := 7;  stop := Serials.Stop1;  par := Serials.ParNo |
		MSC1, MSC2, MSC3, MSC4:  word := 8;  stop := Serials.Stop2;  par := Serials.ParNo |
		MM:  word := 8;  stop := Serials.Stop1;  par := Serials.ParOdd |
		Logi:  word := 8;  stop := Serials.Stop2;  par := Serials.ParNo |
		LogiMan:  word := 7;  stop := Serials.Stop1;  par := Serials.ParNo |
		MSI:  word := 7;  stop := Serials.Stop1;  par := Serials.ParNo
	иначе
		(* ignore *)
	всё;
	если (mouseType = Logi) или (mouseType = LogiMan) то
		port.Open(oldBPS, word, par, stop, res);
		если res = Serials.Ok то
			если newBPS = 9600 то c := "q"
			аесли newBPS = 4800 то c := "p"
			аесли newBPS = 2400 то c := "o"
			иначе c := "n"
			всё;
			port.SendChar("*", res);
			port.SendChar(c, res);
			Wait(100);
			port.Закрой();
		всё
	всё;
	port.Open(newBPS, word, par, stop, res);
	если res = Serials.Ok то
		port.SetMC({Serials.DTR, Serials.RTS})
	всё;
кон SetSpeed;

проц GetMouseType(конст s : массив из симв8) : цел32;
перем type : цел32;
нач
	type := MinType-1;
	если (s[0] >= "0") и (s[0] <= "9") то (* old style config *)
		type := устарПреобразуйКБолееУзкомуЦел(кодСимв8(s[0])-кодСимв8("0"))
	иначе (* new style config *)
		если s = "" то
		(* default if none specified *)
		аесли (ASCII_вЗаглавную(s[0]) = "L") и (ASCII_вЗаглавную(s[1]) = "M") то (* Logitech *)
			просей s[2] из
				|"1": type := LogiMan;
				|"2": type := MM;
				|"3": type := Logi;
			иначе
				type := MinType-1; (* Unknown *)
			всё;
		аесли (ASCII_вЗаглавную(s[0]) = "M") и (ASCII_вЗаглавную(s[1]) = "S") то (* Mouse Systems or Microsoft *)
			если ASCII_вЗаглавную(s[2]) = "M" то
				type := MS;
			аесли ASCII_вЗаглавную(s[2]) = "I" то
				type := MSI;
			иначе
				просей s[2] из
					|"1": type := MSC1;
					|"2": type := MSC2;
					|"3": type := MSC3|"4": type := MSC4;
				иначе
					type := MinType-1;
				всё;
			всё;
		аесли ASCII_вЗаглавную(s[0]) = "P" то (* PS/2 *)
			type := PS2
		всё
	всё;
	если (type < MinType) или (type > MaxType) то type := PS2 всё; (* unknown mouse type *)
	возврат type;
кон GetMouseType;

проц Detect(port : Serials.Port; перем mouseType : цел32): булево;
перем ch: симв8; i : цел32; res : целМЗ; mouseIdent : массив DetectMaxIdent из симв8;
нач
	утв(port # НУЛЬ);
	port.Open(BPS, 7,  Serials.ParNo, Serials.Stop1, res);
	если res # Serials.Ok то возврат ложь; всё;

	(* Lower/Raise DTR/RTS for autodetection, and to start an Intellimouse *)
	port.ClearMC({Serials.DTR, Serials.RTS});
	Wait(DetectOffTime);
	port.SetMC({Serials.DTR, Serials.RTS});
	Wait(DetectOnTime);

	нцДо
		если port.Available() = 0 то возврат ложь всё;
		port.ReceiveChar(ch, res);
		если ch >= 80X то ch := симв8ИзКода(кодСимв8(ch)-80H) всё
	кцПри ch = "M";

	mouseIdent[0] := ch; i := 1;
	нцПока (port.Available() # 0) и (i < DetectMaxIdent-1) делай
		port.ReceiveChar(ch, res);
		если ch >= 80X то ch := симв8ИзКода(кодСимв8(ch)-80H) всё;
		если (ch < " ") или (ch >= 7FX) то ch := "." всё;
		mouseIdent[i] := ch; увел(i)
	кц;
	mouseIdent[i] := 0X;
	если Debug то
		ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Mouse ident:"); ЛогЯдра.пВК_ПС; ЛогЯдра.пСрезБуфера16рично(mouseIdent, 0, i); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
	если mouseIdent[1] = "3" то mouseType := LogiMan;
	аесли mouseIdent[1] = "Z" то mouseType := MSI;
	иначе mouseType := MS;
	всё;
	возврат истина
кон Detect;

проц Init;
перем
	value: массив DetectMaxIdent из симв8;
	port : Serials.Port;
	mouseType, portNbr : цел32;
нач
	Machine.GetConfig("MT", value);
	если value[0] # 0X то (* manual config *)
		mouseType := GetMouseType(value);

		(* Get port number *)
		Machine.GetConfig("MP", value);
		если (value[0] >= "1") и (value[0] <= "8") то
			portNbr := кодСимв8(value[0]) - кодСимв8("0");
		иначе
			portNbr := 1;
		всё;
		если Trace то
			ЛогЯдра.пСтроку8("MouseSerial: Manual configuration (Port: "); ЛогЯдра.пЦел64(portNbr, 0);
			ЛогЯдра.пСтроку8(", type: "); ЛогЯдра.пЦел64(mouseType, 0); ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
		всё;
		port := Serials.GetPort(portNbr);
		если port # НУЛЬ то
			если mouse[portNbr] # НУЛЬ то mouse[portNbr].Close; всё;
			нов(mouse[portNbr], port, mouseType);
			если Trace то ЛогЯдра.пСтроку8("MouseSerial: Mouse at COM port "); ЛогЯдра.пЦел64(portNbr, 0); ЛогЯдра.пСтроку8(" started."); ЛогЯдра.пВК_ПС; всё;
		иначе
			если Trace то ЛогЯдра.пСтроку8("MouseSerial: COM port "); ЛогЯдра.пЦел64(portNbr, 0); ЛогЯдра.пСтроку8(" not avaiable."); ЛогЯдра.пВК_ПС; всё;
		всё;
	иначе
		если Trace то ЛогЯдра.пСтроку8("MouseSerial: Auto-detect serial mice..."); ЛогЯдра.пВК_ПС; всё;
		нцДля portNbr := 1 до Serials.MaxPorts делай
			port := Serials.GetPort(portNbr);
			если port # НУЛЬ то
				если Detect(port, mouseType) то
					если Trace то
						ЛогЯдра.пСтроку8("MouseSerial: Detected mouse at port "); ЛогЯдра.пЦел64(portNbr, 0);
						ЛогЯдра.пСтроку8(" (type: "); ЛогЯдра.пЦел64(mouseType, 0); ЛогЯдра.пСтроку8(")"); ЛогЯдра.пВК_ПС;
					всё;
					если mouse[portNbr] # НУЛЬ то mouse[portNbr].Close; всё;
					нов(mouse[portNbr], port, mouseType);
				всё;
			всё;
		кц;
		если Trace то ЛогЯдра.пСтроку8("MouseSerial: Auto-detection finished."); ЛогЯдра.пВК_ПС; всё;
	всё;
кон Init;

проц Wait(milliseconds : цел32);
нач {единолично}
	утв(milliseconds > 0);
	timer.Sleep(milliseconds);
кон Wait;

проц LoadSerialPortDriver;
перем msg : массив 64 из симв8; res : целМЗ;
нач
	ЛогЯдра.пСтроку8("MouseSerial: Loading serial port driver..."); ЛогЯдра.пВК_ПС;
	Commands.Call("V24.Install", {Commands.Wait}, res, msg);
	если res = Commands.Ok то
		ЛогЯдра.пСтроку8("MouseSerial: Serial port driver loaded.");
	иначе
		ЛогЯдра.пСтроку8("MouseSerial: Loading serial port driver failed, res: "); ЛогЯдра.пЦел64(res, 0); ЛогЯдра.пСтроку8(" ("); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пСтроку8(")");
	всё;
	ЛогЯдра.пВК_ПС;
кон LoadSerialPortDriver;

проц Install*; (** ~ *)
кон Install;

(** Manually set the mouse type *)
проц ToggleMouseType*(context : Commands.Context); (** PortNumber  ~ *)
перем portNbr, type : цел32; m : Mouse;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦел32(portNbr, ложь) и (1 <= portNbr) и (portNbr <= Serials.MaxPorts) то
		m := mouse[portNbr];
		если m # НУЛЬ то
			type := (m.type + 1) остОтДеленияНа (MaxType + 1);
			m.Close;
			нов(mouse[portNbr], m.port, type);
			context.out.пСтроку8("MouseSerial: Set type of mouse at port "); context.out.пЦел64(portNbr, 0);
			context.out.пСтроку8(" to "); context.out.пЦел64(m.type, 0); context.out.пВК_ПС;
		иначе
			context.out.пСтроку8("MouseSerial: No mouse at port "); context.out.пЦел64(portNbr, 0); context.out.пВК_ПС;
		всё;
	иначе context.error.пСтроку8("MouseSerial: Invalid port number"); context.error.пВК_ПС;
	всё;
кон ToggleMouseType;

проц Cleanup;
перем i : цел32;
нач
	нцДля i := 0 до длинаМассива(mouse)-1 делай
		если mouse[i] # НУЛЬ то
			mouse[i].Close; mouse[i] := НУЛЬ;
		всё;
	кц;
кон Cleanup;

нач
	нов(timer);
	Modules.InstallTermHandler(Cleanup);
	если LoadV24 то LoadSerialPortDriver; всё;
	Init;
кон MouseSerial.

MouseSerial.Install ~	MouseSerial.ToggleMouseType 1 ~

System.Free MouseSerial ~

