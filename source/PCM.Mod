(* Paco, Copyright 2000 - 2002, Patrik Reali, ETH Zurich *)

модуль PCM; (** AUTHOR "prk"; PURPOSE "Parallel Compiler: input and output module"; *)

использует
		НИЗКОУР,
		ЛогЯдра, Modules, Objects, Потоки, Files, Diagnostics,
		StringPool, PCDebug, Strings, Reflection,Machine;

	конст
		(* value of constant NIL *)
		nilval* = 0;

		(* target machine minimum values of basic types expressed in host machine format: *)
		MinSInt* = -80H;
		MinInt* = -8000H;
		MinLInt* =  цел32(80000000H);	(* i386: -2147483648*)

		(* target machine maximum values of basic types expressed in host machine format: *)
		MaxSInt* = 7FH;
		MaxInt* = 7FFFH;
		MaxLInt* = 7FFFFFFFH;	(* i386: 2147483647*)
		MaxSet* = 31;	(* must be >= 15, else the bootstraped compiler cannot run (IN-tests) *)

		(* parametrization of numeric scanner: *)
		MaxHDig* = 8;	(* maximal hexadecimal longint length *)
		MaxHHDig* = 16;	(* maximal hexadecimal hugeint length *)
		MaxRExp* = 38;	(* maximal real exponent *)
		MaxLExp* = 308;	(* maximal longreal exponent *)

		(** code generator options *)
		ArrayCheck* = 0;		(* x - perform array boundary checks *)
		OverflowCheck* = 1;	(* v - perform overflow check *)
		NilCheck* = 2;			(* N - explicit hard-coded nil checks *)
		TypeCheck*= 3;		(* t - perform type checks *)
		PtrInit* = 5;				(* p - initialize pointers to NIL *)
		AssertCheck* = 6;		(* a - evaluate asserts *)
		Optimize* = 13;
		FullStackInit* = 20;		(* z - clear all values on stack *)
		AlignedStack*=21; 		(* A - generate code with stack alignment for unix Aos *)

		ExportDefinitions* = 30;
		UseDefinitions* = 31;

		(** parser options *)
		NewSF* = 16;				(* s - generation of new symbol file allowed *)
		ExtSF* = 17;				(* e - generation of extended symbol file allowed *)
		Breakpoint* = 18;			(* f - find position in code *)
		CacheImports* = 19; 		(* c - Cache imported modules *)
		NoFiles* = 21;				(* n - don't generate files, parse only*)
		NoOpOverloading* = 22;	(* o - do NOT allow operator overloading *)
		BigEndian* = 23;			(* b - generate big endian code, makes only sense together with ARM backend *)
		Warnings* = 24;				(* W - display warnings *)
		SkipOldSFImport* = 25;		(* S - skip old symbol file import in PCOM.Export, avoids compiler error when migrating to new object file *) (* ug *)
		MultipleModules*= 26; 		(* M - allow compilation of multiple modules within one file *)


		(** sysflags and objflags written to and read from symbol file *)
		Untraced* = 4;	(** global vars + fields - weak pointer *)
		WinAPIParam* = 13; (* ejz *)
		CParam*=14; (* fof for linux *)
		ReadOnly* = 15;   (* fof *)
		RealtimeProc* = 21; (* ug *)
		RealtimeProcType* = 21; (* ug *)

		(** compiler generated traps *)
		WithTrap* = 1;
		CaseTrap* = 2;
		ReturnTrap* = 3;
		TypeEqualTrap* = 5;
		TypeCheckTrap* = 6;
		IndexCheckTrap* = 7;
		AssertTrap* = 8;
		ArraySizeTrap* = 9;
		ArrayFormTrap*=10; (* fof: indicates that array cannot be (re-)allocated since shape, type or size does not match *)


			(** file names and extentions *)
		FileTag = 0BBX;				(* same constants are defined in Linker and Loader *)
		NoZeroCompress = 0ADX;	(* do. *)
		FileVersion* = 0B1X;			(* do. *)
		FileVersionOC*=0B2X; (* fof, preparation for new compiler *)

		LocalUnicodeSupport* = истина;
		ExportedUnicodeSupport* = ложь;

		InitErrMsgSize = 300;	(* initial size of array of error messages *)

		MaxErrors = 100;	(* maximum number of diagnostic messages *)
		MaxWarnings = 100;

		InvalidCode* = -1;
		InvalidPosition* = Потоки.НевернаяПозицияВПотоке;

	тип
		SymReader* = Files.Reader;

		Rider* = запись
			symmodF, symF, objF, refF: Files.File;
			symmod, sym, obj, ref: Files.Writer;		(*temp modlist, temp symfile, main file*)
		кон;

		Attribute* = окласс кон Attribute;

		ErrorMsgs = укль на массив из StringPool.Index;

	перем
		bigEndian*: булево;
	(** fof >> *)
	tracebackOnError: булево;
	(** << fof  *)

			(** status *)
		codeOptions*, parserOptions*: мнвоНаБитахМЗ;
		error*: булево;		(** actual compilation status *)
		errors, warnings: цел32;	(* number of errors and warnings *)
		errMsg: ErrorMsgs;	(*error messages*)

			(** input *)
		breakpc*: цел32;	(** code offset to be found or MAX(SIGNED32) *)
		breakpos*: цел32;	(** text pos corresponding to breakpc (err 400 pos) *)

			(** output *)
		prefix*, suffix*: массив 128 из симв8;
			(** procedure to dump (/D option) *)
		dump*: массив 32 из симв8;

		source-: Files.FileName;
		log-: Потоки.Писарь;
		diagnostics-: Diagnostics.Diagnostics;

(** ---------- low level functions --------------------- *)

	проц GetProcessID*(): адресВПамяти;
	нач
		возврат НИЗКОУР.подмениТипЗначения(адресВПамяти, Objects.ActiveObject())
	кон GetProcessID;


(** ---------- file IO functions --------------------- *)

	проц MakeFileName(перем file: массив из симв8; конст name, prefix, suffix: массив из симв8);
		перем i, j: цел32;
	нач
		i := 0; нцПока prefix[i] # 0X делай  file[i] := prefix[i];  увел(i)  кц;
		j := 0; нцПока name[j] # 0X делай  file[i+j] := name[j];  увел(j)  кц;
		увел(i, j);
		j := 0; нцПока suffix[j] # 0X делай file[i+j] := suffix[j]; увел(j)  кц;
		file[i+j] := 0X;
	кон MakeFileName;

	проц WriteString(w: Потоки.Писарь; конст s: массив из симв8);
		перем i: цел16; ch: симв8;
	нач
		i:=0; ch:=s[0];
		нцПока ch # 0X делай
			w.пСимв8(ch); увел(i); ch := s[i];
		кц;
		w.пСимв8(0X);
	кон WriteString;

	(** OpenSymFile - Open a symfile for reading *)

	проц OpenSymFile*(конст name: массив из симв8;  перем r: SymReader;  перем version: симв8; перем zeroCompress: булево): булево;
		перем res: булево;  file: Files.FileName;  f: Files.File; dummy: цел32; ch: симв8;
	нач
		res := ложь; zeroCompress := истина;
		MakeFileName(file, name, prefix, suffix);
		f := Files.Old(file);
		если f # НУЛЬ то
			Files.OpenReader(r, f, 0);
			r.чСимв8(ch);
			если ch = FileTag то
				r.чСимв8(version);
				если version = NoZeroCompress то
					zeroCompress := ложь;
					r.чСимв8(version);
				всё;
				если version = FileVersion то
				r.чЦел32_7б(dummy);	(*skip symfile size*)
				аесли version = FileVersionOC то
				r.чЦел32_мз(dummy);
				всё;
				res := истина
			всё
		всё;
		возврат res
	кон OpenSymFile;

	проц SymW*(перем R: Rider; ch: симв8);
	нач  R.sym.пСимв8(ch)  кон SymW;

	проц SymWNum*(перем R: Rider; i: цел32);
	нач  R.sym.пЦел64_7б(i)  кон SymWNum;

	проц SymWSet*(перем R: Rider; s: мнвоНаБитахМЗ);
	нач  R.sym.пЦел64_7б(цел32( s))  кон SymWSet;

	проц SymWString*(перем R: Rider; конст str: массив из симв8);
	нач  WriteString(R.sym, str)  кон SymWString;

	проц SymWMod*(перем R: Rider; конст str: массив из симв8);
	нач  WriteString(R.symmod, str) кон SymWMod;

	проц SymWReal*(перем R: Rider; r: вещ32);
	нач  R.sym.пВещ32_мз(r)  кон SymWReal;

	проц SymWLReal*(перем R: Rider; r: вещ64);
	нач  R.sym.пВещ64_мз(r)  кон SymWLReal;


	проц ObjWGetPos*(перем R: Rider; перем pos: Files.Position);
	нач pos := R.obj.МестоВПотоке()
	кон ObjWGetPos;

	проц ObjW*(перем R: Rider; ch: симв8);
	нач  R.obj.пСимв8(ch)
	кон ObjW;

	проц ObjWNum*(перем R: Rider; i: цел32);
	нач R.obj.пЦел64_7б(i)
	кон ObjWNum;

	проц ObjWInt*(перем R: Rider; i: цел16);
	нач R.obj.пЦел16_мз(i)
	кон ObjWInt;

	проц ObjWIntAt*(перем R: Rider; pos: цел32; i: цел16);
		перем w: Files.Writer;
	нач
		R.obj.ПротолкниБуферВПоток;
		Files.OpenWriter(w, R.objF, pos);
		w.пЦел16_мз(i);
		w.ПротолкниБуферВПоток
	кон ObjWIntAt;

	проц ObjWLInt*(перем R: Rider; i: цел32);
	нач R.obj.пЦел32_мз(i)
	кон ObjWLInt;

	проц ObjWLIntAt*(перем R: Rider; pos: Files.Position; i: цел32);
		перем w: Files.Writer;
	нач
		R.obj.ПротолкниБуферВПоток;
		Files.OpenWriter(w, R.objF, pos);
		w.пЦел32_мз(i);
		w.ПротолкниБуферВПоток
	кон ObjWLIntAt;

	проц ObjWName*(перем R: Rider; конст str: массив из симв8);
	нач R.obj.пСтроку8˛включаяСимв0(str)
	кон ObjWName;

	проц RefW*(перем R: Rider; ch: симв8);
	нач R.ref.пСимв8(ch)
	кон RefW;

	проц RefWNum*(перем R: Rider; i: цел32);
	нач R.ref.пЦел64_7б(i)
	кон RefWNum;

	проц RefWString*(перем R: Rider; конст str: массив из симв8);
	нач R.ref.пСтроку8˛включаяСимв0(str)
	кон RefWString;


	проц Open*(конст name: массив из симв8; перем R: Rider; перем version: симв8);
		перем file: Files.FileName;
	нач
		MakeFileName(file, name, prefix, suffix);
		R.symmodF := Files.New("");
		R.symF := Files.New("");
		R.objF := Files.New(file);
		R.refF := Files.New("");
		Files.OpenWriter(R.symmod, R.symmodF, 0);
		Files.OpenWriter(R.sym, R.symF, 0);
		Files.OpenWriter(R.obj, R.objF, 0);
		Files.OpenWriter(R.ref, R.refF, 0);
		R.obj.пСимв8(FileTag);
		R.obj.пСимв8(NoZeroCompress);
		R.obj.пСимв8(version)
	кон Open;

	проц AppendFile(f: Files.File;  to: Потоки.Писарь);
		перем buffer: массив 1024 из симв8;  r: Files.Reader;  read: размерМЗ;
	нач
		Files.OpenReader(r, f, 0);
		нцДо
			r.чБайты(buffer, 0, 1024, read);
			to.пБайты(buffer, 0, read)
		кцПри read # 1024
	кон AppendFile;

	проц CloseSym*(перем R: Rider);
	нач
		R.symmod.ПротолкниБуферВПоток;	(* flush buffers to file *)
		R.sym.ПротолкниБуферВПоток;
(*		IF OldFileFormat THEN
			R.obj.RawNum(R.symmod.sent + R.sym.sent);
		ELSE
			R.obj.RawNum(4 + R.symmod.sent + R.sym.sent);
			R.obj.RawSet(codeOptions)
		END; *)
		R.obj.пЦел64_7б(4 + R.symmod.отправленоБайт + R.sym.отправленоБайт);
		R.obj.пНеБолее32БитИзМнваНаБитах_мз(codeOptions);
		AppendFile(R.symmodF, R.obj);
		AppendFile(R.symF, R.obj)
	кон CloseSym;

	проц CloseObj*(перем R: Rider);
	нач
		R.ref.ПротолкниБуферВПоток;
		AppendFile(R.refF, R.obj);
		R.obj.ПротолкниБуферВПоток;
		Files.Register(R.objF)
	кон CloseObj;

	проц RefSize*(перем R: Rider): цел32;
	нач  возврат R.ref.МестоВПотоке()(цел32);
	кон RefSize;


(** ---------- text output functions --------------------- *)

	проц GetMessage (err: целМЗ; конст msg: массив из симв8; перем res: массив из симв8);
	перем str: массив 128 из симв8;
	нач
		копируйСтрокуДо0 (msg, res);
		если (errMsg # НУЛЬ) и (0 <= err) и (err < длинаМассива(errMsg)) то
			StringPool.GetString(errMsg[err], str);
			Strings.Append(res, "  ");
			Strings.Append(res, str);
		всё;
	кон GetMessage;

	проц TraceBackThis( eip, ebp: адресВПамяти );   (* do a stack trace back w.r.t. given instruction and frame pointers *)
	нач
		log.пВК_ПС;  log.пСтроку8( "##################" );
		log.пВК_ПС;  log.пСтроку8( "# Debugging.TraceBack #" );
		log.пВК_ПС;  log.пСтроку8( "##################" );
		log.пВК_ПС;  Reflection.StackTraceBack( log, eip, ebp, ebp, 0, истина , ложь );
		log.ПротолкниБуферВПоток;
	кон TraceBackThis;

	проц TraceBack*;   (* do a stack trace back starting at the calling instruction position *)
	нач
		TraceBackThis( Machine.CurrentPC(), НИЗКОУР.GetFramePointer() );
	кон TraceBack;

	проц Error* (err: целМЗ; pos: цел32;  конст msg: массив из симв8);
		перем str: массив 128 из симв8;
	нач {единолично}
		(** fof >> *)
		если tracebackOnError то TraceBack() всё;
		(** << fof  *)
		error := error или (err <= 400) или (err >= 404);
		если err = 400 то breakpos := pos всё;
		GetMessage (err, msg, str);
		если (err < 400) или (err > 403) то
			увел (errors);
			если errors > MaxErrors то
				возврат
			аесли errors = MaxErrors то
				err := InvalidCode; pos := InvalidPosition; str := "too many errors"
			всё;
			если diagnostics # НУЛЬ то
				diagnostics.Error (source, pos, str);
			всё;
		иначе
			если diagnostics # НУЛЬ то
				diagnostics.Information (source, pos, str);
			всё;
		всё;
	кон Error;

	проц ErrorN* (err: целМЗ; pos: цел32; msg: StringPool.Index);
		перем str: массив 256 из симв8;
	нач
		StringPool.GetString(msg, str);
		Error(err, pos, str)
	кон ErrorN;

	проц Warning* (err, pos: цел32;  конст msg: массив из симв8);
		перем str: массив 128 из симв8;
	нач {единолично}
		если ~(Warnings в parserOptions) то возврат всё;
		увел (warnings);
		если warnings > MaxWarnings то
			возврат
		аесли warnings = MaxWarnings то
			err := InvalidCode; pos := InvalidPosition; str := "too many warnings"
		иначе
			GetMessage (err, msg, str);
		всё;
		если diagnostics # НУЛЬ то
			diagnostics.Warning (source, pos, str);
		всё;
	кон Warning;

	проц LogW* (ch: симв8);
	нач log.пСимв8(ch)
	кон LogW;

	проц LogWStr* (конст str: массив из симв8);
	нач  log.пСтроку8(str)
	кон LogWStr;

	проц LogWStr0* (str: StringPool.Index);
	перем str0: массив 256 из симв8;
	нач
		StringPool.GetString(str, str0); LogWStr(str0)
	кон LogWStr0;

	проц LogWHex* (i: цел32);
	нач log.п16ричное(i, 0)
	кон LogWHex;

	проц LogWNum* (i: цел64);
	нач log.пЦел64(i, 0)
	кон LogWNum;

	проц LogWBool* (b: булево);
	нач
		если b то LogWStr("TRUE") иначе LogWStr("FALSE") всё
	кон LogWBool;

	проц LogWType* (p: динамическиТипизированныйУкль);
		перем name: массив 32 из симв8;
	нач
		PCDebug.GetTypeName(p, name); LogWStr(name)
	кон LogWType;

	проц LogWLn*;
	нач log.пВК_ПС
	кон LogWLn;

	проц LogFlush*;
	нач  log.ПротолкниБуферВПоток
	кон LogFlush;

(** ---------- configuration functions --------------------- *)

	(** Init - Prepare module for a new compilation *)

	проц Init*(конст s: массив из симв8; l: Потоки.Писарь; d: Diagnostics.Diagnostics);	(* don't assume Reset is executed *)
	нач
		копируйСтрокуДо0 (s, source);
		log := l;
		если log = НУЛЬ то Потоки.НастройПисаря( log, ЛогЯдра.ЗапишиВПоток ) всё;
		diagnostics := d;
		error := ложь;
		errors := 0; warnings := 0;
		PCDebug.ResetToDo;
	кон Init;

	(** Reset - allow deallocation of structures*)

	проц Reset*;
	нач
		PCDebug.ResetToDo;
	кон Reset;

	(** SetErrorMsg - Set message for error n *)

	проц SetErrorMsg*(n: цел32; конст msg: массив из симв8);
	нач
		если errMsg = НУЛЬ то нов(errMsg, InitErrMsgSize) всё;
		нцПока длинаМассива(errMsg^) < n делай Expand(errMsg) кц;
		StringPool.GetIndex(msg, errMsg[n])
	кон SetErrorMsg;

	проц Expand(перем oldAry: ErrorMsgs);
	перем
		len, i: размерМЗ;
		newAry: ErrorMsgs;
	нач
		если oldAry = НУЛЬ то возврат всё;
		len := длинаМассива(oldAry^);
		нов(newAry, len * 2);
		нцДля i := 0 до len-1 делай
			newAry[i] := oldAry[i];
		кц;
		oldAry := newAry;
	кон Expand;

	проц InitMod;
	нач
		PCDebug.ResetToDo
	кон InitMod;

проц SwapBytes*(перем p: массив из НИЗКОУР.октет; offset, len: цел32);
перем i: цел32;
	tmp: НИЗКОУР.октет;
нач
	нцДля i := 0 до (len-1) DIV 2 делай
		tmp := p[offset+i];
		p[offset+i] := p[offset+len-1-i];
		p[offset+len-1-i] := tmp;
	кц;
кон SwapBytes;

	проц MakeErrorFile*;
	перем f: Files.File; w: Files.Writer;
		msg, code: массив 256 из симв8; i: цел32;
	нач
		f := Files.New("Errors2.XML");
		если f # НУЛЬ то
			Files.OpenWriter(w, f, 0);
			нцПока i < длинаМассива(errMsg)-1 делай
				StringPool.GetString(errMsg[i], msg);
				w.пСтроку8("    <Error code="); w.пСимв8(симв8ИзКода(34));
				Strings.IntToStr(i, code); w.пСтроку8(code);
				w.пСимв8(симв8ИзКода(34)); w.пСтроку8(">");
				w.пСтроку8(msg);
				w.пСтроку8("</Error>");
				w.пВК_ПС;
				увел(i);
			кц;
			w.ПротолкниБуферВПоток;
			Files.Register(f);
		иначе
			ЛогЯдра.пСтроку8("Could not create file"); ЛогЯдра.пВК_ПС;
		всё;
	кон MakeErrorFile;

(** fof >> *)
	проц TracebackOnError*;
	нач
		tracebackOnError := ~tracebackOnError;
		если tracebackOnError то LogWStr( "TracebackOnError=TRUE" );  иначе LogWStr( "TracebackOnError=FALSE" ) всё;
		LogWLn;  LogFlush;
	кон TracebackOnError;
(** << fof  *)

нач
	Потоки.НастройПисаря( log, ЛогЯдра.ЗапишиВПоток );
	InitMod;
	prefix := "";
	копируйСтрокуДо0(Modules.extension[0], suffix)
кон PCM.

(*
	15.11.06	ug	new compiler option /S added, FileVersion incremented
	20.09.03	prk	"/Dcode" compiler option added
	24.06.03	prk	Remove TDMask (no need to mask typedescriptors)
	22.02.02	prk	unicode support
	22.01.02	prk	cosmetic changes, some constants renamed
	22.01.02	prk	ToDo list moved to PCDebug
	18.01.02	prk	AosFS used instead of Files
	10.12.01	prk	ENTIER: rounding mode set to chop, rounding modes caches as globals
	22.11.01	prk	improved flag handling
	19.11.01	prk	definitions
	23.07.01	prk	read error messages into stringpool
	05.07.01	prk	optional explicit NIL checks
	27.06.01	prk	StringPool cleaned up
	14.06.01	prk	type descs for dynamic arrays of ptrs generated by the compiler
	17.05.01	prk	Delegates
	26.04.01	prk	separation of RECORD and OBJECT in the parser
	25.04.01	prk	array allocation: if length < 0 then trap PCM.ArraySizeTrap
	30.03.01	prk	object file version changed to 01X
	29.03.01	prk	Java imports
*)
