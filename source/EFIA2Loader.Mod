модуль EFIA2Loader; (** AUTHOR "Matthias Frei"; PURPOSE "EFI A2 Boot Loader"; *)

использует
	НИЗКОУР, EFI, Machine := EFIMachine, EFILib, EFIFileProtocol, EFIGraphicsOutput, EFIGraphicalConsole, Трассировка;

конст
	traceDebug = истина;

	(* address to which the executable will be loaded to. Loaded to arbitrary address if  = -1 *)
	kernelAddress = 100000H;

	(* address at which the boot table is generated. Arbitrary if -1 *)
	bootTableAddress = -1;

перем
	bootTableBegin : адресВПамяти; (* address at which boot table is really located. Can be equal to bootTableAddress *)
	bootTableEnd : адресВПамяти; (* bootTableBegin + current size of bootTable - sizeof sentinel (=5) + 1. *)
	bootTableCfgStrEntry : адресВПамяти; (* address of the config string entry (type 8) of the boot table *)

	(* search a mode that with the preferred horizontal and vertical resolution. Returns the corresponding mode
	number or -1 if no such mode is found or if an error occurs *)
	проц SearchGraphicsMode(prot : EFIGraphicsOutput.Protocol; prefWidth, prefHeight, prefDepth, prefFormat : цел32; перем info: EFIGraphicsOutput.GraphicsModeInfo): цел32;
	перем
		mode : EFIGraphicsOutput.GraphicsMode;
		sizeofInfo : EFI.Int;
		maxMode : EFI.Int32;
		i : цел32;
		status : EFI.Status;
	нач
		(* the currently set mode stores the number of available modes. Valid mode numbers are 0 to maxMode-1 *)
		mode := prot.Mode;
		maxMode := mode.MaxMode;

		(* enumerate all modes *)
		нцДля i := 0 до maxMode-1 делай
			(* get info about mode i *)
			status := prot.QueryMode(prot, i, sizeofInfo, info); (* ignore sizeofInfo. Just some compatibility stuff *)
			если (status # EFI.Success) то
				возврат -1;
			всё;
			(* now check whether this is the mode we are looking for *)
(*			IF (prefWidth = info.HorizontalResolution) & (prefHeight = info.VerticalResolution)
					& (prefFormat = info.PixelFormat) & (prefDepth = 32) THEN (* all pixel formats are 32 bit *) *)
			если (prefWidth = info.HorizontalResolution) и (prefHeight = info.VerticalResolution)
					и (prefDepth = 32) то (* all pixel formats are 32 bit *)

				возврат i;
			всё;
		кц;
		возврат -1;
	кон SearchGraphicsMode;

	проц GetFrameBuffer(prefWidth, prefHeight, prefDepth, prefFormat : цел32; перем framebufAddr : адресВПамяти; перем framebufSize : размерМЗ; перем info: EFIGraphicsOutput.GraphicsModeInfo): EFI.Status;
	перем
		handle : EFI.Handle;
		handleBuf : массив 128 из EFI.Handle;
		handleBufSize, i : EFI.Int;
		prot : EFI.Protocol; goProt : EFIGraphicsOutput.Protocol;
		modeNumber : цел32;
		framebufPhysAddr : EFI.PhysicalAddress;
		status : EFI.Status;
	нач
		handleBufSize := длинаМассива(handleBuf)*размер16_от(EFI.Handle);
		status := EFI.table.BS.LocateHandle(EFI.ByProtocol, EFIGraphicsOutput.GUID, 0, handleBufSize, handleBuf);
		если status = EFI.Success то
			i := handleBufSize DIV размер16_от(EFI.Handle); (* probably either 0 or 1. *)
			нцПока i > 0 делай
				умень(i);
				handle := handleBuf[i];
				status := EFI.table.BS.HandleProtocol(handle, EFIGraphicsOutput.GUID, prot);
				goProt := НИЗКОУР.подмениТипЗначения(EFIGraphicsOutput.Protocol, prot);
				если status = EFI.Success то
					modeNumber := SearchGraphicsMode(goProt, prefWidth, prefHeight, prefDepth, prefFormat, info);
					если modeNumber >= 0 то
						status := goProt.SetMode(goProt, modeNumber);
						если (status = EFI.Success) и (goProt.Mode.Mode = modeNumber) то
							framebufPhysAddr := goProt.Mode.FrameBufferBase;
							framebufAddr := адресВПамяти(framebufPhysAddr);


							framebufSize := goProt.Mode.FrameBufferSize;
							возврат EFI.Success;
						всё;
					всё;
				всё;
			кц;
		всё;
		возврат EFI.Error;
	кон GetFrameBuffer;

	(* Prints all available graphics modes where PixelFormat # BltOnly *)
	проц PrintGraphicsModes;
	перем
		handle : EFI.Handle;
		handleBuf : массив 512 из EFI.Handle;
		handleBufSize, i: EFI.Int;  j : цел32;
		prot : EFI.Protocol; goProt : EFIGraphicsOutput.Protocol;
		mode : EFIGraphicsOutput.GraphicsMode;
		info : EFIGraphicsOutput.GraphicsModeInfo;
		sizeofInfo : EFI.Int;
		maxMode : EFI.Int32;
		status : EFI.Status;

	нач
		handleBufSize := длинаМассива(handleBuf)*размер16_от(EFI.Handle);
		status := EFI.table.BS.LocateHandle(EFI.ByProtocol, EFIGraphicsOutput.GUID, 0, handleBufSize, handleBuf);
		если status = EFI.Success то
			i := handleBufSize DIV размер16_от(EFI.Handle); (* probably either 0 or 1. *)
			если (i = 0) то
				Трассировка.пСтроку8(" - none - "); Трассировка.пВК_ПС;
			всё;
			нцПока i > 0 делай
				умень(i);
				handle := handleBuf[i];
				status := EFI.table.BS.HandleProtocol(handle, EFIGraphicsOutput.GUID, prot);
				goProt := НИЗКОУР.подмениТипЗначения(EFIGraphicsOutput.Protocol, prot);
				если status = EFI.Success то
					(* the currently set mode stores the number of available modes. Valid mode numbers are 0 to maxMode-1 *)
					mode := goProt.Mode;
					maxMode := mode.MaxMode;

					(* enumerate all modes *)
					нцДля j := 0 до maxMode-1 делай
						(* get info about mode j *)
						status := goProt.QueryMode(goProt, j, sizeofInfo, info); (* ignore sizeofInfo. Just some compatibility stuff *)
						если (status # EFI.Success) то
							возврат;
						всё;
						Трассировка.пЦел64(info.HorizontalResolution, 0); Трассировка.пСтроку8("x");
						Трассировка.пЦел64(info.VerticalResolution, 0); Трассировка.пСтроку8("x");
						Трассировка.пЦел64(32, 0); Трассировка.пСтроку8(" - ");
						просей info.PixelFormat из
							EFIGraphicsOutput.PFRGBX8Bit : Трассировка.пСтроку8("RGB");
							| EFIGraphicsOutput.PFBGRX8Bit : Трассировка.пСтроку8("BGR");
							| EFIGraphicsOutput.PFBitMask : Трассировка.пСтроку8("R: "); Трассировка.п16ричное(info.PixelBitmask.RedMask,8);
								Трассировка.пСтроку8("G: "); Трассировка.п16ричное(info.PixelBitmask.GreenMask,8);
								Трассировка.пСтроку8("B: "); Трассировка.п16ричное(info.PixelBitmask.BlueMask,8);
							| EFIGraphicsOutput.PFBltOnly : Трассировка.пСтроку8("Blocktransfer only - no physical framebuffer");
						всё;
						Трассировка.пВК_ПС;
					кц;
				всё;
			кц;
		иначе
			Трассировка.пСтроку8("Error while accessing GraphicsOutputProtocol.");Трассировка.пВК_ПС;
			EFILib.ReportError(status);
		всё;
	кон PrintGraphicsModes;

	(* Loads the configuration file and creates the corresponding boot-table. *)
	проц LoadBootTable(конст configFileName : массив из EFI.Char16; перем btAddr : адресВПамяти) : EFI.Status;
	перем
		configFile : EFIFileProtocol.Protocol;
		cfAddr, cfPos, cfEnd : адресВПамяти;
		btAddrPhys : EFI.PhysicalAddress;
		btPos : адресВПамяти;
		status : EFI.Status;

		type : цел32;
		fileSize : цел32;

		cfgname, cfgval : адресВПамяти;
		cfgnamelen, cfgvallen : цел32;

		parseError : булево;

		ramSize : EFI.Int64;

		проц ReportError(at : адресВПамяти);
		перем status : EFI.Status;
		нач
			parseError := истина;
			Трассировка.пСтроку8("Sytnax error in file ");
			status := EFI.table.ConOut.OutputString(EFI.table.ConOut, configFileName); (* configFileName is a Unicode string *)
			Трассировка.пСтроку8(" at position ");
			Трассировка.пАдресВПамяти(at);
			Трассировка.пВК_ПС;
		кон ReportError;

		проц IsWhitespace(ch : симв8) : булево;
		нач
			возврат (ch = ' ') или (ch = 09X) или (ch = 0AX); (* space, tab or newline *)
		кон IsWhitespace;

		(* searches the next configuration entry in the config file and returns the positions and lengths of its name and value *)
		проц ReadConfig (перем name : адресВПамяти; перем namelen : цел32; перем val : адресВПамяти; перем vallen : цел32) : булево;
		перем ch : симв8;
		нач
			(* skip whitespace *)
			если (cfPos # cfEnd) то НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos); всё;
			нцПока (cfPos # cfEnd) и IsWhitespace(ch) делай
				НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos);
			кц;

			если (ch = '~') то возврат ложь; всё;

			name := cfPos-1;
			namelen := 0;
			(* skip name *)
			нцПока (cfPos # cfEnd) и (ch # '=') и ~IsWhitespace(ch)  делай
				увел(namelen);
				НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos);
			кц;

			(* skip whitespace before = *)
			нцПока (cfPos # cfEnd) и IsWhitespace(ch) делай
				НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos);
			кц;
			если (ch # '=') то ReportError(cfPos - cfAddr); возврат ложь; всё;
			если (cfPos # cfEnd) то НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos); всё;

			(* skip whitespace after = *)
			нцПока (cfPos # cfEnd) и IsWhitespace(ch) делай
				НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos);
			кц;
			если (ch # '"') то ReportError(cfPos - cfAddr); возврат ложь; всё;
			если (cfPos # cfEnd) то НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos); всё;

			val := cfPos-1;
			vallen := 0;
			(* skip val *)
			нцПока (cfPos # cfEnd) и (ch # '"') делай
				увел(vallen);
				НИЗКОУР.прочтиОбъектПоАдресу(cfPos, ch); увел(cfPos);
			кц;
			если (ch # '"') то ReportError(cfPos - cfAddr); возврат ложь; всё;

			если  (name = cfEnd) или (val = cfEnd) то
				возврат ложь;
			всё;
			возврат истина;
		кон ReadConfig;

	нач
		(* open and load the configuration file *)
		configFile := EFILib.OpenFile(configFileName);
		если (configFile = НУЛЬ) то
			Трассировка.пСтроку8("Error: Could not find file ");
			status := EFI.table.ConOut.OutputString(EFI.table.ConOut, configFileName);
			Трассировка.пВК_ПС;
			возврат EFI.ErrNotFound;
		всё;
		cfAddr := -1; (* don't care *)
		status := EFILib.LoadFile(configFile, cfAddr);
		если (status # EFI.Success) то
			возврат status;
		всё;
		fileSize := устарПреобразуйКБолееУзкомуЦел(EFILib.GetFileSize(configFile));
		cfPos := cfAddr;
		cfEnd := cfAddr + fileSize;

		(* allocate memory for the boot table *)
		btAddrPhys := bootTableAddress;
		status := EFILib.AllocateMemory(btAddrPhys, устарПреобразуйКБолееУзкомуЦел(fileSize DIV EFI.PageSize) + 3); (* a few additional pages *)
		(* now bootTablePhAddr contains the base address of the allocated pages *)
		если status # EFI.Success то
			возврат status;
		всё;
		bootTableBegin := адресВПамяти(btAddrPhys);
		btAddr := bootTableBegin; (* out *)
		btPos := bootTableBegin;

		(* boot memory/top of low memory. Trying to stay compatible with OBL.Asm *)
		type := 3;
		НИЗКОУР.запиши32битаПоАдресу(btPos, type); увел(btPos, 4);
		НИЗКОУР.запиши32битаПоАдресу(btPos, 16); увел(btPos, 4); (* entry size *)
		НИЗКОУР.запиши32битаПоАдресу(btPos, 0); увел(btPos, 4); (* boot memory address (?) *)
		НИЗКОУР.запиши32битаПоАдресу(btPos, 640*1024); увел(btPos, 4); (* boot memory size (in bytes) *)

		(* free memory/extended memory size *)
		status := EFILib.GetMemorySize(ramSize);
		type := 4;
		НИЗКОУР.запиши32битаПоАдресу(btPos, type); увел(btPos, 4);
		НИЗКОУР.запиши32битаПоАдресу(btPos, 16); увел(btPos, 4); (* entry size *)
		НИЗКОУР.запиши32битаПоАдресу(btPos, 100000H); увел(btPos, 4); (* extended memory address *)
		НИЗКОУР.запиши32битаПоАдресу(btPos, устарПреобразуйКБолееУзкомуЦел(ramSize) - 100000H); увел(btPos, 4); (* extended memory size *)
		если traceDebug то
			Трассировка.пСтроку8("DEBUG: ramsize: "); Трассировка.п16ричное(устарПреобразуйКБолееУзкомуЦел(ramSize), 0); Трассировка.пСтроку8("H B"); Трассировка.пВК_ПС;
		всё;

		(* config strings; Parse the configuration file and copy the content to the boot table *)
		type := 8;
		(* write a config string entry 'header'. *)
		bootTableCfgStrEntry := btPos;
		НИЗКОУР.запиши32битаПоАдресу(btPos, type); увел(btPos, 4);
		НИЗКОУР.запиши32битаПоАдресу(btPos, 0); увел(btPos, 4); (* reserve space for the size field *)

		bootTableEnd := btPos;
		(* write trailer - will be overwritten by addconfig *)
		НИЗКОУР.запиши8битПоАдресу(btPos, 0); увел(btPos);
		НИЗКОУР.запиши32битаПоАдресу(btPos, -1); увел(btPos, 4);

		parseError := ложь;
		нцПока (ReadConfig(cfgname, cfgnamelen, cfgval, cfgvallen))  делай
			AddConfigA(cfgname, cfgnamelen, cfgval, cfgvallen);
		кц;

		если (parseError) то
			возврат EFI.Error;
		иначе
			возврат EFI.Success;
		всё;
	кон LoadBootTable;

	проц AddConfig(конст name, val : массив из симв8);
	перем strlenName, strlenVal : цел32;
	нач
		strlenName:=0; нцПока (name[strlenName] # 0X) и (strlenName < длинаМассива(name)) делай увел(strlenName); кц;
		strlenVal:=0; нцПока (val[strlenVal] # 0X) и (strlenVal < длинаМассива(val)) делай увел(strlenVal); кц;

		AddConfigA(адресОт(name[0]), strlenName, адресОт(val[0]), strlenVal);
	кон AddConfig;

	(* add a config string entry to the boot table. The table always ends with a termination symbol which is overwritten
	if another entry is added. The lengths must not include a terminating 0. *)
	проц AddConfigA(name : адресВПамяти; namelen : цел32; val : адресВПамяти; vallen : цел32);
	перем btEnd : адресВПамяти;
	cfgStrSize : размерМЗ;
	нач
		btEnd := bootTableEnd;
		НИЗКОУР.копируйПамять(name,btEnd,namelen);
		увел(btEnd,namelen);
		НИЗКОУР.запишиОбъектПоАдресу(btEnd, 0); увел(btEnd); (*separator *)
		НИЗКОУР.копируйПамять(val,btEnd,vallen);
		увел(btEnd,vallen);
		НИЗКОУР.запишиОбъектПоАдресу(btEnd, 0); увел(btEnd); (*separator *)

		увел(bootTableEnd, namelen + 1 + vallen + 1);

		НИЗКОУР.запишиОбъектПоАдресу(btEnd, 0); увел(btEnd); (* marks the end of the config string entry *)
		НИЗКОУР.запиши32битаПоАдресу(btEnd, -1); (* marks the end of the table *)
		(* do not increment bootTableEnd here again. the 0 and the -1 will be overwritten if another entry is added *)

		cfgStrSize := bootTableEnd + 1 - bootTableCfgStrEntry; (* size including the 0 byte of the trailer *)
		НИЗКОУР.запиши32битаПоАдресу(bootTableCfgStrEntry + 4, cfgStrSize);
	кон AddConfigA;

	(* search in the config string entries of the boot table (NOT in the config file) for the configuration with name 'name' *)
	проц GetConfig(конст name : массив из симв8; перем val : массив из симв8);
	перем btIdx : адресВПамяти; i : цел32; ch : симв8;
	нач
		btIdx := bootTableCfgStrEntry + 8; (* skip type and size fields *)
		(* copied from BIOS.I386.Machine.Mod - GetConfig *)
		нц
			НИЗКОУР.прочтиОбъектПоАдресу(btIdx,ch);
			если ch = 0X то прервиЦикл всё;
			i := 0;
			нц
				НИЗКОУР.прочтиОбъектПоАдресу(btIdx,ch);
				если (ch # name[i]) или (name[i] = 0X) то прервиЦикл всё;
				увел (i); увел (btIdx)
			кц;
			если (ch = 0X) и (name[i] = 0X) то	(* found: (src^ = 0X) & (name[i] = 0X) *)
				i := 0;
				нцДо
					увел (btIdx); НИЗКОУР.прочтиОбъектПоАдресу(btIdx,ch); val[i] := ch; увел (i);
					если i = длинаМассива(val) то val[i - 1] := 0X; возврат всё	(* val too short *)
				кцПри ch = 0X;
				val[i] := 0X; возврат
			иначе
				нцПока ch # 0X делай	(* skip to end of name *)
					увел (btIdx); НИЗКОУР.прочтиОбъектПоАдресу(btIdx,ch);
				кц;
				увел (btIdx);
				нцДо	(* skip to end of value *)
					НИЗКОУР.прочтиОбъектПоАдресу(btIdx,ch); увел (btIdx)
				кцПри ch = 0X
			всё
		кц;
		val[0] := 0X
	кон GetConfig;

	проц Allocate(allocAddr: EFI.PhysicalAddress; kernelPages: цел32): EFI.Status;
	перем allocAdrCopy : EFI.PhysicalAddress; chunkPages: цел32; status: EFI.Status;
	нач
		chunkPages := kernelPages;
		chunkPages := 1;
		нцДо
			allocAdrCopy := allocAddr; (* to protect allocAddr from being overwritten in AllocateMemory *)
			status := EFILib.AllocateMemory(allocAdrCopy, chunkPages);
			если status = EFI.Success то
				умень(kernelPages, chunkPages);
				allocAddr := allocAddr + EFI.PageSize*chunkPages;
			иначе
				трассируй(kernelPages, chunkPages, allocAddr, status);
				chunkPages := chunkPages DIV 2;
				если chunkPages > kernelPages то chunkPages := kernelPages всё;
			всё;
		кцПри (kernelPages = 0) или (chunkPages = 0);
		возврат status;
	кон Allocate;

	(* 	loads the kernel image to kernelAddress if possible. Otherwise loads it to somewhere else and return WarnWriteFailure.
		Allocates makes sure the memory at kernelRelocAddress is allocated (by us or by someone else) s.t.
		the boot table will certainly not be allocated there.
	*)
	проц LoadKernel(конст kernelFileName : массив из EFI.Char16; перем kernelAddr: адресВПамяти; перем kernelSize : цел32) : EFI.Status;
	перем
		loadAddr : адресВПамяти;
		allocAddr : EFI.PhysicalAddress;
		kernelImageFile : EFIFileProtocol.Protocol;
		kernelPages : цел32;
		i : цел32;
		status : EFI.Status;
	нач
		kernelImageFile := EFILib.OpenFile(kernelFileName);
		если (kernelImageFile # НУЛЬ) то
			kernelSize := устарПреобразуйКБолееУзкомуЦел(EFILib.GetFileSize(kernelImageFile));
			трассируй(kernelSize);
			kernelPages := (kernelSize DIV EFI.PageSize) + 1;

			(* allocate all memory at kernelRelocAddress *)
			если (kernelAddress # -1) то
				(*status := Allocate(kernelRelocAddress, kernelPages);*)
				трассируй(status);
				(*
				allocAddr := kernelRelocAddress;
				status := EFILib.AllocateMemory(allocAddr, kernelPages);
				TRACE(status);
				(* try to make sure all pages are allocated somehow *)
				IF (status # EFI.Success) THEN
					FOR i := 0 TO kernelPages DO
						status := EFI.Success;
						status := EFILib.AllocateMemory(allocAddr, 1);
						TRACE(status, allocAddr);
						allocAddr := allocAddr + EFI.PageSize;
					END;
				END;
				*)
			всё;

			loadAddr := kernelAddress;
			(*loadAddr := 400000H; *)

			allocAddr:= 0;  (*we use the variable to reserve page 0 and 1 *)
			status := EFILib.AllocateMemory(allocAddr, 2); (* allocate page 0 and 1 to be sure the kernel is not put there *)
			если status # EFI.Success то
				Трассировка.пСтроку8("could not allocate page 0 and 1 - this might be a problem for the relocation process");
			всё;
			(*
			loadAddr := -1;  (*we let EFI decide where to put it *)
			*)

			status := EFILib.LoadFile(kernelImageFile, loadAddr);
			если (status = EFI.Success ) то
				трассируй(loadAddr);
				kernelAddr := loadAddr;
				трассируй(kernelAddress);
				возврат EFI.Success;
			иначе
				трассируй("could not load kernel with fixed adr");
				(* try to recover: load it anywhere and relocate it later. make sure all pages at kernelAddress are allocated *)
				(*status := Allocate(kernelAddress, kernelPages);*)
				трассируй(status);
				(*
				allocAddr := kernelAddress;
				FOR i := 0 TO kernelPages DO
					status := EFI.Success;
					status := EFILib.AllocateMemory(allocAddr, 1);
					TRACE(status, allocAddr);
					allocAddr := allocAddr + EFI.PageSize;
				END;
				*)

				loadAddr := -1;
				status := EFILib.LoadFile(kernelImageFile, loadAddr);
				трассируй(status);
				если (status = EFI.Success) то
					трассируй("loaded kernel to ", loadAddr);
					kernelAddr := loadAddr;
					трассируй(kernelAddress);
					возврат EFI.WarnWriteFailure;
				иначе
					возврат EFI.Error;
				всё;
			всё;
		иначе
			Трассировка.пСтроку8("Error: Could not find file ");
			status := EFI.table.ConOut.OutputString(EFI.table.ConOut, kernelFileName);
			Трассировка.пВК_ПС;
			возврат EFI.ErrNotFound;
		всё;
	кон LoadKernel;

	проц LoadA2;
	перем
		kernelAddr, btAddr, fbAddr, highAddr, memmapAddr: адресВПамяти;
		fbSize: EFI.Int;
		adr: EFI.PhysicalAddress;
		kernelSize: цел32;
		kernelStat, btStat, fbStat, status, memmapStat : EFI.Status;
		kernelFileName, configFileName, arg : массив 128 из EFI.Char16;
		val : массив 100 из симв8;
		i: размерМЗ; dWidth, dHeight, dDepth, dFormat : цел32;
		size: EFI.Int64;
		buf : массив 100 из EFI.Char16;
		fbInfo: EFIGraphicsOutput.GraphicsModeInfo;
	нач
		Трассировка.пСтроку8("Starting"); Трассировка.пВК_ПС;

		status := EFILib.GetMemorySize(size);
		трассируй(size);

		если EFILib.GetNextArg(kernelFileName) и EFILib.GetNextArg(configFileName) то
			(* load the kernel file into memory *)
			kernelStat := LoadKernel(kernelFileName, kernelAddr, kernelSize);
			(* parse the configuration file and generate the boot table *)
			btStat := LoadBootTable(configFileName, btAddr);

			если (btStat = EFI.Success) то
				(* get the configurations for the framebuffer and get the address of the corresponding framebuffer *)
				GetConfig("DWidth",val); i:=0; dWidth := EFILib.StringToInt(i, val);
				GetConfig("DHeight",val); i:=0; dHeight := EFILib.StringToInt(i, val);
				GetConfig("DDepth",val); i:=0; dDepth := EFILib.StringToInt(i, val);
				dFormat := EFIGraphicsOutput.PFBGRX8Bit;  (* Displays.Mod uses BGR *)

				fbStat := GetFrameBuffer(dWidth,dHeight,dDepth, dFormat, fbAddr, fbSize, fbInfo);
				трассируй(fbAddr);

(*
				EFIGraphicalConsole.Init;
				EFIGraphicalConsole.SetupConsole(fbInfo, fbAddr);
*)

				если (fbStat = EFI.Success) то
					(* Does not work because IntToString does not function properly for large numbers.
					GetConfig("DMem", val);
					IF (val = "") THEN
						Trace.String("DEBUG: fbSize:"); Trace.Int(fbSize, 0); Trace.Ln;
						EFILib.IntToString(fbSize, val);
						AddConfig("DMem", val);
					ELSE
						i:= 0;
						IF (fbSize # EFILib.StringToInt(i, val)) THEN
							Trace.String("Warning: Configurated framebuffer size DMem = ");
							Trace.String(val);
							Trace.String(" does not match actual framebuffer size of ");
							Trace.Int(fbSize, 0);
							Trace.String("."); Trace.Ln;
						END;
					END;

					*)
				иначе
					Трассировка.пСтроку8("Warning: Requested display mode ");
					Трассировка.пЦел64(dWidth,0); Трассировка.пСтроку8("x"); Трассировка.пЦел64(dHeight,0); Трассировка.пСтроку8("x"); Трассировка.пЦел64(dDepth,0);
					Трассировка.пСтроку8(" not available in BGR mode. Available modes are :"); Трассировка.пВК_ПС;
					PrintGraphicsModes; Трассировка.пВК_ПС;
					Трассировка.пСтроку8("Continuing happily."); Трассировка.пВК_ПС;
				всё;
			всё;
			если traceDebug то
				Трассировка.пСтроку8("DEBUG: Kernel at ");Трассировка.пАдресВПамяти(kernelAddr);Трассировка.пВК_ПС;
				Трассировка.пСтроку8("DEBUG: BootTable at ");Трассировка.пАдресВПамяти(btAddr);Трассировка.пВК_ПС;
				Трассировка.пСтроку8("DEBUG: FrameBuffer at "); Трассировка.пАдресВПамяти(fbAddr); Трассировка.пВК_ПС;
				(*
				Trace.String("DEBUG: boot table:"); Trace.Ln; Trace.Memory(bootTableBegin, bootTableEnd-bootTableBegin + 5);
				*)
			всё;

			если (EFILib.GetNextArg(arg) и (симв8ИзКода(arg[0]) = '-') и (симв8ИзКода(arg[1]) = 'd')) то
				Трассировка.пВК_ПС;
				Трассировка.пСтроку8("Dry Run Complete"); Трассировка.пВК_ПС;
				возврат;
			всё;

			(*
			Trace.Memory(kernelAddress, 100);
			*)

			memmapStat := EFILib.GetMemoryMapping(adr);
			memmapAddr := адресВПамяти(adr);
			трассируй(memmapAddr);
			трассируй(fbAddr);
			если (memmapStat = EFI.Success ) то
				Трассировка.пВК_ПС;
				Трассировка.пСтроку8("saved memory mappings");
			иначе
				Трассировка.пВК_ПС;
				Трассировка.пСтроку8("failed to saved memory mappings");
			всё;
			PrintGraphicsModes; Трассировка.пВК_ПС;

			если ((kernelStat = EFI.Success) или (kernelStat = EFI.WarnWriteFailure)) и (btStat = EFI.Success) то
				трассируй(kernelAddr, btAddr, kernelSize, fbAddr);

				Трассировка.пСтроку8("Shutting down Boot Services - WTF?!"); Трассировка.пВК_ПС;
				трассируй(fbInfo);
				status := EFILib.ExitBootServices();

				если status = EFI.Success то
					(* sshhhhhht!!!  nomore EFITrace from here on *)

					утв(fbInfo # НУЛЬ);
					Machine.JumpTo(kernelAddr, btAddr (* eax *) , kernelSize (* esi *) , fbAddr (* edi *), memmapAddr (* ecx *), НИЗКОУР.подмениТипЗначения(адресВПамяти, fbInfo)(* edx *) );

					Трассировка.пСтроку8("returned ??"); Трассировка.пВК_ПС;
					(*
					SYSTEM.MOVE(kernelAddr, kernelAddress, kernelSize);



					kernelAddr := kernelAddress;
					IF (kernelStat = EFI.WarnWriteFailure) THEN
						(* couldn't allocate suitable memory portion before. Relocate it to the correct load address, EFI is not running anymore! *)

					END;
					Machine.JumpTo(kernelAddr, btAddr, 0, fbAddr);*)
				иначе
					Трассировка.пСтроку8("Could not exit boot services"); Трассировка.пВК_ПС;
				всё;
			иначе
				Трассировка.пСтроку8("Error! ");
				если ~((kernelStat = EFI.Success) или (kernelStat = EFI.WarnWriteFailure)) то
					Трассировка.пСтроку8("Kernel could not be loaded. ");
				всё;
				если ~(btStat = EFI.Success) то
					Трассировка.пСтроку8("Boot table could not be loaded. ");
				всё;
				Трассировка.пСтроку8("Aborting"); Трассировка.пВК_ПС;
			всё;
		иначе
			Трассировка.пСтроку8("Arguments not correct. Usage:");Трассировка.пВК_ПС;Трассировка.пСтроку8("name.efi kernel config"); Трассировка.пВК_ПС;
		всё;

		(* If we come here, something went wrong! Cleanup the memory, such that we can try another time *)
		EFILib.FreeMemory;
	кон LoadA2;

нач
	LoadA2;
кон EFIA2Loader.

PET.Open EFI.Tool ~
