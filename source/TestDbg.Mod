MODULE TestDbg;
 IMPORT SYSTEM, KernelLog, Random, Kernel, Modules;

TYPE
	TestRec = RECORD
		tint:SIGNED32;
		treal:FLOAT32;
		tarr1:ARRAY 10 OF SIGNED32;
		tarr2:ARRAY 10,10 OF FLOAT32;
		tarr3:ARRAY 80 OF CHAR;
	END;
	TestObj* = OBJECT
		VAR
			oint*:SIGNED32;
			oreal*:FLOAT32;
			oarr1*:ARRAY 10 OF SIGNED32;
			oarr2:ARRAY 10,10 OF FLOAT32;
			oarr3*:ARRAY 80 OF CHAR;
		(** *)
		PROCEDURE &Init;
		VAR
		BEGIN
			
		END Init;
		
	END TestObj;
	
VAR
 globb:FLOAT32;
 globa,globc:SIGNED32;
 globt:TestRec;
 globo:TestObj;
 
PROCEDURE Test*;
VAR
 a:SIGNED32;
BEGIN
 a:=12;
 KernelLog.String("ds= "); KernelLog.Int(a,4); KernelLog.Ln;
END Test;

PROCEDURE TestTrap*;
VAR
	LocVar:SIGNED32;
	m, i, j, ofs, t, res : SIGNED32;
BEGIN
	LocVar:=45H;
	CODE 
		PUSH 3
		INT 3	
		END;
END TestTrap;
TYPE
	GenericArray* = POINTER TO ARRAY OF SIGNED32;
	(** has to return true iff obj1 occurs before obj2 *)
	GenericCompareFunct* = PROCEDURE {DELEGATE} (obj1, obj2: SIGNED32): BOOLEAN;


PROCEDURE QuickSortRec(VAR genArray: GenericArray; comp: GenericCompareFunct; lo, hi: SIGNED32);
VAR i, j: SIGNED32; x, t: SIGNED32;
BEGIN
	i := lo; j := hi;
	x := genArray[(lo+hi) DIV 2];

	WHILE (i <= j) DO
		WHILE (comp(genArray[i], x)) DO INC(i) END;
		WHILE (comp(x, genArray[j])) DO DEC(j) END;
		IF (i <= j) THEN
			t := genArray[i]; genArray[i] := genArray[j]; genArray[j] := t;
			INC(i); DEC(j)
		END
	END;

	IF (lo < j) THEN QuickSortRec(genArray, comp, lo, j) END;
	IF (i < hi) THEN QuickSortRec(genArray, comp, i, hi) END
END QuickSortRec;

PROCEDURE CompareFunct(obj1, obj2: SIGNED32): BOOLEAN;
BEGIN
	RETURN obj1<obj2;
END CompareFunct;

PROCEDURE QuickSort*(VAR genArray: GenericArray; compFunc: GenericCompareFunct);
BEGIN
	QuickSortRec(genArray, compFunc, 0, LEN(genArray)-1)
END QuickSort;

VAR
	arr:GenericArray;
PROCEDURE TestSort*;
	PROCEDURE Dump;
	VAR
		i:SIGNED32;
	BEGIN
		FOR i:=0 TO LEN(arr)-1 DO
			KernelLog.Int(arr[i],6);
		END;
		KernelLog.Ln;
	END Dump;
VAR
	i:SIGNED32;
	random : Random.Generator;
BEGIN
	NEW(arr,10);NEW(random);
	FOR i:=0 TO LEN(arr)-1 DO
		arr[i]:=random.Dice(5000);
	END;
	Dump;
	QuickSort(arr, CompareFunct);
	Dump;
END TestSort;

PROCEDURE TestLng(s:ARRAY OF CHAR);
VAR
 b:FLOAT32;
 a,c:SIGNED32;
PROCEDURE TestSubProc(c:SIGNED32):SIGNED32;
VAR
 a1,a2:SIGNED32;
BEGIN
	c:=0;
(*	a1:=a1 DIV c;*)
	a1:=a+c;
	a2:=a-c;
 IF a1>a2 THEN	
 	RETURN a;
 ELSE
 	RETURN	a2;
 END;
END TestSubProc;
BEGIN
	FOR a:=1 TO 28 DO
		b:=23;
		globb:=b;
		c:=-23;
		globc:=c;
		s:="test";
		b:=(a+b)/c*a-(a-b)/b*c;
		globb:=b;
		a:=TestSubProc(c);
		IF (b> ((a-b)/c*a-(a-b)/b*c) )&(s="test") THEN
			WHILE a<b DO
				INC(a);
				globa:=a;
			END;
		ELSIF s="test2" THEN
			REPEAT
				DEC(a);
			UNTIL b>a;
		 ELSE
			CASE a OF
				|1:b:=1;
				|2:b:=3;
			ELSE
				b:=a/5;
			END;
		END;
	END; 
END TestLng;

PROCEDURE TestTry*;
VAR
 a:SIGNED32;
BEGIN
		 a:=12;
		 TestLng("test");
	FINALLY
		 KernelLog.String("ds= "); KernelLog.Int(a,4); KernelLog.Ln;
END TestTry;


PROCEDURE Test2*;
VAR
			pc:SIGNED32;
BEGIN
	pc:=136H;
END Test2;

PROCEDURE TestBreak*;
VAR
	timer : Kernel.Timer;
BEGIN
	NEW(timer);

	LOOP
		timer.Sleep(100);
		KernelLog.Char('*'); 
	END;
END TestBreak;

PROCEDURE TestBreak2*;
VAR
	timer : Kernel.Timer;
	i:SIGNED32;
BEGIN
	i:=0;
	LOOP
	 INC(i);
	END;
END TestBreak2;


BEGIN
	globb:=3.14;
 	globa:=1234;
 	globc:=01234H;
 	NEW(globo)
END TestDbg.TestTry~


TestDbg.TestModule ~
TestDbg.Test~
TestDbg.Test2~
TestDbg.TestTrap~
TestDbg.TestProc~

TestDbg.TestSort~
TestDbg.TestBreak~

TestDbg.TestBreak2~

