модуль WMSlideshow;
(****
 *
 *  A simple slideshow/presentation tool with transition effect (see: WMTransitions.Mod).
 *
 *  Represents a  Model-View-Controller pattern
 *	(some controlling parts are also in the view):
 *	- Controller	= SlideshowApp
 *	- Model		= SlideshowData
 *	- View		= SlideshowWindow & SlideshowNavigation
 *
 *  Keyboard and mouse controls:
 *		- Next:		Spacebar/LeftMouseButton/PageDown/RightArrow
 *		- Previous:	PageUp/LeftArrow
 *		- First:		Home/UpArrow
 *		- Last:		End/DownArrow
 *		- Exit:		ESC
 *		- (Re)Open navigation panel: "n"
 *		- (Re)Open slide window: "w"
 *		- Show/Dump internal file list: "l"
 *
 *
 * Usage description:
 *	Drag & Drop new images on navigator window or use a predefinied XML file.
 *
 ****)

использует
	Codecs, Inputs, Modules, Потоки, ЛогЯдра, Files, Commands,
	Raster,
	Strings,
	WMDropTarget, (* Drag&Drop interface *)
	WMWindowManager, WMGraphics, WMRectangles,
	WMComponents, WMStandardComponents, WMDialogs,
	WMTransitions,
	XML, XMLObjects, XMLScanner, XMLParser;
конст
	DEBUG= ложь;

(****
 *
 *  Just some type alias (typedefs)
 *
 ****)
тип String =  Strings.String;
тип Image =  WMGraphics.Image;
тип TransitionMask =  WMTransitions.TransitionMask;
тип TransitionFade =  WMTransitions.TransitionFade;
тип ObjectArray = укль на массив из динамическиТипизированныйУкль;   (* Data Container for List Object *)

(****
 *
 *  A Slide contains:
 *	- an image filename
 *	- a transition effect to the next slide (optional)
 *	- duration of the transition (optional) STILL IN FRAMES -> MS IS BETTER
 *	- a short description (optional)
 *
 ****)
тип Slide* = окласс
	перем
		img, trans	: String;
		dur			: цел32;
		desc		: String;
	проц &New*(img : String;   trans : String; dur : цел32;   desc : String);
	нач
		сам.img := img; сам.trans := trans; сам.dur := dur; сам.desc := desc;
	кон New;
кон Slide;


(****
 *
 *  Generic Lockable Object List
 *
 *  Author		: TF (-> TFClasses.Mod), with a few modification by Reto Ghioldi
 *  Purpose		: Generic helper (similar to the well known vector class)
 *  Note			: Needed userdefinied initial size of the vector
 *
 ****)
тип List* = окласс
	перем
		list : ObjectArray;
		count : цел32;
		readLock : цел32;

	проц &New*(size: цел32);
	нач
		нов(list, size); readLock := 0
	кон New;

	(* ****
	 *
	 * Return the number of objects in the list. If count is used for indexing elements
	 * (e.g. FOR - Loop) in a multi-process situation, the process calling the GetCount method
	 * should call Lock before GetCount and Unlock after the last use of an index based on GetCount
	 *
	 *** *)
	проц GetCount*():цел32;
	нач
		возврат count
	кон GetCount;

	проц Grow;
	перем
		old: ObjectArray; i : цел32;
	нач
		old := list;  нов(list, длинаМассива(list)*2);
		нцДля i := 0 до count-1 делай list[i] := old[i] кц;
	кон Grow;

	(* ***
	 *
	 * Add an object to the list. Add may block if number of calls to Lock is bigger than the number of calls to Unlock
	 *
	 *** *)
	проц Add*(x : динамическиТипизированныйУкль);
	нач {единолично}
		дождись(readLock = 0);
		если (count = длинаМассива(list)) то Grow всё; list[count] := x;  увел(count);
	кон Add;

	(* ***
	 *
	 * Atomic replace x by y
	 *
	 *** *)
	проц Replace*(x, y : динамическиТипизированныйУкль);
	перем
		i : цел32;
	нач {единолично}
		дождись(readLock = 0);
		i := IndexOf(x);  если (i >= 0) то list[i] := y всё;
	кон Replace;

	(* ***
	 *
	 * Return the index of an object. In a multi-process situation, the process calling the IndexOf method
	 * should call Lock before IndexOf and Unlock after the last use of an index based on IndexOf.
	 *
	 * If the object is not found, -1 is returned
	 *
	 *** *)
	проц IndexOf *(x:динамическиТипизированныйУкль) : цел32;
	перем
		i : цел32;
	нач
		i := 0 ;
		нцПока (i < count) делай   если (list[i] = x) то возврат i всё;   увел(i); кц;
		возврат -1;
	кон IndexOf;

	(* ***
	 *
	 * Remove an object from the list. Remove may block if number of calls to Lock is bigger than the
	 *  number of calls to Unlock
	 *
	 *** *)
	проц Remove*(x : динамическиТипизированныйУкль);
	перем i : цел32;
	нач {единолично}
		дождись(readLock = 0);
		i:=0;
		нцПока ( (i<count) и (list[i]#x) ) делай увел(i)  кц;
		если (i<count) то
			нцПока (i<count-1) делай  list[i]:=list[i+1]; увел(i);  кц;
			умень(count); list[count]:=НУЛЬ
		всё
	кон Remove;

	проц RemoveByIndex*(index : цел32);
	перем i : цел32;
	нач {единолично}
		дождись(readLock = 0);
		i := index;
		если (i >= 0) и (i < count) то
			нцПока (i<count-1) делай  list[i]:=list[i+1]; увел(i);  кц;
			умень(count); list[count]:=НУЛЬ;
		всё;
	кон RemoveByIndex;

	(* ***
	 *
	 * Removes all objects from the list. Clear may block if number of calls to Lock is bigger than the
	 *  number of calls to Unlock
	 *
	 *** *)
	проц Clear*;
	перем i : цел32;
	нач {единолично}
		дождись(readLock = 0);
		нцДля i := 0 до count - 1 делай  list[i] := НУЛЬ;  кц;
		count := 0
	кон Clear;

	(* ***
	 *
	 * Return an object based on an index. In a multi-process situation, GetItem is only safe in a locked
	 * region Lock / Unlock
	 *
	 *** *)
	проц GetItem*(i:цел32) : динамическиТипизированныйУкль;
	нач
		утв((i >= 0) и (i < count), 101);
		возврат list[i];
	кон GetItem;

	(* ***
	 *
	 * Lock prevents modifications to the list. All calls to Lock must be followed by a call to Unlock.
	 * Lock can be nested.
	 *
	 *** *)
	проц Lock*;
	нач {единолично}
		увел(readLock);
		утв(readLock > 0);
	кон Lock;

	(* ***
	 *
	 * Unlock removes one modification lock. All calls to Unlock must be preceeded by a call to Lock.
	 *
	 *** *)
	проц Unlock*;
	нач {единолично}
		умень(readLock);
		утв(readLock >= 0);
	кон Unlock;
кон List;





(****
 *
 *  The slideshow application
 *
 ****)
тип SlideshowApp= окласс
	перем
		data : SlideshowData;
		win : SlideshowWindow;
		nav : SlideshowNavigation;
		slideNr : цел32;
		fullscreen : булево;
	(*****
	 *
	 *  Constructor
	 *
	 *****)
	проц &New*(конст filename : массив из симв8);
	нач
		нов(data);
		(* Load slides via drag & drop *)
		если (filename # "") то
			data.LoadSlideshow(filename);
		всё;
		если app = НУЛЬ то  app := сам  всё;  (* fld, adapt to new semantc of NEW *)
		(* Create a application window *)
		нов(win, 320, 240, ложь, data);
		fullscreen := ложь;
		WMWindowManager.DefaultAddWindow(win);
		нов(nav, data);
		WMWindowManager.DefaultAddWindow(nav);
		slideNr := 0;
	кон New;

	(*****
	 *
	 *  Handles the navigation inputs from the views
	 *
	 *****)
	проц Next;
	нач
		если (data.CountSlides() = 0) то возврат; всё;
		если ( slideNr < data.CountSlides() ) то
			win.Show(slideNr+1);
			увел(slideNr);
			nav.UpdatePreview();
		всё;
	кон Next;

	проц Previous;
	нач
		если (data.CountSlides() = 0) то возврат; всё;
		slideNr := slideNr-1;
		если (slideNr < 0) то slideNr := 0; возврат; всё;
		win.Update();
		nav.UpdatePreview();
	кон Previous;

	проц First;
	нач
		если (data.CountSlides() = 0) то возврат; всё;
		slideNr := 0;
		win.Update();
		nav.UpdatePreview();
	кон First;

	проц Last;
	нач
		если (data.CountSlides() = 0) то возврат; всё;
		slideNr := data.CountSlides()-1;
		если (slideNr< 0) то slideNr := 0; всё;
		win.Update();
		nav.UpdatePreview();
	кон Last;


	(*****
	 *
	 *  Handles the important keyboard events from the views
	 *
	 *****)
	проц ToggleFullscreen;
	перем
		view : WMWindowManager.ViewPort;
		manager : WMWindowManager.WindowManager;
		w, h : размерМЗ;
	нач
		если (win = НУЛЬ) то возврат; всё;
		fullscreen := ~fullscreen;
		manager := WMWindowManager.GetDefaultManager();
		view := WMWindowManager.GetDefaultView();
		если (fullscreen) то
			w := округлиВниз(view.range.r - view.range.l);
			h := округлиВниз(view.range.b - view.range.t);
			manager.SetWindowSize(win, w, h);
			manager.SetWindowPos(win, округлиВниз(view.range.l), округлиВниз(view.range.t));
			win.Resized(w, h);
			win.Invalidate( WMRectangles.MakeRect(0, 0, w, h) );
		иначе
			w := win.img.width;  h := win.img.height;
			manager.SetWindowSize(win, w, h);
			manager.SetWindowPos(win, округлиВниз(view.range.l)+50, округлиВниз(view.range.t)+50);
			win.Resized(w, h);
			win.Invalidate( WMRectangles.MakeRect(0, 0, w, h) );
		всё;
	кон ToggleFullscreen;

	(*****
	 *
	 *  Outputs the internal filelist of the slideshow
	 *
	 *****)
	проц ShowFileList;
	перем
		dummy : массив 2048 из симв8;
		nl : массив 2 из симв8;
		slide : Slide;
		i : цел32;
	нач
		nl[0] := 0DX; nl[1] := 0X;
		dummy[0] := 0X;
		нцДля i := 0 до data.CountSlides()-1 делай
			slide := data.GetSlide(i);
			Strings.Append(dummy, slide.img^);
			Strings.Append(dummy, nl);
		кц;
		WMDialogs.Information("Slideshow file list", dummy); (* don't care for user click *)
	кон ShowFileList;

	(*****
	 *
	 *  Display a exit confirmation dialog
	 *
	 *****)
	проц ExitDialog;
	нач
		если (WMDialogs.Confirmation("Exit Slideshow?", "You pressed ESC. Do you really want to exit the slideshow?") = WMDialogs.ResOk) то
			Cleanup();
		всё;
	кон ExitDialog;

	(*****
	 *
	 *  Remove current slide
	 *
	 *****)
	проц RemoveCurrentSlide;
	перем
		isLast : булево;
	нач
		если (DEBUG) то ЛогЯдра.пСтроку8("Remove slide nr."); ЛогЯдра.пЦел64(slideNr, 0); ЛогЯдра.пВК_ПС; всё;
		isLast := slideNr = data.CountSlides()-1;
		data.RemoveSlide(slideNr);
		если (~isLast) то
			если (data.CountSlides() > 0) то
				nav.UpdatePreview();
				win.Update();
			иначе

			всё;
		иначе
			если (DEBUG) то ЛогЯдра.пСтроку8("# of remaining slides is "); ЛогЯдра.пЦел64(data.CountSlides(), 0); ЛогЯдра.пВК_ПС; всё;
			если (data.CountSlides() > 0) то
				умень(slideNr);
				win.Update();
			иначе
				(* there was just one slide left *)
				если (DEBUG) то
					ЛогЯдра.пСтроку8("All slides deleted!"); ЛогЯдра.пВК_ПС;
				всё;
				slideNr := 0;
				win.Close();
				data.ClearSlides();
				нов(win, 320, 240, ложь, data);
				WMWindowManager.DefaultAddWindow(win);
			всё;
		всё;
	кон RemoveCurrentSlide;

	(*****
	 *
	 *  Destructor
	 *
	 *****)
	проц Close;
	нач
		(* close WM stuff *)
		win.Close();
		nav.Close();
	кон Close;
кон SlideshowApp;


(****
 *
 *  The slideshow application
 *
 ****)
тип  SlideshowNavigation = окласс(WMComponents.FormWindow);
	перем
		data : SlideshowData;
		imageP : WMStandardComponents.ImagePanel;
		prevLen : цел32;

	проц &New*(data : SlideshowData);
	перем
		panel, nav: WMStandardComponents.Panel;
		button : WMStandardComponents.Button;
		manager : WMWindowManager.WindowManager;
		windowStyle : WMWindowManager.WindowStyle;
	нач
		сам.data := data;
		prevLen := 180;
		Init(prevLen, prevLen+20, ложь);

		manager := WMWindowManager.GetDefaultManager();
		windowStyle := manager.GetStyle();

		нов(panel);
		panel.bounds.SetExtents(prevLen, prevLen+20);
		panel.fillColor.Set(0000000H);
		panel.takesFocus.Set(истина);

		нов(imageP);
		imageP.bounds.SetExtents(prevLen, prevLen);
		imageP.alignment.Set(WMComponents.AlignTop);

		нов(nav);
		nav.bounds.SetExtents(prevLen, 20);
		nav.fillColor.Set(цел32(0AAAAAAAAH));
		nav.takesFocus.Set(истина);
		nav.alignment.Set(WMComponents.AlignTop);

		нов(button);
		button.caption.SetAOC("|<");
		button.alignment.Set(WMComponents.AlignLeft);
		button.onClick.Add(ButtonHandlerFirst);
		button.bounds.SetWidth(40); button.bounds.SetHeight(20);
		nav.AddContent(button);

		нов(button);
		button.caption.SetAOC("Previous");
		button.alignment.Set(WMComponents.AlignLeft);
		button.onClick.Add(ButtonHandlerPrevious);
		button.bounds.SetWidth(50); button.bounds.SetHeight(20);
		nav.AddContent(button);

		нов(button);
		button.caption.SetAOC("Next");
		button.alignment.Set(WMComponents.AlignLeft);
		button.onClick.Add(ButtonHandlerNext);
		button.bounds.SetWidth(50); button.bounds.SetHeight(20);
		nav.AddContent(button);

		нов(button);
		button.caption.SetAOC(">|");
		button.alignment.Set(WMComponents.AlignLeft);
		button.onClick.Add(ButtonHandlerLast);
		button.bounds.SetWidth(40); button.bounds.SetHeight(20);
		nav.AddContent(button);

		panel.AddContent(nav);
		panel.AddContent(imageP);

		SetContent(panel);
		SetTitle( Strings.NewString("Slideshow Navigation") );

		если (data.CountSlides() > 0) то
			UpdatePreview();
		всё;
	кон New;

	проц UpdatePreview;
	перем
		nextSlide : Slide;
		nextIndex : цел32;
		image : Image;
		fact : вещ32;
		c : WMGraphics.BufferCanvas;
		w, h : размерМЗ;
	нач
		(* End? -> indiacted with a white preview panel *)
		если (app.slideNr >= data.CountSlides()-1) то
			imageP.SetImage(сам, НУЛЬ);
		иначе
			(* load next image and scale slide to correct size *)
			nextIndex := app.slideNr+1;
			nextSlide := data.GetSlide(nextIndex);
			image := LoadImage(nextSlide.img^, Raster.BGR565);
			нцПока (image = НУЛЬ) и (nextIndex < data.CountSlides()) делай
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in UpdatePreview(): Remove invalid image "); ЛогЯдра.пСтроку8(nextSlide.img^); ЛогЯдра.пСтроку8("."); ЛогЯдра.пВК_ПС; всё;
				data.RemoveSlide(nextIndex);
				если (nextIndex < data.CountSlides()) то
					nextSlide := data.GetSlide(nextIndex);
					image := LoadImage(nextSlide.img^, Raster.BGR565);
				всё;
			кц;
			если (image = НУЛЬ) то
				imageP.SetImage(сам, НУЛЬ);
			иначе
				нов(c, image);
				если (image.width > prevLen) или (image.height > prevLen) то
					если (image.width >= image.height) то
						fact := image.width / prevLen;
					иначе
						fact := image.height / prevLen;
					всё;
					c.ScaleImage(image, WMRectangles.MakeRect(0, 0, image.width, image.height),
						WMRectangles.MakeRect(0, 0, округлиВниз(image.width/fact), округлиВниз(image.height/fact)), WMGraphics.ModeCopy, WMGraphics.ScaleBilinear);
					image.width := округлиВниз(image.width/fact);
					image.height := округлиВниз(image.height/fact);
				всё;
				w := image.width; h := image.height + 20; (* Buttons = 20px *)
				imageP.SetImage(сам, image);
			всё;
		всё;
		(* correct window width & height *)
		manager := WMWindowManager.GetDefaultManager();
		w := матМаксимум(w, 180);
		manager.SetWindowSize(сам, w, h);
		Resized(w, h);
		Invalidate( WMRectangles.MakeRect(0, 0, w, h) );
	кон UpdatePreview;

	проц ButtonHandlerNext(sender, data: динамическиТипизированныйУкль);
	нач
		app.Next();
	кон ButtonHandlerNext;

	проц ButtonHandlerPrevious(sender, data: динамическиТипизированныйУкль);
	нач
		app.Previous();
	кон ButtonHandlerPrevious;

	проц ButtonHandlerFirst(sender, data: динамическиТипизированныйУкль);
	нач
		app.First();
	кон ButtonHandlerFirst;

	проц ButtonHandlerLast(sender, data: динамическиТипизированныйУкль);
	нач
		app.Last();
	кон ButtonHandlerLast;

	проц {перекрыта}KeyEvent*(ucs : размерМЗ; flags : мнвоНаБитахМЗ; keysym : размерМЗ);
	нач
		если Inputs.Release в flags то возврат; всё;
		если ucs = кодСимв8("f") то
			app.ToggleFullscreen();
			возврат;
		аесли ucs = кодСимв8("w") то
			app.win.Close();
			нов(app.win, 320, 240, ложь, data);
			WMWindowManager.DefaultAddWindow(app.win);
			возврат;
		аесли ucs = кодСимв8("l") то
			app.ShowFileList();
			возврат;
		всё;
		если (keysym = 0FF51H) то (* Cursor Left *)
			app.Previous();
		аесли (keysym = 0FF53H) то (* Cursor Right *)
			app.Next();
		аесли (keysym = 0FF54H) то (* Cursor Down *)
			app.Last();
		аесли (keysym = 0FF52H) то (* Cursor Up *)
			app.First();
		аесли (keysym = 0FF56H) то (* Page Down *)
			app.Next();
		аесли (keysym = 0FF55H) то (* Page Up *)
			app.Previous();
		аесли (keysym = 0FF50H) то (* Cursor Home *)
			app.First();
		аесли (keysym = 0FF57H) то (* Cursor End *)
			app.Last();
		аесли (keysym = 00020H) то (* Spacebar *)
			app.Next();
		аесли (keysym = 0FF1BH) то (* ESC = 65307*)
			app.ExitDialog();
		аесли (keysym = 0FFFFH) то (* DEL = 65535*)
			app.RemoveCurrentSlide();
		иначе
			если (DEBUG) то ЛогЯдра.пСтроку8("unknown keysym= "); ЛогЯдра.пЦел64(keysym, 0); ЛогЯдра.пВК_ПС; всё;
		всё;
	кон KeyEvent;

	(** Dropped is called via the message handler to indicate an item has been dropped. *)
	проц {перекрыта}DragDropped*(x, y: размерМЗ; dragInfo : WMWindowManager.DragInfo);
	перем
		dropTarget : URLDropTarget;
	нач
		ЛогЯдра.пВК_ПС; (* fix to begin with new line later on *)
		нов(dropTarget);
		dragInfo.data := dropTarget;
		ConfirmDrag(истина, dragInfo)
	кон DragDropped;

кон SlideshowNavigation;

(****
 *
 *  When drag & dropping files/URLs into window (build slideshow on the fly, without transition!)
 *
 ****)
тип URLDropTarget* = окласс(WMDropTarget.DropTarget);
	проц {перекрыта}GetInterface*(type : цел32) : WMDropTarget.DropInterface;
	перем di : DropURL;
	нач
		если (type = WMDropTarget.TypeURL) то
			нов(di);
			возврат di;
		иначе
			возврат НУЛЬ;
		всё
	кон GetInterface;
кон URLDropTarget;

тип DropURL* = окласс(WMDropTarget.DropURLs)
	проц {перекрыта}URL*(конст url : массив из симв8; перем res : целМЗ);
	нач
		(* handle dropped files -> build up SlideshowData on-the-fly (not via XML file) *)
		ЛогЯдра.пСтроку8("Dropped new URL: "); ЛогЯдра.пСтроку8(url); ЛогЯдра.пВК_ПС;
		если (app # НУЛЬ) то
			app.data.AddSlide(url);
			если (app.data.CountSlides() = 1) то
				(* Load first slide *)
				app.win.Update();
			иначе
				app.nav.UpdatePreview();
			всё;
			res := 0
		иначе
			res := -1;
		всё;
	кон URL;
кон DropURL;


(****
 *
 *  The slideshow application
 *
 ****)
тип  SlideshowWindow = окласс(WMWindowManager.DoubleBufferWindow);
	перем
		data: SlideshowData;

	проц &New*( width, height : цел32; alpha : булево; data : SlideshowData);
	нач
		Init(width, height, alpha);
		SetTitle( Strings.NewString("Bluebottle Slideshow (ETHZ, 2005)") );
		сам.data := data;
		если (data.CountSlides() = 0) то возврат; всё;
		(* Load first slide *)
		Update();
	кон New;


	проц {перекрыта}PointerDown*(x, y : размерМЗ; keys : мнвоНаБитахМЗ);
	нач
		если (0 в keys) то
			(* Go to next Slide  *)
			app.Next();
		всё;
	кон PointerDown;

	проц {перекрыта}KeyEvent*(ucs : размерМЗ; flags : мнвоНаБитахМЗ; keysym : размерМЗ);
	нач
		если Inputs.Release в flags то возврат; всё;
		если ucs = кодСимв8("f") то
			app.ToggleFullscreen();
			возврат;
		аесли ucs = кодСимв8("n") то
			app.nav.Close();
			нов(app.nav, data);
			WMWindowManager.DefaultAddWindow(app.nav);
			возврат;
		аесли ucs = кодСимв8("l") то
			app.ShowFileList();
			возврат;
		всё;
		если (keysym = 0FF51H) то (* Cursor Left *)
			app.Previous();
		аесли (keysym = 0FF53H) то (* Cursor Right *)
			app.Next();
		аесли (keysym = 0FF54H) то (* Cursor Down *)
			app.Last();
		аесли (keysym = 0FF52H) то (* Cursor Up *)
			app.First();
		аесли (keysym = 0FF56H) то (* Page Down *)
			app.Next();
		аесли (keysym = 0FF55H) то (* Page Up *)
			app.Previous();
		аесли (keysym = 0FF50H) то (* Cursor Home *)
			app.First();
		аесли (keysym = 0FF57H) то (* Cursor End *)
			app.Last();
		аесли (keysym = 00020H) то (* Spacebar *)
			app.Next();
		аесли (keysym = 0FF1BH) то (* ESC = 65307*)
			app.ExitDialog();
		аесли (keysym = 0FFFFH) то (* DEL = 65535*)
			app.RemoveCurrentSlide();
		иначе
			если (DEBUG) то ЛогЯдра.пСтроку8("unknown keysym= "); ЛогЯдра.пЦел64(keysym, 0); ЛогЯдра.пВК_ПС; всё;
		всё;
	кон KeyEvent;

	(*
	PROCEDURE Jump(slideNr : SIGNED32);
	VAR s : Slide;
		w, h : SIGNED32;
	BEGIN
		(*  Load image *)
		s := data.GetSlide(slideNr);
		img := LoadImage(s.img^, Raster.BGR565);
		manager := WMWindowManager.GetDefaultManager();
		w := img.width;  h := img.height;
		manager.SetWindowSize(SELF, w, h);
		Resized(w, h);
		Invalidate( WMRectangles.MakeRect(0, 0, w, h) );
	END Jump;
	*)

	проц Show(nextSlideNr : цел32 );
	перем
		current, next : Slide;
		src, dest : Image;
		maskFile : String;
	нач
		(* At least two slides are needed *)
		если (data.CountSlides() < 2) то возврат; всё;
		(* End? *)
		если (nextSlideNr > data.CountSlides()-1) то возврат; всё;
		(* Advance to the next for transition rendering *)
		current := data.GetSlide(app.slideNr);
		next := data.GetSlide(nextSlideNr);
		src := LoadImage(current.img^, Raster.BGR565);
		dest := LoadImage(next.img^, Raster.BGR565);
		если (dest = НУЛЬ) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Error: Invalid image - no decoder found for "); ЛогЯдра.пСтроку8(next.img^); ЛогЯдра.пВК_ПС; всё;
			data.RemoveSlide(nextSlideNr);
			Update();
			возврат;
		всё;
		если (src = НУЛЬ) или (dest = НУЛЬ) то СТОП(99); всё;
		(*
		 	1) Mask
		 	2) Fade
		 	3) None
		 *)
		если (current.trans^ = "") то
			ShowNone(dest);
		аесли (Strings.Match("mask:*", current.trans^)) то
			maskFile := Strings.NewString(current.trans^);
			Strings.Delete(maskFile^, 0, 5);
			ShowMask(src, dest, maskFile^, current.dur);
		аесли (Strings.Match("fade", current.trans^)) то
			ShowFade(src, dest, current.dur);
		иначе
			ЛогЯдра.пСтроку8("Invalid transition. Use 'mask:[URL]', 'fade' or '' (empty) in XML file!"); ЛогЯдра.пВК_ПС;
			СТОП(99);
		всё;
	кон Show;

	проц ShowMask(current, next : Image; конст mask: массив из симв8; len : цел32);
	перем
		tm : TransitionMask;
		i, step: цел32;
		w, h : размерМЗ;
	нач
		если (DEBUG) то ЛогЯдра.пСтроку8("Mask transition: "); ЛогЯдра.пСтроку8(mask); ЛогЯдра.пВК_ПС; всё;
		w := current.width; h := current.height;
		i := 0;
		step := 256 DIV len;
		нов(tm);
		tm.Init(w, h);
		tm.SetMask(WMGraphics.LoadImage(mask, истина));
		нцПока (i < 256) делай
			tm.CalcImage(next, current, img, i);
			Invalidate(WMRectangles.MakeRect(0, 0, w, h));
			i := i + step;
		кц;
		если (i # 255) то
			img := next;
			Invalidate(WMRectangles.MakeRect(0, 0, w, h));
		всё;
	кон ShowMask;


	проц ShowFade(current, next : Image; len : цел32);
	перем
		tf : TransitionFade;
		i,step : цел32;
		w, h : размерМЗ;
	нач
		если (DEBUG) то ЛогЯдра.пСтроку8("Fade transition"); ЛогЯдра.пВК_ПС; всё;
		w := current.width; h := current.height;
		i := 0;
		step := 256 DIV len;
		нов(tf);
		tf.Init(w, h);
		нцПока (i < 256) делай
			tf.CalcImage(current, next, img, i);
			Invalidate(WMRectangles.MakeRect(0, 0, w, h));
			i := i + step;
		кц;
		если (i #255) то
			img := next;
			Invalidate(WMRectangles.MakeRect(0, 0, w, h));
		всё;
	кон ShowFade;

	проц ShowNone(next : Image);
	нач
		img := next;
		Invalidate(WMRectangles.MakeRect(0, 0, next.width, next.height));
	кон ShowNone;

	проц Update;
	перем s : Slide;
		w, h : размерМЗ;
		manager : WMWindowManager.WindowManager;
		img: Image;
	нач
		(* Load current slide *)
		если (app.slideNr > data.CountSlides()-1) то возврат; всё;
		s := data.GetSlide(app.slideNr);
		img := LoadImage(s.img^, Raster.BGR565);
		нцПока (img = НУЛЬ) делай
			если (DEBUG) то ЛогЯдра.пСтроку8("Error: Invalid image - no decoder found for "); ЛогЯдра.пСтроку8(s.img^); ЛогЯдра.пВК_ПС; всё;
			data.RemoveSlide(app.slideNr);
			если (app.slideNr < data.CountSlides()-1) то
				s := data.GetSlide(app.slideNr);
				img := LoadImage(s.img^, Raster.BGR565);
			аесли ( (data.CountSlides() > 0) и (app.slideNr > 0) ) то
				умень(app.slideNr);
				s := data.GetSlide(app.slideNr);
				img := LoadImage(s.img^, Raster.BGR565);
			иначе
				(* no more slides -> can't display one :-) *)
				если (DEBUG) то ЛогЯдра.пСтроку8("Error: No more images in slideshow. Add new ones by dropping URLs in navigation window."); ЛогЯдра.пВК_ПС; всё;
				возврат;
			всё;
		кц;
		сам.img := img;
		manager := WMWindowManager.GetDefaultManager();
		w := img.width;  h := img.height;
		manager.SetWindowSize(сам, w, h);
		Resized(w, h);
		Invalidate( WMRectangles.MakeRect(0, 0, w, h) );
		если (app.nav # НУЛЬ) то
			app.nav.UpdatePreview();
		всё;
	кон Update;


кон SlideshowWindow;


 тип SlideshowData= окласс
	перем
		slides : List;
		hasErrors : булево; (* XML Parsing *)

	проц &New*;
	нач
		нов(slides, 50);
		если (DEBUG) то ЛогЯдра.пСтроку8("All slides have been loaded!"); ЛогЯдра.пВК_ПС; всё;
	кон New;

	проц GetSlide(i : цел32) : Slide;
	перем
		p : динамическиТипизированныйУкль; 	s : Slide;
	нач
		p := slides.GetItem(i);
		если (p = НУЛЬ) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Slide nr. "); ЛогЯдра.пЦел64(i, 0); ЛогЯдра.пСтроку8(" doesn't exist!");  ЛогЯдра.пВК_ПС; всё;
			возврат НУЛЬ;
		всё;
		 s := p(Slide); возврат s;
	кон GetSlide;

	проц CountSlides() : цел32;
	нач
		возврат slides.GetCount();
	кон CountSlides;

	проц LoadSlideshow(конст name : массив из симв8);
	перем
		f : Files.File;
		scanner : XMLScanner.Scanner;
		parser : XMLParser.Parser;
		reader : Files.Reader;
		doc : XML.Document;
	нач {единолично}
		hasErrors := ложь;
		f := Files.Old(name);
		если (f = НУЛЬ) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Couldn't open "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(". Slideshow NOT loaded."); ЛогЯдра.пВК_ПС; всё;
			СТОП (99);
		всё;
		(* Build up XML parser structure *)
		нов(reader, f, 0);
		нов(scanner, reader); scanner.reportError := ErrorReport;
		нов(parser, scanner); parser.reportError := ErrorReport;
		(* Parse the XML file (without DTD/Schema checking) *)
		doc := parser.Parse();
		(* Check for parser errors *)
		если (hasErrors) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Slideshow "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8("NOT ok."); ЛогЯдра.пВК_ПС; всё;
			СТОП (99);
		всё;
		если (LoadSlides(doc)) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Slideshow "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" loaded."); ЛогЯдра.пВК_ПС; всё;
		иначе
			если (DEBUG) то ЛогЯдра.пСтроку8("Slideshow "); ЛогЯдра.пСтроку8(name); ЛогЯдра.пСтроку8(" NOT loaded."); ЛогЯдра.пВК_ПС; всё;
			СТОП (99);
		всё;
	кон LoadSlideshow;

	проц LoadSlides(doc: XML.Document) : булево;
	перем
		enum: XMLObjects.Enumerator;
		e, root: XML.Element;
		p: динамическиТипизированныйУкль;
		s, imgStr, transStr, durStr, descStr : String;
		dur : цел32;
		slide : Slide;
	нач
		если (doc = НУЛЬ) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): doc = NIL"); всё;
			возврат ложь;
		всё;
		root := doc.GetRoot();
		если (root = НУЛЬ) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): root = NIL"); всё;
			возврат ложь;
		всё;
		enum := root.GetContents();
		нцПока ( enum.HasMoreElements() ) делай
			p := enum.GetNext();
			если ~(p суть XML.Element) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): p # XML.Element"); всё;
				возврат ложь;
			всё;
			(* Try to read 'Slide' element *)
			e := p(XML.Element);
			s := e.GetName();
			если (s = НУЛЬ) или (s^ # "Slide") то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): s = NIL OR s # 'Slide'"); всё;
				возврат ложь;
			всё;
			(*
				(* 0. try to read 'key' attribut -> not yet used!!! *)
				s := e.GetAttributeValue("key");
				IF (s = NIL) THEN
					IF (DEBUG) THEN KernelLog.String("Error in LoadSlides(): s(key) = NIL"); END;
					RETURN FALSE;
				END;
				Strings.StrToInt(s^, i);
				IF (i<=0) & (i>WMTrans.duration) THEN KernelLog.String("Error: wrong index in XML"); RETURN FALSE; END;
			*)
			(* **
			 *
			 * WARNING: Values  NOT yet zero terminated!!! Bug in XML Parser?!?
			 *                    ==> create a new String with Strings.NewString()
			 *
			 ** *)
			(** 1. try to read 'imgage' attribut **)
			s := e.GetAttributeValue("image");
			если (s = НУЛЬ) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): s(image) = NIL"); всё;
				возврат ложь;
			всё;
			imgStr := Strings.NewString(s^);
			если ( (imgStr = НУЛЬ) или (imgStr^ = "") ) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): filename = NIL   OR   empty"); всё;
				возврат ложь;
			всё;
			(** 2. try to read 'transition' attribut **)
			s := e.GetAttributeValue("transition");
			если (s = НУЛЬ) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): s(transition) = NIL"); всё;
				возврат ложь;
			всё;
			transStr := Strings.NewString(s^);
			если (transStr = НУЛЬ) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): transition = NIL"); всё;
				возврат ложь;
			всё;
			(** 3. try to read 'duration' attribut **)
			s := e.GetAttributeValue("duration");
			если (s = НУЛЬ) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): s(duration) = NIL"); всё;
				возврат ложь;
			всё;
			durStr := Strings.NewString(s^);
			Strings.StrToInt(durStr^, dur);
			(** 4. try to read 'description' attribut **)
			s := e.GetAttributeValue("description");
			если (s = НУЛЬ) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): s(description) = NIL"); всё;
				возврат ложь;
			всё;
			descStr := Strings.NewString(s^);
			если (descStr = НУЛЬ) то
				если (DEBUG) то ЛогЯдра.пСтроку8("Error in LoadSlides(): description = NIL"); всё;
				возврат ложь;
			всё;
			(** create slide entry and add it to list **)
			если (DEBUG) то
				ЛогЯдра.пСтроку8("Loading Slide (image="); ЛогЯдра.пСтроку8(imgStr^); ЛогЯдра.пСтроку8(", transition="); ЛогЯдра.пСтроку8(transStr^);   ЛогЯдра.пСтроку8(")."); ЛогЯдра.пВК_ПС;
			всё;
			нов(slide, imgStr, transStr, dur, descStr);
			slides.Add(slide);
		кц; (* while loop *)
		если (slides.GetCount() = 0) то
			если (DEBUG) то ЛогЯдра.пСтроку8("Slideshow "); ЛогЯдра.пСтроку8(" NOT loaded (empty file)."); ЛогЯдра.пВК_ПС; всё;
			возврат ложь;
		иначе
			возврат истина;
		всё;
	кон LoadSlides;

	(*****
	 *
	 *  XML slideshow file reading stuff
	 *
	 *****)
	проц ErrorReport(pos, line, row: Потоки.ТипМестоВПотоке; конст msg: массив из симв8);
	нач
		ЛогЯдра.пСтроку8("Parse error at pos "); ЛогЯдра.пЦел64(pos, 5); ЛогЯдра.пСтроку8(" in line "); ЛогЯдра.пЦел64(line, 5);
		ЛогЯдра.пСтроку8(" row "); ЛогЯдра.пЦел64(row, 5); ЛогЯдра.пСтроку8(" - "); ЛогЯдра.пСтроку8(msg); ЛогЯдра.пВК_ПС;
		hasErrors := истина
	кон ErrorReport;

	(*****
	 *
	 *  Add a slide on the fly (uses a short fade transition)
	 *
	 *****)
	проц AddSlide(конст filename : массив из симв8);
	перем
		slide : Slide;
	нач
		нов(slide, Strings.NewString(filename), Strings.NewString("fade"), 15, Strings.NewString(filename));
		slides.Add(slide);
	кон AddSlide;

	(*****
	 *
	 *  Remove a slide on the fly (if it has been detected as invalid image format)
	 *
	 *****)
	проц RemoveSlide(i : цел32);
	нач
		slides.RemoveByIndex(i);
	кон RemoveSlide;

	(*****
	 *
	 *  Clears everything
	 *
	 *****)
	проц ClearSlides;
	нач
		slides.Clear();
	кон ClearSlides;

 кон SlideshowData;



(****
 *
 *  Global variables
 *
 ****)
перем
	app : SlideshowApp; (* using the singleton pattern *)

(****
 *
 *  Global functions
 *
 ****)
проц Open*(context : Commands.Context);
перем dstring : массив 256 из симв8;
нач {единолично}
	если (app # НУЛЬ) то
		app.Close();
	всё;
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(dstring);
	нов(app, dstring);
кон Open;

проц Cleanup;
нач
	если (app # НУЛЬ) то app.Close(); всё
кон Cleanup;


(****
 *
 *  Load Image in given Format as WM class, Image is NOT SHAREABLE although it has a key!
 *
 * 	NOTE: With the "Raster.Image" you will have many type troubles with WM Framework
 *
 ****)
проц LoadImage(конст name : массив из симв8; fmt : Raster.Format): Image;
перем img : Image;
	res, x: целМЗ; w, h : размерМЗ;
	decoder : Codecs.ImageDecoder;
	in : Потоки.Чтец;
	ext : массив 16 из симв8;
нач
	если (name = "") то возврат НУЛЬ всё;
	GetExtension(name, ext);
	Strings.UpperCase(ext);
	decoder := Codecs.GetImageDecoder(ext);
	если (decoder = НУЛЬ) то
		ЛогЯдра.пСтроку8("No decoder found for "); ЛогЯдра.пСтроку8(ext); ЛогЯдра.пВК_ПС;
		возврат НУЛЬ;
	всё;
	in := Codecs.OpenInputStream(name);
	если (in # НУЛЬ) то
		decoder.Open(in, res);
		если (res = 0) то
			decoder.GetImageInfo(w, h, x, x);
			нов(img);
			Raster.Create(img, w, h, fmt);
			decoder.Render(img);
			нов(img.key, длинаМассива(name)); копируйСтрокуДо0(name, img.key^);
		всё;
	всё;
	возврат img;
кон LoadImage;

(*****
 *
 *  Procedure to split filename in the name and the extension
 *
 *****)
проц GetExtension (конст name: массив из симв8; перем ext: массив из симв8);
перем
	i, j: цел32;
	ch: симв8;
нач
	i := 0; j := 0;
	нцПока (name[i] # 0X) делай
		если (name[i] = ".") то j := i+1 всё;
		увел(i)
	кц;
	i := 0;
	нцДо
		ch := name[j]; ext[i] := ch; увел(i); увел(j)
	кцПри (ch = 0X) или (i = длинаМассива(ext));
	ext[i-1] := 0X
кон GetExtension;

нач
	Modules.InstallTermHandler(Cleanup)

кон WMSlideshow.

(* Testing commands *)
System.Free WMSlideshow WMTransFade WMTransMask WMTrans ~
System.Free WMSlideshow~

PC.Compile RetoWMTrans.Mod RetoWMTransMask.Mod RetoWMTransFade.Mod RetoWMSlideshow.Mod~

WMSlideshow.Open ~
WMSlideshow.Open RetoWMSlideshow.XML~
