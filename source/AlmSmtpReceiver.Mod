(* Aubrey McIntosh, Ph.D.  Jan 21, 2003
 *	This code may be distributed under the same terms and conditions as the Bluebottle operating system
 *	from ETH, Zürich.
 *
 *	RFC 821 Receiver module.  Listen on port 25, put messages and envelopes into files.
 *
 *	Hand modified Coco-R output.
 *	The scanner differs from the Coco provided scanner.
 *
 *	This code is a result of hand merging a working but poorly designed prototype,
 *	AosSMTPReceiver1.Mod, and the Coco output.
 *
 *	Jan 21, 2003 21:53 CST:	The merge is essentially complete, the scanner has not been
 *	written, a code walk through has not been done. i.e. major omissions may exist.
 *	Jan 23, 2003 16:35 CST:	Accepted first message.
 *	Jan 25, 2003  20:00:  Have log files.  Capture raw data sendmail sends here.
 *	Jan 26, 2003  20:00:  Place onto alternate path:  FAT:/Mail/<message name>
 *	Feb 1, 2003	0.1.28	Add Logging: flood of connections from 160.94.128.45
 *    Feb 11, 2003 0.1.29  Test whether DNS extension is on name when writing message id.
 *    Feb 18, 2003 0.1.33  Limit total number of connections.  Place diagnostics in log.
 *		accept core.inf.ethz.ch always.
 *	Feb 19, 2003 0.1.34	Add diagnostic test for EOF condition.
 *		0.1.38	Fix error where 1st of many messages is dropped (overwritten?)
 *	Feb 23, 2003
 *		0.1.40	Reconcile behavior of emwac and the .Rcp file format.
 *	Mar 16, 2003
 *		0.1.41	Make file name initialization more robust.
 *    Mar 31, 2003  The program seems to hang if the remote sender does not issue QUIT.
 *    Jan 3, 2004  Name changed to AlmSMTPReceiver for release to BlueBottle community.
 *        Changes 0.1.42 appear lost in move.
 *	Sept 21, 2004	Initialise config file on first execution.  Use AOS filesystem names.

 (* Initial SMTP handshake *)
		BEGIN {EXCLUSIVE}
			TakeNumber;
			AdvanceNumber
		END;
		OpenLog;
		LOOP	(*Parse SMTP*)
			OpenMail;
			IF Finished THEN EXIT END
			BEGIN {EXCLUSIVE}
				TakeNumber;
				AdvanceNumber
			END
		END

 *
 *
 *	To Do:
 *		Make robust.  E.g. HALT is not good error recovery.
 *		Integrate with Frey's Abstract Mail
 *		Produce message and broadcast mechanism.  E.g., for use in poping up display when VIP
 *			sends message.
 *
 *)

модуль AlmSmtpReceiver;

использует DNS, Files, Потоки, IP, Modules, ЛогЯдра, TCP, TCPServices, Dates, Strings;

конст
(* Some of the configurable items. *)
	AlmSmtpReceiverPort = 25; (* Well, semi-configurable. *)

	MaxActive = 3+1;
	ID = "BlueBottle Receiver ";
	Version = "MailBottle (0.2.00.16)";
	Rcp = ".Rcp";
	Msg = ".Msg";
	Log = ".Log";
	ConfigFileName = "mail.config";
	ToDisk = истина; (* Debug *)
	DebugMsg = ложь;
	RcptInFileName = истина;
	MaxUserName = 11;
	Prefix = "In."; (*Administer must create this manually.*)
	AlwaysAccept = "129.132.178.196";

(* End of these configurable items. *)

(* Constants for the Scanner *)
конст
	EOF = 0X;
	maxLexLen = 127;
	noSym = 13;
(* Types for the Scanner *)
тип
	ErrorProc* = проц (n: цел16);
	StartTable = массив 128 из цел16;
(* Variables for the Scanner *)
перем
	errors*: цел16;	(*number of errors detected*)
	lasterror* : цел16;
	charcount : цел32;
	getCalls : цел32;
	start: StartTable;	(*start state for every character*)
	Pattern, Ack : массив 6 из симв8;
	active : цел32;

конст
	maxP				= 13;
	maxT				= 13;
	nrSets = 3;

	setSize = 32;	nSets = (maxT DIV setSize) + 1;

	SyEol	= 1;
	SyCopy	= 2;
	SyHelo	=3;
	SyQuit	=4;
	SyNoop	=5;
	SyRset	=6;
	SyData	=7;
	SyDot	=8;
	SyRcpt	=9;
	SyTo	=10;
	SyMail	=11;
	SyFrom	=12;
	SyTimeout = 14;

	Tab = 09X;
	LF = 0AX;
	CR = 0DX;

тип
	SymbolSet = массив nSets из мнвоНаБитахМЗ;


тип
	String = массив 128 из симв8;
	TokenPtr = укль на Token;
	Token = запись s : String; next : TokenPtr кон;
	EnvelopePtr = укль на Envelope;
	Envelope = запись
			mta, revMta, from : String;
			to : TokenPtr;
		кон;

	Message* = запись env* : EnvelopePtr; file* :Files.File; кон;

	SmtpAgent* = окласс (TCPServices.Agent)
	перем
			ch: симв8;				(*current input character*)
			res: целМЗ;
			out: Потоки.Писарь; in: Потоки.Чтец;
			log : Files.Writer;
			env : Envelope;
			thisName, verbSy : String;
			finished : булево;
			sym: цел16;	 (* current input symbol *)
			state : цел16;
			badTokens : цел32;
			auxString : String;

	(* Support procedures *)
	проц GetCh():симв8;
	перем ch : симв8;
	нач
		ch := in.чИДайСимв8();
		log.пСимв8 (ch); log.ПротолкниБуферВПоток;
		возврат ch
	кон GetCh;

	проц ConsumeName;
	нач {единолично}
				копируйСтрокуДо0 (nextName, thisName);
				UpdateName (nextName)
	кон ConsumeName;

	проц AvailableName;
	перем
		name : String;
		msgFile: Files.File;
	нач
		копируйСтрокуДо0 (Prefix, name);
		AddExt (name, thisName);
		AddExt (name, Log);
		нцПока (Files.Old (name) # НУЛЬ)
		делай
			ConsumeName;
			копируйСтрокуДо0 (Prefix, name);
			AddExt (name, thisName);
			AddExt (name, Log);
			msgFile := Files.Old (name);
		кц;
	кон AvailableName;

	проц OpenLog; (*1 file per session.  Name is same as when session opens, i.e., not agree w/ .Msg & .Rcp *)
	перем
		msgFile: Files.File;
		name : String;
	нач
		копируйСтрокуДо0 (Prefix, name);
		AddExt (name, thisName);
		AddExt (name, Log);
		msgFile := Files.Old (name);
		ToLog0 ("before search."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		нцПока msgFile # НУЛЬ
		делай
			ToLog0 ("during search."); ЛогЯдра.пСтроку8 (name); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			ConsumeName;
			копируйСтрокуДо0 (Prefix, name);
			AddExt (name, thisName);
			AddExt (name, Log);
			msgFile := Files.Old (name);
		кц;
		ToLog0 ("after search."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		msgFile := Files.New (name);
		Files.OpenWriter ( log, msgFile, 0);
		Files.Register (msgFile);
	кон OpenLog;

	проц ToMemory* (перем token: массив из симв8);
	перем maxix, ix : размерМЗ; trash, next : симв8;
	нач
		next := in.ПодглядиСимв8();
		нцПока (next=" ") или (next=Tab) делай trash := GetCh (); увел (charcount); next := in.ПодглядиСимв8() кц;
		maxix := длинаМассива (token)-1;
		нцПока (next#" ") и (next#Tab) и (next#CR) и (next#LF)
		делай
			ch := GetCh (); увел (charcount); next := in.ПодглядиСимв8(); (* Jan 23, 2003 v. 0.1.02 *)
			если ix < maxix
			то
				token [ix] := ch;
				увел (ix)
			всё
		кц;
		token [ix] := 0X;
		Expect (SyCopy)
	кон ToMemory;

	проц DebugMsg1* (msg : массив из симв8);
	нач
		если DebugMsg
		то
			out.пСтроку8 (msg);
			out.пВК_ПС;
			out.ПротолкниБуферВПоток()
		всё
	кон DebugMsg1;

	проц PutStatus1* (msg : массив из симв8);
	нач
		Confirm(SyEol);	(*Expect is split to a Confirm / Get pair to let the output occur.*)
		out.пСтроку8 (msg);
		out.пВК_ПС;
		out.ПротолкниБуферВПоток();
		Get
	кон PutStatus1;

	проц ChangeStatus1* (newsym : цел16; msg : массив из симв8);
	нач
		Confirm(SyEol);
		sym := newsym;
		out.пСтроку8 (msg);
		out.пВК_ПС;
		out.ПротолкниБуферВПоток();
	кон ChangeStatus1;

	проц PutStatus2* (msg0, msg1 : массив из симв8);
	нач
		Confirm(SyEol);	(*Expect is split to a Confirm / Get pair to let the output occur.*)
		out.пСтроку8 (msg0);
		out.пСтроку8 (msg1);
		out.пВК_ПС;
		out.ПротолкниБуферВПоток;	(* ignore out.res *)
		Get
	кон PutStatus2;

	проц ChangeStatus2* (newsym : цел16; msg0, msg1 : массив из симв8);
	нач
		Confirm(SyEol);
		sym := newsym;
		out.пСтроку8 (msg0);
		out.пСтроку8 (msg1);
		out.пВК_ПС;
		out.ПротолкниБуферВПоток;	(* ignore out.res *)
	кон ChangeStatus2;

	проц AddExt* ( перем name : String; ext : массив из симв8);
		перем i, j, skipped : цел16;
	нач
		i := 0;
		нцПока ( i < длинаМассива(name)-1 ) и ~(name[i] < " ")
		делай
			увел (i)
		кц;
		j := 0; skipped := 0;
		нцПока ( i+j < длинаМассива(name)-1 ) и (j<длинаМассива(ext)-1) и (ext[j] # 0X)
		делай
			если (ext[j] = "<") или (ext[j] = ">")
			то
				увел (j); увел (skipped)
			иначе
				name[i+j-skipped] := ext[j];
				увел (j)
			всё;
		кц;
		name[i+j] := 0X
	кон AddExt;

	проц PutBareName ( name : String; перем wr : Files.Writer );
		перем ix : цел32; ch : симв8;
	нач
		ix := 0;
		нцПока (ix<длинаМассива(name)) и (name[ix]#0X)
		делай
			ch := name [ix];
			если (ch#"<") и (ch#">") то wr.пСимв8 (ch) всё;
			увел (ix)
		кц
	кон PutBareName;

	проц PutEnvelope ( (* not VAR! *) name : String );
	перем envF : Files.File; ew : Files.Writer; to: TokenPtr;
		msgName, rcpPathName : String;
	нач
		копируйСтрокуДо0 (name, msgName);
		(*
		AddExt (msgName, "@");
		AddExt (msgName, NetSystem.hostName);
		AddExt (msgName, ".");
		AddExt (msgName, DNS.domain);
		 *)

		копируйСтрокуДо0 (Prefix, rcpPathName);
		AddExt (rcpPathName, name); (*alm 9/21/2004*)
		AddExt (rcpPathName, Rcp); (*Name with no prefix*)

		envF := Files.New (rcpPathName);
		(*A trap sometimes happens when here:
		Process:  354 run 0 3 01F159B0:AlmSmtpReceiver.SmtpAgent ATADisks.Interrupt.Wait pc=815 {}

		*)
		Files.OpenWriter ( ew, envF, 0);

		ew.пСтроку8 ("Message-ID: <");
		ew.пСтроку8 (msgName);
		(*
		ew.Char ("@");
		ew.String (DNS.domain);
		*)
		ew.пСимв8 (">");
		ew.пВК_ПС;

		ew.пСтроку8 ("Return-path: ");
		PutBareName (env.from, ew);
		ew.пВК_ПС;

		to := env.to;
		нцПока to # НУЛЬ делай
			ew.пСтроку8 ("Recipient: ");
			PutBareName (to.s, ew);
			to := to.next;
			ew.пВК_ПС;
		кц;

		ew.ПротолкниБуферВПоток;
		Files.Register (envF);
	кон PutEnvelope;

	проц UpdateName (перем s : String);
	перем i : цел16; ch : симв8; carry : цел16;
	нач
		i := 10; (* 10 digits significant in name *)
		carry := 1;
		нцПока (1<=i) и (carry = 1) делай
			ch := симв8ИзКода (кодСимв8(s[i]) + carry);
			если '9' < ch
			то
				ch := "0";
				carry := 1
			иначе
				carry := 0
			всё;
			s[i] := ch;
			умень (i)
		кц
	кон UpdateName;


	(* Begin Parser Productions *)
	проц HELO*;
		перем res : целМЗ;
	нач
		Confirm(SyHelo);
		sym := SyCopy; ToMemory (env.mta);
		DNS.HostByNumber (сам.client.fip, env.revMta, res);
		PutStatus2 ("250 Your email is welcome here, ", env.mta);
	кон HELO;

	проц RSET*;
	нач
		Expect(SyRset);
		env.mta	:= ""; env.from := ""; env.to := НУЛЬ;
		PutStatus1 ("250 Requested mail action okay, completed.");
	кон RSET;

	проц NOOP*;
	нач
		Expect(SyNoop);
		PutStatus1 ("250 Requested mail action okay, completed.");
	кон NOOP;

	проц QUIT*;
	нач
		Expect(SyQuit);
		finished := истина;
		ChangeStatus1 (SyQuit, "221 Goodbye.."); (*Avoid executing another Get.*)
		client.Закрой();
	кон QUIT;

	проц RCPT*;
	перем to : TokenPtr;
	нач
		Expect(SyRcpt);
		Confirm(SyTo);
		нов (to);
		sym := SyCopy; ToMemory (to.s);
		to.next := env.to; env.to := to;
		PutStatus2 ("250 Recipient okay:  ", to.s);
	кон RCPT;

	проц Test;
	нач
		если in.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() < 1
		то СТОП( 44 )
		всё
	кон Test;

	проц ToFile(name : String);
	перем
		msg: Files.File;
		msgWr : Files.Writer;
		ix, testIx : цел32;
		receiveTime, remoteIP : String;

		проц WriteIPNr( ip : IP.Adr );
			перем result : целМЗ; str : массив 128 из симв8;
		нач
					IP.AdrToStr(ip, remoteIP);
					msgWr.пСтроку8 (" (");
					msgWr.пСтроку8 (remoteIP);
					DNS.HostByNumber (ip, str, result);
					msgWr.пСтроку8 (" --> ");
					если result = DNS.Ok
					то
						msgWr.пСтроку8 (str)
					иначе
						msgWr.пСтроку8 ("lookup failed.")
					всё;
					msgWr.пСимв8 (")");
		кон WriteIPNr;

	нач
		AddExt (name, Msg);
		если ToDisk то
			msg := Files.New (name);
			Files.OpenWriter ( msgWr, msg, 0);
			ToLog0 (name);
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			Strings.FormatDateTime("www, dd mmm yyyy hh:nn:ss -0600 (CST)", Dates.Now(), receiveTime);
			msgWr.пСтроку8 ("Received: ");
			msgWr.пВК_ПС; msgWr.пСимв8 (Tab); msgWr.пСтроку8 ("from ");
				msgWr.пСтроку8 (env.mta);
				WriteIPNr(сам.client.fip);
			msgWr.пВК_ПС; msgWr.пСимв8 (Tab); msgWr.пСтроку8 ("by ");
				msgWr.пСтроку8 (DNS.domain);
				WriteIPNr(сам.client.int.localAdr);
			msgWr.пВК_ПС; msgWr.пСимв8 (Tab);
			msgWr.пСтроку8 ("with ");
			msgWr.пСтроку8 (Version);
			msgWr.пСтроку8 (" id "); msgWr.пСтроку8 (thisName);
				msgWr.пСимв8 ("@"); msgWr.пСтроку8 (DNS.domain);
			msgWr.пВК_ПС; msgWr.пСимв8 (Tab); msgWr.пСтроку8 ("for ");
				 msgWr.пСтроку8 (env.to.s);
			msgWr.пСимв8 (Tab); msgWr.пСтроку8 (";  "); msgWr.пСтроку8 (receiveTime);
			msgWr.пВК_ПС
		всё;
		ch := GetCh (); увел (charcount); (* Read first v 0.1.02 *)
		testIx := 0;
		нц
			если in.кодВозвратаПоследнейОперации = Потоки.КонецФайла
			то
				ToLog0 ("EOF on input stream."); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования; sym := SyEol; прервиЦикл
			всё;
			если ch=Pattern[0]
			то
				нц
					ch := GetCh (); увел (charcount);
					testIx := 1;
					нцПока (testIx <= 4) и (ch=Pattern[testIx])
					делай
						если testIx < 4
						то
							ch := GetCh ();
							увел (charcount);
						всё;
						увел (testIx)
					кц;
					если DebugMsg
					то
						нцДля ix := 0 до testIx-1
						делай
							out.пСимв8 (Ack[ix])
						кц;
						out.ПротолкниБуферВПоток
					всё;
					если testIx=5
					то
						msgWr.пСимв8 (CR); msgWr.пСимв8 (LF);
						sym := SyEol; (*Have read both "." and CR/LF*)
					иначе
						нцДля ix := 0 до testIx-1
						делай
							msgWr.пСимв8 (Pattern[ix])
						кц;
						(* msgWr.Char (ch); *)
						(* testIx := 0 *)
					всё;
					прервиЦикл
				кц;
				если testIx=5 то прервиЦикл всё
			иначе
				msgWr.пСимв8 (ch)
			всё;
			если testIx#0 то testIx := 0 (*Start test again at current character.*) иначе ch := GetCh (); увел (charcount) всё
		кц ;
		если DebugMsg то out.пСимв8 ("!"); out.ПротолкниБуферВПоток всё;
		если ToDisk то msgWr.ПротолкниБуферВПоток всё;
		если ToDisk то Files.Register (msg) всё
	кон ToFile;

	проц DATA* (name : String);
	нач
		Expect(SyData);
		ChangeStatus1 (SyCopy, "354 Send message now, end with CRLF . CRLF");
		sym := SyCopy; ToFile (name);
		Confirm(SyEol);
	кон DATA;

	проц AddUserToName (перем thisName : String);
	перем
		pos : цел16;
	нач
			если RcptInFileName
			то
				AddExt ( thisName, "."); (*Preparation for mailbox-in-name interpretation.*)
				pos := 0;
				нцПока (pos < длинаМассива (thisName)) и (thisName [pos] # 0X) делай увел (pos) кц;
				AddExt ( thisName, env.to.s); (*Preparation for mailbox-in-name interpretation.*)
				thisName [pos + MaxUserName] := 0X;

				нцПока (pos < длинаМассива (thisName)) и (thisName [pos] # "@")
				делай
					увел (pos)
				кц;
				если pos < длинаМассива (thisName)  то thisName [pos] := 0X всё;
			всё;
	кон AddUserToName;

	проц MAIL*;
	перем
		to : TokenPtr;
		pathName : String;
		localSym : цел16;	(*to debug*)
	нач
		Expect(SyMail);
		env.from := ""; env.to := НУЛЬ;
		Confirm(SyFrom);
		sym := SyCopy; ToMemory (env.from);
		PutStatus2 ("250 Sender okay. ", env.from);
		нов( to );
		если StartOf(1) то
			reset; если finished то возврат всё;
		аесли (sym = SyRcpt) то
			RCPT;
			нцПока (sym = SyRcpt) делай
				RCPT;
			кц ;
			AddUserToName (thisName);
			копируйСтрокуДо0 (Prefix, pathName);
			AddExt (pathName, thisName);
			AddExt (pathName, Rcp);

			(* alm 3/16/2003 Skips previously used names. *)
			нцПока (Files.Old (pathName) # НУЛЬ)
			делай
				ConsumeName;
				AddUserToName (thisName);
				копируйСтрокуДо0 (Prefix, pathName);
				AddExt (pathName, thisName);
				AddExt (pathName, Rcp);
			кц;
			копируйСтрокуДо0 (Prefix, pathName);
			AddExt (pathName, thisName);

			если StartOf(1)	то
				reset;
				ToLog0 ("Post RCPT cmd in mail.");
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
				если finished то возврат всё;
			аесли (sym = SyData) то
				ToLog0 ("Data cmd in mail.");
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
				DATA (pathName);
			иначе Error1(14)
			всё ;
		иначе Error1(15)
		всё ;
		PutEnvelope (thisName);
		если DebugMsg то out.пСимв8 ("@"); out.ПротолкниБуферВПоток всё;
		localSym := сам.sym;
		PutStatus2 ("250 Your confirmation number is ", thisName);

		(*  Feb. 22, 2003 *)
		просей sym из
			SyQuit : ToLog0 ("Quit detected.")
			| SyMail : ToLog0 ("Mail detected.")
			| SyRset : ToLog0 ("Rset detected.")
			| SyNoop : ToLog0 ("Noop detected.")
			| SyEol : ToLog0 ("dead connection detected.")
		иначе
			ToLog0 ("Unexpected path in case statement.")
		всё;
		ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;

		если sym в {SyMail, SyRset, SyNoop} (*Noop DOES allow more mail in this session.*)
		то
			ToLog0 ("update name.");
			ConsumeName;
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		(* PutRegistry (nextName) *)
		иначе
			ToLog0 ("Keep existing name.");
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			возврат
		всё
	кон MAIL;

	проц reset;
	нач
		DebugMsg1 ("Entering reset.");
		если (sym = SyHelo) то HELO;
		аесли (sym = SyNoop) то NOOP;
		аесли (sym = SyRset) то RSET;
		аесли (sym = SyMail) то MAIL;
		иначе Error1(16)
		всё ;
		DebugMsg1 ("Exiting reset.")
	кон reset;

	проц Get;
	нач
		увел (getCalls);
		ch := GetCh (); увел (charcount); (*No characters in buffer on entry.*)
		нцПока (ch=" ") или (ch=Tab) делай ch := GetCh (); увел (charcount) кц;
		если ch > 7FX то ch := " " всё;
		если ("a"<=ch) и (ch<="z") то ch := ASCII_вЗаглавную (ch) всё;
		state := start[кодСимв8(ch)];
		(*Intercept single character symbols to avoid read-ahead*)
		просей state из
			24: sym := SyDot; возврат
		|	3: если (ASCII_вЗаглавную(in.ПодглядиСимв8()) ="R") то (* state := 35; (*does not block across CR LF on legal input.*) *)
					иначе sym := SyCopy; возврат
					всё;
		иначе (* Continue with multi character symbols. *)
		всё;
		нц
			ch := GetCh (); увел (charcount);
			если ("a"<=ch) и (ch<="z") то ch := ASCII_вЗаглавную (ch) всё;
			если state > 0 то
				просей state из
			|	1: если (ch=LF) то state := 2; sym := SyEol; возврат
						иначе sym := noSym; возврат
						всё;
			|	2: СТОП (52) (*Avoid look ahead character read*)
			|	3: если (ch ="R") то state := 35;
						иначе sym := SyCopy; возврат
						всё;
			|	4: если (ch ="E") то state := 5;
						иначе sym := noSym; возврат
						всё;
			|	5: если (ch ="L") то state := 6;
						иначе sym := noSym; возврат
						всё;
			|	6: если (ch ="O") то state := 7; sym := SyHelo; возврат
						иначе sym := noSym; возврат
						всё;
			|	7: СТОП (57) (*Avoid look ahead character read*)
			|	8: если (ch ="U") то state := 9;
						иначе sym := noSym; возврат
						всё;
			|	9: если (ch ="I") то state := 10;
						иначе sym := noSym; возврат
						всё;
			| 10: если (ch ="T") то state := 11; sym := SyQuit; возврат
						иначе sym := noSym; возврат
						всё;
			| 11: СТОП (61) (*Avoid look ahead character read*)
			| 12: если (ch ="O") то state := 13;
						иначе sym := noSym; возврат
						всё;
			| 13: если (ch ="O") то state := 14;
						иначе sym := noSym; возврат
						всё;
			| 14: если (ch ="P") то state := 15; sym := SyNoop; возврат
						иначе sym := noSym; возврат
						всё;
			| 15: СТОП (65) (*Avoid look ahead character read*)
			| 16: если (ch ="S") то state := 17;
						аесли (ch ="C") то state := 25;
						иначе sym := noSym; возврат
						всё;
			| 17: если (ch ="E") то state := 18;
						иначе sym := noSym; возврат
						всё;
			| 18: если (ch ="T") то state := 19; sym := SyRset; возврат
						иначе sym := noSym; возврат
						всё;
			| 19: СТОП (69) (*Avoid look ahead character read*)
			| 20: если (ch ="A") то state := 21;
						иначе sym := noSym; возврат
						всё;
			| 21: если (ch ="T") то state := 22;
						иначе sym := noSym; возврат
						всё;
			| 22: если (ch ="A") то state := 23; sym := SyData; возврат
						иначе sym := noSym; возврат
						всё;
			| 23: СТОП (73) (*Avoid look ahead character read*)
			| 24: sym := SyDot; СТОП(74); возврат
			| 25: если (ch ="P") то state := 26;
						иначе sym := noSym; возврат
						всё;
			| 26: если (ch ="T") то state := 27; sym := SyRcpt; возврат
						иначе sym := noSym; возврат
						всё;
			| 27: СТОП (77) (*Avoid look ahead character read*)
			| 28: если (ch ="O") то state := 29;
						иначе sym := noSym; возврат
						всё;
			| 29: если (ch =":") то state := 30; sym := SyTo; возврат
						иначе sym := noSym; возврат
						всё;
			| 30: СТОП (80) (*Avoid look ahead character read*)
			| 31: если (ch ="A") то state := 32;
						иначе sym := noSym; возврат
						всё;
			| 32: если (ch ="I") то state := 33;
						иначе sym := noSym; возврат
						всё;
			| 33: если (ch ="L") то state := 34; sym := SyMail; возврат
						иначе sym := noSym; возврат
						всё;
			| 34: СТОП (84) (*Avoid look ahead character read*)
			| 35: если (ch ="O") то state := 36;
						иначе sym := noSym; возврат
						всё;
			| 36: если (ch ="M") то state := 37;
						иначе sym := noSym; возврат
						всё;
			| 37: если (ch =":") то state := 38; sym := SyFrom; возврат
						иначе sym := noSym; возврат
						всё;
			| 38: СТОП (88) (*Avoid look ahead character read*)
			| 39: sym := 0; ch := 0X; возврат

				всё (*CASE*)
			иначе sym := noSym; возврат (*NextCh already done*)
			всё; (*IF*)
		кц (*LOOP*)
	кон Get;

	проц ErrMsg(msg : String);
	нач
		ЛогЯдра.пСтроку8 (msg);
	кон ErrMsg;

	проц Error1(n: цел16);
	нач
		увел(errors);
		lasterror := n;
		ЛогЯдра.ЗахватВЕдиноличноеПользование;
   		просей n из
		  | 13: ErrMsg("??? expected")
		  | 14: ErrMsg("invalid MAIL")
		  | 15: ErrMsg("invalid MAIL")
		  | 16: ErrMsg("invalid reset")
		 иначе всё;
		 ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		кон Error1;

	проц Error2 (n, sym: цел16);
	нач
		увел(errors);
		lasterror := n;
		ЛогЯдра.ЗахватВЕдиноличноеПользование;
   		просей n из
  		    0: ErrMsg("EOF expected, ")
		  |  1: ErrMsg("Eol expected, ")
		  |  2: ErrMsg("ident expected, ")
		  |  3: ErrMsg("'HELO' expected, ")
		  |  4: ErrMsg("'QUIT' expected, ")
		  |  5: ErrMsg("'NOOP' expected, ")
		  |  6: ErrMsg("'RSET' expected, ")
		  |  7: ErrMsg("'DATA' expected, ")
		  |  8: ErrMsg("'.' expected, ")
		  |  9: ErrMsg("'RCPT' expected, ")
		  | 10: ErrMsg("'TO:' expected, ")
		  | 11: ErrMsg("'MAIL' expected, ")
		  | 12: ErrMsg("'FROM:' expected, ")
		 иначе всё;
   		просей sym из
  		    0: ErrMsg("EOF found")
		  |  1: ErrMsg("Eol found")
		  |  2: ErrMsg("ident found")
		  |  3: ErrMsg("'HELO' found")
		  |  4: ErrMsg("'QUIT' found")
		  |  5: ErrMsg("'NOOP' found")
		  |  6: ErrMsg("'RSET' found")
		  |  7: ErrMsg("'DATA' found")
		  |  8: ErrMsg("'.' found")
		  |  9: ErrMsg("'RCPT' found")
		  | 10: ErrMsg("'TO:' found")
		  | 11: ErrMsg("'MAIL' found")
		  | 12: ErrMsg("'FROM:' found")
		 иначе всё;
		 ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		кон Error2;

	проц Confirm(n: цел16);
	нач если sym = n то (* Nothing *) иначе Error2(n, sym) всё
	кон Confirm;

	проц Expect(n: цел16);
	нач если sym = n то Get иначе Error2(n, sym) всё
	кон Expect;

	проц StartOf(s: цел16): булево;
		нач возврат (sym остОтДеленияНа setSize) в symSet[s, sym DIV setSize]
	кон StartOf;

	проц Who;
		перем	ipStr : String;
	нач
			IP.AdrToStr (сам.client.fip, ipStr);
			ЛогЯдра.пСтроку8 (ipStr);
	кон Who;

	проц BackStagePass (pass : String) : булево;
		перем ipStr : String; ix: цел32;
	нач
		IP.AdrToStr (сам.client.fip, ipStr);
		ix := 0;
		нцПока (ix<=15) и (ipStr[ix] = pass[ix]) и (ipStr[ix] # 0X)
		делай
			увел (ix)
		кц;
		возврат pass[ix] = 0X
	кон BackStagePass;

	нач {активное}
		нач {единолично}
			увел (active)
		кон;
		(* open streams *)
		Потоки.НастройЧтеца(in, client.ПрочтиИзПотока);
		Потоки.НастройПисаря(out, client.ЗапишиВПоток);
		если (active < MaxActive) или BackStagePass (AlwaysAccept)
		то
			ConsumeName;
			finished := ложь;
			charcount := 0;
			getCalls := 0;
				ToLog0 ("Connection made. ");
				Who;
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			Announce(out);
				ToLog0 ("Log open sequence. ");
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			OpenLog;
			log.пСтроку8 ("Log file opened on ");
			Strings.FormatDateTime("www, dd mmm yyyy hh:nn:ss -0600 (CST)", Dates.Now(), auxString);
			log.пСтроку8 (auxString);
			log.пВК_ПС;
			log.пСтроку8 ("From IP ");
			IP.AdrToStr(сам.client.fip, auxString);
			log.пСтроку8 (auxString);
			DNS.HostByNumber (сам.client.fip, auxString, res);
			если res = DNS.Ok
			то
				log.пСтроку8 (" <");
				log.пСтроку8 (auxString);
				log.пСтроку8 ("> ")
			всё;
			log.пВК_ПС;
				ToLog0 ("Log now open. ");
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;

			(* production Smtp *)
			Get;
			badTokens := 0;

			нцПока ~finished и (badTokens < 100) и (sym#0) делай
				нцПока ~StartOf(2) делай
					out.пСтроку8 ("500 Not implemented"); out.пВК_ПС; out.ПротолкниБуферВПоток;
					ch := GetCh (); нцПока ch # CR делай ch := GetCh () кц; ch := GetCh ();
					Get; увел (badTokens);
				кц;
				нцПока StartOf(1)
				делай
					reset
				кц;
				QUIT
			кц
		иначе
			out.пСтроку8 ("421 PeerGrade.mrs.umn.edu, Service Not Available, Max connections exceeded.");
			out.пВК_ПС; out.ПротолкниБуферВПоток;
			ToLog0 ("Connection rejected, too many connections. ");
			Who;
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
		всё;
		Terminate;
		нач {единолично} умень (active) кон;
		ToLog0 ("Connection closed. ");
		Who;
		ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	кон SmtpAgent;

перем
	symSet:	массив nrSets из SymbolSet;
	smtp: TCPServices.Service;
	nextName : String;

	проц ToLog0 (msg : String);
	нач
		ЛогЯдра.ЗахватВЕдиноличноеПользование;
		ЛогЯдра.пСтроку8 (ID);
		ЛогЯдра.пСтроку8 ("  ");
		ЛогЯдра.пСтроку8 (msg);
	кон ToLog0;


проц InitSmtpSTable;
нач
	start[0]:=39; start[1]:=0; start[2]:=0; start[3]:=0;
	start[4]:=0; start[5]:=0; start[6]:=0; start[7]:=0;
	start[8]:=0; start[9]:=0; start[10]:=0; start[11]:=0;
	start[12]:=0; start[13]:=1; start[14]:=0; start[15]:=0;
	start[16]:=0; start[17]:=0; start[18]:=0; start[19]:=0;
	start[20]:=0; start[21]:=0; start[22]:=0; start[23]:=0;
	start[24]:=0; start[25]:=0; start[26]:=0; start[27]:=0;
	start[28]:=0; start[29]:=0; start[30]:=0; start[31]:=0;
	start[32]:=0; start[33]:=0; start[34]:=0; start[35]:=0;
	start[36]:=0; start[37]:=0; start[38]:=0; start[39]:=0;
	start[40]:=0; start[41]:=0; start[42]:=0; start[43]:=0;
	start[44]:=0; start[45]:=0; start[46]:=24; start[47]:=0;
	start[48]:=0; start[49]:=0; start[50]:=0; start[51]:=0;
	start[52]:=0; start[53]:=0; start[54]:=0; start[55]:=0;
	start[56]:=0; start[57]:=0; start[58]:=0; start[59]:=0;
	start[60]:=0; start[61]:=0; start[62]:=0; start[63]:=0;
	start[64]:=0; start[65]:=0; start[66]:=3; start[67]:=0;
	start[68]:=20; start[69]:=0; start[70]:=3; start[71]:=3;
	start[72]:=4; start[73]:=0; start[74]:=3; start[75]:=3;
	start[76]:=0; start[77]:=31; start[78]:=12; start[79]:=0;
	start[80]:=0; start[81]:=8; start[82]:=16; start[83]:=0;
	start[84]:=28; start[85]:=0; start[86]:=3; start[87]:=3;
	start[88]:=3; start[89]:=3; start[90]:=3; start[91]:=0;
	start[92]:=0; start[93]:=0; start[94]:=0; start[95]:=0;
	start[96]:=0; start[97]:=0; start[98]:=3; start[99]:=0;
	start[100]:=0; start[101]:=0; start[102]:=3; start[103]:=3;
	start[104]:=0; start[105]:=0; start[106]:=3; start[107]:=3;
	start[108]:=0; start[109]:=0; start[110]:=0; start[111]:=0;
	start[112]:=0; start[113]:=0; start[114]:=0; start[115]:=0;
	start[116]:=0; start[117]:=0; start[118]:=3; start[119]:=3;
	start[120]:=3; start[121]:=3; start[122]:=3; start[123]:=0;
	start[124]:=0; start[125]:=0; start[126]:=0; start[127]:=0;
кон InitSmtpSTable;

проц NewSmtpAgent(c: TCP.Connection; s: TCPServices.Service): TCPServices.Agent;
перем a: SmtpAgent;
нач
	нов(a, c, s); возврат a
кон NewSmtpAgent;

(* This should become XML aware. *)
проц GetRegistry (перем filename : String);
перем regF : Files.File; regR : Files.Reader;
нач
	regF := Files.Old (ConfigFileName);
	если regF # НУЛЬ
	то
		Files.OpenReader (regR, regF, 0);
		regR.чСтроку8˛включаяСимв0 (filename)
	иначе
		filename := "D0000000000.Msg";
		regF := Files.New (ConfigFileName);
		Files.Register (regF)
	всё;
кон GetRegistry;

проц PutRegistry (перем filename : String);
перем regF : Files.File; regW : Files.Writer;
нач
	regF := Files.Old (ConfigFileName);
	если regF=НУЛЬ то regF := Files.New (ConfigFileName); Files.Register (regF)  всё;
	Files.OpenWriter (regW, regF, 0);
	regW.пСтроку8˛включаяСимв0 (filename);
	regW.ПротолкниБуферВПоток;
	regF.Update;
кон PutRegistry;

проц Announce ( перем out: Потоки.Писарь);
нач
	out.пСтроку8 ("220 ");
	out.пСтроку8 (DNS.domain);
	out.пСимв8 (" ");
	out.пСтроку8 ("SMTP");
	out.пСимв8 (" ");
	out.пСтроку8 (ID);
	out.пСтроку8 (Version);
	out.пСтроку8 (" Ready ");
	out.пВК_ПС();
	out.ПротолкниБуферВПоток;
кон Announce;

проц Open*;
перем res : целМЗ;
нач
	если smtp = НУЛЬ то
		нов(smtp, AlmSmtpReceiverPort, NewSmtpAgent, res);
		active := 0;
		GetRegistry (nextName);
		ToLog0 (Version); ЛогЯдра.пСтроку8(" opened.  Next name: ");
		ЛогЯдра.пСтроку8 (nextName);
		ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
кон Open;

проц Close*;
нач
	если smtp # НУЛЬ то
		smtp.Stop(); smtp := НУЛЬ;
		PutRegistry (nextName);
		ToLog0 (Version); ЛогЯдра.пСтроку8(" closed"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
	всё;
кон Close;

проц Cleanup;
нач
	Close;
кон Cleanup;



нач
	Pattern[0] := CR;
	Pattern[1] := LF;
	Pattern[2] := ".";
	Pattern[3] := CR;
	Pattern[4] := LF;
	Pattern[5] := 0X;
	Ack[0] := "0";
	Ack[1] := "1";
	Ack[2] := "2";
	Ack[3] := "3";
	Ack[4] := "4";
	Ack[5] := 0X;

	symSet[0, 0] := {0};
	symSet[1, 0] := {SyHelo,SyNoop,SyRset,SyMail};
	symSet[2, 0] := {SyHelo,SyQuit,SyNoop,SyRset,SyMail};
	InitSmtpSTable;
	Modules.InstallTermHandler(Cleanup);
кон AlmSmtpReceiver.





AlmSmtpReceiver.Tool
System.Directory FAT:/Mail/Incoming/*\d
System.Directory C0*\d
Aos.Call AlmSmtpReceiver.Open
Aos.Call AlmSmtpReceiver.Close
Aos.Call NetTracker.Open 100 ~
System.Free AlmSmtpReceiver  ~
System.Free AlmSmtpReceiver ~

EditTools.OpenAscii ^
Telnet.Open cda
System.State AlmSmtpReceiver ~
Builder.Compile *
Telnet.Open "sci1355-am.mrs.umn.edu" 27
Colors.Panel
Hex.Open mail.config

	ch =  0000000DX
	charcount = 26
	config = ""
	errors = 0
	lasterror = 0
	nextName = "D0000000101"
	smtp =  022685D0H
	start = 39, 0, 0, 0, 0, 0, 0, 0, 0, 0 ...
	state = 7
	sym = 3

