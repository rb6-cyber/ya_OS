(**
	AUTHOR: Alexey Morozov
	PURPOSE: Interface for system level configuration/control of Zynq Processing System (PS)
*)
модуль PsConfig;

использует
	НИЗКОУР, Platform, BootConfig, Трассировка;

конст
	(** error codes *)
	Ok* = 0;
	InvalidChannel* = 1; (** invalid channel (e.g. PL clock channel) specified *)
	InvalidClockSource* = 2; (** invalid clock source specified *)
	InvalidDivisor* = 3; (** invalid clock divisor value specified *)
	InvalidModule* = 4; (** invalid I/O module specified *)
	InvalidDevice* = 5; (** invalid I/O device for specified module *)

	UnknownError* = 256;

	(** PLL clock source types *)
	IoPll* = 0; (** IO PLL used as a clock source for IO peripherals *)
	ArmPll* = 1; (** ARM PLL used as a clock source for the CPU *)
	DdrPll* = 3; (** DDR PLL used as a clock source for DDR memory *)

	(** I/O Clock Modules *)
	IoUsb* = 0;
	IoGem* = 1;
	IoSdio* = 2;
	IoSmc* = 3;
	IoSpi* = 4;
	IoQuadSpi* = 5;
	IoUart* = 6;
	IoCan* = 7;
	IoGpio* = 8;
	IoI2c* = 9;

перем
	psRefClockHz: цел64;

	(**
		Get clock frequency of a given PLL clock source

		srcSel: source selector (either of IoPll, ArmPll, DdrPll)
		res: result code; zero in case of success

		Returns the frequency in Hz
	*)
	проц GetPllClockFrequency*(srcSel: цел32; перем res: целМЗ): цел64;
	нач
		просей srcSel из
			|IoPll: возврат цел64(логСдвиг(НИЗКОУР.MSK(Platform.slcr.IO_PLL_CTRL,0x7F000),-12)) * psRefClockHz;
			|ArmPll: возврат цел64(логСдвиг(НИЗКОУР.MSK(Platform.slcr.ARM_PLL_CTRL,0x7F000),-12)) * psRefClockHz;
			|DdrPll: возврат цел64(логСдвиг(НИЗКОУР.MSK(Platform.slcr.DDR_PLL_CTRL,0x7F000),-12)) * psRefClockHz;
		иначе
			res := InvalidClockSource; возврат 0;
		всё;
	кон GetPllClockFrequency;

	(**
		Setup reset signals to programming logic

		assertedChannels: specifies the set of PL channels with asserted reset; can include 0, 1, 2, 3
		res: result code; zero in case of success

		Returns TRUE in case of success
	*)
	проц SetPlResets*(assertedChannels: мнвоНаБитахМЗ; перем res: целМЗ): булево;
	нач
		если assertedChannels * {4..31} # {} то res := InvalidChannel; возврат ложь; всё;

		Platform.slcr.SLCR_UNLOCK := Platform.SlcrUnlockKey; (* enable writing to SLCR registers *)
		Platform.slcr.FPGA_RST_CTRL := assertedChannels;
		Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)

		res := Ok;
		возврат истина;
	кон SetPlResets;

	(**
		Setup a given channel of Programming Logic (PL) clock

		channel: selected channel (either of 0, 1, 2, 3)
		srcSel: source selector (either of IoPll, ArmPll, DdrPll)
		divisor0: provides the divisor used to divide the source clock to generate the required generated clock frequency. First cascade divider.
		divisor1: provides the divisor used to divide the source clock to generate the required generated clock frequency. Second cascade divider.
		res: result code; zero in case of success

		Returns TRUE in case of success
	*)
	проц SetPlClock*(channel: цел32; srcSel: цел32; divisor0, divisor1: цел32; перем res: целМЗ): булево;
	нач
		если (srcSel > 0) или (srcSel > 3) то res := InvalidClockSource; возврат ложь; всё;
		если (divisor0 < 1) или (divisor0 > 63) или (divisor1 < 0) или (divisor1 > 63) то res := InvalidDivisor; возврат ложь; всё;

		Platform.slcr.SLCR_UNLOCK := Platform.SlcrUnlockKey; (* enable writing to SLCR registers *)

		просей channel из
			|0: Platform.slcr.FPGA0_CLK_CTRL := srcSel + логСдвиг(divisor0,8) + логСдвиг(divisor1,20);
			|1: Platform.slcr.FPGA1_CLK_CTRL := srcSel + логСдвиг(divisor0,8) + логСдвиг(divisor1,20);
			|2: Platform.slcr.FPGA2_CLK_CTRL := srcSel + логСдвиг(divisor0,8) + логСдвиг(divisor1,20);
			|3: Platform.slcr.FPGA3_CLK_CTRL := srcSel + логСдвиг(divisor0,8) + логСдвиг(divisor1,20);
		иначе
			Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)
			res := InvalidChannel;
			возврат ложь;
		всё;

		Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)

		res := Ok;
		возврат истина;
	кон SetPlClock;

	(**
		Get clock frequency of a given PL clock channel

		res: result code; zero in case of success

		Returns the frequency in Hz
	*)
	проц GetPlClockFrequency*(channel: цел32; перем res: целМЗ): цел64;
	перем
		d, srcSel, divisor0, divisor1: цел32;
	нач
		просей channel из
			|0: d := Platform.slcr.FPGA0_CLK_CTRL;
			|1: d := Platform.slcr.FPGA1_CLK_CTRL;
			|2: d := Platform.slcr.FPGA2_CLK_CTRL;
			|3: d := Platform.slcr.FPGA3_CLK_CTRL;
		иначе
			res := InvalidChannel;
			возврат 0;
		всё;

		srcSel := логСдвиг(НИЗКОУР.MSK(d,0x30),-4);
		divisor0 := логСдвиг(НИЗКОУР.MSK(d,0x3F00),-8);
		divisor1 := логСдвиг(НИЗКОУР.MSK(d,0x3F00000),-20);

		возврат GetPllClockFrequency(srcSel,res) DIV (divisor0*divisor1);
	кон GetPlClockFrequency;

	(**
		Stop a given PL clock

		channel: clock channel number
		res: result code; zero in case of success

		Returns TRUE in case of success
	*)
	проц StopPlClock*(channel: цел32; перем res: целМЗ): булево;
	нач

		Platform.slcr.SLCR_UNLOCK := Platform.SlcrUnlockKey; (* enable writing to SLCR registers *)

		просей channel из
			|0: Platform.slcr.FPGA0_THR_CNT := 1;
			|1: Platform.slcr.FPGA1_THR_CNT := 1;
			|2: Platform.slcr.FPGA2_THR_CNT := 1;
			|3: Platform.slcr.FPGA3_THR_CNT := 1;
		иначе
			Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)
			res := InvalidChannel;
			возврат ложь;
		всё;

		Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)

		res := Ok;
		возврат истина;
	кон StopPlClock;

	(**
		Start a given PL clock

		channel: clock channel number
		res: result code; zero in case of success

		Returns TRUE in case of success
	*)
	проц StartPlClock*(channel: цел32; перем res: целМЗ): булево;
	нач
		Platform.slcr.SLCR_UNLOCK := Platform.SlcrUnlockKey; (* enable writing to SLCR registers *)

		просей channel из
			|0: Platform.slcr.FPGA0_THR_CNT := 0;
			|1: Platform.slcr.FPGA1_THR_CNT := 0;
			|2: Platform.slcr.FPGA2_THR_CNT := 0;
			|3: Platform.slcr.FPGA3_THR_CNT := 0;
		иначе
			Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)
			res := InvalidChannel;
			возврат ложь;
		всё;

		Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)

		res := Ok;
		возврат истина;
	кон StartPlClock;

	(**
		Stop given PL clocks

		channels: a set of clock channels to stop
		res: result code; zero in case of success

		Returns TRUE in case of success
	*)
	проц StopPlClocks*(channels: мнвоНаБитахМЗ; перем res: целМЗ): булево;
	нач
		если channels * {0,1,2,3} = {} то res := InvalidChannel; возврат ложь; всё;

		Platform.slcr.SLCR_UNLOCK := Platform.SlcrUnlockKey; (* enable writing to SLCR registers *)

		если 0 в channels то Platform.slcr.FPGA0_THR_CNT := 1; всё;
		если 1 в channels то Platform.slcr.FPGA1_THR_CNT := 1; всё;
		если 2 в channels то Platform.slcr.FPGA2_THR_CNT := 1; всё;
		если 3 в channels то Platform.slcr.FPGA3_THR_CNT := 1; всё;

		Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)

		res := Ok;
		возврат истина;
	кон StopPlClocks;

	(**
		Start given PL clocks

		channels: a set of clock channels to start
		res: result code; zero in case of success

		Returns TRUE in case of success
	*)
	проц StartPlClocks*(channels: мнвоНаБитахМЗ; перем res: целМЗ): булево;
	нач
		если channels * {0,1,2,3} = {} то res := InvalidChannel; возврат ложь; всё;

		Platform.slcr.SLCR_UNLOCK := Platform.SlcrUnlockKey; (* enable writing to SLCR registers *)

		если 0 в channels то Platform.slcr.FPGA0_THR_CNT := 0; всё;
		если 1 в channels то Platform.slcr.FPGA1_THR_CNT := 0; всё;
		если 2 в channels то Platform.slcr.FPGA2_THR_CNT := 0; всё;
		если 3 в channels то Platform.slcr.FPGA3_THR_CNT := 0; всё;

		Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)

		res := Ok;
		возврат истина;
	кон StartPlClocks;

	проц GetIoClockFrequency*(module: цел32; перем res: целМЗ): цел64;
	перем
		baseFreq: цел64;
		val: цел32;
	нач
		просей module из
			 IoUsb: (*!TODO*)
			|IoGem: (*!TODO*)
			|IoSdio: val := Platform.slcr.SDIO_CLK_CTRL;
			|IoSmc: val := Platform.slcr.SMC_CLK_CTRL;
			|IoSpi: val := Platform.slcr.SPI_CLK_CTRL;
			|IoQuadSpi: val := Platform.slcr.LQSPI_CLK_CTRL;
			|IoUart: val := Platform.slcr.UART_CLK_CTRL;
			|IoCan: (*!TODO*)
			|IoGpio: (*!TODO*)
			|IoI2c: (*!TODO*)
		иначе
			res := InvalidModule;
			возврат 0;
		всё;
		просей module из
			 IoUsb: (*!TODO*)
			|IoGem: (*!TODO*)
			|IoSdio, IoSmc, IoSpi, IoQuadSpi, IoUart:
				val := логСдвиг(НИЗКОУР.MSK(val, 0x3f00), -8)
			|IoCan:(*!TODO*)
			|IoGpio:(*!TODO*)
			|IoI2c:(*!TODO*)
		всё;
		baseFreq := GetPllClockFrequency(GetIoClockSource(module, res), res);
		если res # Ok то возврат 0 всё;
		возврат baseFreq DIV val
	кон GetIoClockFrequency;

	проц GetIoClockSource*(module: цел32; перем res: целМЗ): цел32;
	перем
		pll, val: цел32;
	нач
		res := Ok;
		просей module из
			 IoUsb: (*!TODO*)
			|IoGem: (*!TODO*)
			|IoSdio: val := Platform.slcr.SDIO_CLK_CTRL;
			|IoSmc: val := Platform.slcr.SMC_CLK_CTRL;
			|IoSpi: val := Platform.slcr.SPI_CLK_CTRL;
			|IoQuadSpi: val := Platform.slcr.LQSPI_CLK_CTRL;
			|IoUart: val := Platform.slcr.UART_CLK_CTRL;
			|IoCan: (*!TODO*)
			|IoGpio: (*!TODO*)
			|IoI2c: (*!TODO*)
		иначе
			res := InvalidModule;
			возврат -1;
		всё;
		просей module из
			 IoUsb: (*!TODO*)
			|IoGem: (*!TODO*)
			|IoSdio, IoSmc, IoSpi, IoQuadSpi, IoUart:
				pll := логСдвиг(НИЗКОУР.MSK(val, 0x30), -4);
				если pll = 2 то pll := ArmPll всё;
			|IoCan:(*!TODO*)
			|IoGpio:(*!TODO*)
			|IoI2c:(*!TODO*)
		всё;
		возврат pll
	кон GetIoClockSource;

	проц SetIoClockFrequency*(module: цел32; freq: цел64; перем res: целМЗ): булево;
	перем
		baseFreq: цел64;
		val, div: цел32;
	нач
		res := Ok;
		просей module из
			 IoUsb: (*!TODO*)
			|IoGem: (*!TODO*)
			|IoSdio: val := Platform.slcr.SDIO_CLK_CTRL;
			|IoSmc: val := Platform.slcr.SMC_CLK_CTRL;
			|IoSpi: val := Platform.slcr.SPI_CLK_CTRL;
			|IoQuadSpi: val := Platform.slcr.LQSPI_CLK_CTRL;
			|IoUart: val := Platform.slcr.UART_CLK_CTRL;
			|IoCan: (*!TODO*)
			|IoGpio: (*!TODO*)
			|IoI2c: (*!TODO*)
		иначе
			res := InvalidModule;
			возврат ложь;
		всё;
		baseFreq := GetPllClockFrequency(GetIoClockSource(module, res), res);
		если res # Ok то возврат ложь всё;

		Platform.slcr.SLCR_UNLOCK := Platform.SlcrUnlockKey; (* enable writing to SLCR registers *)
		просей module из
			 IoUsb: (*!TODO*)
			|IoGem: (*!TODO*)
			|IoSdio, IoSmc, IoSpi, IoQuadSpi, IoUart:
				div := цел32(baseFreq DIV freq);
				val := НИЗКОУР.подмениТипЗначения(цел32, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, val) - {8 .. 13} + НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, логСдвиг(div, 8)) * {8 .. 13});
				Platform.slcr.SDIO_CLK_CTRL := val
			|IoCan:(*!TODO*)
			|IoGpio:(*!TODO*)
			|IoI2c:(*!TODO*)
		всё;
		Platform.slcr.SLCR_LOCK := Platform.SlcrLockKey; (* disable writing to SLCR registers *)
		возврат истина
	кон SetIoClockFrequency;

	проц SetIoClockSource*(module, source: цел32; перем res: целМЗ): булево;
	нач

	кон SetIoClockSource;

	проц StartIoClock*(module, device: цел32; перем res: целМЗ): булево;
	нач

	кон StartIoClock;

	проц StopIoClock*(module, device: цел32; перем res: целМЗ): булево;
	нач

	кон StopIoClock;

нач
	psRefClockHz := BootConfig.GetIntValue("PsRefClockHz");
кон PsConfig.
