модуль CryptoTwofish;   (** Twofish en/decryption *)

(*	Oberon port, based on twofish.c (vers. 1.0, Apr. 1998),
		2002.07.22	g.f.
 *)

использует S := НИЗКОУР, Ciphers := CryptoCiphers, U := CryptoUtils;

конст
	N = 16;

тип
	Block = массив 4 из мнвоНаБитах32;
	SKey = массив 4 из мнвоНаБитах32;

перем
	tab0: массив 256 из мнвоНаБитах32;
	tab1: массив 256 из мнвоНаБитах32;

тип
	Cipher* = окласс (Ciphers.Cipher)
			перем keybits: цел32;
				sbox: массив 4 из цел32;
				subkeys: массив 8 + 2*N из цел32;
				iv: Block;

				проц {перекрыта}InitKey*( конст src: массив из симв8; keybits: цел32 );
				конст step = 02020202H;  bump = 01010101H;
				перем
					i, A, B, m, nsub: цел32;
					k32e, k32o: массив 4 из цел32;   (* even/odd key dwords *)
				нач
					InitKey^( src, keybits );  сам.keybits := keybits;
					нцДля i := 0 до keybits DIV 32 - 1 делай
						если нечётноеЛи¿( i ) то  k32o[i DIV 2] := U.IntFromBufferLE( src, i*4 )
						иначе  k32e[i DIV 2] := U.IntFromBufferLE( src, i*4 )
						всё
					кц;
					m := keybits DIV 64 - 1;
					нцДля i := 0 до m делай
						(* compute S-box keys using (12,8) Reed-Solomon code over GF(256) *)
						sbox[m - i] := Encode( k32e[i], k32o[i] );   (* reverse order *)
					кц;
					nsub := 8 + N*2;
					нцДля i := 0 до nsub DIV 2 - 1 делай
						(* compute round subkeys for PHT *)
						A := F32( S.подмениТипЗначения( мнвоНаБитах32, i*step ), k32e, keybits );  			 (* A uses even key dwords *)
						B := вращБит( F32( S.подмениТипЗначения( мнвоНаБитах32, i*step + bump ), k32o, keybits ), 8 );   (* B uses odd  key dwords *)
						subkeys[i*2] := A + B;   							(* combine with a PHT *)
						subkeys[i*2 + 1] := вращБит( A + 2*B, 9 );
					кц
				кон InitKey;

				проц {перекрыта}SetIV*( конст src: массив из симв8; mode: цел8 );
				перем i: цел16;
				нач
					SetIV^( src, mode );   (* set mode *)
					нцДля i := 0 до 3 делай  iv[i] := U.SetFromBufferLE( src, 4*i )  кц;
				кон SetIV;

				проц {перекрыта}Encrypt*( перем buf: массив из симв8;  ofs, len: размерМЗ );
				перем i: размерМЗ;
				нач
					утв( isKeyInitialized и (mode в {Ciphers.ECB, Ciphers.CBC}) );
					утв( len остОтДеленияНа blockSize = 0 );   (* padding must have been added *)
					i := 0;
					нцПока i < len делай  EncryptBlock( buf, ofs + i );  увел( i, blockSize );   кц
				кон Encrypt;

				проц {перекрыта}Decrypt*( перем buf: массив из симв8;  ofs, len: размерМЗ );
				перем i: размерМЗ;
				нач
					утв( isKeyInitialized );
					утв( len остОтДеленияНа blockSize = 0 );   (* padding must have been added *)
					i := 0;
					нцПока i < len делай  DecryptBlock( buf, ofs + i );  увел( i, blockSize );   кц
				кон Decrypt;

				проц EncryptBlock( перем buf: массив из симв8;  pos: размерМЗ );
				перем x: Block;  t0, t1, i, r: цел32;  s0, s1: мнвоНаБитах32;
				нач
					(* copy in the block, add whitening *)
					нцДля i := 0 до 3 делай
						x[i] := U.SetFromBufferLE( buf, pos + i*4 ) / S.подмениТипЗначения( мнвоНаБитах32, subkeys[i] );
						если mode = Ciphers.CBC то  x[i] := x[i] / iv[i]  всё
					кц;
					(* main Twofish encryption loop *)
					нцДля r := 0 до N - 1 делай
						t0 := F32( x[0], sbox, keybits );
						t1 := F32( вращБит( x[1], 8 ), sbox, keybits );
						x[2] := вращБит( x[2] / S.подмениТипЗначения( мнвоНаБитах32, t0 + t1 + subkeys[8 + 2*r] ), -1 );
						x[3] := вращБит( x[3], 1 ) / S.подмениТипЗначения( мнвоНаБитах32, t0 + t1*2 + subkeys[8 + 2*r + 1] );
						если r < N - 1 то  (* unswap, except for last round *)
							s0 := x[0];  x[0] := x[2];  x[2] := s0;  s1 := x[1];  x[1] := x[3];  x[3] := s1;
						всё
					кц;
					(* copy out, with whitening *)
					нцДля i := 0 до 3 делай
						x[i] := x[i] / S.подмениТипЗначения( мнвоНаБитах32, subkeys[4 + i] );  U.SetToBufferLE( x[i], buf, pos + i*4 );
						если mode = Ciphers.CBC то  iv[i] := x[i]  всё
					кц;
				кон EncryptBlock;

				проц DecryptBlock( перем buf: массив из симв8;  pos: размерМЗ );
				перем x0, x: Block;  t0, t1, i, r: цел32;  s0, s1: мнвоНаБитах32;
				нач
					(* copy in the block, add whitening *)
					нцДля i := 0 до 3 делай
						x0[i] := U.SetFromBufferLE( buf, pos + i*4 );
						x[i] := x0[i] / S.подмениТипЗначения( мнвоНаБитах32, subkeys[4 + i] );
					кц;
					(* main Twofish decryption loop *)
					нцДля r := N - 1 до 0 шаг -1 делай
						t0 := F32( x[0], sbox, keybits );
						t1 := F32( вращБит( x[1], 8 ), sbox, keybits );
						x[2] := вращБит( x[2], 1 );  x[2] := x[2] / S.подмениТипЗначения( мнвоНаБитах32, t0 + t1 + subkeys[8 + 2*r] );
						x[3] := вращБит( x[3] / S.подмениТипЗначения( мнвоНаБитах32, t0 + t1*2 + subkeys[8 + 2*r + 1] ), -1 );
						если r > 0 то  (* unswap, except for last round *)
							s0 := x[0];  x[0] := x[2];  x[2] := s0;
							s1 := x[1];  x[1] := x[3];  x[3] := s1;
						всё
					кц;
					(* copy out, with whitening *)
					нцДля i := 0 до 3 делай
						x[i] := x[i] / S.подмениТипЗначения( мнвоНаБитах32, subkeys[i] );
						если mode = Ciphers.CBC то  x[i] := x[i] / iv[i];  iv[i] := x0[i]  всё;
						U.SetToBufferLE( x[i], buf, pos + i*4 );
					кц;
				кон DecryptBlock;

				проц & {перекрыта}Init*;
				нач
					SetNameAndBlocksize( "twofish", 16 )
				кон Init;

			кон Cipher;

	проц NewCipher*(): Ciphers.Cipher;
	перем cipher: Cipher;
	нач
		нов( cipher );  возврат cipher
	кон NewCipher;

(*-------------------------------------------------------------------------------*)

конст
	FDBK = 169H;
	Fdbk2 = мнвоНаБитах32( FDBK DIV 2 );
	Fdbk4 = мнвоНаБитах32( FDBK DIV 4 );
	Byte0 = мнвоНаБитах32( 0FFH );
	S14d = мнвоНаБитах32( 14DH );
	S0a6 = мнвоНаБитах32( 0A6H );


	проц m1( x: цел32 ): мнвоНаБитах32;
	нач
		возврат S.подмениТипЗначения( мнвоНаБитах32, x )
	кон m1;

	проц mx( x: цел32 ): мнвоНаБитах32;
	перем t: мнвоНаБитах32;
	нач
		t := S.подмениТипЗначения( мнвоНаБитах32, x DIV 4 );
		если нечётноеЛи¿( x DIV 2 ) то  t := t / Fdbk2  всё;
		если нечётноеЛи¿( x ) то  t := t / Fdbk4  всё;
		возврат S.подмениТипЗначения( мнвоНаБитах32, x ) / t
	кон mx;

	проц my( x: цел32 ): мнвоНаБитах32;
	перем t1, t2: мнвоНаБитах32;
	нач
		t1 := S.подмениТипЗначения( мнвоНаБитах32, x DIV 2 );  t2 := S.подмениТипЗначения( мнвоНаБитах32, x DIV 4 );
		если нечётноеЛи¿( x DIV 2 ) то  t2 := t2 / Fdbk2  всё;
		если нечётноеЛи¿( x ) то  t1 := t1 / Fdbk2;  t2 := t2 / Fdbk4  всё;
		возврат S.подмениТипЗначения( мнвоНаБитах32, x ) / t1 / t2
	кон my;

	проц split( x: цел32;  перем v: SKey );
	нач
		v[3] := S.подмениТипЗначения( мнвоНаБитах32, x DIV 1000000H остОтДеленияНа 100H );
		v[2] := S.подмениТипЗначения( мнвоНаБитах32, x DIV 10000H остОтДеленияНа 100H );
		v[1] := S.подмениТипЗначения( мнвоНаБитах32, x DIV 100H остОтДеленияНа 100H );
		v[0] := S.подмениТипЗначения( мнвоНаБитах32, x остОтДеленияНа 100H );
	кон split;

	проц -Int( x: мнвоНаБитах32 ): цел32;
	нач
		возврат S.подмениТипЗначения( цел32, x )
	кон Int;


	проц F32( x: мнвоНаБитах32;  конст k32: массив из цел32;  keybits: цел32 ): цел32;
	перем a, b, c, d, l: цел32;  k, k1: SKey;
	нач
		(* Run each byte thru 8x8 S-boxes, xoring with key byte at each stage. *)
		(* Note that each byte goes through a different combination of S-boxes.*)
		d := Int( x ) DIV 1000000H остОтДеленияНа 100H;
		c := Int( x ) DIV 10000H остОтДеленияНа 100H;
		b := Int( x ) DIV 100H остОтДеленияНа 100H;
		a := Int( x ) остОтДеленияНа 100H;

		l := ((keybits + 63) DIV 64) остОтДеленияНа 4;
		если l = 0 то  (* 256 bits of key *)
			split( k32[3], k );
			a := Int( tab1[a] / k[0] );
			b := Int( tab0[b] / k[1] );
			c := Int( tab0[c] / k[2] );
			d := Int( tab1[d] / k[3] );
		всё;
		если l в {0, 3} то  (* 192 <= bits of key *)
			split( k32[2], k );
			a := Int( tab1[a] / k[0] );
			b := Int( tab1[b] / k[1] );
			c := Int( tab0[c] / k[2] );
			d := Int( tab0[d] / k[3] )
		всё;
		(* 128 <= bits of key *)
		split( k32[1], k1 );  split( k32[0], k );
		a := Int( tab1[Int( tab0[Int( tab0[a] / k1[0] )] / k[0] )] );
		b := Int( tab0[Int( tab0[Int( tab1[b] / k1[1] )] / k[1] )] );
		c := Int( tab1[Int( tab1[Int( tab0[c] / k1[2] )] / k[2] )] );
		d := Int( tab0[Int( tab1[Int( tab1[d] / k1[3] )] / k[3] )] );

		(* Now perform the MDS matrix multiply  *)
		возврат Int( m1( a ) / my( b ) / mx( c ) / mx( d ) ) +
				арифмСдвиг( Int( mx( a ) / my( b ) / my( c ) / m1( d ) ), 8 ) +
				арифмСдвиг( Int( my( a ) / mx( b ) / m1( c ) / my( d ) ), 16 ) +
				арифмСдвиг( Int( my( a ) / m1( b ) / my( c ) / mx( d ) ), 24 )
	кон F32;

	(* RS_MDS_Encode *)
	проц Encode( k0, k1: цел32 ): цел32;
	перем i, j, b: цел32;  r, g2, g2s16, g3, g3s8, g3s24: мнвоНаБитах32;
	нач
		r := S.подмениТипЗначения( мнвоНаБитах32, k1 );
		нцДля i := 0 до 1 делай
			если i # 0 то  r := r / S.подмениТипЗначения( мнвоНаБитах32, k0 )  всё;
			нцДля j := 0 до 3 делай
				b := S.подмениТипЗначения( цел32, логСдвиг( r, -24 ) );

				g2 := S.подмениТипЗначения( мнвоНаБитах32, b*2 );
				если b > 7FH то  g2 := (g2 / S14d) * Byte0  всё;
				g2s16 := логСдвиг( g2, 16 );

				g3 := S.подмениТипЗначения( мнвоНаБитах32, b DIV 2 ) / g2;
				если нечётноеЛи¿( b ) то  g3 := g3 / S0a6  всё;
				g3s8 := логСдвиг( g3, 8 );
				g3s24 := логСдвиг( g3s8, 16 );

				r := логСдвиг( r, 8 ) / g3s24 / g2s16 / g3s8 / S.подмениТипЗначения( мнвоНаБитах32, b )
			кц
		кц;
		возврат S.подмениТипЗначения( цел32, r )
	кон Encode;



	проц Init0;
	перем
		buf: U.InitBuffer;  i: цел32;
	нач
		нов( buf, 2048 );
		buf.Add( "0A9 067 0B3 0E8 004 0FD 0A3 076 09A 092 080 078 0E4 0DD 0D1 038 " );
		buf.Add( "00D 0C6 035 098 018 0F7 0EC 06C 043 075 037 026 0FA 013 094 048 " );
		buf.Add( "0F2 0D0 08B 030 084 054 0DF 023 019 05B 03D 059 0F3 0AE 0A2 082 " );
		buf.Add( "063 001 083 02E 0D9 051 09B 07C 0A6 0EB 0A5 0BE 016 00C 0E3 061 " );
		buf.Add( "0C0 08C 03A 0F5 073 02C 025 00B 0BB 04E 089 06B 053 06A 0B4 0F1 " );
		buf.Add( "0E1 0E6 0BD 045 0E2 0F4 0B6 066 0CC 095 003 056 0D4 01C 01E 0D7 " );
		buf.Add( "0FB 0C3 08E 0B5 0E9 0CF 0BF 0BA 0EA 077 039 0AF 033 0C9 062 071 " );
		buf.Add( "081 079 009 0AD 024 0CD 0F9 0D8 0E5 0C5 0B9 04D 044 008 086 0E7 " );
		buf.Add( "0A1 01D 0AA 0ED 006 070 0B2 0D2 041 07B 0A0 011 031 0C2 027 090 " );
		buf.Add( "020 0F6 060 0FF 096 05C 0B1 0AB 09E 09C 052 01B 05F 093 00A 0EF " );
		buf.Add( "091 085 049 0EE 02D 04F 08F 03B 047 087 06D 046 0D6 03E 069 064 " );
		buf.Add( "02A 0CE 0CB 02F 0FC 097 005 07A 0AC 07F 0D5 01A 04B 00E 0A7 05A " );
		buf.Add( "028 014 03F 029 088 03C 04C 002 0B8 0DA 0B0 017 055 01F 08A 07D " );
		buf.Add( "057 0C7 08D 074 0B7 0C4 09F 072 07E 015 022 012 058 007 099 034 " );
		buf.Add( "06E 050 0DE 068 065 0BC 0DB 0F8 0C8 0A8 02B 040 0DC 0FE 032 0A4 " );
		buf.Add( "0CA 010 021 0F0 0D3 05D 00F 000 06F 09D 036 042 04A 05E 0C1 0E0 " );
		нцДля i := 0 до 255 делай  tab0[i] :=  buf.GetSet()  кц;

		buf.Init( 2048 );
		buf.Add( "075 0F3 0C6 0F4 0DB 07B 0FB 0C8 04A 0D3 0E6 06B 045 07D 0E8 04B " );
		buf.Add( "0D6 032 0D8 0FD 037 071 0F1 0E1 030 00F 0F8 01B 087 0FA 006 03F " );
		buf.Add( "05E 0BA 0AE 05B 08A 000 0BC 09D 06D 0C1 0B1 00E 080 05D 0D2 0D5 " );
		buf.Add( "0A0 084 007 014 0B5 090 02C 0A3 0B2 073 04C 054 092 074 036 051 " );
		buf.Add( "038 0B0 0BD 05A 0FC 060 062 096 06C 042 0F7 010 07C 028 027 08C " );
		buf.Add( "013 095 09C 0C7 024 046 03B 070 0CA 0E3 085 0CB 011 0D0 093 0B8 " );
		buf.Add( "0A6 083 020 0FF 09F 077 0C3 0CC 003 06F 008 0BF 040 0E7 02B 0E2 " );
		buf.Add( "079 00C 0AA 082 041 03A 0EA 0B9 0E4 09A 0A4 097 07E 0DA 07A 017 " );
		buf.Add( "066 094 0A1 01D 03D 0F0 0DE 0B3 00B 072 0A7 01C 0EF 0D1 053 03E " );
		buf.Add( "08F 033 026 05F 0EC 076 02A 049 081 088 0EE 021 0C4 01A 0EB 0D9 " );
		buf.Add( "0C5 039 099 0CD 0AD 031 08B 001 018 023 0DD 01F 04E 02D 0F9 048 " );
		buf.Add( "04F 0F2 065 08E 078 05C 058 019 08D 0E5 098 057 067 07F 005 064 " );
		buf.Add( "0AF 063 0B6 0FE 0F5 0B7 03C 0A5 0CE 0E9 068 044 0E0 04D 043 069 " );
		buf.Add( "029 02E 0AC 015 059 0A8 00A 09E 06E 047 0DF 034 035 06A 0CF 0DC " );
		buf.Add( "022 0C9 0C0 09B 089 0D4 0ED 0AB 012 0A2 00D 052 0BB 002 02F 0A9 " );
		buf.Add( "0D7 061 01E 0B4 050 004 0F6 0C2 016 025 086 056 055 009 0BE 091 " );
		нцДля i := 0 до 255 делай  tab1[i] :=  buf.GetSet()  кц;
	кон Init0;

нач
	Init0
кон CryptoTwofish.
