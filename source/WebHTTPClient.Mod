(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль WebHTTPClient; (** AUTHOR "TF"; PURPOSE "HTTP client"; *)

использует
	TFLog, Потоки, Strings, IP, DNS, TCP, TLS, WebHTTP, Modules;

перем log : TFLog.Log;

конст Ok* = 0;

тип
	ContentReader * = окласс (Потоки.Чтец)
	перем
		in: Потоки.Чтец;
		encoding: массив 64 из симв8;
		length: размерМЗ;

		проц & InitContentReader * (in: Потоки.Чтец; конст h: WebHTTP.ResponseHeader);
		перем
			token: массив 64 из симв8;
			res: целМЗ;
		нач
			новЧтец(Receive, 1024);
			сам.in := in;
			копируйСтрокуДо0(h.transferencoding, encoding);
			если (encoding # "") и Strings.Match("chunked", encoding) то
				in.ПропустиПробелыИТабуляции(); in.чЦепочкуСимв8ДоБелогоПоля(token); Strings.HexStrToSize(token, length, res); in.ПропустиДоКонцаСтрокиТекстаВключительно();
			иначе
				length := h.contentlength;
			всё;
		кон InitContentReader;

		проц Receive * (перем buf: массив из симв8;  ofs, size, min: размерМЗ;  перем len: размерМЗ; перем res: целМЗ);
		перем
			token: массив 16 из симв8;
			i, total: цел32; r: целМЗ;
			ch: симв8;
		нач
			если (encoding # "") и Strings.Match("chunked", encoding) то
				нцПока (length # 0) и (in.кодВозвратаПоследнейОперации = Потоки.Успех) делай
					нцПока (total < size) и (i < length) делай
						in.чСимв8(ch);
						buf[ofs + total] := ch;
						увел(i);
						увел(total)
					кц;
					i := 0;
					in.ПропустиДоКонцаСтрокиТекстаВключительно;in.ПропустиПробелыИТабуляции; in.чЦепочкуСимв8ДоБелогоПоля(token); Strings.HexStrToSize(token, length, r); in.ПропустиДоКонцаСтрокиТекстаВключительно;
				кц;
				len := total
			иначе
				если length >= 0 то
					нцПока (i < size) и (i < length) делай in.чСимв8(ch);  buf[ofs + i] := ch; увел(i) кц;
					умень(length, i);
				иначе
					нцПока (in.кодВозвратаПоследнейОперации = Потоки.Успех) и (i < size) делай in.чСимв8(ch); buf[ofs + i] := ch; увел(i) кц;
				всё;
				len := i
			всё;
			если len < min то
				res := Потоки.КонецФайла
			иначе
				res := Потоки.Успех
			всё
		кон Receive;
	кон ContentReader;

проц Head*(конст url : массив из симв8; перем con : TCP.Connection; перем header: WebHTTP.ResponseHeader; перем res : целМЗ);
перем
	host : массив 128 из симв8;
	path : массив 256 из симв8;
	port : цел32;
	fadr : IP.Adr;
	w : Потоки.Писарь;
	r : Потоки.Чтец;
нач
	если WebHTTP.SplitHTTPAdr(url, host, path, port) то
		если path = "" то path := "/" всё;
		DNS.HostByName(host, fadr, res);
		если res = DNS.Ok то
			если  con = НУЛЬ то нов(con); con.Open(TCP.NilPort, fadr, port, res); всё;
			если res = TCP.Ok то
				Потоки.НастройПисаря(w, con.ЗапишиВПоток);
				Потоки.НастройЧтеца(r, con.ПрочтиИзПотока);
				WebHTTP.WriteRequestLine(w, 1, 1, WebHTTP.HeadM, path, host); w.пВК_ПС(); w.ПротолкниБуферВПоток();
				WebHTTP.ParseReply(r, header, res, log);
				если res = WebHTTP.OK то res := Ok всё;
				con.Закрой
			иначе
				log.Enter; log.String("Head could not connect to  : "); log.String(host); log.Exit
			всё
		иначе
			log.Enter; log.String("Host not found : "); log.String(host); log.Exit
		всё;
	всё;
кон Head;

(** The HTTP versions is ignored and set to 1.0; uri and host are ignored and taken from the url parameter *)
проц Get*(конст url : массив из симв8; перем rHeader : WebHTTP.RequestHeader;
								перем con : TCP.Connection; перем header: WebHTTP.ResponseHeader; перем out : Потоки.Чтец; перем res : целМЗ);
перем
	host : массив 128 из симв8;
	path : массив 256 из симв8;
	port : цел32;
	fadr : IP.Adr;
	w : Потоки.Писарь;
	x : WebHTTP.AdditionalField;
	tls: TLS.Connection;
нач
	если WebHTTP.SplitHTTPAdr(url, host, path, port) то
		если path = "" то path := "/" всё;
		DNS.HostByName(host, fadr, res);
		если res = DNS.Ok то
			если  con = НУЛЬ то
				если port = WebHTTP.HTTPPort то
					нов(con);
				иначе
					нов(tls);
					con := tls;
				всё;
				con.Open(TCP.NilPort, fadr, port, res);
			всё;
			если res = TCP.Ok то
				Потоки.НастройПисаря(w, con.ЗапишиВПоток);
				Потоки.НастройЧтеца(out, con.ПрочтиИзПотока);
				WebHTTP.WriteRequestLine(w, 1, 0, WebHTTP.GetM, path, host);

				если rHeader.referer # "" то w.пСтроку8("Referer: "); w.пСтроку8(rHeader.referer); w.пВК_ПС() всё;
				если rHeader.useragent # "" то w.пСтроку8("User-Agent: "); w.пСтроку8(rHeader.useragent); w.пВК_ПС() всё;
				если rHeader.accept # "" то w.пСтроку8("Accept: "); w.пСтроку8(rHeader.accept); w.пВК_ПС() всё;
				x := rHeader.additionalFields;

				нцПока x # НУЛЬ делай
					w.пСтроку8(x.key);  w.пСимв8(" "); w.пСтроку8(x.value);w.пВК_ПС();
					x := x.next
				кц;
				w.пВК_ПС(); w.ПротолкниБуферВПоток();

				WebHTTP.ParseReply(out, header, res, log);

				WebHTTP.LogResponseHeader(log, header);

				если res = WebHTTP.OK то res := Ok всё;
			иначе
				log.Enter; log.String("Get could not connect to  : "); log.String(host); log.Exit
			всё
		иначе
			log.Enter; log.String("Host not found : "); log.String(host); log.Exit
		всё;
	всё
кон Get;

проц CleanUp;
нач
	log.Close
кон CleanUp;

нач
	нов(log, "HTTP Client");
	log.SetLogToOut(ложь);
	Modules.InstallTermHandler(CleanUp)
кон WebHTTPClient.
