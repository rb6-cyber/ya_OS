(* ETH Oberon, Copyright 2000 ETH Zürich Institut für Computersysteme, ETH Zentrum, CH-8092 Zürich.
Refer to the general ETH Oberon System license contract available at: http://www.oberon.ethz.ch/ *)

модуль KbdMouse;   (* g.f.	9.7.07 *)


(* replacement for the keyboard and mouse drivers in the Unix ports *)

использует Machine, S := НИЗКОУР, Inputs, Plugins, X11, Displays, XDisplay, Commands, Objects;

#если COOP то
	использует Environment;
#кон

конст
	ML = 0;  MM = 1;  MR = 2;

	ModeSwitch = 13;

	MetaMask = { X11.Mod4Mask, ModeSwitch };


перем
	event: X11.Event;  xbuttons: мнвоНаБитах32;
	compstatus: X11.ComposeStatus;

	disp: XDisplay.Display;

	MMseen, MRseen: булево;

тип
	Poll = окласс
		нач {активное, SAFE,приоритет(Objects.High - 1)}
			нц
				Objects.Sleep( 15 );  PollXQueue;
				#если COOP то
					если Environment.status # Environment.Running то прервиЦикл всё;
				#кон
			кц
		кон Poll;

перем
	poll: Poll; keySymbol: массив 256 из цел32;

	проц TerminateA2;
	перем
		res: целМЗ;
		s: массив 256 из симв8;
	нач
		Commands.Call( "WMTerminator.Shutdown", {}, res, s );
	кон TerminateA2;

	проц CheckAlternateKeys( перем mb: мнвоНаБитахМЗ );
	нач
		если ~MMseen и (X11.ControlMask в xbuttons) то включиВоМнвоНаБитах( mb, MM ) всё;
		если ~MRseen и (X11.Mod1Mask в xbuttons) то включиВоМнвоНаБитах( mb, MR ) всё
	кон CheckAlternateKeys;


	проц SendMouseMsg( x, y, dz: цел32; xbuttons1: мнвоНаБитах32 );
	перем mm: Inputs.AbsMouseMsg;
	нач
		Machine.Release( Machine.X11 );
		mm.keys := {};
		mm.x := x;  mm.y := y;  mm.dz := dz;
		если X11.Button1Mask в xbuttons1 то  включиВоМнвоНаБитах( mm.keys, ML )  всё;
		если X11.Button2Mask в xbuttons1 то  включиВоМнвоНаБитах( mm.keys, MM );  MMseen := истина  всё;
		если X11.Button3Mask в xbuttons1 то  включиВоМнвоНаБитах( mm.keys, MR );  MRseen := истина  всё;
		если ~(MMseen и MRseen) то  CheckAlternateKeys( mm.keys )  всё;
		Inputs.mouse.Handle( mm );
		Machine.Acquire( Machine.X11 );
	кон SendMouseMsg;

	проц SendKeyboardMsg( km: Inputs.KeyboardMsg );
	нач
		Machine.Release( Machine.X11 );
		Inputs.keyboard.Handle( km );
		Machine.Acquire( Machine.X11 );
	кон SendKeyboardMsg;

	проц PollXQueue;
	конст bufsize = 20;
	перем keycount, xr, yr, x, y, dz, i: цел32;
		rw, cw: X11.Window;
		keysym: X11.KeySym;  xd: X11.DisplayPtr;
		newxbuttons, bdiff: мнвоНаБитах32;
		km: Inputs.KeyboardMsg;
		kp : X11.KeyEvent;
		be : X11.ButtonEvent;
		em: X11.ExposeEvent;
		cm : X11.ClientMessageEvent;
		cn: X11.ConfigureEvent;
		events: цел32;
		buffer: массив bufsize из симв8;
		
		
	(* проц ВыведиСобытиеКлавиатуры(kp1 : X11.KeyEvent);
	тело
		KernelLog.String("«Unix.KbdMouse: typ = "); KernelLog.Int(event.typ, 0); 
		KernelLog.String(", keycount = "); KernelLog.Int(keycount,0); 
		KernelLog.String(", keysym = ");
		KernelLog.Int(keysym,0);
		KernelLog.String(", keycode = "); KernelLog.Int(kp1.keycode, 0);
		KernelLog.String(", state = "); KernelLog.Int(системно_зависимое_целое(kp1.state), 0);
		KernelLog.String("»"); KernelLog.Ln;
	кн ВыведиСобытиеКлавиатуры; *)
		
	нач
		xd := disp.xdisp;
		Machine.Acquire( Machine.X11 );
		X11.Synchronize(xd, X11.True);
		events := X11.Pending( xd );
		Machine.Release( Machine.X11 );
		нцПока events > 0 делай
			keycount := 0; keysym := 0; bdiff := {}; newxbuttons := {};
			Machine.Acquire( Machine.X11 );
			X11.NextEvent( xd, event );
			просей event.typ из
			| X11.KeyPress: kp := S.подмениТипЗначения( X11.KeyEvent, event );
					X11.lastEventTime := kp.time;
					исключиИзМнваНаБитах(kp.state, ModeSwitch);
					keycount := X11.LookupString( kp, buffer, bufsize, keysym, compstatus );
					
					(* ВыведиСобытиеКлавиатуры(kp); *)
					
					X11.QueryPointer( xd, event.window, rw, cw, xr, yr, x, y, newxbuttons );
					исключиИзМнваНаБитах(newxbuttons, ModeSwitch);
					
					(* KernelLog.String("newxbuttons = "); KernelLog.Int(системно_зависимое_целое(newxbuttons),0); KernelLog.Ln; *)
					если keycount = 0 то
						bdiff := newxbuttons / xbuttons;  xbuttons := newxbuttons;
						km.ch := 0X;
						если X11.ShiftMask в bdiff то km.keysym := Inputs.KsShiftL
						аесли X11.ControlMask в bdiff то
							km.keysym := Inputs.KsControlL;
							если ~MMseen то  SendMouseMsg( x, y, 0, xbuttons )  всё
						аесли X11.Mod1Mask в bdiff то
							km.keysym := Inputs.KsAltL;
							если ~MRseen то SendMouseMsg( x, y, 0, xbuttons )  всё
						аесли MetaMask*bdiff # {} то km.keysym := Inputs.KsMetaL
						аесли X11.Mod5Mask в bdiff то km.keysym := Inputs.KsAltR
						аесли keysym = 0FE20H то  (* handle Shift-Tab key *)
							km.keysym := Inputs.KsTab; km.ch :=09X
						иначе
							km.keysym := цел32(keysym);
						всё;
						km.flags := KeyState( );
						SendKeyboardMsg( km )
					иначе
						если (X11.ControlMask в kp.state) и (keysym = 32) то (* check Ctrl-space *)
							km.ch := симв8ИзКода( keysym );  km.flags := KeyState( );
							km.keysym := цел32(keysym);
							SendKeyboardMsg( km ); (* IME keys *)
						иначе
							xbuttons := newxbuttons;  i := 0;
							нцПока i < keycount делай
								km.ch := buffer[i];
								если km.ch = 0F1X то  km.ch := 0A4X
								аесли km.ch = 0F2X то km.ch := 0A5X
								всё;
								km.keysym := keySymbol[кодСимв8( km.ch )];
								km.flags := KeyState( );
								SendKeyboardMsg( km );
								увел( i )
							кц
						всё;
					всё;
			| X11.KeyRelease: kp := S.подмениТипЗначения(X11.KeyEvent, event);
					исключиИзМнваНаБитах(kp.state, ModeSwitch);

					(* ВыведиСобытиеКлавиатуры(kp); *)

					X11.lastEventTime := kp.time;
					X11.QueryPointer( xd, event.window, rw, cw, xr, yr, x, y, newxbuttons );
					исключиИзМнваНаБитах(newxbuttons, ModeSwitch);
					(* KernelLog.String("newxbuttons = "); KernelLog.Int(системно_зависимое_целое(newxbuttons),0); KernelLog.Ln; *)

					bdiff := newxbuttons / xbuttons;  xbuttons := newxbuttons;
					если bdiff # {} то
						km.ch := 0X;
						если X11.ShiftMask в bdiff то km.keysym := Inputs.KsShiftL
						аесли X11.ControlMask в bdiff то
							km.keysym := Inputs.KsControlL;
							если ~MMseen то  SendMouseMsg( x, y, 0, xbuttons )  всё
						аесли X11.Mod1Mask в bdiff то
							km.keysym := Inputs.KsAltL;
							если ~MRseen то SendMouseMsg( x, y, 0, xbuttons )  всё
						аесли MetaMask*bdiff # {} то km.keysym := Inputs.KsMetaL
						аесли X11.Mod5Mask в bdiff то km.keysym := Inputs.KsAltR
						всё;
						km.flags := KeyState( ) + {Inputs.Release};
						SendKeyboardMsg( km )
					иначе
						если kp.keycode = 041H то (* пробел *)
							km.keysym := 020H; (* http://wiki.linuxquestions.org/wiki/List_of_Keysyms_Recognised_by_Xmodmap *)
							km.ch := симв8ИзКода( km.keysym );
							km.flags := KeyState( ) + {Inputs.Release};
							SendKeyboardMsg( km ) всё;
						(* KernelLog.String("Release..."); KernelLog.Hex(kp.keycode, 0); KernelLog.Ln; *)
					всё
			| X11.ButtonPress: be := S.подмениТипЗначения(X11.ButtonEvent, event);
					X11.lastEventTime := be.time;
					dz := 0;
					просей be.button из
					| X11.Button1:  включиВоМнвоНаБитах( xbuttons, X11.Button1Mask )
					| X11.Button2:  включиВоМнвоНаБитах( xbuttons, X11.Button2Mask )
					| X11.Button3:  включиВоМнвоНаБитах( xbuttons, X11.Button3Mask )
					| X11.Button4: dz := -1
					| X11.Button5: dz := +1
					иначе  (* ignore *)
					всё;
					SendMouseMsg( be.x, be.y, dz, xbuttons )
			| X11.ButtonRelease: be := S.подмениТипЗначения(X11.ButtonEvent, event);
					X11.lastEventTime := be.time;
					просей be.button из
					| X11.Button1:  исключиИзМнваНаБитах( xbuttons, X11.Button1Mask )
					| X11.Button2:  исключиИзМнваНаБитах( xbuttons, X11.Button2Mask )
					| X11.Button3:  исключиИзМнваНаБитах( xbuttons, X11.Button3Mask )
					иначе  (* ignore *)
					всё;
					SendMouseMsg( be.x, be.y, 0, xbuttons )
			| X11.MotionNotify:
					X11.QueryPointer( xd, event.window, rw, cw, xr, yr, x, y, xbuttons );
					SendMouseMsg( x, y, 0, xbuttons )
			| X11.Expose, X11.GraphicsExpose:
					em := S.подмениТипЗначения( X11.ExposeEvent, event );
					если em.count = 0 то  (* wait until last message*)
						(* Let DisplayRefresher handle this *)
						km.keysym := 0FFC6H;
						SendKeyboardMsg( km );
					всё;
			| X11.NoExpose:
			| X11.MappingNotify:
					(* KernelLog.String("MappingNotify"); KernelLog.Ln; *)
					X11.RefreshKeyboardMapping( адресОт( event ) )
			| X11.ClientMessage:
					cm := S.подмениТипЗначения( X11.ClientMessageEvent, event );
					если  cm.data[0] = disp.wmDelete то
						(* shutdown *)
						Machine.Release( Machine.X11 );
						TerminateA2;
						возврат;
					(*	Modules.Shutdown( Modules.Reboot ); *)
					всё;
			| X11.UnmapNotify:
			| X11.MapNotify:
			| X11.SelectionClear:
					если X11.ClearSelection # НУЛЬ то X11.ClearSelection(); всё
			| X11.SelectionNotify:
					если X11.ReceiveSelection # НУЛЬ то
						Machine.Release( Machine.X11 );
						X11.ReceiveSelection( S.подмениТипЗначения( X11.SelectionEvent, event ) );
						Machine.Acquire( Machine.X11 );
					всё
			| X11.SelectionRequest:
					если X11.SendSelection # НУЛЬ то
						Machine.Release( Machine.X11 );
						X11.SendSelection( S.подмениТипЗначения( X11.SelectionRequestEvent, event ) );
						Machine.Acquire( Machine.X11 )
					всё
			| X11.ConfigureNotify:  cn := S.подмениТипЗначения(X11.ConfigureEvent, event);
						(* KernelLog.String("ConfigureNotify"); KernelLog.Ln; *)
			иначе
				(* ignore *)
			всё;
			events := X11.Pending( xd );
			Machine.Release( Machine.X11 );
		кц;
	кон PollXQueue;

	(* Returns wether key (SHIFT, CTRL or ALT) is pressed *)
	проц KeyState( ): мнвоНаБитахМЗ;
	перем keys: мнвоНаБитахМЗ;
	нач
		keys := {};
		если X11.ShiftMask в xbuttons то  включиВоМнвоНаБитах( keys, Inputs.LeftShift )  всё;
		если X11.ControlMask в xbuttons то  включиВоМнвоНаБитах( keys, Inputs.LeftCtrl )  всё;
		если X11.Mod1Mask в xbuttons то  включиВоМнвоНаБитах( keys, Inputs.LeftAlt )  всё;
		если MetaMask*xbuttons # {} то  включиВоМнвоНаБитах( keys, Inputs.LeftMeta )  всё;
		если X11.Mod5Mask в xbuttons то  включиВоМнвоНаБитах( keys, Inputs.RightAlt )  всё;
		возврат keys
	кон KeyState;


	проц Keysym( конст str: массив из симв8 ): X11.KeySym;
	нач
		возврат X11.StringToKeysym( адресОт( str ) )
	кон Keysym;

	проц DisableMouseEmulation*;
	нач
		MMseen := истина; MRseen := истина;
	кон DisableMouseEmulation;


	проц Init*;
	перем FK: массив 8 из симв8;
		n, i, k: цел32;  modifiers: X11.Modifiers;
		shift, control, meta, alt, capslock, numlock: X11.KeySym;


		проц Rebind( конст keystr: массив из симв8;  nofmod: цел32;  key: симв8 );
		перем newkeystr: массив 8 из симв8;
			oldkeysym: X11.KeySym;
		нач
			Machine.Acquire( Machine.X11 );
			oldkeysym := Keysym( keystr );
			newkeystr[0] := key;  newkeystr[1] := 0X;
			X11.RebindKeysym( disp.xdisp, oldkeysym, modifiers, nofmod, адресОт( newkeystr ), 1 );
			Machine.Release( Machine.X11 )
		кон Rebind;

		проц Rebind4( конст keyString: массив из симв8;  n1: цел32;  key: симв8 );
		нач
			Rebind( keyString, n1, key );
			modifiers[n1] := shift;  Rebind( keyString, n1 + 1, key );
			modifiers[n1] := control;  Rebind( keyString, n1 + 1, key );
			modifiers[n1 + 1] := shift;  Rebind( keyString, n1 + 2, key );
		кон Rebind4;

	нач
		MMseen := ложь;  MRseen := ложь;


		Machine.Acquire( Machine.X11 );
		X11.SelectInput( disp.xdisp, disp.primary,
						  X11.ExposureMask + X11.ButtonPressMask + X11.OwnerGrabButtonMask +
						  X11.ButtonReleaseMask + X11.PointerMotionHintMask + X11.PointerMotionMask +
						  X11.KeyPressMask + X11.KeyReleaseMask + X11.StructureNotifyMask );
	 	shift := Keysym( "Shift_L" );  control := Keysym( "Control_L" );
		meta := Keysym( "Meta-L" );  alt := Keysym( "Alt_L" );
		capslock := Keysym( "Caps_Lock" );  numlock := Keysym( "Num_Lock" );
		Machine.Release( Machine.X11 );

		modifiers[0] := shift;
		Rebind( "Pause", 1, 0ADX );   (* SHIFT-BREAK *)

		modifiers[0] := control;	Rebind( "Return", 1, 0AX );
		   modifiers[1] := numlock;	Rebind( "Return", 2, 0AX );
		   modifiers[1] := capslock;	Rebind( "Return", 2, 0AX );
			modifiers[2] := numlock;	Rebind( "Return", 3, 0AX );


		нцДля k := 0 до 4 делай
			просей k из
			| 0:   n := 0;
			| 1:   modifiers[0] := meta;  n := 1;
			| 2:   modifiers[0] := capslock;  n := 1
			| 3:   modifiers[0] := numlock;  n := 1
			| 4:   modifiers[0] := capslock;  modifiers[1] := numlock;  n := 2
			всё;
			i := 0;  FK := "F0";
			нцПока i < 10 делай FK[1] := симв8ИзКода( кодСимв8( "0" ) + i );  Rebind4( FK, n, симв8ИзКода( 0F0H + i ) );  увел( i ) кц;
			i := 10;  FK := "F10";
			нцПока i <= 12 делай FK[2] := симв8ИзКода( кодСимв8( "0" ) + i - 10 );  Rebind4( FK, n, симв8ИзКода( 0F0H + i ) );  увел( i ) кц;

			Rebind4( "BackSpace", n, 7FX );
			Rebind4( "Delete", n, 0A1X );
			Rebind4( "Escape", n, 1BX );
			Rebind4( "Up", n, 0C1X );  Rebind4( "Down", n, 0C2X );
			Rebind4( "Left", n, 0C4X );  Rebind4( "Right", n, 0C3X );
			если k < 3 то
				(* do not for NumLock on *)
				Rebind4( "KP_Up", n, 0C1X );  Rebind4( "KP_Down", n, 0C2X );
				Rebind4( "KP_Left", n, 0C4X );  Rebind4( "KP_Right", n, 0C3X );
			всё;
			Rebind4( "Prior", n, 0A2X );  Rebind4( "KP_Prior", n, 0A2X );
			Rebind4( "Next", n, 0A3X );  Rebind4( "KP_Next", n, 0A3X );
			Rebind4( "Insert", n, 0A0X );
			Rebind4( "Home", n, 0A8X );  Rebind4( "KP_Home", n, 0A8X );
			Rebind4( "End", n, 0A9X );  Rebind4( "KP_End", n, 0A9X );
		кц;


		(* special keyboard: *)
		modifiers[0] := shift;  modifiers[1] := meta;
		нцДля i := 0 до 2 делай
			Rebind( "aacute", i, 094X );
			Rebind( "agrave", i, 08BX );
			Rebind( "Adiaeresis", i, 080X );		Rebind( "adiaeresis", i, 083X );
			Rebind( "acircumflex", i, 086X );
			Rebind( "eacute", i, 090X );
			Rebind( "egrave", i, 08CX );
			Rebind( "ediaeresis", i, 091X );
			Rebind( "ecircumflex", i, 087X );
			Rebind( "igrave", i, 08DX );
			Rebind( "idiaeresis", i, 092X );
			Rebind( "icircumflex", i, 088X );
			Rebind( "ograve", i, 08EX );
			Rebind( "Odiaeresis", i, 081X );		Rebind( "odiaeresis", i, 084X );
			Rebind( "ocircumflex", i, 089X );
			Rebind( "ugrave", i, 08FX );
			Rebind( "Udiaeresis", i, 082X );		Rebind( "udiaeresis", i, 085X );
			Rebind( "ucircumflex", i, 08AX );
			Rebind( "ccedilla", i, 093X );
			Rebind( "ntilde", i, 095X );
			Rebind( "ssharp", i, 096X );

			Rebind( "idotless", i, 0FDX);
			Rebind( "Iabovedot", i, 0DDX);
			Rebind( "gbreve", i, 0F0X );
			Rebind( "Gbreve", i, 0D0X );
			Rebind( "scedilla", i, 0FEX );
			Rebind( "Scedilla", i, 0DEX );
		кц;

		InitKeysym;

		нов( poll );
	кон Init;

	проц InitKeysym;
	перем i: цел32;
	нач
		нцДля i := 0 до 255 делай keySymbol[i] := i кц;
		keySymbol[07FH] := Inputs.KsBackSpace;
		keySymbol[009H] := Inputs.KsTab;
		keySymbol[00AH] := Inputs.KsReturn;
		keySymbol[00DH] := Inputs.KsReturn;

		keySymbol[0C1H] := Inputs.KsUp;
		keySymbol[0C2H] := Inputs.KsDown;
		keySymbol[0C3H] := Inputs.KsRight;
		keySymbol[0C4H] := Inputs.KsLeft;

		keySymbol[0A0H] := Inputs.KsInsert;
		keySymbol[0A1H] := Inputs.KsDelete;
		keySymbol[0A2H] := Inputs.KsPageUp;
		keySymbol[0A3H] := Inputs.KsPageDown;
		keySymbol[0A8H] := Inputs.KsHome;
		keySymbol[0A9H] := Inputs.KsEnd;
		keySymbol[01BH] := Inputs.KsEscape;

		нцДля i := 0F1H до 0FCH делай keySymbol[i] := 0FFBEH + (i - 0F1H) кц;
		keySymbol[0A4H] := Inputs.KsF1;
		keySymbol[0A5H] := Inputs.KsF2;
	кон InitKeysym;

	проц GetXDisplay;
	перем p: Plugins.Plugin;
	нач
		p := Displays.registry.Await( "XDisplay" );  disp := p( XDisplay.Display )
	кон GetXDisplay;

нач
	утв( цел32({0} ) = 1 );
	GetXDisplay;
кон KbdMouse.


(** Remark:

1. Keyboard character codes correspond to the ASCII character set. Some other important codes are:

	SHIFT-BREAK	0ADX
	BREAK	0ACX
	F1 ... F12	0F1X ... 0FCX
	UP ARROW	0C1X
	RIGHT ARROW	0C3X
	DOWN ARROW	0C2X
	LEFT ARROW	0C4X
	INSERT	0A0X
	DELETE	0A1X
	PAGE-UP	0A2X
	PAGE-DOWN	0A3X

	some none ascii character get mapped to UTF8:
	ä, Ä	131, 128
	ö, Ö	132, 129
	ü, Ü	133, 130
	ß	150
	. . .

*)


Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFC6 (F9)
>
>
>
>Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFC6 (F9)Key:  '' (00000000)Keysym: 0000FFC6 (F9)
>
>
>
>Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)



Key:  '' (00000000)Keysym: 000006A3 (No Keysym)Key:  '1' (00000031)Keysym: 00000031 (No Keysym)Key:  '2' (00000032)Keysym: 00000032 (No Keysym)Key:  '3' (00000033)Keysym: 00000033 (No Keysym)Key:  '4' (00000034)Keysym: 00000034 (No Keysym)Key:  '5' (00000035)Keysym: 00000035 (No Keysym)Key:  '6' (00000036)Keysym: 00000036 (No Keysym)Key:  '7' (00000037)Keysym: 00000037 (No Keysym)Key:  '8' (00000038)Keysym: 00000038 (No Keysym)Key:  '9' (00000039)Keysym: 00000039 (No Keysym)Key:  '0' (00000030)Keysym: 00000030 (No Keysym)Key:  '-' (0000002D)Keysym: 0000002D (No Keysym)Key:  '=' (0000003D)Keysym: 0000003D (No Keysym)
>
>
>
>Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 



000006CA (No Keysym)Key:  '' (00000000)Keysym: 000006C3 (No Keysym)Key:  '' (00000000)Keysym: 000006D5 (No Keysym)Key:  '' (00000000)Keysym: 000006CB (No Keysym)Key:  '' (00000000)Keysym: 000006C5 (No Keysym)Key:  '' (00000000)Keysym: 000006CE (No Keysym)Key:  '' (00000000)Keysym: 000006C7 (No Keysym)Key:  '' (00000000)Keysym: 000006DB (No Keysym)Key:  '' (00000000)Keysym: 000006DD (No Keysym)Key:  '' (00000000)Keysym: 000006DA (No Keysym)Key:  '' (00000000)Keysym: 000006C8 (No Keysym)Key:  '' (00000000)Keysym: 000006DF (No Keysym)
>
>Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  'a' (00000061)Keysym: 00000061 (No Keysym)Key:  's' (00000073)Keysym: 00000073 (No Keysym)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)Keysym: 0000FFE7 (MetaL)Key:  '' (00000000)Keysym: 0000FFE9 (AltL)Key:  '' (00000000)





Keysym: 000006C6 (No Keysym)Key:  '' (00000000)Keysym: 000006D9 (No Keysym)Key:  '' (00000000)Keysym: 000006D7 (No Keysym)Key:  '' (00000000)Keysym: 000006C1 (No Keysym)Key:  '' (00000000)Keysym: 000006D0 (No Keysym)Key:  '' (00000000)Keysym: 000006D2 (No Keysym)Key:  '' (00000000)Keysym: 000006CF (No Keysym)Key:  '' (00000000)Keysym: 000006CC (No Keysym)Key:  '' (00000000)Keysym: 000006C4 (No Keysym)Key:  '' (00000000)Keysym: 000006D6 (No Keysym)Key:  '' (00000000)Keysym: 000006DC (No Keysym)Key:  '' (00000000)





Keysym: 000006D1 (No Keysym)Key:  '' (00000000)Keysym: 000006DE (No Keysym)Key:  '' (00000000)Keysym: 000006D3 (No Keysym)Key:  '' (00000000)Keysym: 000006CD (No Keysym)Key:  '' (00000000)Keysym: 000006C9 (No Keysym)Key:  '' (00000000)Keysym: 000006D4 (No Keysym)Key:  '' (00000000)Keysym: 000006D8 (No Keysym)Key:  '' (00000000)Keysym: 000006C2 (No Keysym)Key:  '' (00000000)Keysym: 000006C0 (No Keysym)Key:  '.' (0000002E)Keysym: 0000002E (No Keysym)
