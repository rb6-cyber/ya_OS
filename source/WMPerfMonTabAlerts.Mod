модуль WMPerfMonTabAlerts; (** AUTHOR "staubesv"; PURPOSE "Perfomance Monitor tab for Alerts"; *)
(**
 * History:
 *
 *	02.03.2007	First release (staubesv)
 *)

использует
	Kernel, Strings,
	WMComponents, WMStandardComponents, WMGrids, WMStringGrids, WMGraphics, WMEditors, WMDialogs,
	Perf := WMPerfMonComponents, Alerts := WMPerfMonAlerts;

конст
	PollingInterval = 500; (* ms *)

	GridBgFillColor = 0646464FFH;

тип

	CellData = укль на запись
		id : цел32;
	кон;

	AlertsTab* = окласс(WMComponents.VisualComponent)
	перем
		(* status panel *)
		statusIndicator, statusLabel : WMStandardComponents.Label;
		onOffBtn : WMStandardComponents.Button;

		(* alerts panel *)
		alertsGrid : WMStringGrids.StringGrid;

		(* rules panel *)
		rulesGrid : WMStringGrids.StringGrid;
		resetBtn, addBtn, removeBtn : WMStandardComponents.Button;
		alertEdit : WMEditors.TextField;

		(* rule files panel *)
		saveBtn, loadBtn, appendBtn : WMStandardComponents.Button;
		filenameEdit : WMEditors.TextField;

		oldStatus, status : Alerts.Status;

		filename : массив 256 из симв8;
		modified : булево;

		spacings : WMGrids.Spacings;

		timer : Kernel.Timer;
		dead, alive : булево;

		проц HandleButtons(sender, data : динамическиТипизированныйУкль);
		перем string, msg : массив 256 из симв8; nofRemoved, res : целМЗ;
		нач
			если sender = onOffBtn то
				если status.enabled то
					Alerts.DisableAlerts;
				иначе
					Alerts.EnableAlerts;
				всё;
			аесли sender = resetBtn то
				ResetSelectedAlerts;
			аесли sender = addBtn то
				alertEdit.GetAsString(string);
				Alerts.AddByString(string, msg, res);
				если res # Alerts.Ok то
					WMDialogs.Error("Error", msg);
				иначе
					modified := истина;
				всё;
			аесли sender = removeBtn то
				nofRemoved := RemoveSelectedAlerts();
				если nofRemoved > 0 то modified := истина; всё;
			аесли (sender = loadBtn) или (sender = appendBtn) то
				filenameEdit.GetAsString(string);
				Alerts.LoadRules(string, sender = appendBtn, msg, res);
				если res # Alerts.Ok то
					WMDialogs.Error("Error", msg);
				всё;
			аесли sender = saveBtn то
				filenameEdit.GetAsString(string);
				Alerts.StoreRules(string, msg, res);
				если res # Alerts.Ok то
					WMDialogs.Error("Error", msg);
				всё;
			всё;
		кон HandleButtons;

		проц ResetSelectedAlerts;
		перем scol, srow, ecol, erow, row: размерМЗ; res: целМЗ; ptr : динамическиТипизированныйУкль; cellData : CellData; msg : массив 128 из симв8;
		нач
			rulesGrid.Acquire;
			rulesGrid.model.Acquire;
			rulesGrid.GetSelection(scol, srow, ecol, erow);
			если (srow >= 1) и (erow >= 1) то
				нцДля row := srow до erow делай
					ptr := rulesGrid.model.GetCellData(0, row);
					если ptr # НУЛЬ то cellData := ptr (CellData); всё;
					если cellData # НУЛЬ то
						Alerts.SetStateByID(cellData.id, Alerts.Reset, msg, res);
					всё;
				кц;
			всё;
			rulesGrid.model.Release;
			rulesGrid.Release;
		кон ResetSelectedAlerts;

		проц RemoveSelectedAlerts() : цел32;
		перем scol, srow, ecol, erow, row : размерМЗ; ptr : динамическиТипизированныйУкль; cellData : CellData; nofRemoved : цел32;
		нач
			rulesGrid.Acquire;
			rulesGrid.model.Acquire;
			rulesGrid.GetSelection(scol, srow, ecol, erow);
			если (srow >= 1) и (erow >= 1) то
				нцДля row := srow до erow делай
					ptr := rulesGrid.model.GetCellData(0, row);
					если ptr # НУЛЬ то cellData := ptr (CellData); всё;
					если cellData # НУЛЬ то
						nofRemoved := nofRemoved + Alerts.RemoveAlertByID(cellData.id);
					всё;
				кц;
			всё;
			rulesGrid.model.Release;
			rulesGrid.Release;
			возврат nofRemoved;
		кон RemoveSelectedAlerts;

		проц Update;
		нач
			status := Alerts.GetStatus();
			если status.stamp # oldStatus.stamp то
				если (status.filename # "") и (filename = "") то
					копируйСтрокуДо0(status.filename, filename); modified := ложь;
				иначе
					если status.filename # filename то modified := ложь; всё;
				всё;
				UpdateStatusIndicator(status.enabled);
				UpdateStatusLabel(status);
				UpdateGrids;
				oldStatus := status;
			аесли modified то
				UpdateStatusLabel(status);
			всё;
		кон Update;

		проц UpdateStatusIndicator(enabled : булево);
		нач
			если enabled то
				statusIndicator.fillColor.Set(WMGraphics.Green);
				statusIndicator.caption.SetAOC("ON");
				onOffBtn.caption.SetAOC("Disable");
			иначе
				statusIndicator.fillColor.Set(WMGraphics.Red);
				statusIndicator.caption.SetAOC("OFF");
				onOffBtn.caption.SetAOC("Enable");
			всё;
		кон UpdateStatusIndicator;

		проц UpdateStatusLabel(status : Alerts.Status);
		перем nbr, caption : массив 128 из симв8;
		нач
			caption := "  ";
			Strings.IntToStr(status.nbrOfAlerts, nbr);
			Strings.Append(caption, nbr);
			Strings.Append(caption, " alerts, ");

			Strings.IntToStr(status.nbrOfRules, nbr);
			Strings.Append(caption, nbr);
			Strings.Append(caption, " rules loaded");
			если status.filename # "" то
				Strings.Append(caption, " from file "); Strings.Append(caption, status.filename);
				если modified то Strings.Append(caption, " (modified)"); всё;
			всё;
			Strings.Append(caption, ".");
			statusLabel.caption.SetAOC(caption);
		кон UpdateStatusLabel;

		проц ComputeAlertInfo(alerts : Alerts.Alerts; перем nbrOfRules, nbrOfAlerts : цел32);
		перем i : размерМЗ;
		нач
			nbrOfRules := 0; nbrOfAlerts := 0;
			если alerts # НУЛЬ то
				нцДля i := 0 до длинаМассива(alerts)-1 делай
					увел(nbrOfRules);
					если alerts[i].state = Alerts.Triggered то
						увел(nbrOfAlerts);
					всё;
				кц;
			всё;
		кон ComputeAlertInfo;

		проц SetRowText(grid : WMStringGrids.StringGrid; alert : Alerts.AlertInfo; row : цел32);
		перем caption : массив 128 из симв8; col : цел32; cellData : CellData;
		нач
			нов(cellData); cellData.id := alert.id;
			grid.model.SetCellData(0, row, cellData);
			grid.model.SetTextAlign(0, row, WMGraphics.AlignCenter);
			grid.model.SetTextAlign(4, row, WMGraphics.AlignCenter);
			нцДля col := 0 до 4 делай
				просей col из
					|0: Alerts.GetStateString(alert.state, caption);
					|1: Alerts.GetTypeString(alert.type, caption);
					|2: Alerts.GetFullTriggerString(alert, caption);
					|3: Strings.FloatToStr(alert.violation, 4, 2, 0, caption);
					|4: Strings.IntToStr(alert.nbrOfViolations, caption);
				иначе
				всё;
				grid.model.SetCellText(col, row, Strings.NewString(caption));
			кц;
		кон SetRowText;

		проц UpdateGrids;
		перем
			gridRow, alertsGridRow : цел32; alerts : Alerts.Alerts;
			nbrOfRules, nbrOfAlerts : цел32;
			i : размерМЗ;
		нач
			alerts := Alerts.GetAlerts();
			ComputeAlertInfo(alerts, nbrOfRules, nbrOfAlerts);
			rulesGrid.Acquire;
			rulesGrid.model.Acquire;
			alertsGrid.Acquire;
			alertsGrid.model.Acquire;
			если alerts # НУЛЬ то
				rulesGrid.model.SetNofRows(nbrOfRules + 1); gridRow := 1;
				alertsGrid.model.SetNofRows(nbrOfAlerts + 1); alertsGridRow := 1;
				нцДля i := 0 до длинаМассива(alerts)-1 делай
					если alerts[i].state = Alerts.Triggered то
						SetRowText(alertsGrid, alerts[i], alertsGridRow);
						увел(alertsGridRow);
					всё;
					SetRowText(rulesGrid, alerts[i], gridRow);
					увел(gridRow);
				кц;
			иначе
				rulesGrid.model.SetNofRows(1);
				alertsGrid.model.SetNofRows(1);
			всё;
			alertsGrid.model.Release;
			alertsGrid.Release;
			rulesGrid.model.Release;
			rulesGrid.Release;
		кон UpdateGrids;

		проц NewGrid() : WMStringGrids.StringGrid;
		перем grid : WMStringGrids.StringGrid;
		нач
			нов(grid);
			grid.fixedRows.Set(1);
			grid.fillColor.Set(GridBgFillColor);
			grid.alignment.Set(WMComponents.AlignTop);
			grid.SetSelectionMode(WMGrids.GridSelectRows);
			grid.SetSelection(-1, -1, -1, -1);
			grid.alwaysShowScrollX.Set(ложь); grid.showScrollX.Set(истина);
			grid.alwaysShowScrollY.Set(ложь); grid.showScrollY.Set(истина);
			grid.allowColResize.Set(истина); grid.allowRowResize.Set(ложь);
			grid.adjustFocusPosition.Set(ложь);

			нов(spacings, 5);
			spacings[0] := 40; spacings[1] := 80; spacings[2] := 480; spacings[3] := 50; spacings[4] := 70;

			grid.Acquire;
			grid.model.Acquire;
			grid.model.SetNofCols(5); grid.SetColSpacings(spacings);
			grid.model.SetNofRows(1);
			(* column titles *)
			grid.model.SetCellText(0, 0, Strings.NewString("Status")); grid.model.SetTextAlign(0, 0, WMGraphics.AlignCenter);
			grid.model.SetCellText(1, 0, Strings.NewString("Type"));
			grid.model.SetCellText(2, 0, Strings.NewString("Trigger"));
			grid.model.SetCellText(3, 0, Strings.NewString("Violation"));
			grid.model.SetCellText(4, 0, Strings.NewString("NbrOfAlerts"));
			grid.model.Release;
			grid.Release;
			возврат grid;
		кон NewGrid;

		проц CreateStatusPanel() : WMStandardComponents.GroupPanel;
		перем panel : WMStandardComponents.GroupPanel;
		нач
			panel := Perf.NewGroupPanel("Status", WMComponents.AlignTop, 45);

			statusIndicator := Perf.NewLabel("", WMComponents.AlignLeft, 80, 0);
			statusIndicator.alignH.Set(WMGraphics.AlignCenter); statusIndicator.alignV.Set(WMGraphics.AlignCenter);
			panel.AddContent(statusIndicator);

			onOffBtn := Perf.NewButton("", HandleButtons); panel.AddContent(onOffBtn);
			statusLabel := Perf.NewLabel("", WMComponents.AlignClient, 0, 0); panel.AddContent(statusLabel);

			возврат panel;
		кон CreateStatusPanel;

		проц CreateAlertsPanel() : WMStandardComponents.GroupPanel;
		перем panel : WMStandardComponents.GroupPanel;
		нач
			panel := Perf.NewGroupPanel("Alerts", WMComponents.AlignTop, 100);
			alertsGrid := NewGrid(); alertsGrid.bounds.SetHeight(100); alertsGrid.SetSelectionMode(WMGrids.GridSelectNone);
			panel.AddContent(alertsGrid);
			возврат panel;
		кон CreateAlertsPanel;

		проц CreateRulesPanel() : WMStandardComponents.GroupPanel;
		перем panel : WMStandardComponents.GroupPanel; line : WMStandardComponents.Panel;
		нач
			panel := Perf.NewGroupPanel("Rules", WMComponents.AlignTop, 180);
			line := Perf.NewPanel(WMComponents.AlignBottom, 0, Perf.LineHeight); panel.AddContent(line);

			resetBtn := Perf.NewButton("Reset", HandleButtons); line.AddContent(resetBtn);
			removeBtn := Perf.NewButton("Remove", HandleButtons); line.AddContent(removeBtn);
			addBtn := Perf.NewButton("Add", HandleButtons); line.AddContent(addBtn);
			alertEdit := Perf.NewTextField(0); alertEdit.alignment.Set(WMComponents.AlignClient); line.AddContent(alertEdit);
			alertEdit.SetAsString("PluginName.ValueName sticky|singleshot|multishot trigger value1 value2 onAlertCmd onLeaveCmd");

			rulesGrid := NewGrid(); rulesGrid.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(rulesGrid);

			возврат panel;
		кон CreateRulesPanel;

		проц CreateRulefilesPanel() : WMStandardComponents.GroupPanel;
		перем panel : WMStandardComponents.GroupPanel;
		нач
			panel := Perf.NewGroupPanel("Rule files", WMComponents.AlignTop, 45);

			loadBtn := Perf.NewButton("Load", HandleButtons); panel.AddContent(loadBtn);
			appendBtn := Perf.NewButton("Append", HandleButtons); panel.AddContent(appendBtn);
			saveBtn := Perf.NewButton("Store", HandleButtons); panel.AddContent(saveBtn);
			filenameEdit := Perf.NewTextField(0); filenameEdit.alignment.Set(WMComponents.AlignClient); panel.AddContent(filenameEdit);

			возврат panel;
		кон CreateRulefilesPanel;

		проц {перекрыта}Finalize*;
		нач
			alive := ложь;
			timer.Wakeup;
			нач {единолично} дождись(dead); кон;
			Finalize^;
		кон Finalize;

		проц &{перекрыта}Init*;
		перем panel : WMStandardComponents.Panel;
		нач
			Init^;
			SetNameAsString(StrAlertsTab);
			нов(timer);
			alive := истина; dead := ложь;
			AddContent(CreateStatusPanel());
			AddContent(CreateRulefilesPanel());
			AddContent(CreateAlertsPanel());
			panel := CreateRulesPanel(); panel.alignment.Set(WMComponents.AlignClient);
			AddContent(panel);
		кон Init;

	нач {активное}
		нцПока alive делай
			Update;
			timer.Sleep(PollingInterval);
		кц;
		нач {единолично} dead := истина; кон;
	кон AlertsTab;

перем
	StrAlertsTab : Strings.String;

нач
	StrAlertsTab := Strings.NewString("AlertsTab");
кон WMPerfMonTabAlerts.
