модуль FoxDocumentationScanner; (** AUTHOR ""; PURPOSE ""; *)

использует Потоки, Diagnostics, D := Debugging;

конст
	(* scanner constants *)
	EOT* = 0X; LF* = 0AX; CR* = 0DX;
	Trace=ложь;
тип

	Token*= перечисление
		EndOfText*, NewLine*, Header*, Pipe*, LeftItalic*, RightItalic*, LeftBold*, RightBold*, LeftUnderline*, RightUnderline*,
		LinkBegin*, LinkEnd*, Heading*, Number*, LeftDescription*, RightDescription*, Description*,
		Bullet*, Line*, CodeBegin*, CodeEnd*, LabelBegin*, LabelEnd*,
		LineBreak*, Section*, String*
	кон;

	String*= укль на массив из симв8;

	Symbol*= запись
		position*: цел32;
		token*: Token;
		level*: цел32;
		string*: String;
		stringLength*: цел32;
		hasWhitespace*: булево;
	кон;

	(** scanner reflects the following EBNF
	**)
	Scanner* = окласс
	перем
		(* helper state information *)
		reader: Потоки.Чтец;   (* source *)
		diagnostics: Diagnostics.Diagnostics;   (* error logging *)

		ch: симв8;   (* look-ahead character *)
		position-: цел32;   (* current position *)
		prevToken: Token;

		(*
			source: name of the source code for reference in error outputs
			reader: input stream
			position: reference position (offset) of the input stream , for error output
			diagnostics: error output object
		*)
		проц & InitializeScanner*(reader: Потоки.Чтец; position: цел32; diagnostics: Diagnostics.Diagnostics);
		нач
			сам.reader := reader;
			сам.diagnostics := diagnostics;
			ch := " ";
			если reader = НУЛЬ то ch := EOT иначе GetNextCharacter всё;
			сам.position := position;
		кон InitializeScanner;

		(** get next character, end of text results in ch = EOT **)
		проц GetNextCharacter;
		нач
			утв(ch # EOT);
			reader.чСимв8(ch); увел(position);
		кон GetNextCharacter;

		проц Peek(): симв8;
		нач
			возврат reader.ПодглядиСимв8()
		кон Peek;

		проц BreaksLiteral(): булево;
		нач
			просей ch из
				"*", "_", "/","#":
					если IsWhitespace(Peek(),истина) то возврат истина всё; (* right of bold, underline, italics and description *)
				|"]",">","\":
					если ch = Peek() то возврат истина всё; (* right of link or label *)
				|"|": возврат истина (* pipe in link or table *)
			иначе
				возврат ложь
			всё;
			возврат ложь
		кон BreaksLiteral;

		проц IdentifierStart(ch: симв8): булево;
		нач
			просей ch из
				| 'a' .. 'z', 'A' .. 'Z', '_': возврат истина
			иначе возврат ложь
			всё;
		кон IdentifierStart;

		проц AppendCharacter(перем symbol: Symbol; ch: симв8);
		перем s: String;  i: цел32;
		нач
			если symbol.string = НУЛЬ то нов(symbol.string,32); symbol.stringLength := 0; всё;
			если symbol.stringLength = длинаМассива(symbol.string)-1 то
				s := symbol.string;
				нов(symbol.string, symbol.stringLength*2);
				нцДля i := 0 до symbol.stringLength делай
					symbol.string[i] := s[i];
				кц;
			всё;
			symbol.string[symbol.stringLength] := ch;
			увел(symbol.stringLength);
		кон AppendCharacter;

		проц AppendCharacters(перем symbol: Symbol; ch: симв8; number: цел32);
		нач
			нцПока number > 0 делай
				AppendCharacter(symbol,ch); умень(number)
			кц;
		кон AppendCharacters;

		проц ReadLiteral(перем symbol: Symbol; token: Token);
		нач
			symbol.token := token;
			нцДо (* consumes at least one character *)
				AppendCharacter(symbol, ch);
				GetNextCharacter;
			кцПри (ch <= " ") или BreaksLiteral();
		кон ReadLiteral;

		проц ReadLiteralWS(перем symbol: Symbol; token: Token);
		нач
			symbol.token := token;
			нцДо (* consumes at least one character *)
				AppendCharacter(symbol, ch);
				GetNextCharacter;
			кцПри (ch <= " ");
		кон ReadLiteralWS;

		проц ReadCharacters(this: симв8; min,max: цел32; перем symbol: Symbol; token: Token);
		нач
			symbol.level := 0;
			нцПока (ch = this) делай увел(symbol.level); GetNextCharacter кц;
			если (symbol.level >= min) или (symbol.level <= max) то
				symbol.token := token;
			иначе
				AppendCharacters(symbol, this, symbol.level);
				ReadLiteral(symbol, Token.String);
			всё;
		кон ReadCharacters;

		проц IsWhitespace(ch: симв8; includeNewLine: булево): булево;
		нач
			возврат (ch <= " ") и (ch # EOT) и (includeNewLine или (ch # CR) и (ch # LF))
		кон IsWhitespace;

		(** get next symbol **)
		проц GetNextSymbol*(перем symbol: Symbol);
		перем s: Token; prev: симв8; firstInLine: булево;

			проц SkipBlanks;
			нач
				нцПока IsWhitespace(ch,ложь) делай  (*ignore control characters*)
					prev := ch;
					GetNextCharacter
				кц;
			кон SkipBlanks;

			проц ReadCode;
			перем ending: цел32; end:булево;
			нач
				ending := 0; end := ложь;
				нцДо
					AppendCharacter(symbol, ch);
					если ch = "}" то
						увел(ending);
						GetNextCharacter
					аесли (ending = 3) и (ch # "'") и (ch # '"')  (* to allow for referencing '}}}' in descriptions of the documentation *)и (ending = 3) то
						end := истина
					иначе
						ending := 0;
						GetNextCharacter
					всё;
				кцПри (ch = EOT) или end;
				если end то
					symbol.token := Token.CodeEnd;
					умень(symbol.stringLength,4);
					symbol.string[symbol.stringLength] := 0X
				иначе
					symbol.token := Token.EndOfText
				всё;
			кон ReadCode;

			(* return if the current character is preceded by a whitespace and next character is not a whitespace, such as in the beginning of *b o l d*  *)
			проц IsLeft(): булево;
			нач
				возврат IsWhitespace(prev,ложь) и ~IsWhitespace(Peek(),ложь)
			кон IsLeft;

			(* return if the current character is preceded by a non-whitespace and next character is a whitespace, such as at the end of *b o l d*  *)
			проц IsRight(): булево;
			нач
				возврат ~IsWhitespace(prev,истина) и IsWhitespace(Peek(),истина)
			кон IsRight;

		нач
			symbol.stringLength := 0;
			symbol.string := НУЛЬ;
			symbol.level := 0;
			symbol.position := position;

			prev := ch;
			SkipBlanks;
			firstInLine := prevToken = Token.NewLine;

			symbol.hasWhitespace := IsWhitespace(prev, ложь);

			если symbol.token = Token.CodeBegin то
				ReadCode;
				возврат
			всё;

			просей ch из  (* ch > " " *)
			EOT: symbol.token := Token.EndOfText
			| CR: symbol.token := Token.NewLine; GetNextCharacter; если ch = LF то GetNextCharacter всё;
			| LF: symbol.token := Token.NewLine; GetNextCharacter; если ch = CR то GetNextCharacter всё;
			|  '|': GetNextCharacter;
				если (ch = "=") то symbol.token := Token.Header; GetNextCharacter иначе symbol.token := Token.Pipe всё;
			|  '/':
				если IsLeft() то  symbol.token := Token.LeftItalic; GetNextCharacter;
				аесли IsRight() то symbol.token := Token.RightItalic; GetNextCharacter;
				иначе ReadLiteral( symbol, Token.String)
				всё
			|  '_':
				если IsLeft() то  symbol.token := Token.LeftUnderline;GetNextCharacter;
				аесли IsRight() то symbol.token := Token.RightUnderline;GetNextCharacter;
				иначе ReadLiteral( symbol, Token.String)
				всё
			|  '[': ReadCharacters (ch, 2, 2, symbol, Token.LinkBegin);
			|  ']': ReadCharacters (ch, 2, 2, symbol, Token.LinkEnd);
			|  '=':
				если firstInLine то
					ReadCharacters (ch, 1, 3, symbol, Token.Heading);
					если IdentifierStart(ch) то
						нцПока ~IsWhitespace(ch,истина) и (ch#EOT) делай
							AppendCharacter(symbol,ch);
							GetNextCharacter;
						кц;
					всё;
				иначе ReadLiteral(symbol, Token.String);
				всё;
			|  '#':
				если firstInLine то (* number *)
					ReadCharacters(ch, 1, 3, symbol, Token.Number);
					если IsWhitespace(ch,ложь) то
						symbol.token := Token.Number;
					аесли symbol.level = 1 то
						symbol.token := Token.LeftDescription;
					иначе
						ReadLiteral(symbol, Token.String);
					всё;
				иначе
					если IsLeft() то symbol.token := Token.LeftDescription; symbol.level := 1; GetNextCharacter;
					аесли IsRight() то symbol.token := Token.RightDescription;GetNextCharacter;
					иначе ReadLiteral(symbol, Token.String);
					всё;
				всё;
			|  '*':
				если firstInLine то
					ReadCharacters(ch, 1, 3, symbol, Token.Bullet);
					если IsWhitespace(ch,ложь) то
						symbol.token := Token.Bullet;
					аесли symbol.level = 1 то
						symbol.token := Token.LeftBold;
					иначе
						AppendCharacters(symbol, '*', symbol.level);
						ReadLiteral(symbol, Token.String);
					всё;
				иначе
					если IsLeft() то symbol.token := Token.LeftBold;GetNextCharacter;
					аесли  IsRight() то symbol.token := Token.RightBold;GetNextCharacter;
					иначе ReadLiteral(symbol, Token.String);
					всё;
				всё;
			|  '-':
				если firstInLine то ReadCharacters (ch, 4, матМаксимум(цел32), symbol, Token.Line)
				иначе ReadLiteral(symbol, Token.String);
				всё;
			|  '{': ReadCharacters (ch, 3, 3, symbol, Token.CodeBegin);
			|  '}': ReadCharacters (ch, 3, 3, symbol, Token.CodeEnd);
			|  '<':
				ReadCharacters (ch, 2, 2, symbol, Token.LabelBegin);
				если IsWhitespace(ch, истина) и (symbol.level = 2) то
					AppendCharacters(symbol,'<',2);
					symbol.token := Token.String
				всё;
			|  '>':
				ReadCharacters (ch, 2, 2, symbol, Token.LabelEnd);
				если IsWhitespace(prev, ложь) и (symbol.level = 2) то
					AppendCharacters(symbol,'>',2);
					ReadLiteral(symbol, Token.String);
				всё;
			|  '\':
				ReadCharacters (ch, 2, 2, symbol, Token.LineBreak);
			|  '@': ReadCharacters (ch, 1, 10, symbol, Token.Section);
				если IdentifierStart(ch) то
					нцПока ~IsWhitespace(ch,истина) и (ch#EOT) делай
						AppendCharacter(symbol,ch);
						GetNextCharacter;
					кц;
				всё;
			иначе
				ReadLiteral(symbol,Token.String)
			всё;
			prevToken := symbol.token;

			если (firstInLine) и (symbol.token = Token.String) и (symbol.stringLength>0) и (symbol.string[symbol.stringLength-1] = ":") то
				умень(symbol.stringLength);
				symbol.string[symbol.stringLength] := 0X;
				symbol.token := Token.Description;
			всё;

			если Trace то DumpSymbol(D.Log, symbol); D.Ln всё;
		кон GetNextSymbol;

	кон Scanner;

	проц DumpSymbol(w: Потоки.Писарь; конст symbol: Symbol);
	нач
		w.пСтроку8("token: ");
		просей symbol.token из
			Token.EndOfText: w.пСтроку8("EndOfText");
			|Token.NewLine: w.пСтроку8("NewLine");
			|Token.Header:w.пСтроку8("Header");
			|Token.Pipe:w.пСтроку8("Pipe");
			|Token.LeftItalic:w.пСтроку8("LeftItalic");
			|Token.RightItalic:w.пСтроку8("RightItalic");
			|Token.LinkBegin:w.пСтроку8("LinkBegin");
			|Token.LinkEnd:w.пСтроку8("LinkEnd");
			|Token.Heading:w.пСтроку8("Heading");
			|Token.Number:w.пСтроку8("Number");
			|Token.Bullet:w.пСтроку8("Bullet");
			|Token.LeftBold:w.пСтроку8("LeftBold");
			|Token.RightBold:w.пСтроку8("RightBold");
			|Token.LeftUnderline:w.пСтроку8("LeftUnderline");
			|Token.RightUnderline:w.пСтроку8("RightUnderline");
			|Token.Line:w.пСтроку8("Line");
			|Token.CodeBegin:w.пСтроку8("CodeBegin");
			|Token.CodeEnd:w.пСтроку8("CodeEnd");
			|Token.LabelBegin:w.пСтроку8("LabelBegin");
			|Token.LabelEnd:w.пСтроку8("LabelEnd");
			|Token.LineBreak:w.пСтроку8("LineBreak");
			|Token.Section:w.пСтроку8("Section");
			|Token.String:w.пСтроку8("String");
		всё;
	кон DumpSymbol;

кон FoxDocumentationScanner.
