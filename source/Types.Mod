модуль Types; (** AUTHOR "staubesv"; PURPOSE "Generic types and type conversion"; *)

использует
	Strings, Texts, TextUtilities, Repositories, Потоки;

конст

	(*
		res > Ok: Could convert but maybe lost precision / truncated string
		res = Ok; Conversion succeeded with no loss of precision
		res < Ok; Conversion errors
	*)
	Ok* = 0;

	Truncated* = 1;
	CannotConvert* = -10;
	ConversionError* = -11;
	TruncatedError* = -12; (* string truncation leads to error *)
	CannotRead* = -20;
	CannotWrite* = -21;
	TargetIsNIL* = -30;

тип

	Any* = запись кон;

	(** Basic types *)

	Boolean* = запись(Any)
		value* : булево;
	кон;

	Integer* = запись(Any)
		value* : цел32;
	кон;

	Hugeint* = запись(Any)
		value* : цел64;
	кон;

	Size* = запись(Any)
		value* : размерМЗ;
	кон;

	Real* = запись(Any)
		value* : вещ32;
	кон;

	Longreal* = запись(Any)
		value* : вещ64;
	кон;

	Char* = запись(Any)
		value* : симв8;
	кон;

	String32* = запись(Any)
		value* : массив 32 из симв8;
	кон;

	String256* = запись(Any)
		value* : массив 256 из симв8;
	кон;

	String* = запись(Any)
		value* : Strings.String;
	кон;

	DynamicString* = запись(Any) (** Initialize using NewString! *)
		value- : Strings.String; (* {value  # NIL} *)
		length : размерМЗ; (* {(0 <= length) & (length < LEN(value))} *)
		bufferLength : размерМЗ; (* {(0 <= bufferLength) & (bufferLength = LEN(value))} *)
	кон;

	Set* = запись(Any)
		value* : мнвоНаБитахМЗ;
	кон;

	Text* = запись(Any)
		value* : Texts.Text;
	кон;

	Object* = запись(Any)
		value* : Repositories.Component;
	кон;

тип

	(** Generic types *)

	Generic* = запись(Any)
		Get* : Getter;
		Set* : Setter;
	кон;

	(** Get own value and store it into target *)
	Getter* = проц {делегат} (конст self : Generic; перем target : Any; перем res : целМЗ);

	(** Set own value from source *)
	Setter* = проц {делегат} (конст self : Generic; конст source : Any; перем res : целМЗ);

перем
	StrEmptyString : Strings.String;

проц GetBoolean*(конст source : Any; перем value : булево; перем res : целМЗ);
перем temp : массив 2 из симв8; boolean : Boolean;
нач
	res := Ok;
	если (source суть Boolean) то
		value := source(Boolean).value;
	аесли (source суть Char) то
		temp[0] := source(Char).value;
		temp[1] := 0X;
		Strings.StrToBool(temp, value);
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			Strings.StrToBool(source(String).value^, value);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		Strings.StrToBool(source(String).value^, value);
	аесли (source суть String32) то
		Strings.StrToBool(source(String32).value, value);
	аесли (source суть String256) то
		Strings.StrToBool(source(String256).value, value);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), boolean, res);
			если (res = Ok) то
				value := boolean.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetBoolean;

проц SetBoolean*(перем target : Any; value : булево; перем res : целМЗ);
перем temp : массив 2 из симв8; boolean : Boolean;
нач
	res := Ok;
	если (target суть Boolean) то
		target(Boolean).value := value;
	аесли (target суть Char) то
		Strings.BoolToStr(value, temp);
		target(Char).value := temp[0];
	аесли (target суть String) то
		если (target(String).value # НУЛЬ) то
			Strings.BoolToStr(value, target(String).value^);
		иначе
			res := ConversionError;
		всё;
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 5);
		Strings.BoolToStr(value, target(DynamicString).value^);
	аесли (target суть String32) то
		Strings.BoolToStr(value, target(String32).value);
	аесли (target суть String256) то
		Strings.BoolToStr(value, target(String256).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			boolean.value := value;
			target(Generic).Set(target(Generic), boolean, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetBoolean;

проц GetInteger*(конст source : Any; перем value : цел32; перем res : целМЗ);
перем integer : Integer;
нач
	res := Ok;
	если (source суть Integer) то
		value := source(Integer).value;
	аесли (source суть Hugeint) то
		value := source(Hugeint).value(цел32);
	аесли (source суть Size) то
		value := source(Size).value(цел32);
	аесли (source суть Real) то
		value := округлиВниз(source(Real).value);
	аесли (source суть Longreal) то
		value := округлиВниз(source(Longreal).value);
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			Strings.StrToInt(source(String).value^, value);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		Strings.StrToInt(source(DynamicString).value^, value);
	аесли (source суть String32) то
		Strings.StrToInt(source(String32).value, value);
	аесли (source суть String256) то
		Strings.StrToInt(source(String256).value, value);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), integer, res);
			если (res = Ok) то
				value := integer.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetInteger;

проц SetInteger*(перем target : Any; value : цел32;  перем res : целМЗ);
перем integer : Integer;
нач
	res := Ok;
	если (target суть Integer) то
		target(Integer).value := value;
	аесли (target суть Hugeint) то
		target(Hugeint).value := value;
	аесли (target суть Size) то
		target(Size).value := value;
	аесли (target суть Real) то
		target(Real).value := value;
	аесли (target суть Longreal) то
		target(Longreal).value := value;
	аесли (target суть String) то
		EnsureStringLength(target(String).value, 11);
		Strings.IntToStr(value, target(String).value^);
		(*
		IF (target(String).value # NIL) THEN
			Strings.IntToStr(value, target(String).value^);
		ELSE
			res := ConversionError;
		END;
		*)
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 11);
		Strings.IntToStr(value, target(DynamicString).value^);
	аесли (target суть String32) то
		Strings.IntToStr(value, target(String32).value);
	аесли (target суть String256) то
		Strings.IntToStr(value, target(String256).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			integer.value := value;
			target(Generic).Set(target(Generic), integer, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetInteger;

проц GetHugeint*(конст source : Any; перем value : цел64; перем res : целМЗ);
перем hugeint : Hugeint;
нач
	res := Ok;
	если (source суть Integer) то
		value := source(Integer).value;
	аесли (source суть Hugeint) то
		value := source(Hugeint).value;
	аесли (source суть Size) то
		value := source(Size).value;
	аесли (source суть Real) то
		value := округлиВниз(source(Real).value);
	аесли (source суть Longreal) то
		value := округлиВниз(source(Longreal).value);
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			Strings.StrToHInt(source(String).value^, value);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		Strings.StrToHInt(source(DynamicString).value^, value);
	аесли (source суть String32) то
		Strings.StrToHInt(source(String32).value, value);
	аесли (source суть String256) то
		Strings.StrToHInt(source(String256).value, value);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), hugeint, res);
			если (res = Ok) то
				value := hugeint.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetHugeint;

проц SetHugeint*(перем target : Any; value : цел64;  перем res : целМЗ);
перем hugeint : Hugeint;
нач
	res := Ok;
	если (target суть Integer) то
		target(Integer).value := value(цел32);
	аесли (target суть Hugeint) то
		target(Hugeint).value := value;
	аесли (target суть Size) то
		target(Size).value := value(размерМЗ);
	аесли (target суть Real) то
		target(Real).value := value;
	аесли (target суть Longreal) то
		target(Longreal).value := value;
	аесли (target суть String) то
		EnsureStringLength(target(String).value, 11);
		Strings.IntToStr(value, target(String).value^);
		(*
		IF (target(String).value # NIL) THEN
			Strings.IntToStr(value, target(String).value^);
		ELSE
			res := ConversionError;
		END;
		*)
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 11);
		Strings.IntToStr(value, target(DynamicString).value^);
	аесли (target суть String32) то
		Strings.IntToStr(value, target(String32).value);
	аесли (target суть String256) то
		Strings.IntToStr(value, target(String256).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			hugeint.value := value;
			target(Generic).Set(target(Generic), hugeint, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetHugeint;

проц GetSize*(конст source : Any; перем value : размерМЗ; перем res : целМЗ);
перем size : Size;
нач
	res := Ok;
	если (source суть Integer) то
		value := source(Integer).value;
	аесли (source суть Hugeint) то
		value := source(Hugeint).value(размерМЗ);
	аесли (source суть Size) то
		value := source(Size).value;
	аесли (source суть Real) то
		value := округлиВниз(source(Real).value);
	аесли (source суть Longreal) то
		value := округлиВниз(source(Longreal).value);
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			Strings.StrToSize(source(String).value^, value);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		Strings.StrToSize(source(DynamicString).value^, value);
	аесли (source суть String32) то
		Strings.StrToSize(source(String32).value, value);
	аесли (source суть String256) то
		Strings.StrToSize(source(String256).value, value);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), size, res);
			если (res = Ok) то
				value := size.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetSize;

проц SetSize*(перем target : Any; value : размерМЗ;  перем res : целМЗ);
перем size : Size;
нач
	res := Ok;
	если (target суть Integer) то
		target(Integer).value := value(цел32);
	аесли (target суть Hugeint) то
		target(Hugeint).value := value;
	аесли (target суть Size) то
		target(Size).value := value;
	аесли (target суть Real) то
		target(Real).value := value;
	аесли (target суть Longreal) то
		target(Longreal).value := value;
	аесли (target суть String) то
		EnsureStringLength(target(String).value, 11);
		Strings.IntToStr(value, target(String).value^);
		(*
		IF (target(String).value # NIL) THEN
			Strings.IntToStr(value, target(String).value^);
		ELSE
			res := ConversionError;
		END;
		*)
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 11);
		Strings.IntToStr(value, target(DynamicString).value^);
	аесли (target суть String32) то
		Strings.IntToStr(value, target(String32).value);
	аесли (target суть String256) то
		Strings.IntToStr(value, target(String256).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			size.value := value;
			target(Generic).Set(target(Generic), size, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetSize;

проц GetReal*(конст source : Any; перем value : вещ32; перем res : целМЗ);
перем longreal : вещ64; real : Real;
нач
	res := Ok;
	если (source суть Real) то
		value := source(Real).value;
	аесли (source суть Longreal) то
		value := устарПреобразуйКБолееУзкомуЦел(source(Longreal).value);
	аесли (source суть Integer) то
		value := source(Integer).value;
	аесли (source суть Hugeint) то
		value := source(Hugeint).value;
	аесли (source суть Size) то
		value := source(Size).value;
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			Strings.StrToFloat(source(String).value^, longreal);
			value := устарПреобразуйКБолееУзкомуЦел(longreal);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		Strings.StrToFloat(source(DynamicString).value^, longreal);
		value := устарПреобразуйКБолееУзкомуЦел(longreal);
	аесли (source суть String32) то
		Strings.StrToFloat(source(String32).value, longreal);
		value := устарПреобразуйКБолееУзкомуЦел(longreal);
	аесли (source суть String256) то
		Strings.StrToFloat(source(String256).value, longreal);
		value := устарПреобразуйКБолееУзкомуЦел(longreal);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), real, res);
			если (res = Ok) то
				value := real.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetReal;

проц SetReal*(перем target : Any; value : вещ32; перем res : целМЗ);
перем real : Real; w:Потоки.ПисарьВСтроку8ФиксированногоРазмера;
нач
	res := Ok;
	если (target суть Real) то
		target(Real).value := value;
	аесли (target суть Longreal) то
		target(Longreal).value := value;
	аесли (target суть Integer) то
		target(Integer).value := округлиВниз(value);
	аесли (target суть Hugeint) то
		target(Hugeint).value := округлиВниз(value);
	аесли (target суть Size) то
		target(Size).value := округлиВниз(value);
	аесли (target суть String) то
		EnsureStringLength(target(String).value, 32);
		если (target(String).value # НУЛЬ) то
			(*FloatToString(value, 8, target(String).value^);*)
			нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(String).value^);
		иначе
			res := ConversionError;
		всё;
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 32); (* TBD: max length of real? *)
		(*FloatToString(value, 8, target(DynamicString).value^);*)
		нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(DynamicString).value^);
	аесли (target суть String32) то
		(*FloatToString(value, 8,  target(String32).value);*)
		нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(String32).value);
	аесли (target суть String256) то
		(*FloatToString(value, 8, target(String256).value);*)
		нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(String256).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			real.value := value;
			target(Generic).Set(target(Generic), real, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetReal;

проц GetLongreal*(конст source : Any; перем value : вещ64; перем res : целМЗ);
перем longreal : Longreal;
нач
	res := Ok;
	если (source суть Longreal) то
		value := source(Longreal).value;
	аесли (source суть Real) то
		value := source(Real).value;
	аесли (source суть Integer) то
		value := source(Integer).value;
	аесли (source суть Hugeint) то
		value := source(Hugeint).value;
	аесли (source суть Size) то
		value := source(Size).value;
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			Strings.StrToFloat(source(String).value^, value);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		Strings.StrToFloat(source(DynamicString).value^, value);
	аесли (source суть String32) то
		Strings.StrToFloat(source(String32).value, value);
	аесли (source суть String256) то
		Strings.StrToFloat(source(String256).value, value);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), longreal, res);
			если (res = Ok) то
				value := longreal.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetLongreal;

проц SetLongreal*(перем target : Any; value : вещ64; перем res : целМЗ);
перем longreal : Longreal; w:Потоки.ПисарьВСтроку8ФиксированногоРазмера;
нач
	res := Ok;
	если (target суть Longreal) то
		target(Longreal).value := value;
	аесли (target суть Real) то
		target(Real).value := устарПреобразуйКБолееУзкомуЦел(value);
	аесли (target суть Integer) то
		target(Integer).value := округлиВниз(value);
	аесли (target суть Hugeint) то
		target(Hugeint).value := округлиВниз(value);
	аесли (target суть Size) то
		target(Size).value := округлиВниз(value);
	аесли (target суть String) то
		если (target(String).value # НУЛЬ) то
			(*FloatToString(value, 8, target(String).value^);*)
			нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(String).value^);
		иначе
			res := ConversionError;
		всё;
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 128); (* TBD max length of longreal? *)
		(*FloatToString(value, 8, target(DynamicString).value^);*)
		нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(DynamicString).value^);
	аесли (target суть String32) то
		(*FloatToString(value, 8, target(String32).value);*)
		нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(String32).value);
	аесли (target суть String256) то
		(*FloatToString(value, 8, target(String256).value);*)
		нов(w,30); w.пВещ64(value,24); w.ДайПрочитанное˛сколькоПоместитсяИСимвол0(target(String256).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			longreal.value := value;
			target(Generic).Set(target(Generic), longreal, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetLongreal;

проц GetChar*(конст source : Any; перем value : симв8; перем res : целМЗ);
перем string : массив 8 из симв8; char : Char;
нач
	res := Ok;
	если (source суть Char) то
		value := source(Char).value;
	аесли (source суть Boolean) то
		Strings.BoolToStr(source(Boolean).value, string);
		value := string[0];
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			value := source(String).value[0];
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		value := source(DynamicString).value[0];
	аесли (source суть String32) то
		value := source(String32).value[0];
	аесли (source суть String256) то
		value := source(String256).value[0];
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), char, res);
			если (res = Ok) то
				value := char.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetChar;

проц SetChar*(перем target : Any; value : симв8; перем res : целМЗ);
перем temp : массив 2 из симв8; char : Char;
нач
	res := Ok;
	если (target суть Char) то
		target(Char).value := value;
	аесли (target суть String) то
		если (target(String).value # НУЛЬ) и (длинаМассива(target(String).value) >= 2) то
			target(String).value[0] := value;
			target(String).value[1] := 0X;
		иначе
			res := ConversionError;
		всё;
	аесли (target суть DynamicString) то
		temp[0] := value;
		temp[1] := 0X;
		SetValue(target(DynamicString), temp);
	аесли (target суть String32) то
		target(String32).value[0] := value;
		target(String32).value[1] := 0X;
	аесли (target суть String256) то
		target(String256).value[0] := value;
		target(String256).value[1] := 0X;
	аесли (target суть Boolean) то
		temp[0] := value;
		temp[1] := 0X;
		Strings.StrToBool(temp, target(Boolean).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			char.value := value;
			target(Generic).Set(target(Generic), char, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetChar;

проц GetAOC*(конст source : Any; перем value : массив из симв8; перем res : целМЗ);
нач
	res := Ok;
	если (source суть String) то
		если (source(String).value # НУЛЬ) то
			копируйСтрокуДо0(source(String).value^, value);
			если (Strings.Length(source(String).value^) >= длинаМассива(value)) то
				res := Truncated;
			всё;
		иначе
			value := "";
		всё;
	аесли (source суть DynamicString) то
		копируйСтрокуДо0(source(DynamicString).value^, value);
		если (source(DynamicString).length >= длинаМассива(value)) то
			res := Truncated;
		всё;
	аесли (source суть String32) то
		копируйСтрокуДо0(source(String32).value, value);
	аесли (source суть String256) то
		копируйСтрокуДо0(source(String256).value, value);
	аесли (source суть Boolean) то
		Strings.BoolToStr(source(Boolean).value, value);
	аесли (source суть Integer) то
		Strings.IntToStr(source(Integer).value, value);
	аесли (source суть Hugeint) то
		Strings.IntToStr(source(Hugeint).value, value);
	аесли (source суть Size) то
		Strings.IntToStr(source(Size).value, value);
	аесли (source суть Real) то
		(* TODO *)
	аесли (source суть Longreal) то
		(* TODO *)
	аесли (source суть Char) то
		value[0] := source(Char).value;
		value[1] := 0X;
	аесли (source суть Object) то
		ComponentToString(source(Object).value, value, res);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			(* TBD *)
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetAOC;

проц EnsureStringLength(перем s: Strings.String; length: размерМЗ);
перем l: размерМЗ;
нач
	если (s = НУЛЬ) или (Strings.Length(s^) < length) то
		нов(s,length)
	всё;
кон EnsureStringLength;


проц SetAOC*(перем target : Any; конст value : массив из симв8; перем res : целМЗ);
перем temp : вещ64; string : String;
нач
	res := Ok;
	если (target суть String) то
		(*TRACE(Strings.Length(value));*)
		EnsureStringLength(target(String).value, Strings.Length(value)+1);
		копируйСтрокуДо0(value, target(String).value^);
		(*
		IF (target(String).value # NIL) THEN
			COPY(value, target(String).value^);
			IF (Strings.Length(value) >= LEN(target(String).value)) THEN
				res := Truncated;
			END;
		ELSE
			res := ConversionError;
		END;
		*)
	аесли (target суть DynamicString) то
		SetValue(target(DynamicString), value);
	аесли (target суть String32) то
		копируйСтрокуДо0(value, target(String32).value);
	аесли (target суть String256) то
		копируйСтрокуДо0(value, target(String256).value);
	аесли (target суть Boolean) то
		Strings.StrToBool(value, target(Boolean).value);
	аесли (target суть Integer) то
		Strings.StrToInt(value, target(Integer).value);
	аесли (target суть Hugeint) то
		Strings.StrToHInt(value, target(Hugeint).value);
	аесли (target суть Size) то
		Strings.StrToSize(value, target(Size).value);
	аесли (target суть Real) то
		Strings.StrToFloat(value, temp);
		target(Real).value := устарПреобразуйКБолееУзкомуЦел(temp);
	аесли (target суть Char) то
		target(Char).value := value[0];
	аесли (target суть Object) то
		StringToComponent(value, target(Object).value, res);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			string.value := Strings.NewString(value); (*? !!! *)
			target(Generic).Set(target(Generic), string, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetAOC;

проц GetString*(конст source : Any; перем string : Strings.String; перем res : целМЗ);
перем
	value : массив 64 из симв8; (*? !!! *)
нач
	res := Ok;
	если (source суть String) то
		если (source(String).value # НУЛЬ) то
			копируйСтрокуДо0(source(String).value^, value);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то

	аесли (source суть String32) то
		копируйСтрокуДо0(source(String32).value, value);
	аесли (source суть String256) то
		копируйСтрокуДо0(source(String256).value, value);
	аесли (source суть Boolean) то
		Strings.BoolToStr(source(Boolean).value, value);
	аесли (source суть Integer) то
		Strings.IntToStr(source(Integer).value, value);
	аесли (source суть Hugeint) то
		Strings.IntToStr(source(Hugeint).value, value);
	аесли (source суть Size) то
		Strings.IntToStr(source(Size).value, value);
	аесли (source суть Real) то
		(* TODO *)
	аесли (source суть Longreal) то
		(* TODO *)
	аесли (source суть Char) то
		value[0] := source(Char).value;
		value[1] := 0X;
	аесли (source суть Object) то
		ComponentToString(source(Object).value, value, res);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			(* TBD *)
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;

	если res = Ok то
		string := Strings.NewString(value);
	всё;
кон GetString;

проц SetString*(перем target : Any; value : Strings.String; перем res : целМЗ);
нач
	если (value # НУЛЬ) то
		SetAOC(target, value^, res);
	аесли (target суть String) то
		target(String).value := value;
		res := Ok;
	иначе (*? semantics *)
		res := CannotConvert;
	всё;
кон SetString;

проц GetSet*(конст source : Any; перем value : мнвоНаБитахМЗ; перем res : целМЗ);
перем set : Set;
нач
	res := Ok;
	если (source суть Set) то
		value := source(Set).value;
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			Strings.StrToSet(source(String).value^, value);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		Strings.StrToSet(source(DynamicString).value^, value);
	аесли (source суть String32) то
		Strings.StrToSet(source(String32).value, value);
	аесли (source суть String256) то
		Strings.StrToSet(source(String256).value, value);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), set, res);
			если (res = Ok) то
				value := set.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetSet;

проц SetSet*(перем target : Any; value : мнвоНаБитахМЗ; перем res : целМЗ);
перем set : Set;
нач
	res := Ok;
	если (target суть Set) то
		target(Set).value := value;
	аесли (target суть String) то
		если (target(String).value # НУЛЬ) то
			Strings.SetToStr(value, target(String).value^);
		иначе
			res := ConversionError;
		всё;
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 64); (* TBD: Max length of set *)
		Strings.SetToStr(value, target(DynamicString).value^);
	аесли (target суть String32) то
		Strings.SetToStr(value, target(String32).value);
	аесли (target суть String256) то
		Strings.SetToStr(value, target(String256).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			set.value := value;
			target(Generic).Set(target(Generic), set, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetSet;

проц GetText*(конст source : Any; перем value : Texts.Text; перем res : целМЗ);
перем
	temp : массив 64 из симв8;
	text : Text;

	проц SetAsString(конст string : массив из симв8);
	нач
		если (value = НУЛЬ) то
			нов(value);
			value.AcquireWrite;
		иначе
			value.AcquireWrite;
			value.Delete(0, value.GetLength());
		всё;
		value.InsertUTF8(0, string);
		value.ReleaseWrite;
	кон SetAsString;

нач
	res := Ok;
	если (source суть Text) то
		value := source(Text).value;
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			SetAsString(source(String).value^);
		иначе
			SetAsString(""); (*? semantics? *)
		всё;
	аесли (source суть DynamicString) то
		SetAsString(source(String).value^);
	аесли (source суть String32) то
		SetAsString(source(String32).value);
	аесли (source суть String256) то
		SetAsString(source(String256).value);
	аесли (source суть Integer) то
		Strings.IntToStr(source(Integer).value, temp);
		SetAsString(temp);
	аесли (source суть Hugeint) то
		Strings.IntToStr(source(Hugeint).value, temp);
	аесли (source суть Size) то
		Strings.IntToStr(source(Size).value, temp);
		SetAsString(temp);
	аесли (source суть Boolean) то
		Strings.BoolToStr(source(Boolean).value, temp);
		SetAsString(temp);
	аесли (source суть Set) то
		Strings.SetToStr(source(Set).value, temp);
		SetAsString(temp);
	аесли (source суть Char) то
		temp[0] := source(Char).value;
		temp[1] := 0X;
		SetAsString(temp);
	аесли (source суть Real) то
		Strings.FloatToStr(source(Real).value, 20, 8, 10, temp);
		SetAsString(temp);
	аесли (source суть Longreal) то
		Strings.FloatToStr(source(Longreal).value, 20, 8, 10, temp);
		SetAsString(temp);
	аесли (source суть Object) то
		ComponentToString(source(Object).value, temp, res);
		если (res = Ok) то
			SetAsString(temp);
		всё;
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), text, res);
			если (res = Ok) то
				value := text.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetText;

проц SetText*(перем target : Any; конст value : Texts.Text; перем res : целМЗ);
перем text : Text; temp : массив 64 из симв8; lr : вещ64;
нач
	res := Ok;
	если (target суть Text) то
		target(Text).value := value;
	аесли (target суть String) то
		если (target(String).value # НУЛЬ) то
			TextUtilities.TextToStr(value, target(String).value^); (*? no valid result code, e.g. truncation *)
		иначе
			res := ConversionError;
		всё;
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 256); (* Text length in UTF8 *)
		TextUtilities.TextToStr(value, target(DynamicString).value^);
	аесли (target суть String32) то
		TextUtilities.TextToStr(value, target(String32).value); (*? no valid result code, e.g. truncation *)
	аесли (target суть String256) то
		TextUtilities.TextToStr(value, target(String256).value); (*? no valid result code, e.g. truncation *)
	аесли (target суть Integer) то
		TextUtilities.TextToStr(value, temp);
		Strings.StrToInt(temp, target(Integer).value);
	аесли (target суть Hugeint) то
		TextUtilities.TextToStr(value, temp);
		Strings.StrToHInt(temp, target(Hugeint).value);
	аесли (target суть Size) то
		TextUtilities.TextToStr(value, temp);
		Strings.StrToSize(temp, target(Size).value);
	аесли (target суть Boolean) то
		TextUtilities.TextToStr(value, temp);
		Strings.StrToBool(temp, target(Boolean).value);
	аесли (target суть Set) то
		TextUtilities.TextToStr(value, temp);
		Strings.StrToSet(temp, target(Set).value);
	аесли (target суть Real) то
		TextUtilities.TextToStr(value, temp);
		Strings.StrToFloat(temp, lr);
		target(Real).value := устарПреобразуйКБолееУзкомуЦел(lr);
	аесли (target суть Longreal) то
		TextUtilities.TextToStr(value, temp);
		Strings.StrToFloat(temp, target(Longreal).value);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			text.value := value;
			target(Generic).Set(target(Generic), text, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetText;

проц GetObject*(конст source : Any; перем value : Repositories.Component; перем res : целМЗ);
перем object : Object;
нач
	res := Ok;
	если (source суть Object) то
		value := source(Object).value;
	аесли (source суть String32) то
		StringToComponent(source(String32).value, value, res);
	аесли (source суть String256) то
		StringToComponent(source(String256).value, value, res);
	аесли (source суть String) то
		если (source(String).value # НУЛЬ) то
			StringToComponent(source(String).value^, value, res);
		иначе
			res := ConversionError;
		всё;
	аесли (source суть DynamicString) то
		StringToComponent(source(String).value^, value, res);
	аесли (source суть Generic) то
		если (source(Generic).Get # НУЛЬ) то
			source(Generic).Get(source(Generic), object, res);
			если (res = Ok) то
				value := object.value;
			всё;
		иначе
			res := CannotRead;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон GetObject;

проц SetObject*(перем target : Any; конст value : Repositories.Component; перем res : целМЗ);
перем object : Object;
нач
	res := Ok;
	если (target суть Object) то
		target(Object).value := value;
	аесли (target суть String32) то
		ComponentToString(value, target(String32).value, res);
	аесли (target суть String256) то
		ComponentToString(value, target(String256).value, res);
	аесли (target суть String) то
		ComponentToString(value, target(String).value^, res);
	аесли (target суть DynamicString) то
		EnsureLength(target(DynamicString), 256); (* TBD: max length of component name *)
		ComponentToString(value, target(DynamicString).value^, res);
	аесли (target суть Generic) то
		если (target(Generic).Set # НУЛЬ) то
			object.value := value;
			target(Generic).Set(target(Generic), object, res);
		иначе
			res := CannotWrite;
		всё;
	иначе
		res := CannotConvert;
	всё;
кон SetObject;

проц Assign*(перем to : Any; конст from : Any; перем res : целМЗ);
перем string : String;
нач
	если (from суть Boolean) то
		SetBoolean(to, from(Boolean).value, res);
	аесли (from суть Integer) то
		SetInteger(to, from(Integer).value, res);
	аесли (from суть Hugeint) то
		SetHugeint(to, from(Hugeint).value, res);
	аесли (from суть Size) то
		SetSize(to, from(Size).value, res);
	аесли (from суть Real) то
		SetReal(to, from(Real).value, res);
	аесли (from суть Longreal) то
		SetLongreal(to, from(Longreal).value, res);
	аесли (from суть Char) то
		SetChar(to, from(Char).value, res);
	аесли (from суть String32) то
		SetAOC(to, from(String32).value, res);
	аесли (from суть String256) то
		SetAOC(to, from(String256).value, res);
	аесли (from суть String) то
		SetString(to, from(String).value, res);
	аесли (from суть DynamicString) то
		SetString(to, from(DynamicString).value, res);
	аесли (from суть Set) то
		SetSet(to, from(Set).value, res);
	аесли (from суть Text )то
		SetText(to, from(Text).value, res);
	аесли (from суть Object) то
		SetObject(to, from(Object).value, res);
	аесли (from суть Generic) то
		если (from(Generic).Get # НУЛЬ) то
			from(Generic).Get(from(Generic), string, res);
			если ( res = Ok) то
				SetString(to, string.value, res);
			всё;
		иначе
			res := CannotRead;
		всё;
	всё;
кон Assign;

(* Helper procedures *)




проц StringToComponent(конст string : массив из симв8; перем component : Repositories.Component; перем res : целМЗ);
перем repositoryName, componentName : массив 64 из симв8; componentID : размерМЗ;
нач
	component := НУЛЬ;
	если Repositories.SplitName(string, repositoryName, componentName, componentID) то
		Repositories.GetComponent(repositoryName, componentName, componentID, component, res);
	иначе
		res := 99; (*? TBD *)
	всё;
кон StringToComponent;

проц ComponentToString(конст component : Repositories.Component; перем string : массив из симв8; перем res : целМЗ);
перем repository : Repositories.Repository; componentName : Repositories.Name; nbr : массив 16 из симв8; componentID : размерМЗ;
нач
	string := "";
	если (component # НУЛЬ) то
		component.GetRepository(repository, componentName, componentID);
		если (repository # НУЛЬ) то
			Strings.IntToStr(componentID, nbr);
			если (Strings.Length(repository.name) + 1 + Strings.Length(componentName) + 1 + Strings.Length(nbr) + 1 <= длинаМассива(string)) то
				копируйСтрокуДо0(repository.name, string);
				Strings.Append(string, Repositories.Delimiter);
				Strings.Append(string, componentName);
				Strings.Append(string, Repositories.Delimiter);
				Strings.Append(string, nbr);
				res := Ok;
			иначе
				res := TruncatedError;
			всё;
		иначе
			res := 99; (*? TBD *)
		всё;
	иначе
		res := 99; (*? TBD *)
	всё;
кон ComponentToString;

(* DynamicString operations *)

проц NewString*() : DynamicString;
перем string : DynamicString;
нач
	string.value := StrEmptyString;
	string.length := 0;
	string.bufferLength := 0; (* force allocation of new string when set *)
	возврат string;
кон NewString;

(* Ensure the <string> can hold at least minLength characters (excl. 0X-terminator) *)
проц EnsureLength*(перем string : DynamicString; minLength : размерМЗ);
перем newBuffer : Strings.String; i : размерМЗ;
нач
	утв((string.value # НУЛЬ) и ((длинаМассива(string.value) = string.bufferLength) или (string.value = StrEmptyString)) и (minLength >= 0));
	если (minLength + 1 >= string.bufferLength) то
		если (string.bufferLength = 0) то string.bufferLength := 2; всё;
		нцПока (minLength + 1 >=  string.bufferLength) делай
			string.bufferLength := 2 * string.bufferLength;
		кц;
		нов(newBuffer, string.bufferLength);
		нцДля i := 0 до string.length - 1 делай
			newBuffer[i] := string.value[i];
		кц;
		newBuffer[string.length] := 0X;
		string.value := newBuffer;
	всё;
кон EnsureLength;

проц SetValue*(перем string : DynamicString; конст value : массив из симв8);
перем length : размерМЗ;
нач
	length := Strings.Length(value);
	EnsureLength(string, length);
	копируйСтрокуДо0(value, string.value^);
	string.length := length;
кон SetValue;

проц Free*(перем string : DynamicString);
нач
	string.value := StrEmptyString;
	string.length := 0;
	string.bufferLength := 0; (* force allocation of new string when set *)
кон Free;

проц Append*(перем string : DynamicString; конст suffix : массив из симв8);
перем suffixLength : размерМЗ;
нач
	suffixLength := Strings.Length(suffix);
	EnsureLength(string, string.length + suffixLength);
	Strings.Append(string.value^, suffix);
	string.length := string.length + suffixLength;
кон Append;

проц GetLength*(конст string : DynamicString) : размерМЗ;
нач
	возврат string.length;
кон GetLength;

проц CleanString(перем s: массив из симв8);
перем i, lead: размерМЗ;
нач
	i := 0;
	нцПока s[i] = " " делай увел(i) кц;
	lead := i;
	нцПока s[i] # 0X делай s[i-lead] := s[i]; увел(i) кц;
	умень(i, lead);
	если (i>0) и (s[i-1] =".") то умень(i) всё;
	s[i] := 0X;
кон CleanString;

проц FloatToString*(r: вещ64; fractionalDigits: цел32; перем s: массив из симв8);
нач
	Strings.FloatToStr(r, 0, fractionalDigits, 0, s);
	CleanString(s);
кон FloatToString;

проц InitStrings;
нач
	StrEmptyString := Strings.NewString("");
кон InitStrings;

нач
	InitStrings;
кон Types.
