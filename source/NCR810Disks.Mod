модуль NCR810Disks;	(** non-portable *)	(* Contributed by P. Ryser to the System 3 project *)
(* ported by phk *)

использует НИЗКОУР, Machine, ЛогЯдра, ASPI := SymbiosASPI, Disks, Plugins;

	конст
		chsdebug = ложь;	readdebug = ложь;

		MaxDevices = 10;

		BS = 512;	(* disk block size *)

	тип
		Part = укль на PartDesc;
		PartDesc = запись
			bootind, head, sector, cyl, type, head2, sector2, cyl2: симв8;
			start, num: цел32
		кон;

	Device = окласс (Disks.Device)
	перем drive: цел32;

		проц {перекрыта}Transfer*(op, block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
		нач
			если op = Disks.Read то res := ReadBlocks(drive, block, num, адресОт(data[ofs]), 0)
			аесли op = Disks.Write то res := WriteBlocks(drive, block, num, адресОт(data[ofs]), 0)
			всё
		кон Transfer;

		проц {перекрыта}GetSize*(перем size: цел32; перем res: целМЗ);
		нач
			size := devs[drive].capacity;
			res := Disks.Ok
		кон GetSize;

		проц {перекрыта}Handle*(перем msg: Disks.Message;  перем res: целМЗ);
		нач
			если msg суть Disks.GetGeometryMsg то
				просейТип msg: Disks.GetGeometryMsg делай
					msg.cyls := devs[drive].cylinders0;
					msg.hds := devs[drive].heads0;
					msg.spt := devs[drive].sectrack;
					res := Disks.Ok
				всё
			иначе res := Disks.Unsupported
			всё
		кон Handle;
	кон Device;

		DeviceDesc = запись
			ha, target, lun: симв8;
			cylinders, cylinders0: цел32;	(* number of cylinders *)
			heads, heads0: цел32;	(* number of heads *)
			sectrack: цел32;	(* sectors/track *)
			writePreComp: цел32;
			capacity, blocksize: цел32;
			obstart, obend: цел32;
			dev: Device
		кон;

	перем
		devs: массив MaxDevices из DeviceDesc;
		numdevs: цел32;	(* number of physical devices *)

(* ReadBlocks - Read disk blocks *)

	проц ReadBlocks(drive, sec, num: цел32; adr: адресВПамяти; offset: цел32): цел32;
	перем srb: ASPI.ExecIOCmdSRB; err: цел32;
	нач
		err := 0;
		нов(srb);
		увел(sec, offset);
		если (sec < 0) или (num >= 256*256) то ЛогЯдра.пЦел64(sec, 1); ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(num, 1); СТОП(100) всё;
		если readdebug и ~((sec >= devs[drive].obstart) и (sec+num <= devs[drive].obend)) то
			ЛогЯдра.пСтроку8("Read on block:"); ЛогЯдра.пЦел64(sec, 1); ЛогЯдра.пСтроку8("   length:"); ЛогЯдра.пЦел64(num, 1); ЛогЯдра.пВК_ПС
		всё;
		srb.HaId := devs[drive].ha;
		srb.Flags := {ASPI.FlagsDirIn};
		srb.Target := devs[drive].target;
		srb.Lun := devs[drive].lun;
		srb.BufLen := BS*num;
		srb.BufPointer := adr(Machine.Address32);
		srb.SenseLen := 0X;
		srb.CDBLen := 0AX;
		srb.CDB[0] := 28X; srb.CDB[1] := логСдвиг(devs[drive].lun, 5);
		srb.CDB[2] := симв8ИзКода(sec DIV 1000000H); srb.CDB[3] := симв8ИзКода((sec DIV 10000H) остОтДеленияНа 100H);
		srb.CDB[4] := симв8ИзКода((sec DIV 100H) остОтДеленияНа 100H); srb.CDB[5] := симв8ИзКода(sec остОтДеленияНа 100H);
		srb.CDB[6] := 0X;
		srb.CDB[7] := симв8ИзКода(num DIV 100H); srb.CDB[8] := симв8ИзКода(num остОтДеленияНа 100H);
		srb.CDB[9] := 0X;
		srb.meas := НУЛЬ;
		srb.Status := 55X;
		ASPI.SendASPICommand(srb, истина);
		утв(srb.Status = ASPI.SSComp);
		возврат err;
	кон ReadBlocks;


(* WriteBlocks - Write disk blocks *)

	проц WriteBlocks(drive, sec, num: цел32; adr: адресВПамяти; offset: цел32): цел32;
	перем srb: ASPI.ExecIOCmdSRB; err: цел32;
	нач
		err := 0;
		нов(srb);
		увел(sec, offset);
		если (sec < 0) или (num >= 256*256) то ЛогЯдра.пЦел64(sec, 1); ЛогЯдра.пСимв8(" "); ЛогЯдра.пЦел64(num, 1); СТОП(100) всё;
		если readdebug и ~((sec >= devs[drive].obstart) и (sec+num <= devs[drive].obend)) то
			ЛогЯдра.пСтроку8("Write on block:"); ЛогЯдра.пЦел64(sec, 1); ЛогЯдра.пСтроку8("   length:"); ЛогЯдра.пЦел64(num, 1); ЛогЯдра.пВК_ПС;
			если (sec # 0) или (num > 1) то (*HALT(101)*) всё;
		всё;
		srb.HaId := devs[drive].ha;
		srb.Flags := {ASPI.FlagsDirOut};
		srb.Target := devs[drive].target;
		srb.Lun := devs[drive].lun;
		srb.BufLen := BS*num;
		srb.BufPointer := adr(Machine.Address32);
		srb.SenseLen := 0X;
		srb.CDBLen := 0AX;
		srb.CDB[0] := 2AX; srb.CDB[1] := логСдвиг(devs[drive].lun, 5);
		srb.CDB[2] := симв8ИзКода(sec DIV 1000000H); srb.CDB[3] := симв8ИзКода((sec DIV 10000H) остОтДеленияНа 100H);
		srb.CDB[4] := симв8ИзКода((sec DIV 100H) остОтДеленияНа 100H); srb.CDB[5] := симв8ИзКода(sec остОтДеленияНа 100H);
		srb.CDB[6] := 0X;
		srb.CDB[7] := симв8ИзКода(num DIV 100H); srb.CDB[8] := симв8ИзКода(num остОтДеленияНа 100H);
		srb.CDB[9] := 0X;
		srb.meas := НУЛЬ;
		srb.Status := 55X;
		ASPI.SendASPICommand(srb, истина);
		утв(srb.Status = ASPI.SSComp);
		возврат err
	кон WriteBlocks;

(* ---- Calculate disk geometry ---- *)

	проц Partsize(drive: цел32; перем res: целМЗ);
	перем
		p, cyl, lcyl, cyl2, sector2, head2, extpend, extcyl, lend, pend, i: цел32;
		pp, lpp: Part;
		pd: массив 512 из симв8;
	нач
		res := ReadBlocks(drive, 0, 1, адресОт(pd[0]), 0); утв(res = 0);
		res := -1; devs[drive].obstart := -1; devs[drive].obend := -1;
		если (pd[510] = 055X) и  (pd[511] = 0AAX) то
			p := (1BEH+адресОт(pd[0]))(Machine.Address32); lcyl := -1;
			нцДля i := 0 до 3 делай
				если chsdebug то ЛогЯдра.пСтроку8("Partition"); ЛогЯдра.пЦел64(i, 1); ЛогЯдра.пВК_ПС всё;
				pp := НИЗКОУР.подмениТипЗначения(Part, p);
				если pp.type # 0X то
					если pp.type = 4FX то
						devs[drive].obstart := pp.start; devs[drive].obend := pp.start+pp.num
					всё;
					cyl := кодСимв8(pp.cyl) + кодСимв8(НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, pp.sector)*{6,7}))*4;
					если cyl > lcyl то lpp := pp; lcyl := cyl всё;
					если chsdebug то
						ЛогЯдра.пСтроку8("   BootInd:"); ЛогЯдра.пЦел64(кодСимв8(pp.bootind), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   Type:"); ЛогЯдра.пЦел64(кодСимв8(pp.type), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   Start:"); ЛогЯдра.пЦел64(pp.start, 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   Num:"); ЛогЯдра.пЦел64(pp.num, 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   Head:"); ЛогЯдра.пЦел64(кодСимв8(pp.head), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   Sector:"); ЛогЯдра.пЦел64(кодСимв8(pp.sector), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   cyl:"); ЛогЯдра.пЦел64(кодСимв8(pp.cyl), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   Head2:"); ЛогЯдра.пЦел64(кодСимв8(pp.head2), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   Sector2:"); ЛогЯдра.пЦел64(кодСимв8(pp.sector2), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   cyl2:"); ЛогЯдра.пЦел64(кодСимв8(pp.cyl2), 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   cyl:"); ЛогЯдра.пЦел64(cyl, 1); ЛогЯдра.пВК_ПС;
					всё
				всё;
				увел(p, 16)
			кц;
			если lcyl # -1 то
				cyl2 := кодСимв8(lpp.cyl2) + кодСимв8(НИЗКОУР.подмениТипЗначения(симв8, НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, lpp.sector2)*{6,7}))*4;
				head2 := кодСимв8(lpp.head2);
				sector2 := кодСимв8(lpp.sector2) остОтДеленияНа 40H;
				pend := cyl2*(head2 + 1)*sector2+head2*sector2+sector2;
				lend := lpp.start+lpp.num;

				extcyl := (lend-(head2*sector2+sector2)) DIV (head2 + 1) DIV sector2;
				extpend := extcyl*(head2+1)*sector2+head2*sector2+sector2;

				если chsdebug то
					ЛогЯдра.пСтроку8("   head2:"); ЛогЯдра.пЦел64(head2, 1); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("   cyl2:"); ЛогЯдра.пЦел64(cyl2, 1); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("   sector2:"); ЛогЯдра.пЦел64(sector2, 1); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("   lend:"); ЛогЯдра.пЦел64(lend, 1); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("   pend:"); ЛогЯдра.пЦел64(pend, 1); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("   extpend:"); ЛогЯдра.пЦел64(extpend, 1); ЛогЯдра.пВК_ПС;
					ЛогЯдра.пСтроку8("   extcyl:"); ЛогЯдра.пЦел64(extcyl, 1); ЛогЯдра.пВК_ПС
				всё;

				если (lend = pend) или ((cyl2 = 1023) и (extpend = lend)) то
					devs[drive].sectrack := sector2;
					devs[drive].heads0 := head2+1;
					devs[drive].cylinders0 := devs[drive].capacity DIV ((head2 + 1) * sector2);
					res := 0;
					если chsdebug то
						ЛогЯдра.пСтроку8("Partsize:"); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   secs:"); ЛогЯдра.пЦел64(devs[drive].sectrack, 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   hds:"); ЛогЯдра.пЦел64(devs[drive].heads0, 1); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("   cyls:"); ЛогЯдра.пЦел64(devs[drive].cylinders0, 1); ЛогЯдра.пВК_ПС
					всё
				всё;
			всё
		всё
	кон Partsize;

	проц Setsize(drive: цел32; перем res: целМЗ);
	перем cylinders, temp, heads, sectors, capacity: цел32;
	нач
		cylinders := 1024; sectors := 62;
		temp := cylinders*sectors;
		capacity := devs[drive].capacity;
		heads := capacity DIV temp;

		если capacity остОтДеленияНа temp # 0 то
			увел(heads);
			temp := cylinders*heads;
			sectors := capacity DIV temp;
			если capacity остОтДеленияНа temp # 0 то
				увел(sectors);
				temp := heads*sectors;
				cylinders := capacity DIV temp
			всё
		всё;
		если cylinders = 0 то res := -1 иначе res := 0 всё;
		devs[drive].sectrack := sectors;
		devs[drive].heads0 := heads;
		devs[drive].cylinders0 := cylinders;
		если chsdebug то
			ЛогЯдра.пСтроку8("Setsize:"); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("   secs:"); ЛогЯдра.пЦел64(devs[drive].sectrack, 1); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("   hds:"); ЛогЯдра.пЦел64(devs[drive].heads0, 1); ЛогЯдра.пВК_ПС;
			ЛогЯдра.пСтроку8("   cyls:"); ЛогЯдра.пЦел64(devs[drive].cylinders0, 1); ЛогЯдра.пВК_ПС
		всё
	кон Setsize;

	проц GetGeo(drive: цел32);
	перем res: целМЗ;
	нач
		devs[drive].heads := 0; devs[drive].heads0 := 0;
		devs[drive].cylinders := 0; devs[drive].cylinders0 := 0;
		devs[drive].sectrack := 0;
		res := -1;
		Partsize(drive, res);
		если res # 0 то Setsize(drive, res) всё;
		если res # 0 то ЛогЯдра.пСтроку8("GetGeo: Could not get disk geometry"); ЛогЯдра.пВК_ПС всё
(*		ASSERT(res = 0); *)
	кон GetGeo;

(* ---- Get disk capacity and block size ---- *)

	проц GetCapacity(drive: цел32);
	перем srb: ASPI.ExecIOCmdSRB; buf: массив 8 из симв8; i: цел32;
	нач
		нов(srb);
		srb.HaId := devs[drive].ha;
		srb.Flags := {ASPI.FlagsDirIn};
		srb.Target := devs[drive].target;
		srb.Lun := devs[drive].lun;
		srb.BufLen := 8;
		srb.BufPointer := адресОт(buf[0])(Machine.Address32);
		srb.SenseLen := 0X;
		srb.CDBLen := 0AX;
		srb.CDB[0] := 25X; srb.CDB[1] := логСдвиг(devs[drive].lun, 5); srb.CDB[2] := 0X; srb.CDB[3] := 0X;
		srb.CDB[4] := 0X; srb.CDB[5] := 0X; srb.CDB[6] := 0X; srb.CDB[7] := 0X; srb.CDB[8] := 0X; srb.CDB[9] := 0X;
		srb.meas := НУЛЬ;
		srb.Status := 55X;
		ASPI.SendASPICommand(srb, истина);
		утв(srb.Status = ASPI.SSComp);
		devs[drive].capacity := 0; devs[drive].blocksize := 0;
		нцДля i := 0 до 3 делай
			devs[drive].capacity := devs[drive].capacity*100H + кодСимв8(buf[i]);
			devs[drive].blocksize := devs[drive].blocksize*100H + кодСимв8(buf[i+4])
		кц;
		утв(devs[drive].blocksize=BS)
	кон GetCapacity;

	проц Init;
	перем res, ha, targ, lun: цел32; srb: ASPI.GetDevTypeSRB;
	нач
		numdevs := 0;
		res := ASPI.GetASPISupportInfo();
		если симв8ИзКода(логСдвиг(res, -8)) = ASPI.SSComp то
			res := res остОтДеленияНа 100H;
			нов(srb); ha := 0;
			нцПока ha < res делай
				targ := 0;
				нцПока targ < 7 делай
					lun := 0; srb.Status := ASPI.SSComp;
					нцПока (lun < 8) и (srb.Status = ASPI.SSComp) делай
						ЛогЯдра.пСтроку8("Init: ha:"); ЛогЯдра.пЦел64(ha, 1);
						ЛогЯдра.пСтроку8("  target:"); ЛогЯдра.пЦел64(targ, 1);
						ЛогЯдра.пСтроку8("  lun:"); ЛогЯдра.пЦел64(lun, 1);
						ЛогЯдра.пВК_ПС;
						srb.HaId := симв8ИзКода(ha); srb.Flags := {};
						srb.Target := симв8ИзКода(targ); srb.Lun := симв8ИзКода(lun);
						ASPI.SendASPICommand(srb, ложь);
						ЛогЯдра.пСтроку8("DevType: "); ЛогЯдра.пЦел64(кодСимв8(srb.DevType), 1); ЛогЯдра.пВК_ПС;
						если (srb.Status = ASPI.SSComp) и (srb.DevType = 0X) то (* only support direct access devices *)
							devs[numdevs].ha := симв8ИзКода(ha); devs[numdevs].target := симв8ИзКода(targ);
							devs[numdevs].lun := симв8ИзКода(lun);
							devs[numdevs].writePreComp := 0;
							GetCapacity(numdevs);
							GetGeo(numdevs);
							увел(numdevs)
						всё;
						увел(lun)
					кц;
					увел(targ)
				кц;
				увел(ha)
			кц
		иначе
			ЛогЯдра.пСтроку8("NCR810Disk: no host adapter found"); ЛогЯдра.пВК_ПС
		всё
	кон Init;

	проц Register;
	перем i: цел32; res: целМЗ; dev: Device; name : массив 12 из симв8;
	нач
		нцДля i := 0 до numdevs-1 делай
			нов(dev); devs[i].dev := dev;
			name := "NCR810Disk0";
			name[10] := симв8ИзКода(48 + i);
			dev.SetName(name);
			dev.blockSize := BS;
			dev.flags := {};
			dev.drive := i;
			Disks.registry.Add(dev, res);
			если res # Plugins.Ok то ЛогЯдра.пСтроку8("failed adding to registry"); ЛогЯдра.пВК_ПС; всё
		кц
	кон Register;

	(** Install - Install the driver in the Disk module. *)
	(** The install command has no effect, as all NCR810 disk devices are installed when the module is loaded. *)

	проц Install*;
	кон Install;

нач
	Init;  Register
кон NCR810Disks.
