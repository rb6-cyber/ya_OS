модуль WebCGI; (** AUTHOR "TF"; PURPOSE "HTTP plugin for CGI commands"; *)

использует
	ЛогЯдра, Strings, Commands, Потоки, WebHTTP, Files, WebHTTPServer, Modules,
	HTTPSupport;

тип
	CGIContext* = окласс
	перем request* : HTTPSupport.HTTPRequest;
		w* : Потоки.Писарь;
		reply* : WebHTTP.ResponseHeader;
	кон CGIContext;

	CGIContextProc = проц(context : CGIContext);

	CGIPlugin = окласс(WebHTTPServer.HTTPPlugin)

		проц {перекрыта}CanHandle*(host : WebHTTPServer.Host; перем request : WebHTTP.RequestHeader; secure : булево) : булево;
		перем path : массив 1024 из симв8;
		нач
			WebHTTP.GetPath(request.uri, path);
			возврат MyMatch(path, "/CGI/")
		кон CanHandle;

		проц {перекрыта}Handle*(host : WebHTTPServer.Host; перем request : WebHTTP.RequestHeader; перем reply : WebHTTP.ResponseHeader;
			перем in : Потоки.Чтец; перем out : Потоки.Писарь);
		перем r : HTTPSupport.HTTPRequest;
			cgiContextProc : CGIContextProc;
			w : Потоки.Писарь; chunker : WebHTTP.ChunkedOutStream;
			context : CGIContext;
			path : массив 1024 из симв8;

		нач
			нов(r, request, in);

			WebHTTP.GetPath(r.shortUri, path);
			cgiContextProc := FindProcedure(r.shortUri);
			если (cgiContextProc # НУЛЬ) то
				нов(context); context.w := out; context.request := r; context.reply := reply;
				cgiContextProc(context);
			иначе
				нов(chunker, w, out, request, reply);
				reply.statuscode := WebHTTP.NotFound;
				WebHTTP.SendResponseHeader(reply, out);
				если (request.method = WebHTTP.GetM) то
					w.пСтроку8("<html><head><title>404 - Not Found</title></head>");
					w.пСтроку8("<body>HTTP 404 - File Not Found<hr><address>");
					w.пСтроку8(WebHTTPServer.ServerVersion); w.пСтроку8("</address></body></html>");
					w.пВК_ПС; w.ПротолкниБуферВПоток;
					chunker.Close
				всё
			всё
		кон Handle;

	кон CGIPlugin;

	CGIProcInfo = укль на запись
		name, procedure : массив 128 из симв8;
		next : CGIProcInfo;
	кон;

перем
	cgi : CGIPlugin;
	cgiProcs : CGIProcInfo;

проц MyMatch(перем uri : массив из симв8; y : массив из симв8) : булево;
перем i : цел32;
нач
	нцПока (i < длинаМассива(uri)) и (i < длинаМассива(y)) и (uri[i] = y[i]) и  (y[i] # 0X) делай увел(i) кц;
	возврат(i < длинаМассива(uri)) и (i < длинаМассива(y)) и (y[i] = 0X)
кон MyMatch;

проц FindProcedure(name : массив из симв8) : CGIContextProc;
перем
	cur : CGIProcInfo; cgiProc : CGIContextProc;
	moduleName, procedureName : Modules.Name; msg : массив 32 из симв8; res : целМЗ;
нач {единолично}
	Strings.Delete(name, 0, 5);
	cur := cgiProcs;
	нцПока cur # НУЛЬ делай
		если cur.name = name то
			Commands.Split(cur.procedure, moduleName, procedureName, res, msg);
			если (res = Commands.Ok) то
				дайПроцПоИмени(moduleName, procedureName, cgiProc);
				возврат cgiProc;
			всё;
		всё;
		cur := cur.next
	кц;
	возврат НУЛЬ
кон FindProcedure;

проц RegisterCGI*(context : Commands.Context);
перем c : CGIProcInfo;
нач {единолично}
	нов(c);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.name);
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.procedure);
	c.next := cgiProcs; cgiProcs := c;
	StoreCGIs;
кон RegisterCGI;

проц StoreCGIs;
перем f : Files.File;
	w : Files.Writer;
	cur : CGIProcInfo;
	res : целМЗ;
	n0, n1 : массив 64 из симв8;
нач
	n0 := "CGIConfig.dat"; n1 := "CGIConfig.dat.Bak";
	Files.Rename(n0, n1, res);
	f := Files.New("CGIConfig.dat");
	Files.OpenWriter(w, f, 0);
	Files.Register(f);
	cur := cgiProcs;
	нцПока cur # НУЛЬ делай
		w.пСимв8('"'); w.пСтроку8(cur.name); w.пСимв8('"'); w.пСимв8(09X);
		w.пСимв8('"'); w.пСтроку8(cur.procedure); w.пСимв8('"'); w.пВК_ПС;
		cur := cur.next
	кц;
	w.ПротолкниБуферВПоток
кон StoreCGIs;

проц LoadCGIs;
перем f : Files.File;
	r : Files.Reader;
	c : CGIProcInfo;
нач
	f := Files.Old("CGIConfig.dat");
	если f # НУЛЬ то
		Files.OpenReader(r, f, 0);
		нцПока r.кодВозвратаПоследнейОперации = 0 делай
			нов(c);
			r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.name); r.ПропустиБелоеПоле;
			r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(c.procedure);
			если r.кодВозвратаПоследнейОперации = 0 то c.next := cgiProcs; cgiProcs := c всё;
			r.ПропустиДоКонцаСтрокиТекстаВключительно
		кц
	всё
кон LoadCGIs;

проц ListCGI*(context : Commands.Context);
перем cur : CGIProcInfo;
нач {единолично}
	cur := cgiProcs;
	нцПока cur # НУЛЬ делай
		context.out.пСтроку8(cur.name); context.out.пСтроку8("-->"); context.out.пСтроку8(cur.procedure); context.out.пВК_ПС;
		cur := cur.next
	кц;
кон ListCGI;

проц Install*(context : Commands.Context);
перем hl : WebHTTPServer.HostList;
нач
	если cgi = НУЛЬ то
		нов(cgi, "CGI-Support");
		hl := WebHTTPServer.FindHosts("");
		hl.host.AddPlugin(cgi);
		context.out.пСтроку8("CGI support installed to default host"); context.out.пВК_ПС;
		hl := WebHTTPServer.FindHosts("*");
		нцПока (hl # НУЛЬ) делай
			hl.host.AddPlugin(cgi);
			context.out.пСтроку8("CGI support installed to "); context.out.пСтроку8(hl.host.name); context.out.пВК_ПС;
			hl := hl.next
		кц;
	 иначе
		context.out.пСтроку8("CGI support already installed"); context.out.пВК_ПС;
	всё;
кон Install;

проц Close;
перем h : WebHTTPServer.HostList;
нач
	если cgi # НУЛЬ то
		h := WebHTTPServer.FindHosts("");
		h.host.RemovePlugin(cgi);

		h := WebHTTPServer.FindHosts("*");
		нцПока (h # НУЛЬ) делай
			h.host.RemovePlugin(cgi);
			h := h.next
		кц;
		ЛогЯдра.пСтроку8("CGI support removed"); ЛогЯдра.пВК_ПС;
		cgi := НУЛЬ
	всё
кон Close;

нач
	LoadCGIs;
	Modules.InstallTermHandler(Close)
кон WebCGI.

WebHTTPServerTools.Start ~\r:FAT:/httproot \l:FAT:/httproot/HTTP.Log~
WebHTTPServerTools.Stop

WebCGI.RegisterCGI RegisterRaily4 RegisterRFW.RegisterRFW4 ~
WebCGI.RegisterCGI RegisterRaily3 RegisterRFW.RegisterRFW3 ~
WebCGI.ListCGI ~
WebCGI.Install ~
System.Free RegisterRFW WebCGI ~

