модуль Dates; (** AUTHOR "be, tf, staubesv"; PURPOSE "Date and time functions"; *)

(** Oberon date & time format:
		time: bits 16-12: hours
					11-6: minutes
					5-0: seconds

		date: 30-9: count of years from 1900
					8-5: month of year
					4-0: day of month
*)

(*	Problem of leap seconds is handled as in POSIX:
		http://www.eecis.udel.edu/~mills/leap.html
		http://en.wikipedia.org/wiki/Leap_second
*)

использует
	Clock;

тип

	DateTime* = запись
		year*, month*, day*,
		hour*, minute*, second*: цел32
	кон;

перем
	Months-: массив 12 из массив 10 из симв8;	(** month's names (January = 0....December=11) *)
	Days-: массив 7 из массив 10 из симв8;	(** day's names (Moday = 0, .... Sunday = 6) *)
	NoDays: массив 12 из цел16;
	ZeroDateUnix-, ZeroDateRFC868-, ZeroDateNTP-: DateTime;

(** Date and Time functions *)

(** returns TRUE if 'year' is a leap year *)
проц LeapYear*(year: цел32): булево;
нач
	возврат (year > 0) и (year остОтДеленияНа 4 = 0) и (~(year остОтДеленияНа 100 = 0) или (year остОтДеленияНа 400 = 0))
кон LeapYear;

(** returns the number of days in that month *)
проц NofDays*(year, month: цел32): цел32;
нач
	умень(month);
	утв((month >= 0) и (month < 12));
	если (month = 1) и LeapYear(year) то возврат NoDays[1]+1
	иначе возврат NoDays[month]
	всё
кон NofDays;

(** checks if the values of a DateTime structure are valid *)
проц ValidDateTime*(dt: DateTime): булево;
нач
	возврат (dt.year > 0) и (dt.month > 0) и (dt.month <= 12) и (dt.day > 0) и (dt.day <= NofDays(dt.year, dt.month)) и
		(dt.hour >= 0) и (dt.hour < 24) и (dt.minute >= 0) и (dt.minute < 60) и (dt.second >= 0) и (dt.second < 60)
кон ValidDateTime;

(** convert an Oberon date/time to a DateTime structure *)
проц OberonToDateTime*(Date, Time: цел32): DateTime;
перем dt: DateTime;
нач
	dt.second := Time остОтДеленияНа 64; Time := Time DIV 64;
	dt.minute := Time остОтДеленияНа 64; Time := Time DIV 64;
	dt.hour := Time остОтДеленияНа 24;
	dt.day := Date остОтДеленияНа 32; Date := Date DIV 32;
	dt.month := Date остОтДеленияНа 16; Date := Date DIV 16;
	dt.year := 1900 + Date;
	возврат dt
кон OberonToDateTime;

(** convert a DateTime structure to an Oberon date/time *)
проц DateTimeToOberon*(dt: DateTime; перем date, time: цел32);
нач
	утв(ValidDateTime(dt));
	date := (dt.year-1900)*512 + dt.month*32 + dt.day;
	time := dt.hour*4096 + dt.minute*64 + dt.second
кон DateTimeToOberon;

(** returns the current date and time *)
проц Now*(): DateTime;
перем d, t: цел32;
нач
	Clock.Get(t, d);
	возврат OberonToDateTime(d, t)
кон Now;

(** returns the ISO 8601 year number, week number & week day (Monday=1, ....Sunday=7) *)
(* algorithm by Rick McCarty, http://personal.ecu.edu/mccartyr/ISOwdALG.txt *)
проц WeekDate*(Date: DateTime; перем year, week, weekday: цел32);
перем doy, i, yy, c, g, jan1: цел32; leap: булево;
нач
	если ValidDateTime(Date) то
		leap := LeapYear(Date.year);
		doy := Date.day; i := 0;
		нцПока (i < Date.month-1) делай doy := doy + NoDays[i]; увел(i) кц;
		если leap и (Date.month > 2) то увел(doy) всё;
		yy := (Date.year-1) остОтДеленияНа 100; c := (Date.year-1) - yy; g := yy + yy DIV 4;
		jan1 := 1 + (((((c DIV 100) остОтДеленияНа 4) * 5) + g) остОтДеленияНа 7);

		weekday := 1 + (((doy + (jan1-1))-1) остОтДеленияНа 7);

		если (doy <= (8-jan1)) и (jan1 > 4) то			(* falls in year-1 ? *)
			year := Date.year-1;
			если (jan1 = 5) или ((jan1 = 6) и LeapYear(year)) то week := 53
			иначе week := 52
			всё
		иначе
			если leap то i := 366 иначе i := 365 всё;
			если ((i - doy) < (4 - weekday)) то
				year := Date.year + 1;
				week := 1
			иначе
				year := Date.year;
				i := doy + (7-weekday) + (jan1-1);
				week := i DIV 7;
				если (jan1 > 4) то умень(week) всё
			всё
		всё
	иначе
		year := -1; week := -1; weekday := -1
	всё
кон WeekDate;

проц Equal*(t1, t2 : DateTime) : булево;
нач
	возврат
		(t1.second = t2.second) и (t1.minute = t2.minute) и (t1.hour = t2.hour) и
		(t1.day = t2.day) и (t1.month = t2.month) и (t1.year = t2.year);
кон Equal;

(** Returns -1 if (t1 < t2), 0 if (t1 = t2) or 1 if (t1 >  t2) *)
проц CompareDateTime*(t1, t2 : DateTime) : цел32;
перем result : цел32;

	проц Compare(t1, t2 : цел32) : цел32;
	перем result : цел32;
	нач
		если (t1 < t2) то result := -1;
		аесли (t1 > t2) то result := 1;
		иначе result := 0;
		всё;
		возврат result;
	кон Compare;

нач
	утв(ValidDateTime(t1) и (ValidDateTime(t2)));
	result := Compare(t1.year, t2.year);
	если (result = 0) то
		result := Compare(t1.month, t2.month);
		если (result = 0) то
			result := Compare(t1.day, t2.day);
			если (result = 0) то
				result := Compare(t1.hour, t2.hour);
				если (result = 0) то
					result := Compare(t1.minute, t2.minute);
					если (result = 0) то
						result := Compare(t1.second, t2.second);
					всё;
				всё;
			всё;
		всё;
	всё;
	возврат result;
кон CompareDateTime;

(** Absolute time difference between t1 and t2.
Note that leap seconds are not counted, see http://www.eecis.udel.edu/~mills/leap.html *)
проц TimeDifference*(t1, t2 : DateTime; перем days, hours, minutes, seconds : цел32);
конст SecondsPerMinute = 60; SecondsPerHour = 3600; SecondsPerDay = 86400;
перем start, end : DateTime; year, month, second : цел32;
нач
	если (CompareDateTime(t1, t2) = -1) то start := t1; end := t2; иначе start := t2; end := t1; всё;
	если (start.year = end.year) и (start.month = end.month) и (start.day = end.day) то
		second := end.second - start.second + (end.minute - start.minute) * SecondsPerMinute + (end.hour - start.hour) * SecondsPerHour;
		days := 0;
	иначе
		(* use start date/time as reference point *)
		(* seconds until end of the start.day *)
		second := SecondsPerDay - start.second - start.minute * SecondsPerMinute - start.hour * SecondsPerHour;
		если (start.year = end.year) и (start.month = end.month) то
			(* days between start.day and end.day *)
			days := (end.day - start.day) - 1;
		иначе
			(* days until start.month ends excluding start.day *)
			days := NofDays(start.year, start.month) - start.day;
			если (start.year = end.year) то
				(* months between start.month and end.month *)
				нцДля month := start.month + 1 до end.month - 1 делай
					days := days + NofDays(start.year, month);
				кц;
			иначе
				(* days until start.year ends (excluding start.month) *)
				нцДля month := start.month + 1 до 12 делай
					days := days + NofDays(start.year, month);
				кц;
				нцДля year := start.year + 1 до end.year - 1 делай (* days between start.years and end.year *)
					если LeapYear(year) то days := days + 366; иначе days := days + 365; всё;
				кц;
				нцДля month := 1 до end.month - 1 делай (* days until we reach end.month in end.year *)
					days := days + NofDays(end.year, month);
				кц;
			всё;
			(* days in end.month until reaching end.day excluding end.day *)
			days := days + end.day - 1;
		всё;
		(* seconds in end.day *)
		second := second + end.second + end.minute * SecondsPerMinute + end.hour * SecondsPerHour;
	всё;
	days := days + (second DIV SecondsPerDay); second := second остОтДеленияНа SecondsPerDay;
	hours := second DIV SecondsPerHour; second := second остОтДеленияНа SecondsPerHour;
	minutes := second DIV SecondsPerMinute; second := second остОтДеленияНа SecondsPerMinute;
	seconds := second;
кон TimeDifference;

(** Add/Subtract a number of years to/from dt *)
проц AddYears*(перем dt : DateTime; years : цел32);
нач
	утв(ValidDateTime(dt));
	dt.year := dt.year + years;
	утв(ValidDateTime(dt));
кон AddYears;

(** Add/Subtract a number of months to/from dt. This will adjust dt.year if necessary *)
проц AddMonths*(перем dt : DateTime; months : цел32);
перем years : цел32;
нач
	утв(ValidDateTime(dt));
	years := months DIV 12;
	dt.month := dt.month + (months остОтДеленияНа 12);
	если (dt.month > 12) то
		dt.month := dt.month - 12;
		увел(years);
	аесли (dt.month < 1) то
		dt.month := dt.month + 12;
		умень(years);
	всё;
	если (years # 0) то AddYears(dt, years); всё;
	утв(ValidDateTime(dt));
кон AddMonths;

(** Add/Subtract a number of days to/from dt. This will adjust dt.month and dt.year if necessary *)
проц AddDays*(перем dt : DateTime; days : цел32);
перем nofDaysLeft : цел32;
нач
	утв(ValidDateTime(dt));
	если (days > 0) то
		нцПока (days > 0) делай
			nofDaysLeft := NofDays(dt.year, dt.month) - dt.day;
			если (days > nofDaysLeft) то
				dt.day := 1;
				AddMonths(dt, 1);
				days := days - nofDaysLeft - 1; (* -1 because we consume the first day of the next month *)
			иначе
				dt.day := dt.day + days;
				days := 0;
			всё;
		кц;
	аесли (days < 0) то
		days := -days;
		нцПока (days > 0) делай
			nofDaysLeft := dt.day - 1;
			если (days > nofDaysLeft) то
				dt.day := 1; (* otherwise, dt could become an invalid date if the previous month has less days than dt.day *)
				AddMonths(dt, -1);
				dt.day := NofDays(dt.year, dt.month);
				days := days - nofDaysLeft - 1; (* -1 because we consume the last day of the previous month *)
			иначе
				dt.day := dt.day - days;
				days := 0;
			всё;
		кц;
	всё;
	утв(ValidDateTime(dt));
кон AddDays;

(** Add/Subtract a number of hours to/from dt. This will adjust dt.day, dt.month and dt.year if necessary *)
проц AddHours*(перем dt : DateTime; hours : цел32);
перем days : цел32;
нач
	утв(ValidDateTime(dt));
	dt.hour := dt.hour + hours;
	days := dt.hour DIV 24;
	dt.hour := dt.hour остОтДеленияНа 24;
	если (dt.hour < 0) то
		dt.hour := dt.hour + 24;
		умень(days);
	всё;
	если (days # 0) то AddDays(dt, days); всё;
	утв(ValidDateTime(dt));
кон AddHours;

(** Add/Subtract a number of minutes to/from dt. This will adjust dt.hour, dt.day, dt.month and dt.year if necessary *)
проц AddMinutes*(перем dt : DateTime; minutes : цел32);
перем hours : цел32;
нач
	утв(ValidDateTime(dt));
	dt.minute := dt.minute + minutes;
	hours := dt.minute DIV 60;
	dt.minute := dt.minute остОтДеленияНа 60;
	если (dt.minute < 0) то
		dt.minute := dt.minute + 60;
		умень(hours);
	всё;
	если (hours # 0) то AddHours(dt, hours); всё;
	утв(ValidDateTime(dt));
кон AddMinutes;

(** Add/Subtract a number of seconds to/from dt. This will adjust dt.minute, dt.hour, dt.day, dt.month and dt.year if necessary *)
проц AddSeconds*(перем dt : DateTime; seconds : цел32);
перем minutes : цел32;
нач
	утв(ValidDateTime(dt));
	dt.second := dt.second + seconds;
	minutes := dt.second DIV 60;
	dt.second := dt.second остОтДеленияНа 60;
	если (dt.second < 0) то
		dt.second := dt.second + 60;
		умень(minutes);
	всё;
	если (minutes # 0) то AddMinutes(dt, minutes); всё;
	утв(ValidDateTime(dt));
кон AddSeconds;

нач
	Months[0] := "Янв"; Months[1] := "Февр"; Months[2] := "Мар"; Months[3] := "Апр"; Months[4] := "Мая";
	Months[5] := "Июн"; Months[6] := "Июл"; Months[7] := "Авг"; Months[8] := "Сен";
	Months[9] := "Окт"; Months[10] := "Ноя"; Months[11] := "Дек";
	Days[0] := "Пн"; Days[1] := "Вт"; Days[2] := "Ср"; Days[3] := "Чт";
	Days[4] := "Пт"; Days[5] := "Сб"; Days[6] := "Вс";
	NoDays[0] := 31; NoDays[1] := 28; NoDays[2] := 31; NoDays[3] := 30; NoDays[4] := 31; NoDays[5] := 30;
	NoDays[6] := 31; NoDays[7] := 31; NoDays[8] := 30; NoDays[9] := 31; NoDays[10] := 30; NoDays[11] := 31;
	ZeroDateUnix.year:=1970; ZeroDateUnix.month:=1; ZeroDateUnix.day:=1;
	ZeroDateRFC868.year:=1900; ZeroDateRFC868.month:=1; ZeroDateRFC868.day:=1;
	ZeroDateNTP:=ZeroDateRFC868;
кон Dates.
