модуль WMOGGPlayer;	(** AUTHOR "TF/Christian Wassmer"; PURPOSE "Simple GUI to the OGG Player"; *)

использует
	Commands, Files, Modules,
	Strings, WMMessages, WMComponents, WMStandardComponents,
	WMGrids, WMStringGrids, OGGVorbisPlayer,
	WM := WMWindowManager;

тип
	KillerMsg = окласс
	кон KillerMsg;

	PlaylistEntry = окласс
	перем url : Strings.String;
	кон PlaylistEntry;

	Player = окласс
	перем url : Strings.String;
		dummy : булево;
		проц &Init*(url : Strings.String);
		нач
			сам.url := url
		кон Init;

	нач {активное}
		dummy := OGGVorbisPlayer.PlayURL(url);
	кон Player;

	Window* = окласс (WMComponents.FormWindow)
	перем
		playBtn, stopBtn : WMStandardComponents.Button;
		list : WMStringGrids.StringGrid;
		player : Player;

		проц CreateForm() : WMComponents.VisualComponent;
		перем
			panel : WMStandardComponents.Panel;
			toolbar : WMStandardComponents.Panel;
			button : WMStandardComponents.Button;

		нач
			нов(panel); panel.bounds.SetExtents(200, 700); panel.fillColor.Set(цел32(0FFFFFFFFH)); panel.takesFocus.Set(истина);
			нов(toolbar); toolbar.fillColor.Set(цел32(0A0A0A0FFH)); toolbar.bounds.SetHeight(20); toolbar.alignment.Set(WMComponents.AlignTop);
			panel.AddContent(toolbar);
			нов(button); button.alignment.Set(WMComponents.AlignLeft); button.caption.SetAOC("Play");
			toolbar.AddContent(button);
			playBtn := button;

			нов(button); button.alignment.Set(WMComponents.AlignLeft); button.caption.SetAOC("Stop");
			toolbar.AddContent(button);
			stopBtn := button;

			нов(list); list.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(list);

			возврат panel
		кон CreateForm;

		проц &New*(filename : Files.FileName);
		перем vc : WMComponents.VisualComponent;
		нач
			IncCount;
			(* To create a multi language app, try loading the respective XML instead of CreateForm()
			if the XML was not found or does not contain all needed elements, use CreateForm as fallback *)
			vc := CreateForm();

			(* wire the form *)
			list.model.Acquire;
			list.model.SetNofCols(1);
			list.model.SetNofRows(1);
			list.SetSelectionMode(WMGrids.GridSelectRows);
			list.model.Release;

			playBtn.onClick.Add(Play);
			stopBtn.onClick.Add(Stop);

			Init(vc.bounds.GetWidth(), vc.bounds.GetHeight(), ложь);
			SetContent(vc);

			FillList(filename);

			 WM.DefaultAddWindow(сам);
			SetTitle(Strings.NewString("OGG Player"));
		кон New;

		(* fills the list with radio stations read from a file ("OGGRadios.Text")*)
		проц FillList(OGGlist : массив из симв8);
		перем entry : PlaylistEntry;
			first : булево;
			i : цел32;
			r : Files.Reader;
			f : Files.File;
			url, name : массив 256 из симв8;
		нач
			f := Files.Old(OGGlist);
			Files.OpenReader(r, f, 0);
			i := 0; first := истина;
			list.model.Acquire;
			list.model.SetNofRows(64);
			нцПока (r.кодВозвратаПоследнейОперации = 0) и (i # 64) делай
				r.ПропустиБелоеПоле;
				r.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(url);
				если first или (url[0] = '#') то
					first := ложь;
					r.ПропустиДоКонцаСтрокиТекстаВключительно
				иначе
					r.ПропустиБелоеПоле;
					r.чСтроку8ДоКонцаСтрокиТекстаВключительно(name);
					list.model.SetCellText(0, i,Strings.NewString(name));
					нов(entry);
					entry.url := Strings.NewString(url);
					list.model.SetCellData(0, i, entry);
					увел(i)
				всё
			кц;
			list.model.Release;
			list.Invalidate;
		кон FillList;

		проц Play(sender, data : динамическиТипизированныйУкль);
		перем l, t, r, b : размерМЗ;
			p : динамическиТипизированныйУкль; url : Strings.String;
		нач
			list.Acquire;
			list.model.Acquire;
			list.GetSelection(l, t, r, b);
			p := list.model.GetCellData(0, t);
			если (p # НУЛЬ) и (p суть PlaylistEntry) то
				url := p(PlaylistEntry).url;
				если url # НУЛЬ то
					нов(player, url)
				всё
			всё;
			list.model.Release;
			list.Release;
		кон Play;

		проц Stop(sender, data : динамическиТипизированныйУкль);
		нач
			OGGVorbisPlayer.StopURL()
		кон Stop;

		проц {перекрыта}Close*;
		нач
			Close^;
			DecCount
		кон Close;

		проц {перекрыта}Handle*(перем x : WMMessages.Message);
		нач
			если (x.msgType = WMMessages.MsgExt) и (x.ext # НУЛЬ) и (x.ext суть KillerMsg) то Close
			иначе Handle^(x)
			всё
		кон Handle;

	кон Window;

перем
	nofWindows : цел32;

проц Open*(context : Commands.Context);
перем win : Window; filename : Files.FileName;
нач
	если ~context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename) то filename := "OGGRadios.Text" всё;
	нов(win, filename);
кон Open;

проц IncCount;
нач {единолично}
	увел(nofWindows)
кон IncCount;

проц DecCount;
нач {единолично}
	умень(nofWindows)
кон DecCount;

проц Cleanup;
перем die : KillerMsg;
	 msg : WMMessages.Message;
	 m : WM.WindowManager;
нач {единолично}
	нов(die);
	msg.ext := die;
	msg.msgType := WMMessages.MsgExt;
	m := WM.GetDefaultManager();
	m.Broadcast(msg);
	дождись(nofWindows = 0)
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон WMOGGPlayer.

WMOGGPlayer.Open OGGRadios.Text ~
System.Free WMOGGPlayer OGGVorbisPlayer OGGUtilities ~
System.Free EnsoniqSound ~
