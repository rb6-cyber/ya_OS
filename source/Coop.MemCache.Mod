(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль MemCache; (** AUTHOR "pjm"; PURPOSE "Memory cache control"; *)

использует НИЗКОУР, Machine;

конст
		(** cache properties *)
	UC* = 0; WC* = 1; WT* = 4; WP* = 5; WB* = 6;

(** Set the cache properties of the specified physical memory area on the current processor. {physAdr, size MOD PS = 0}  Must be called from supervisor mode. *)

проц LocalSetCacheProperties*(physAdr: адресВПамяти; size, type: цел32; перем res: целМЗ);
перем i, n, f: цел32; mask, base: мнвоНаБитахМЗ; j, k: адресВПамяти;
нач
	res := 1507	(* implementation restriction - fixed entries not supported *)
кон LocalSetCacheProperties;

(** Broadcast a LocalSetCacheProperties operation to all processors. *)

проц GlobalSetCacheProperties*(physAdr: адресВПамяти; size, type: цел32; перем res: целМЗ);
нач
	res := 1507	(* implementation restriction - fixed entries not supported *)
кон GlobalSetCacheProperties;

кон MemCache.

(*
to do:
o change error codes
*)
