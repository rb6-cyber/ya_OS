модуль HTTPSession; (** AUTHOR "Luc Blaeser/cs"; PURPOSE "HTTP Session Management";
	code parts from CSHTTPSupport of "cs" *)

использует HTTPSupport, WebHTTP, MD5, IP, Random, Dates, Strings, TFClasses, Kernel, Modules, ЛогЯдра;

конст
	(** variable name for the sesiion id in the HTTP request *)
	HTTPVarSessionIdName* = "sessionid";

	(* in units of LeaseManagerInterval *)
	InitialLeaseTime = 5.0;
	LeaseTimeIncrement = 2.0;
	MaxLeaseTime = 15.0; (* 15 min *)
	LeaseManagerInterval = 60*1000; (* 1 min *)

тип
	SessionId* = массив 80 из симв8; (** fixed length for session id *)

	(** name must be unique for all variables for a session *)
	SessionVariable* = укль на запись
		name*: Strings.String;
		value*: динамическиТипизированныйУкль
	кон;

	Session* = окласс (** client activated object by leasing concept *)
		перем
			sessionId*: SessionId;
			sessionVariables*: TFClasses.List; (** List of SessionVariable *)
			leaseTime*: вещ32;

		проц &Init*(sessionId: SessionId);
		нач
			копируйСтрокуДо0(sessionId, сам.sessionId);
			нов(sessionVariables);
			leaseTime :=  InitialLeaseTime
		кон Init;

		проц GetVariableValue*(name: массив из симв8) : динамическиТипизированныйУкль;
		перем var: SessionVariable;
		нач
			var := GetVariableByName(name);
			если (var # НУЛЬ) то
				возврат var.value
			иначе
				возврат НУЛЬ
			всё
		кон GetVariableValue;

		(** returns NIL if the session variable is not present *)
		проц GetVariableByName*(name: массив из симв8) : SessionVariable;
			перем p: динамическиТипизированныйУкль; i : размерМЗ; var: SessionVariable;
		нач
			sessionVariables.Lock;
			нцДля i := 0 до sessionVariables.GetCount()-1 делай
				p := sessionVariables.GetItem(i); var := p(SessionVariable); (* var # NIL *)
				если (var.name^ = name) то
					sessionVariables.Unlock;
					возврат var
				всё
			кц;
			sessionVariables.Unlock;
			возврат НУЛЬ
		кон GetVariableByName;

		проц AddVariableValue*(name: массив из симв8; value: динамическиТипизированныйУкль);
		перем var: SessionVariable;
		нач
			(* avoid multiple occurrences of the same variable *)
			var := GetVariableByName(name);
			если (var # НУЛЬ) то
				sessionVariables.Remove(var)
			всё;
			нов(var); нов(var.name, Strings.Length(name)+1);
			копируйСтрокуДо0(name, var.name^); var.value := value;
			sessionVariables.Add(var)
		кон AddVariableValue;

		проц RemoveVariable*(name: массив из симв8);
		перем var: SessionVariable;
		нач
			var := GetVariableByName(name);
			если (var # НУЛЬ) то
				sessionVariables.Remove(var)
			всё
		кон RemoveVariable;

		проц IncreaseLifeTime*;
		нач {единолично}
			leaseTime := leaseTime + LeaseTimeIncrement;
			если (leaseTime > MaxLeaseTime) то leaseTime := MaxLeaseTime всё
		кон IncreaseLifeTime;
	кон Session;

	SessionExpirationHandler* = проц {делегат} (session: Session);

	(* checks whether certain session have to be freed *)
	LeaseManager = окласс
		перем timer: Kernel.Timer; i, j: размерМЗ; pSession, pHandler: динамическиТипизированныйУкль; s: Session;
			expiredSessions: TFClasses.List; alive, dead: булево; expObj: ExpirationHandlerObject;

		проц Kill*;
		нач
			нач {единолично}
				alive := ложь;
			кон;
			timer.Wakeup;
		кон Kill;

		проц SetDead;
		нач {единолично}
			dead:=истина
		кон SetDead;

		проц WaitDead*;
		нач {единолично}
			дождись(dead)
		кон WaitDead;

	нач {активное}
		ЛогЯдра.пСтроку8("Session.LeaseManager started."); ЛогЯдра.пВК_ПС;
		нов(timer); нов(expiredSessions); alive := истина; (*terminated := FALSE;*)
		нцПока (alive) делай
			(* Session.LeaseManager looks for expired sessions *)

			(* search expired sessions *)
			sessions.Lock;
			expiredSessions.Clear;
			нцДля i:= 0 до sessions.GetCount()-1 делай
				pSession := sessions.GetItem(i); s := pSession(Session); (* s # NIL *)
				нач {единолично}
					если (s.leaseTime <= 1.0) то
						expiredSessions.Add(s)
					иначе
						s.leaseTime := s.leaseTime-1
						(* ;KernelLog.String("Session "); KernelLog.String(s.sessionId); KernelLog.String(" stays alive for ");
						KernelLog.Int(ENTIER(s.leaseTime), 0); KernelLog.String(" minutes."); KernelLog.Ln *)
					всё
				кон
			кц;
			sessions.Unlock;
			нцДля i := 0 до expiredSessions.GetCount()-1 делай
				pSession := expiredSessions.GetItem(i); s := pSession(Session); (* s # NIL *)
				sessions.Remove(s);
			(*	KernelLog.String("Session "); KernelLog.String(s.sessionId); KernelLog.String(" expired."); KernelLog.Ln; *)

				expirationHandlers.Lock;
				нцДля j := 0 до expirationHandlers.GetCount()-1 делай
					pHandler := expirationHandlers.GetItem(j); expObj := pHandler(ExpirationHandlerObject); (* expObj # NIL *)
					expObj.handler(s)
					(* the handler is not allowed to call AddExpirationHandler or
					    RemoveExpirationHandler since this provokes a deadlock *)
				кц;
				expirationHandlers.Unlock
			кц;

			(* Session.LeaseManager suspends. *)
			timer.Sleep(LeaseManagerInterval)
		кц;
		(* Session.LeaseManager terminated. *)
		SetDead
		(* terminated := TRUE*)
	кон LeaseManager;

	ExpirationHandlerObject = укль на запись
		handler: SessionExpirationHandler
	кон;

перем
	sessions: TFClasses.List; (* List of Session *)
	expirationHandlers: TFClasses.List; (* List of ExpirationHandlerPtr *)
	leaseManager: LeaseManager;
	randomSequence: Random.Sequence;
	date, time: цел32;

	проц GetSessionId*(request: HTTPSupport.HTTPRequest; перем sessionId: SessionId);
	перем s: Session;
	нач
		s := GetSession(request);
		копируйСтрокуДо0(s.sessionId, sessionId)
	кон GetSessionId;

	(** looks for an existing session object of the client - returns NIL IF not existing*)
	проц GetExistingSession*(request: HTTPSupport.HTTPRequest) : Session;
	перем var : HTTPSupport.HTTPVariable; id: SessionId; sess : Session;
	нач {единолично} (* request # NIL *)
		var := request.GetVariableByName(HTTPVarSessionIdName);
		sess := НУЛЬ;
		если (var # НУЛЬ) то
			копируйСтрокуДо0(var.value, id);
			sess := FindSessionBySessionId(id)
		всё;
		возврат sess
	кон GetExistingSession;

	(** looks for an existing session object of the client - if no matching session object is present
		then a new session will be created *)
	проц GetSession*(request: HTTPSupport.HTTPRequest) : Session;
	перем var : HTTPSupport.HTTPVariable; id: SessionId; sess : Session;
	нач {единолично} (* request # NIL *)
		var := request.GetVariableByName(HTTPVarSessionIdName);
		если (var # НУЛЬ) то
			копируйСтрокуДо0(var.value, id);
			sess := FindSessionBySessionId(id);
			если (sess = НУЛЬ) то
				(* leasing expired, create a new session *)
				нов(sess, id); sessions.Add(sess);
			всё
		иначе
			NewSessionId(request.header, id);
			нов(sess, id); sessions.Add(sess);
			(* add the new session id variable to the HTTP request *)
			нов(var);
			копируйСтрокуДо0(HTTPVarSessionIdName, var.name); копируйСтрокуДо0(id, var.value);
			request.variables.Add(var)
		всё;
		возврат sess
	кон GetSession;

	проц FindSessionBySessionId(sessionId: SessionId) : Session;
	перем i: размерМЗ; p: динамическиТипизированныйУкль; sess: Session;
	нач
		sessions.Lock;
		нцДля i := 0 до sessions.GetCount()-1 делай
			p := sessions.GetItem(i); sess := p(Session); (* sess # NIL *)
			если (sess.sessionId = sessionId) то
				sessions.Unlock;
				возврат sess
			всё
		кц;
		sessions.Unlock;
		возврат НУЛЬ
	кон FindSessionBySessionId;

	(** Creates a new Session Id. The Session ID is unique and consists of an MD5 Hash of the client IP,
		the date and time and a random component. by "cs" *)
	проц NewSessionId(header: WebHTTP.RequestHeader; перем sessionId: SessionId);
	перем
		i: размерМЗ;
		buffer1,buffer2: укль на массив из симв8;
		context: MD5.Context;
		digest: MD5.Digest;
		date,time: цел32;
	нач
		нов(buffer1,16384);
		нов(buffer2,16384);
		IP.AdrToStr(header.fadr,buffer1^);
		i := Strings.Length(buffer1^);
		buffer1^[i] := "-";
		buffer1^[i+1] := 0X;
		Dates.DateTimeToOberon(Dates.Now(), date, time);
		Strings.IntToStr(8192*date+time,buffer2^); (* some continuous number *)
		Strings.Append(buffer1^,buffer2^);
		Strings.IntToStr(randomSequence.Integer(),buffer2^); (* some random number *)
		Strings.Append(buffer1^,buffer2^);
		context := MD5.New();
		MD5.WriteBytes(context,buffer1^,Strings.Length(buffer1^));
		MD5.Close(context,digest);
		MD5.ToString(digest,sessionId)
	кон NewSessionId;

	проц AddExpirationHandler*(handler: SessionExpirationHandler);
	перем expObj: ExpirationHandlerObject;
	нач {единолично}
		нов(expObj); expObj.handler := handler;
		expirationHandlers.Add(expObj);
	кон AddExpirationHandler;

	проц RemoveExpirationHandler*(handler: SessionExpirationHandler);
	перем expObj, delObj: ExpirationHandlerObject; p: динамическиТипизированныйУкль; i : размерМЗ;
	нач {единолично}
		delObj := НУЛЬ;
		expirationHandlers.Lock;
		нцДля i := 0 до expirationHandlers.GetCount()-1 делай
			p := expirationHandlers.GetItem(i); expObj := p(ExpirationHandlerObject); (* expObj # NIL *)
			если (expObj.handler = handler) то delObj := expObj всё
		кц;
		expirationHandlers.Unlock;
		если (delObj # НУЛЬ) то
			expirationHandlers.Remove(delObj)
		всё
	кон RemoveExpirationHandler;

	проц StopLeaseManager*;
	нач
		если (leaseManager # НУЛЬ) то
			leaseManager.Kill;
			leaseManager.WaitDead;
		всё
	кон StopLeaseManager;

	проц StartLeaseManager*;
	нач {единолично}
		если (leaseManager = НУЛЬ) то
			нов(leaseManager)
		всё
	кон StartLeaseManager;

нач
	нов(sessions); нов(expirationHandlers);
	(* init random sequence for session id *)
	нов(randomSequence);
	Dates.DateTimeToOberon(Dates.Now(), date, time);
	randomSequence.InitSeed(time);
	StartLeaseManager;
	Modules.InstallTermHandler(StopLeaseManager)
кон HTTPSession.

System.Free HTTPSession~
HTTPSession.StopLeaseManager
HTTPSession.StartLeaseManager
