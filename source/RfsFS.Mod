(* Patrick Stuedi, 30.08.01 *)

модуль RfsFS; (** AUTHOR "pstuedi"; PURPOSE "Remote File System"; *)

использует НИЗКОУР, Modules, Clock, Files, RfsClientProxy, ЛогЯдра;

конст
	BufSize = RfsClientProxy.Payload;
	MaxBufs = 2;
	FnLength = 32;	(* includes 0X *)
	EnumRegular = 0;
	EnumDetail = 1;
	Ok = RfsClientProxy.REPLYOK;
	Trace = 0;

тип
	DiskSector = запись кон;	(* Oberon Sector, size SS *)
	FileName = массив FnLength из симв8;

	DataSector = запись (DiskSector)
		B: массив BufSize из симв8
	кон;

	Buffer* = укль на запись (Files.Hint)
		apos*, lim*: цел32;
		mod: булево;
		next: Buffer;
		data*: DataSector
	кон;

тип
	(** Interface from Files. One of the three Interfaces (Filesystem, File and Volume) which have to be implemented **)
	FileSystem* = окласс (Files.FileSystem)	(* Rfs file system type *)
		перем stubs: RfsClientProxy.Proxy;

		(** Creates a new file with the specified name **)
		проц {перекрыта}New0*(конст name: массив из симв8): Files.File;
		перем
			res: целМЗ;
			f: File;
			buf: Buffer;
			namebuf, nameTemp: FileName;
			hashval, errorcode: цел32;
		нач {единолично}
			f := НУЛЬ;
			Check(name, namebuf, res);
			если res <= 0 то
				FillBuf(nameTemp, 0X);
				stubs.CreateTmp(nameTemp, hashval, errorcode);
				если errorcode = Ok то
					нов(buf);
					buf.apos := 0;
					buf.mod := истина;
					buf.lim := 0;
					buf.next := buf;
					нов(f);
					f.fs := сам;
					f.key := hashval;
					f.aleng := 0;
					f.bleng := 0;
					f.modH := истина;
					f.firstbuf := buf;
					f.nofbufs := 1;
					f.name := namebuf;
					f.nameTemp := nameTemp;
					Clock.Get(f.time, f.date);
					f.registered := (f.name[0] = 0X);
				всё;
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("New0->name:");
				ЛогЯдра.пСтроку8(name);
				ЛогЯдра.пСтроку8(", res:");
				ЛогЯдра.пЦел64(res, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;

			возврат f
		кон New0;

		(** opens an existing File **)
		проц {перекрыта}Old0*(конст name: массив из симв8): Files.File;
		перем
			res: целМЗ;
			f: File;
			buf: Buffer;
			namebuf: FileName;
			hashval, errorcode, fileLen, time, date: цел32;
		нач {единолично}
			f := НУЛЬ;
			Check(name, namebuf, res);
			если res = 0 то
				stubs.Lookup(namebuf, hashval, errorcode);
				если (errorcode =  Ok) и (hashval # 0) то
					нов(buf);
					нов(f);
					f.key := hashval;
					f.fs := сам;
					stubs.GetAttr(hashval, fileLen, time, date, errorcode);
					f.aleng := fileLen DIV BufSize;
					f.bleng := fileLen остОтДеленияНа BufSize;
					ReadBuf(f, buf, 0, errorcode);
					если errorcode # Ok то
						возврат НУЛЬ;
					всё;
					buf.next := buf;
					buf.mod := ложь;
					buf.apos := 0;
					если f.aleng = 0 то
						buf.lim := f.bleng;
					иначе
						buf.lim := BufSize;
					всё;
					f.firstbuf := buf;
					f.nofbufs := 1;
					f.name := namebuf;
					f.time := time;
					f.date := date;
					f.registered := истина;
				всё;
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Old0->name:");
				ЛогЯдра.пСтроку8(name);
				ЛогЯдра.пСтроку8(", res:");
				ЛогЯдра.пЦел64(res, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;

			возврат f
		кон Old0;

		(** deletes a file, res = 0 indicates succsess **)
		проц {перекрыта}Delete0*(конст name: массив из симв8; перем key: цел32; перем res: целМЗ);
		перем namebuf: FileName; errorcode: цел32;
		нач {единолично}
			Check(name, namebuf, res);
			если res = 0 то
				stubs.Remove(namebuf, errorcode);
				если errorcode = Ok то
					res := 0;
				иначе
					res := 2;
				всё;
				key := 1;
			иначе
				key := 0
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Delete0->name:");
				ЛогЯдра.пСтроку8(name);
				ЛогЯдра.пСтроку8(", key:");
				ЛогЯдра.пЦел64(key, 1);
				ЛогЯдра.пСтроку8(", res:");
				ЛогЯдра.пЦел64(res, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Delete0;

		(** renames a file. res = 0 indicates success **)
		проц {перекрыта}Rename0*(конст old, new: массив из симв8; f: Files.File; перем res: целМЗ);
		перем oldbuf, newbuf: FileName; errorcode: цел32;
		нач {единолично}
			Check(old, oldbuf, res);
			если res = 0 то
				Check(new, newbuf, res);
				если res = 0 то
					если f # НУЛЬ то
						stubs.Rename(oldbuf, newbuf, errorcode);
						если errorcode # Ok то
							res := 2;
						всё;
					иначе
						res := 2;
					всё;
				всё;
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Rename0->old:");
				ЛогЯдра.пСтроку8(old);
				ЛогЯдра.пСтроку8(", new:");
				ЛогЯдра.пСтроку8(new);
				ЛогЯдра.пСтроку8(", res:");
				ЛогЯдра.пЦел64(res, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Rename0;

		(** enumerates the contents of a directory **)
		проц {перекрыта}Enumerate0*(конст mask: массив из симв8; flags: мнвоНаБитахМЗ; enum: Files.Enumerator);
		перем fnTmp, fn: массив Files.PrefixLength+FnLength из симв8; dir: RfsClientProxy.Dir;
				errorcode, cookie, endOfDir, helper, time, date, size, detail: цел32;
		нач {единолично}
			fn := ".";
			cookie := 0;
			helper := 0;
			если (Files.EnumTime в flags)  или (Files.EnumSize в flags) то
				detail := EnumDetail;
			иначе
				detail := EnumRegular;
			всё;
			нов(dir);

			нцПока helper = 0 делай
				stubs.ReadDir(fn, mask, detail, cookie, dir, endOfDir, errorcode);
				если errorcode = Ok то
					нцПока (dir.nbrOfEntrys > 0) и (dir.first # НУЛЬ) делай
						dir.Get(fnTmp, time, date, size);
						Files.JoinName(prefix, fnTmp, fn);
						enum.PutEntry(fn, {}, time, date, size);
					кц;
					cookie := dir.nbrOfEntrys;
					helper := endOfDir;
				иначе
					helper := 1;
				всё;
			кц;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Enumerate0->mask:");
				ЛогЯдра.пСтроку8(mask);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Enumerate0;

		(** return an unique id for a file registered in the filesystem **)
		проц {перекрыта}FileKey*(конст name: массив из симв8): цел32;
			перем
				res: целМЗ;
				namebuf: FileName;
				hashval, errorcode: цел32;
		нач {единолично}
			hashval := 0;
			Check(name, namebuf, res);
			если res = 0 то
				stubs.Lookup(namebuf, hashval, errorcode);
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("FileKey->name:");
				ЛогЯдра.пСтроку8(name);
				ЛогЯдра.пСтроку8(", filekey:");
				ЛогЯдра.пЦел64(hashval, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;

			возврат hashval;
		кон FileKey;

		проц &RfsInit*(vol: RfsClientProxy.Proxy);
		нач
			сам.stubs := vol;
		кон RfsInit;

		проц {перекрыта}Finalize;
			перем errorcode: цел32;
		нач {единолично}
			stubs.Unmount(errorcode);
			Finalize^;	(* see note in Files *)
		кон Finalize;

	кон FileSystem;

тип
	File* = окласс (Files.File)
		перем
			aleng, bleng: цел32;
			nofbufs: цел32;
			modH, registered: булево;
			firstbuf*: Buffer;
			name*, nameTemp*: FileName;
			time, date: цел32;

		(** positions a rider on a file at a given position. Riders cannot be positioned past the end of a file **)
		проц {перекрыта}Set*(перем r: Files.Rider; pos: Files.Position);
		перем a, b: цел32;
		нач {единолично}
			r.eof := ложь;
			r.res := 0;
			r.file := сам;
			r.fs := fs;
			если pos < 0 то
				a := 0; b := 0;
			аесли pos < aleng*BufSize + bleng то
				a := цел32(pos) DIV BufSize;
				b := цел32(pos) остОтДеленияНа BufSize;
			иначе
				a := aleng;
				b := bleng;
			всё;
			r.apos := a;
			r.bpos := b;
			r.hint := firstbuf;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Set->pos:");
				ЛогЯдра.пЦел64(pos, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Set;

		(** return the current position of a rider **)
		проц {перекрыта}Pos*(перем r: Files.Rider): Files.Position;
		нач
			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Pos->pos:");
				ЛогЯдра.пЦел64(r.apos*BufSize + r.bpos, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;

			возврат r.apos*BufSize + r.bpos;
		кон Pos;

		(** reads one byte from a file **)
		проц {перекрыта}Read*(перем r: Files.Rider; перем x: симв8);
		перем buf: Buffer; errorcode: цел32;
		нач {единолично}
			buf := r.hint(Buffer);
			если цел32(r.apos) # buf.apos то
				buf := GetBuf(сам, цел32(r.apos));
				r.hint := buf;
			всё;
			если r.bpos < buf.lim то
				x := buf.data.B[r.bpos];
				увел(r.bpos);
			аесли цел32(r.apos) < aleng то
				увел(r.apos);
				buf := SearchBuf(сам, цел32(r.apos));
				если buf = НУЛЬ то
					buf := r.hint(Buffer);
					если buf.mod то
						WriteBuf(сам, buf, errorcode);
					всё ;
						ReadBuf(сам, buf, цел32(r.apos), errorcode);
				иначе
					r.hint := buf;
				всё;
				если buf.lim > 0 то
					x := buf.data.B[0];
					r.bpos := 1;
				иначе
					x := 0X;
					r.eof := истина;
				всё;
			иначе
				x := 0X; r.eof := истина
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Read->x:");
				ЛогЯдра.пСимв8(x);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Read;

		(** read len bytes from a file. **)
		проц {перекрыта}ReadBytes*(перем r: Files.Rider; перем x: массив из симв8; ofs, len: размерМЗ);
		перем src, dst: адресВПамяти; m: размерМЗ; errorcode: цел32; buf: Buffer;
		нач {единолично}
			если длинаМассива(x)-ofs < len то
				НИЗКОУР.СТОП(19);
			всё;
			если len > 0 то
				dst := адресОт(x[ofs]);
				buf := r.hint(Buffer);
				если цел32(r.apos) # buf.apos то
					buf := GetBuf(сам, цел32(r.apos)); r.hint := buf;
				всё;
				нц
					если len <= 0 то
						прервиЦикл;
					всё ;
					src := адресОт(buf.data.B[0]) + r.bpos;
					m := r.bpos + len;
					если m <= buf.lim то
						НИЗКОУР.копируйПамять(src, dst, len);
						r.bpos := m;
						r.res := 0;
						прервиЦикл;
					аесли buf.lim = BufSize то
						m := buf.lim - r.bpos;
						если m > 0 то
							НИЗКОУР.копируйПамять(src, dst, m);
							увел(dst, m);
							умень(len, m)
						всё;
						если цел32(r.apos) < aleng то
							увел(r.apos);
							r.bpos := 0;
							buf := SearchBuf(сам, цел32(r.apos));
							если buf = НУЛЬ то
								buf := r.hint(Buffer);
								если buf.mod то
									WriteBuf(сам, buf, errorcode)
								всё;
								ReadBuf(сам, buf, цел32(r.apos), errorcode);
							иначе
								r.hint := buf;
							всё;
						иначе
							r.bpos := buf.lim;
							r.res := len;
							r.eof := истина;
							прервиЦикл;
						всё
					иначе
						m := buf.lim - r.bpos;
						если m > 0 то
							НИЗКОУР.копируйПамять(src, dst, m);
							r.bpos := buf.lim;
						всё;
						r.res := len - m; r.eof := истина; прервиЦикл
					всё;
				кц;
			иначе
				r.res := 0
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("ReadBytes->ofs:");
				ЛогЯдра.пЦел64(ofs, 1);
				ЛогЯдра.пСтроку8(", len:");
				ЛогЯдра.пЦел64(len, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон ReadBytes;

		(** writes one byte into a file **)
		проц {перекрыта}Write*(перем r: Files.Rider; x: симв8);
		перем buf: Buffer; errorcode: цел32;
		нач {единолично}
			buf := r.hint(Buffer);
			если цел32(r.apos) # buf.apos то
				buf := GetBuf(сам, цел32(r.apos));
				r.hint := buf
			всё;
			если r.bpos >= buf.lim то
				если r.bpos < BufSize то
					увел(buf.lim);
					увел(bleng);
					modH := истина;
				иначе
					WriteBuf(сам, buf, errorcode);
					увел(r.apos);
					buf := SearchBuf(сам, цел32(r.apos));
					если buf = НУЛЬ то
						buf := r.hint(Buffer);
						если цел32(r.apos) <= aleng то
							ReadBuf(сам, buf, цел32(r.apos), errorcode)
						иначе
							buf.apos := цел32(r.apos);
							buf.lim := 1;
							увел(aleng);
							bleng := 1;
							modH := истина;
						всё
					иначе
						r.hint := buf;
					всё;
					r.bpos := 0
				всё
			всё;
			buf.data.B[r.bpos] := x;
			увел(r.bpos);
			buf.mod := истина;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Write->x:");
				ЛогЯдра.пСимв8(x);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Write;

		(** write len bytes into a file **)
		проц {перекрыта}WriteBytes*(перем r: Files.Rider; конст x: массив из симв8; ofs, len: размерМЗ);
		перем src, dst: адресВПамяти; m: размерМЗ; errorcode: цел32; buf: Buffer;
		нач {единолично}
			если длинаМассива(x)-ofs < len то
				НИЗКОУР.СТОП(19);
			всё;
			если len > 0 то
				src := адресОт(x[ofs]);
				buf := r.hint(Buffer);
				если цел32(r.apos) # buf.apos то
					buf := GetBuf(сам, цел32(r.apos));
					r.hint := buf
				всё;
				нц
					если len <= 0 то
						прервиЦикл;
					всё;
					buf.mod := истина;
					dst := адресОт(buf.data.B[0]) + r.bpos;
					m := r.bpos + len;
					если m <= buf.lim то
						НИЗКОУР.копируйПамять(src, dst, len);
						r.bpos := m;
						прервиЦикл;
					аесли m <= BufSize то
						НИЗКОУР.копируйПамять(src, dst, len);
						r.bpos := m;
						bleng := цел32(m);
						buf.lim := цел32(m);
						modH := истина;
						прервиЦикл
					иначе
						m := BufSize - r.bpos;
						если m > 0 то
							НИЗКОУР.копируйПамять(src, dst, m);
							buf.lim := BufSize;
							увел(src, m);
							умень(len, m);
						всё;
						WriteBuf(сам, buf, errorcode);
						увел(r.apos);
						r.bpos := 0;
						buf := SearchBuf(сам, цел32(r.apos));
						если buf = НУЛЬ то
							buf := r.hint(Buffer);
							если цел32(r.apos) <= aleng то
								ReadBuf(сам, buf, цел32(r.apos), errorcode)
							иначе
								buf.apos := цел32(r.apos);
								buf.lim := 0;
								увел(aleng);
								bleng := 0;
								modH := истина;
								(*
								IF (aleng - STS) MOD XS = 0 THEN
									NewSub(SELF)
								END
								*)
							всё
						иначе
							r.hint := buf
						всё
					всё
				кц
			всё;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("WriteBytes->ofs:");
				ЛогЯдра.пЦел64(ofs, 1);
				ЛогЯдра.пСтроку8(", len:");
				ЛогЯдра.пЦел64(len, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон WriteBytes;

		(** return the lenght of a file **)
		проц {перекрыта}Length*(): Files.Size;
		нач {единолично}
			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Length->size:");
				ЛогЯдра.пЦел64(aleng*BufSize + bleng, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;

			возврат aleng*BufSize + bleng;
		кон Length;

		(** returns the last modified of a file **)
		проц {перекрыта}GetDate*(перем t, d: цел32);
		нач {единолично}
			t := time;
			d := date;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("GetDate->time:");
				ЛогЯдра.пЦел64(time, 1);
				ЛогЯдра.пСтроку8(", date:");
				ЛогЯдра.пЦел64(date, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;

		кон GetDate;

		(** sets the write-timestamp of file **)
		проц {перекрыта}SetDate*(t, d: цел32);
			перем errorcode: цел32; fsCasted: FileSystem;
		нач {единолично}
			time := t;
			date := d;
			fsCasted := сам.fs(FileSystem);
			fsCasted.stubs.SetAttr(name, time, date, errorcode);

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("SetDate->time:");
				ЛогЯдра.пЦел64(time, 1);
				ЛогЯдра.пСтроку8(", date:");
				ЛогЯдра.пЦел64(date, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон SetDate;

		(** return the name of a file **)
		проц {перекрыта}GetName*(перем name: массив из симв8);
		нач {единолично}
			Files.JoinName(fs.prefix, сам.name, name);

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("GetName->name:");
				ЛогЯдра.пСтроку8(name);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон GetName;

		(** registers a file in the filesystem **)
		проц {перекрыта}Register0*(перем res: целМЗ);
		перем
			buf: Buffer;
			errorcode: цел32;
			fsCasted : FileSystem;
		нач
			если ~registered и (name # "") то
				fsCasted := сам.fs(FileSystem);
				fsCasted.stubs.Rename(nameTemp, name, errorcode);
				если errorcode = Ok то
					registered := истина;
					res := 0;
				иначе
					res := 1;
				всё;
			иначе
				res := 1;
			всё;
			buf := firstbuf;
			нцДо
				если buf.mod то WriteBuf(сам, buf, errorcode) всё;
				buf := buf.next;
			кцПри buf = firstbuf;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Register->res:");
				ЛогЯдра.пЦел64(res, 1);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Register0;


		проц {перекрыта}Update*;
			перем buf: Buffer; errorcode: цел32;
		нач {единолично}
			buf := firstbuf;
			нцДо
				если buf.mod то WriteBuf(сам, buf, errorcode) всё;
				buf := buf.next;
			кцПри buf = firstbuf;

			если Trace = 1 то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Update");
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			всё;
		кон Update;

	кон File;


(* Check a file name. *)


перем newfs*: FileSystem;


проц Check(конст s: массив из симв8; перем name: FileName; перем res: целМЗ);
	перем i: цел32; ch: симв8;
нач
	ch := s[0]; i := 0;
	если ch = "/" то
		res := 3;
	аесли ("A" <= ASCII_вЗаглавную(ch)) и (ASCII_вЗаглавную(ch) <= "Z") то
		нц name[i] := ch; увел(i); ch := s[i];
			если (ch = ".") и (name[i-1] = ".") то
				res := 3;
				прервиЦикл;
			аесли ch = 0X то
				нцПока i < FnLength делай name[i] := 0X; увел(i) кц ;
				res := 0; прервиЦикл
			всё ;
			если ~(("A" <= ASCII_вЗаглавную(ch)) и (ASCII_вЗаглавную(ch) <= "Z")
				или ("0" <= ch) и (ch <= "9") или (ch = ".")) то res := 3; прервиЦикл
			всё ;
			если i = FnLength-1 то res := 4; прервиЦикл всё
		кц
	аесли ch = 0X то name[0] := 0X; res := -1
	иначе res := 3
	всё
кон Check;

(*
PROCEDURE CheckTmp(VAR s: ARRAY OF CHAR; VAR name: FileName; VAR res: INTEGER);
	VAR i: SIGNED32; ch: CHAR;
BEGIN
	s[6] := 0X;
	KernelLog.String("CheckTMP : ");
	KernelLog.String(s);
	KernelLog.Ln;
	ch := s[0]; i := 0;
	IF (ch # ".") THEN
		KernelLog.String("tmpfile");
		KernelLog.Ln;
		res := 3;
	ELSE
		ch := s[1]; i := 1;
		KernelLog.String("step1..");
		KernelLog.Ln;
		IF ("A" <= CAP(ch)) & (CAP(ch) <= "Z") THEN
			KernelLog.String("step2..");
			KernelLog.Ln;
			LOOP name[i] := ch; INC(i); ch := s[i];
				KernelLog.String("step3..");
				KernelLog.Ln;
				IF ch = 0X THEN
					KernelLog.String("step4..");
					KernelLog.Ln;
					WHILE i < FnLength DO
						KernelLog.String("step4..");
						KernelLog.Ln;
						name[i] := 0X; INC(i)
					END ;
					res := 0; EXIT
				END ;
				IF ~(("A" <= CAP(ch)) & (CAP(ch) <= "Z")
					OR ("0" <= ch) & (ch <= "9") OR (ch = ".")) THEN res := 3; EXIT
				END ;
				IF i = FnLength-1 THEN res := 4; EXIT END
			END
		ELSIF ch = 0X THEN name[0] := 0X; res := -1
		ELSE res := 3
		END
	END;
END CheckTmp;
*)


(** Read Buf - reads a buffer from a file f into buf **)
проц ReadBuf(f: File; buf: Buffer; pos: цел32; перем errorcode: цел32);
перем fsCasted: FileSystem; received: размерМЗ;
нач
	fsCasted := f.fs(FileSystem);
	fsCasted.stubs.Read(f.key, pos*BufSize, BufSize, buf.data.B, 0, received, errorcode);
	если errorcode = Ok то
		если pos < f.aleng то
			buf.lim := received(цел32);
		иначе
			buf.lim := f.bleng;
		всё;
		buf.apos := pos;
		buf.mod := ложь;
	аесли errorcode = RfsClientProxy.CACHEMISS то
		если Trace = 1 то
			ЛогЯдра.ЗахватВЕдиноличноеПользование;
			ЛогЯдра.пСтроку8("ReadBuf->errorcode: ");
			ЛогЯдра.пЦел64(errorcode, 1);
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		всё;
		если ~f.registered то
			fsCasted.stubs.Lookup(f.nameTemp, f.key, errorcode);
		иначе
			fsCasted.stubs.Lookup(f.name, f.key, errorcode);
		всё;
		fsCasted.stubs.Read(f.key, pos*BufSize, BufSize, buf.data.B, 0, received, errorcode);
		если errorcode = Ok то
			если pos < f.aleng то
				buf.lim := received(цел32);
			иначе
				buf.lim := f.bleng;
			всё;
			buf.apos := pos;
			buf.mod := ложь;
		всё;
	всё;
кон ReadBuf;

(** WriteBuf - writes a buffer buf into a file f **)
проц WriteBuf(f: File; buf: Buffer; перем errorcode: цел32);
перем fsCasted: FileSystem; written: цел32;
нач
	f.modH := истина;
	fsCasted := f.fs(FileSystem);
	fsCasted.stubs.Write(f.key, buf.apos*BufSize, buf.lim, buf.data.B, written, errorcode);
	если errorcode = Ok то
		buf.mod := ложь;
	аесли errorcode = RfsClientProxy.CACHEMISS то
		если Trace = 1 то
			ЛогЯдра.ЗахватВЕдиноличноеПользование;
			ЛогЯдра.пСтроку8("WriteBuf->errorcode: ");
			ЛогЯдра.пЦел64(errorcode, 1);
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
		всё;
		если ~f.registered то
			fsCasted.stubs.Lookup(f.nameTemp, f.key, errorcode);
		иначе
			fsCasted.stubs.Lookup(f.name, f.key, errorcode);
		всё;
		fsCasted.stubs.Write(f.key, buf.apos*BufSize, buf.lim, buf.data.B, written, errorcode);
		если errorcode = Ok то
			buf.mod := ложь;
		всё;
	всё;
кон WriteBuf;

проц SearchBuf(f: File; pos: цел32): Buffer;
перем buf: Buffer;
нач
	buf := f.firstbuf;
	нц
		если buf.apos = pos то прервиЦикл всё;
		buf := buf.next;
		если buf = f.firstbuf то buf := НУЛЬ; прервиЦикл всё
	кц;
	возврат buf
кон SearchBuf;

проц GetBuf(f: File; pos: цел32): Buffer;
перем buf: Buffer; errorcode: цел32;
нач
	buf := f.firstbuf;
	нц
		если buf.apos = pos то прервиЦикл всё;
		если buf.next = f.firstbuf то
			если f.nofbufs < MaxBufs то (* allocate new buffer *)
				нов(buf); buf.next := f.firstbuf.next; f.firstbuf.next := buf;
				увел(f.nofbufs)
			иначе (* take one of the buffers *)
				f.firstbuf := buf;
				если buf.mod то WriteBuf(f, buf, errorcode) всё
			всё;
			buf.apos := pos;
			если pos <= f.aleng то ReadBuf(f, buf, pos, errorcode) всё;
			прервиЦикл
		всё;
		buf := buf.next
	кц;
	возврат buf
кон GetBuf;


(** Generate a new file system object.  Files.NewVol has volume parameter, Files.Par has mount prefix. *)
проц NewFS*(context : Files.Parameters);
перем vol: RfsClientProxy.Proxy;
нач
	vol := context.vol(RfsClientProxy.Proxy);
	если vol # НУЛЬ то
		нов(newfs, vol);
		newfs.desc := "Remote File System";
		Files.Add(newfs, context.prefix);
		context.out.пСтроку8("FS->"); context.out.пСтроку8(context.prefix); context.out.пСтроку8(" added"); context.out.пВК_ПС
	иначе
		context.error.пСтроку8("FS"); context.error.пСтроку8("RfsFS: Failure"); context.error.пВК_ПС
	всё;
кон NewFS;

проц FillBuf(перем x: массив из симв8; ch: симв8);
	перем len, i: размерМЗ;
нач
	len := длинаМассива(x);
	нцДля i := 0 до len-1 делай
		x[i] := ch;
	кц;
кон FillBuf;



(* Clean up when module unloaded. *)

проц Cleanup;
перем ft: Files.FileSystemTable; i: размерМЗ;
нач
	если Modules.shutdown = Modules.None то
		Files.GetList(ft);
		если ft # НУЛЬ то
			нцДля i := 0 до длинаМассива(ft^)-1 делай
				если ft[i] суть FileSystem то Files.Remove(ft[i]) всё
			кц
		всё
	всё
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
	newfs := НУЛЬ;
кон RfsFS.

(*
	aleng * BufSize + bleng = length of File
	apos * BufSize + bpos = current position
	0 <= bpos <= lim <= BufSize
	0 <= apos <= aleng
	(apos < aleng) & (lim = BufSize) OR (apos = aleng)

	Methods with {} notation are explicitly unprotected.  They must be called only from a protected context.
*)
