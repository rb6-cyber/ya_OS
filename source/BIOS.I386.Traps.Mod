модуль Traps;	(** AUTHOR "pjm"; PURPOSE "Trap handling and symbolic debugging"; *)

использует НИЗКОУР, Machine, ЛогЯдра, Потоки, Modules, Objects, Kernel, Reflection, TrapWriters;

конст
	RecursiveLimit = 2;		(* normally 1 or 2 - how many recursive traps to display before stopping *)
	TraceVerbose = ложь;
	TestTrap = истина;

	(* Process termination halt codes *)
	halt* = Objects.halt;
	haltUnbreakable* = Objects.haltUnbreakable;

тип
	Variable* = запись	(** variable descriptor *)
		adr-: адресВПамяти;
		type-, size-, n-, tdadr-: цел32
	кон;

перем
	trapState: массив Machine.MaxCPU из цел32;	(* indexed by Machine.ID() *)
	modes: массив 25 из симв8;
	flags: массив 13 из симв8;



	(* Write flag values. *)
	проц Flags(w: Потоки.Писарь; s: мнвоНаБитахМЗ);
	перем i: цел8; ch: симв8;
	нач
		нцДля i := 0 до 11 делай
			ch := flags[i];
			если ch # "!" то
				если i в s то ch := ASCII_вЗаглавную(ch) всё;
				w.пСимв8(ch)
			всё
		кц;
		w.пСтроку8(" iopl"); w.пЦел64(арифмСдвиг(НИЗКОУР.подмениТипЗначения(цел32, s * {12,13}), -12), 1)
	кон Flags;

	(** Display trap state. *)
	проц  Show*(p: Objects.Process; перем int: Machine.State; перем exc: Machine.ExceptionState; long: булево);
	перем id: цел32; overflow: булево; w: Потоки.Писарь;

		проц Val(конст s: массив из симв8; val: цел32);
		нач
			w.пСимв8(" "); w.пСтроку8(s); w.пСимв8("="); w.п16ричное(val, -8)
		кон Val;

	нач
		overflow := ложь;
		w := TrapWriters.GetWriter();
		w.ПротолкниБуферВПоток;	(* flush previous output stuck in global writer w *)
		w.пСимв8(1X);	(* "start of trap" *)
		id := Machine.ID();
		увел(trapState[id]);
		если trapState[id] > RecursiveLimit то
			w.пСтроку8(" [Recursive TRAP]")
		иначе
			(* output first line *)
			w.пСтроку8("TRAP "); w.пЦел64(exc.halt, 1);
			w.пСтроку8(" ["); w.пЦел64(trapState[id], 1); w.пСтроку8("]");
			w.пСтроку8(" PL"); w.пЦел64(int.CS остОтДеленияНа 4, 2); w.пСимв8(" ");
			просей exc.halt из
				-14:	(* page fault *)
					если (int.CS остОтДеленияНа 4 > Machine.KernelLevel) и (exc.pf+4 = int.SP) то
						w.пСтроку8("stack overflow"); overflow := истина
					иначе
						w.пСтроку8("memory access violation (page fault)");
					всё
				|0: w.пСтроку8("division error")
				|1: w.пСтроку8("WITH guard failed")
				|2: w.пСтроку8("CASE invalid")
				|3: w.пСтроку8("RETURN missing")
				|4: w.пСтроку8("integer overflow")
				|5: w.пСтроку8("implicit type guard failed")
				|6: w.пСтроку8("type guard failed")
				|7: w.пСтроку8("index out of range")
				|8: w.пСтроку8("ASSERT failed")
				|9: w.пСтроку8("array dimension error")
				|14: w.пСтроку8("out of memory")
				|16: w.пСтроку8("procedure returned")
				|17: w.пСтроку8("nil pointer access")
				иначе
					если (exc.halt > матМаксимум(цел16)+1) или (exc.halt < матМинимум(цел16)) то
						w.пСтроку8("module freed?")
					всё
			всё;
			если exc.locks # {} то
				w.пСтроку8(", Locks: "); w.пМнвоНаБитахМЗ(exc.locks)
			всё;
			w.пВК_ПС; w.пСтроку8("System: "); w.пСтроку8(Machine.version); w.пСтроку8(" Kernel_CRC=");(* w.Hex(SystemVersion.BootCRC,8);*)
			если long то
				w.пСимв8(0EX);	(* "fixed font" *)
				w.пВК_ПС;
				w.пСтроку8("Processor:");
				(* output values *)
				Val("CS", int.CS); Val("DS", exc.DS); Val("ES", exc.ES); Val("SS", exc.SS); Val("CR0", exc.CR[0]);
				Val("FPU", НИЗКОУР.подмениТипЗначения(цел32, exc.FPU[1] * {0..15} + логСдвиг(exc.FPU[2], 16)));
				Val("PC", int.PC); Val("ESI", int.ESI); Val("EDI", int.EDI); Val("SP", exc.SP); Val("CR2", exc.CR[2]);
				Val("PID", id);
				Val("EAX", int.EAX); Val("EBX", int.EBX); Val("ECX", int.ECX); Val("EDX", int.EDX); Val("CR3", exc.CR[3]);
				Val("LCK", НИЗКОУР.подмениТипЗначения(цел32, exc.locks));
				Val("BP", int.BP); Val("FS", exc.FS); Val("GS", exc.GS); Val("ERR", int.ERR); Val("CR4", exc.CR[4]);
				Val("TMR", Kernel.GetTicks()); w.пВК_ПС;
				если НИЗКОУР.подмениТипЗначения(симв8, exc.DR[7]) # 0X то	(* some breakpoints enabled *)
					Val("DR0", exc.DR[0]); Val("DR1", exc.DR[1]); Val("DR2", exc.DR[2]); Val("DR3", exc.DR[3]);
					Val("DR6", exc.DR[6]); Val("DR7", exc.DR[7]); w.пВК_ПС
				всё;
				w.пСтроку8(" FLAGS: "); Flags(w, int.FLAGS);
				w.пСимв8(0FX);	(* "proportional font" *)
				w.пСимв8(" "); w.пМнвоНаБитахМЗ(int.FLAGS); w.пВК_ПС;
				w.пСтроку8(" Features: "); (* w.Set(Machine.features); w.Set(Machine.features2); *) w.пВК_ПС;
				(*IF int.INT = Machine.UD THEN KernelLog.Memory(int.PC, 16) END*)	(* show bad instruction *)
			иначе
				w.пВК_ПС
			всё;
			w.пСтроку8("Process:"); Reflection.WriteProcess(w, p); w.пВК_ПС;
			(*IF exc.halt = 1301 THEN	(* lock timeout - see Machine *)
				KernelLog.Memory(ADDRESSOF(Machine.trapState[0]), LEN(Machine.trapState) *
					(ADDRESSOF(Machine.trapState[1]) - ADDRESSOF(Machine.trapState[0])));
				w.Hex(SYSTEM.VAL(SIGNED32, Machine.trapLocksBusy), 8); w.Ln
			END;
			IF (int.INT = Machine.PF) & (ABS(int.PC-exc.CR[2]) < 100H) THEN	(* PF close to PC *)
				KernelLog.Memory(int.ESP-16, 64)	(* show stack *)
			END;*)
			w.пСтроку8( "StackTraceBack:" );  w.пВК_ПС;
			Reflection.StackTraceBack(w, int.PC, int.BP, int.SP, Objects.GetStackBottom(p), long, overflow);
		всё;
		w.пСтроку8("---------------------------------"); w.пВК_ПС;
		w.пСимв8(02X);	(* "end of trap" *)
		w.ПротолкниБуферВПоток;
		TrapWriters.Trapped();
		trapState[id] := 0
	кон Show;

	проц SetLastExceptionState(ex: Machine.ExceptionState);
	перем id: цел32;
	нач
		id := Machine.AcquirePreemption();
		Objects.running[id].exp := ex;
		Machine.ReleasePreemption;
	кон SetLastExceptionState;

	проц GetLastExceptionState*(): Machine.ExceptionState;
	перем
		id: цел32;
		ex: Machine.ExceptionState;
	нач
		id := Machine.AcquirePreemption();
		ex := Objects.running[id].exp;
		Machine.ReleasePreemption;
		возврат ex;
	кон GetLastExceptionState;

	(**  Handles an exception. Interrupts are on during this procedure. *)
	проц HandleException(перем int: Machine.State; перем exc: Machine.ExceptionState; перем handled: булево);
	перем
		bp, sp, pc, handler: адресВПамяти;
	нач
		bp := int.BP; sp := int.SP; pc := int.PC;
		handler := Modules.GetExceptionHandler(pc);
	 	если handler # -1 то (* Handler in the current PAF *)
			int.PC := handler; handled := истина;
			SetTrapVariable(pc, bp); SetLastExceptionState(exc)
		иначе
			нцПока (bp # 0) и (handler = -1) делай
				НИЗКОУР.прочтиОбъектПоАдресу(bp + 4, pc);
				pc := pc - 1; (*  CALL instruction, machine dependant!!! *)
				handler := Modules.GetExceptionHandler(pc);
				sp :=  bp; (* Save the old basepointer into the stack pointer *)
				НИЗКОУР.прочтиОбъектПоАдресу(bp, bp) (* Unwind PAF *)
			кц;
			если handler = -1 то
				handled := ложь;
			иначе
				int.PC := handler; int.BP := bp; int.SP := sp;
				SetTrapVariable(pc, bp); SetLastExceptionState(exc);
				handled := истина
			всё
		всё
	кон HandleException;

	проц SetTrapVariable(pc, fp: адресВПамяти);
	перем
		varadr: адресВПамяти;
	нач
		varadr := Reflection.GetVariableAdr(pc, fp, "trap");
		если varadr # -1 то
			НИЗКОУР.запиши8битПоАдресу(varadr, 1)
		всё
	кон SetTrapVariable;

	(* Unbreakable stack trace back with regard to every FINALLY on the way *)
	проц Unbreakable(p: Objects.Process; перем int: Machine.State; перем exc: Machine.ExceptionState; перем handled: булево);
	перем
		bp, bpSave, pc, handler, bpBottom:адресВПамяти;
		hasFinally : булево;
	нач
		bp := int.BP;
		pc := int.PC;
		hasFinally := ложь;

		handler := Modules.GetExceptionHandler(pc);

		(* Handler in the current PAF *)
	 	если handler # -1 то
			int.PC := handler;
			hasFinally := истина;
			SetTrapVariable(pc, bp);
		всё;

		(* The first waypoint is the bp of the top PAF *)
		bpSave := bp;

		нцПока (bp # 0) делай
			(* Did we reach the last PAF? *)
			НИЗКОУР.прочтиОбъектПоАдресу(bp, pc);
			если (pc = 0) то
				bpBottom := bp; (* Save the FP of the last PAF *)
			всё;

			(* Get the return pc *)
			НИЗКОУР.прочтиОбъектПоАдресу(bp + размер16_от(адресВПамяти), pc);

			handler := Modules.GetExceptionHandler(pc);

			(* Save the last framepointer as stackpointer *)
			если ~hasFinally то
				int.SP :=  bp;
			всё;

			НИЗКОУР.прочтиОбъектПоАдресу(bp, bp);

			(* Here bp may be 0. *)

			если (handler # -1) и (bp # 0)  то (* If Objects.Terminate has a FINALLY this doesn't work !!! *)
				если hasFinally то
					(* Connect Finally to Finally *)
					НИЗКОУР.запишиОбъектПоАдресу(bpSave + размер16_от(адресВПамяти), handler); (* Adapt the return pc *)
					НИЗКОУР.запишиОбъектПоАдресу(bpSave, bp); (* Adapt the dynamic link *)
					bpSave := bp;
				иначе
					int.PC := handler;
					int.BP := bp;
					bpSave := bp;
					hasFinally := истина;
				всё;
				SetTrapVariable(pc, bp)
			всё
		кц;

		(* Now bp =  0, bottom of the stack, so link the last known return PC to the Termination *)
		если ~hasFinally то
			НИЗКОУР.прочтиОбъектПоАдресу(bpBottom + размер16_от(адресВПамяти), pc); (* PC of the Terminate *)
			int.PC := pc;
			int.BP := bpBottom;
		аесли bpSave # bpBottom то
			НИЗКОУР.прочтиОбъектПоАдресу(bpBottom + размер16_от(адресВПамяти), pc); (* PC of the Terminate *)
			НИЗКОУР.запишиОбъектПоАдресу(bpSave + размер16_от(адресВПамяти), pc);
			SetLastExceptionState(exc)
		всё;

		handled := истина; (* If FALSE the process could be restarted, may be this is the meaning? *)

	кон Unbreakable;

	(* General exception handler. *)
	проц Exception(перем int: Machine.State);
	перем t: Objects.Process; exc: Machine.ExceptionState; user, traceTrap, handled: булево;
	нач	(* interrupts off *)
		t := Objects.running[Machine.ID()];	(* t is running process *)
		handled := ложь;
		Machine.GetExceptionState(int, exc);
		user := (int.CS остОтДеленияНа 4 > Machine.KernelLevel);
		traceTrap := (exc.locks = {}) и (exc.halt >= матМаксимум(цел16)) и (exc.halt <= матМаксимум(цел16)+1);

		Show(t, int, exc, exc.halt # матМаксимум(цел16)+1);	(* Always show the trap info!*)

		если exc.halt = haltUnbreakable то
			Unbreakable(t, int, exc, handled)
		аесли ~ traceTrap то
			HandleException( int, exc, handled)
		всё;

		если ~handled то
			(* Taken from Machine to allow the FINALLY in the kernel *)
			exc.locks := Machine.BreakAll();
			Machine.Sti();
			если ~traceTrap то	(* trap *)
				если user то	(* return to outer level *)
					если TraceVerbose то
						ЛогЯдра.ЗахватВЕдиноличноеПользование;
						ЛогЯдра.пСтроку8("Jump");  ЛогЯдра.п16ричное(t.restartPC, 9);
						ЛогЯдра.п16ричное(t.restartSP, 9);  ЛогЯдра.п16ричное(t.stack.high, 9);
						ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
					всё;
					включиВоМнвоНаБитах(int.FLAGS, Machine.IFBit);	(* enable interrupts *)
					int.BP := 0; int.SP := t.restartSP;	(* reset stack *)
					int.PC := t.restartPC;	(* restart object body or terminate *)
				иначе	(* trap was in kernel (interrupt handler) *)	(* fixme: recover from trap in stack traceback *)
					ЛогЯдра.ЗахватВЕдиноличноеПользование;  ЛогЯдра.пСтроку8("Kernel halt");  ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
					Machine.Shutdown(ложь)
				всё
			всё
		всё;

		если Objects.PleaseHalt в t.flags то
			исключиИзМнваНаБитах(t.flags, Objects.PleaseHalt);
			если Objects.Unbreakable в t.flags то исключиИзМнваНаБитах(t.flags, Objects.Unbreakable) всё;
			если Objects.SelfTermination в t.flags то исключиИзМнваНаБитах(t.flags, Objects.SelfTermination) всё
		всё
	кон Exception;

	(* Page fault handler. *)
	проц PageFault(перем state: Machine.State);
	перем t: Objects.Process;
	нач
		t := Objects.running[Machine.ID()];
		если Machine.IFBit в state.FLAGS то	(* enable interrupts again if they were enabled *)
			Machine.Sti()	(* avoid Processors.StopAll deadlock when waiting for locks below (fixme: remove) *)
		всё;
		если (t = НУЛЬ) или ~Machine.ExtendStack(t.stack, Machine.CR2()) то
			если TraceVerbose то
				если t = НУЛЬ то
					ЛогЯдра.ЗахватВЕдиноличноеПользование;  ЛогЯдра.пСтроку8("GrowStack running=NIL");
					ЛогЯдра.п16ричное(state.PC, 9);  ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				иначе
					ЛогЯдра.ЗахватВЕдиноличноеПользование;
					ЛогЯдра.пСтроку8("GrowStack failed, pf="); ЛогЯдра.п16ричное(Machine.CR2(), 8);
					ЛогЯдра.пСтроку8(" adr="); ЛогЯдра.п16ричное(t.stack.adr, 8);
					ЛогЯдра.пСтроку8(" high="); ЛогЯдра.п16ричное(t.stack.high, 8);
					(*KernelLog.Ln; KernelLog.Memory(t.stack.adr, 256);*)
					ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
				всё
			всё;
			Exception(state)
		иначе
			если TraceVerbose то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;  ЛогЯдра.пСтроку8("GrowStack");
				ЛогЯдра.п16ричное(t.stack.adr, 9);  ЛогЯдра.п16ричное(t.stack.high, 9);  ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё
		всё
	кон PageFault;

	проц Init;
	перем i: цел32; s: массив 8 из симв8;
	нач
		если TestTrap то
			Machine.GetConfig("TestTrap", s);
			если s[0] = "1" то СТОП(98) всё
		всё;
		нцДля i := 0 до Machine.MaxCPU-1 делай trapState[i] := 0 кц;
		Machine.InstallHandler(PageFault, Machine.PF);
		нцДля i := 0 до 31 делай
			если ~(i в {Machine.PF}) то	(* PF handler above *)
				Machine.InstallHandler(Exception, i)
			всё
		кц;
		если TestTrap и (s[0] = "2") то СТОП(99) всё
	кон Init;

нач
	modes := " rdy run awl awc awe rip";	(* 4 characters per mode from Objects.Ready to Objects.Terminated *)
	flags := "c!p!a!zstido";	(* bottom flags, !=reserved *)
	Init
кон Traps.

(*
12.03.1998	pjm	Started
06.08.1998	pjm	Exported Show and removed AosException upcall installation & Modules lock
10.12.1998	pjm	New refblk
23.06.1999	pjm	State added
*)

(*
to do:
o stack overflow message is not correctly displayed in case of dynamic arrays (EDI = CR2, ESP # CR2)
o fix KernelLog.Memory calls removed when switching to Streams
o fix use of KernelLog lock in Show
o if allowing modification of variables using their descriptors, it should also have reference to module to avoid gc after free.
*)
