(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль MousePS2; (** AUTHOR "pjm"; PURPOSE "PS/2 mouse driver"; *)

(*
	Mouse protocol information from XFree in X11R6 distribution (Thomas Roell & David Dawes)
	PS/2 Aux port information from Linux (Johan Myreen et al.)
*)

использует НИЗКОУР, Machine, Modules, Objects, Kernel, Inputs;

тип
	Aux = окласс
		перем
			p, numb: цел32;
			buf: массив 4 из мнвоНаБитахМЗ;
			active: булево;
			timer: Objects.Timer;

		проц HandleInterrupt;
		перем b: мнвоНаБитахМЗ; m: Inputs.MouseMsg; ch: симв8;
		нач {единолично}
			Machine.Portin8(64H, ch); (* check for valid data *)
			если НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, ch) * {0} = {} то увел(ignored); возврат	всё;
			Machine.Portin8(60H, ch);	(* read byte *)
			если active то
				b := НИЗКОУР.подмениТипЗначения(мнвоНаБитахМЗ, устарПреобразуйКБолееШирокомуЦел(кодСимв8(ch)));
				если (p = 0) и (b * {6,7} # {}) то	(* skip package *)
					увел(errors)
				иначе
					buf[p] := b; увел(p);
					если p = numb то
						m.keys := {};
						если 2 в buf[0] то включиВоМнвоНаБитах(m.keys, 1) всё;
						если 1 в buf[0] то включиВоМнвоНаБитах(m.keys, 2) всё;
						если 0 в buf[0] то включиВоМнвоНаБитах(m.keys, 0) всё;
						m.dx := НИЗКОУР.подмениТипЗначения(цел32, buf[1]);
						если 4 в buf[0] то умень(m.dx, 256) всё;
						m.dy := НИЗКОУР.подмениТипЗначения(цел32, buf[2]);
						если 5 в buf[0] то умень(m.dy, 256) всё;
						m.dz := НИЗКОУР.подмениТипЗначения(цел8, buf[3]);
						если 6 в buf[0] то умень(m.dz, 256) всё;
						p := 0; m.dy := -m.dy;
						Inputs.mouse.Handle(m)
					всё
				всё
			иначе
				увел(ignored)
			всё
		кон HandleInterrupt;

		проц HandleTimeout;
		нач {единолично}
			active := истина
		кон HandleTimeout;

		проц &Init*(rate: цел32);

			проц SetRate(r: цел32);
			нач WriteAck(0F3X);  WriteAck(симв8ИзКода(r))
			кон SetRate;

		нач
			active := ложь; p := 0;
			PollAux;
			Machine.Portout8(64H, 0A8X);	(* enable aux *)
				(* enable MS Intellimouse 3rd button *)
			SetRate(200); SetRate(100); SetRate(80); SetRate(rate);
			WriteAck(0F2X);
			если InAux() # 0X то numb := 4 иначе numb := 3 всё;	(* Ident *)
			WriteAck(0E8X);  WriteAck(3X);	(* 8 counts/mm *)
			WriteAck(0E7X);	(* 2:1 scale *)
			PollAux;
			Objects.InstallHandler(сам.HandleInterrupt, Machine.IRQ0+12);
			WriteDev(0F4X);	(* enable aux device *)
			WriteCmd(47X);	(* controller interrupts on *)
			PollAux;
			нов(timer); Objects.SetTimeout(timer, сам.HandleTimeout, 250)	(* ignore bytes in first 1/4s *)
		кон Init;

		проц Remove;
		нач {единолично}
			Objects.RemoveHandler(сам.HandleInterrupt, Machine.IRQ0+12);
			Objects.CancelTimeout(timer)
		кон Remove;

	кон Aux;

перем
	errors*, ignored*: цел32;	(* diagnostic counters *)
	aux: Aux;

проц PollAux;
перем s: мнвоНаБитахМЗ; i: цел32; t: Kernel.MilliTimer;
нач
	i := 10;	(* up to 0.2s! *)
	нц
		Machine.Portin8(64H, НИЗКОУР.подмениТипЗначения(симв8, s));
		если (s * {0,1} = {}) или (i = 0) то прервиЦикл всё;
		Machine.Portin8(64H, НИЗКОУР.подмениТипЗначения(симв8, s));
		если s * {0,5} = {0,5} то Machine.Portin8(60H, НИЗКОУР.подмениТипЗначения(симв8, s)) всё;	(* byte avail *)
		Kernel.SetTimer(t, 20);	(* 20ms *)
		нцДо кцПри Kernel.Expired(t);
		умень(i)
	кц
кон PollAux;

проц InAux(): симв8;
перем s: мнвоНаБитахМЗ; t: Kernel.MilliTimer;ch: симв8; i: цел8;
нач
	i := 10;	(* up to 0.2s! *)
	нцДо
		Machine.Portin8(64H, НИЗКОУР.подмениТипЗначения(симв8, s));
		если s * {0,5} = {0,5} то 	(* byte avail *)
			Machine.Portin8(60H, ch);
			возврат ch
		всё;
		Kernel.SetTimer(t, 20);
		нцДо кцПри Kernel.Expired(t);
		умень(i);
	кцПри i = 0;
	возврат 0X
кон InAux;
проц WriteDev(b: симв8);
нач
	PollAux; Machine.Portout8(64H, 0D4X);	(* aux data coming *)
	PollAux; Machine.Portout8(60H, b)
кон WriteDev;

проц WriteAck(b: симв8);
перем s: мнвоНаБитахМЗ; i: цел32; t: Kernel.MilliTimer;
нач
	WriteDev(b); i := 10;	(* up to 0.2s! *)
	нц
		Machine.Portin8(64H, НИЗКОУР.подмениТипЗначения(симв8, s));
		если (s * {0,5} = {0,5}) или (i = 0) то прервиЦикл всё;
		Kernel.SetTimer(t, 20);	(* 20ms *)
		нцДо кцПри Kernel.Expired(t);
		умень(i)
	кц;
	если i # 0 то Machine.Portin8(60H, НИЗКОУР.подмениТипЗначения(симв8, s)) всё	(* byte avail *)
кон WriteAck;

проц WriteCmd(b: симв8);
нач
	PollAux; Machine.Portout8(64H, 60X);
	PollAux; Machine.Portout8(60H, b)
кон WriteCmd;

проц ConfigMouse;
перем i: размерМЗ; rate: цел32; s: массив 16 из симв8;
нач
	errors := 0; ignored := 0;
	Machine.GetConfig("MouseRate", s);
	i := 0; rate := Machine.StrToInt(i, s);
	если (rate <= 0) или (rate > 150) то rate := 100 всё;
	нов(aux, rate)
кон ConfigMouse;

проц Install*;
нач
	если aux = НУЛЬ то ConfigMouse всё
кон Install;

проц Remove*;
нач
	если aux # НУЛЬ то aux.Remove(); aux := НУЛЬ всё
кон Remove;

нач
	Modules.InstallTermHandler(Remove);
	aux := НУЛЬ; Install
кон MousePS2.
