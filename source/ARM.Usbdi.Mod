модуль Usbdi; (** AUTHOR "staubesv"; PURPOSE "USB Driver Interface"; *)
(**
 * A2 USB Driver Interface
 *
 * This is the interface between USB device drivers and the A2 USB system software. If you want to develop a USB device
 * driver, be sure you don't miss the following ressources:
 *
 *	UsbSkeleton.Mod	USB device driver skeleton
 *
 * Overview:
 *
 *	Driver			Base class of USB device drivers, provides access to UsbDevice object
 *	UsbDevice		Abstraction of USB device, provides access to device descriptors and USB pipes
 *	Pipe			Communication channel used to send/receive data. Association of device endpoint and client buffer
 *	DriverManager	Keeps track of loaded USB device drivers
 *)

использует Plugins, UsbBuffers;

конст

	(** Result codes for USB transfers *)

	Ok* = 0;			(** Transfer completed without errors *)
	ShortPacket* = 1;	(** Device sent less data than requested *)
	Stalled* = 2;		(** Pipe stalled -> needs to be cleared using the Clearhalt request before communication can continue *)
	InProgress* = 3;		(** Transfer is still ongoing. For blocking transfers this means a timeout occured *)
	Error* = 4;			(** Unrecoverable Error *)
	Disconnected* = 5;	(** Device not connected to the bus anymore *)

	(** Coding of the EndpointDescriptor.type field *)
	Control* = 0;
	BulkIn* = 1;	BulkOut* = 2;
	InterruptIn* = 3; InterruptOut* = 4;
	IsochronousIn* = 5; IsochronousOut* = 6;

	(** bmRequestType encoding for control transfers *)

	(** Request direction *)
	ToDevice* = {}; ToHost* = {7};

	(** Request type *)
	Standard* = {}; Class* = {5}; Vendor* = {6};

	(** Request recipient *)
	Device* = {}; Interface* = {0}; Endpoint* = {1}; Other* = {0,1};

	(** Pipe modes *)

	Normal* = 0;			(** Poll pipe status and Yield() if transfer still in progress, slow transfers & less CPU time needed *)
	MaxPerformance* = 1;	(** Poll pipe status continuously, fast transfers & more CPU time needed *)
	MinCpu* = 2;			(** Don't poll pipe status - use interrupt notification, slow transfers & minimal CPU workload *)

тип

	Name* = Plugins.Name;
	Description* = Plugins.Description;

	Buffer* = (*ARRAY OF CHAR;*) UsbBuffers.Buffer;
	BufferPtr* = (*POINTER TO Buffer;*) UsbBuffers.BufferPtr;

	Status* = цел32;

тип

	(** Consider all fields of all descriptors as read-only! *)

	DeviceDescriptor* = укль на запись
		bcdUSB* : цел32;
		bDeviceClass* : цел32;
		bDeviceSubClass* : цел32;
		bDeviceProtocol* : цел32;
		idVendor* : цел32;
		idProduct* : цел32;
		bcdDevice* : цел32;
		bNumConfigurations* : цел32;
	кон;

	(** 	A configuration is a set of interfaces. *)
	ConfigurationDescriptor* = укль на запись
		bNumInterfaces* : цел32;
		bConfigurationValue* : цел32;
		interfaces* : укль на массив из InterfaceDescriptor;
		iads* : Iads; (* Optional Interface Association Descriptors *)
		unknown* : UnknownDescriptor; (* Optional Class-specific descriptors *)
	кон;

	(**	An interface is a set of endpoints. Device driver are typically bound to interfaces *)
	InterfaceDescriptor* = укль на запись
		bInterfaceNumber* : цел32;
		bAlternateSetting* : цел32;
		bNumEndpoints* : цел32;
		bInterfaceClass* : цел32;
		bInterfaceSubClass* : цел32;
		bInterfaceProtocol* : цел32;
		numAlternateInterfaces* : цел32;
		alternateInterfaces*: укль на массив из InterfaceDescriptor;
		endpoints* : укль на массив из EndpointDescriptor;
		unknown* : UnknownDescriptor;
	кон;

	(**	Descriptor of a logical communication endpoint. USB pipe can be allocated for all endpoints using the endpoint
		address field *)
	EndpointDescriptor* = укль на запись
		type* : цел32; (* Control, BulkIn, BulkOut, InterruptIn, InterruptOut, IsochronousIn or IsochronousOut *)
		bEndpointAddress* : цел32;
		wMaxPacketSize* : цел32;
		bmAttributes* : мнвоНаБитахМЗ;
		unknown* : UnknownDescriptor;
	кон;

	(**	An optional Interface Association Descriptor describes a set of associated interfaces which should be handled by
		a single device driver. *)
	InterfaceAssociationDescriptor* = укль на запись;
		bFirstInterface* : цел32;
		bInterfaceCount* : цел32;
		bFunctionClass* : цел32;
		bFunctionSubClass* : цел32;
		bFunctionProtocol* : цел32;
	кон;

	(** Optional non-USB-Standard descriptors.
		Linked list of optional descriptors contained in the configuration descriptor but not specified in the USB specification.
		UnknownDescriptors can be located at configuration, interface or endpoint level *)
	UnknownDescriptor* = укль на запись;
		bLength* : цел32;
		bDescriptorType* : цел32;
		descriptor* : BufferPtr; (* contains bLength and bDescriptorType *)
		next* : UnknownDescriptor;
	кон;

тип

	(** USB device driver base class *)
	Driver* = окласс(Plugins.Plugin)
	перем
		(** The fields below will be initialized by the USB driver before the driver's Connect procedure is called *)

		(** Provides access to USB pipes and all the devices' descriptors *)
		device* : UsbDevice;

		(** Most often, a device driver is bound to one interface of the device. This is its descriptor. *)
		interface* : InterfaceDescriptor;

		(** This procedure is called by the USB driver when this object has been returned by the probe() procedure *)

		проц Connect*() : булево;
		нач СТОП(301); возврат ложь; кон Connect; (* abstract *)

		(** This procedure is called by the USB driver when the device is detached. Note that the allocated pipes will be freed automatically *)

		проц Disconnect*;
		нач СТОП(301); кон Disconnect; (* abstract *)

	кон Driver;

тип

	Configurations* = укль на массив из ConfigurationDescriptor;

	Iads* = укль на массив из InterfaceAssociationDescriptor;

тип

	UsbDevice* = окласс
	перем
		(** Consider all fields of this object as read-only!! *)

		(** Device descriptor *)
		descriptor* : DeviceDescriptor;

		(** Device configurations *)
		configurations* : Configurations;

		(** Currently selected device configuration *)
		actConfiguration* : ConfigurationDescriptor;

		(** Device state *)
		state* : цел32;

		(** Direct access to the Default Control Pipe (endpoint zero). Concurrent access is allowed here. *)

		проц Request*(bmRequestType : мнвоНаБитахМЗ;  bRequest, wValue, wIndex, wLength : цел32; перем buffer : Buffer) : Status;
		нач СТОП(301); возврат Error; кон Request; (* abstract *)

		(** Allocate a pipe for the specified USB device endpoint *)

		проц GetPipe*(endpoint : цел32) : Pipe;
		нач СТОП(301); возврат НУЛЬ; кон GetPipe; (* abstract *)

		(**	Deallocate the ressources associtated with the specified pipe.
			Note: This is done automatically for device drivers after their Disconnect procedure has been called. *)

		проц FreePipe*(pipe : Pipe);
		нач СТОП(301); кон FreePipe; (* abstract *)

	кон UsbDevice;

тип

	(** Handler that can be installed for a pipe and will be called when the USB transfer has been processed. *)
	CompletionHandler* = проц {делегат} (status : Status; actLen : цел32);

	(**	USB Communication Pipe
	 	USB communication happens between buffers provided by client software and USB device endpoints. The association between
	 	a client software buffer and a specific USB device endpoint is called pipe. *)
	Pipe* = окласс
	перем
	 	(**	How many tries should the host controllers retry the USB transaction if it fails. Note that this field is actually depended
	 		on the host controller used. Typically, the allowed range of this value is [0,3], zero meaning infinite number of retries *)
		maxRetries* : цел32;

		(** Maximum packet size supported by endpoint. This field is set by the USB driver and should be considered read-only. *)
		maxPacketSize* : цел32;

		mode* : цел32;

		(** Transfer 'bufferLen' bytes from/to the specified buffer, starting at 'offset' *)

		проц Transfer*(bufferLen, offset : цел32; перем buffer : Buffer) : Status;
		нач СТОП(301); возврат Error; кон Transfer; (* abstract *)

		(** For control transfers (only for Control Pipes) *)

		проц Request*(bmRequestType : мнвоНаБитахМЗ;  bRequest, wValue, wIndex, wLength : цел32; перем buffer : Buffer) : Status;
		нач СТОП(301); возврат Error; кон Request; (* abstract *)

		(** Is the halt feature of the endpoint associated with this pipe set? *)

		проц IsHalted*() : булево;
		нач СТОП(301); возврат ложь; кон IsHalted; (* abstract *)

		(** Clear halt feature of endpoint associated with this pipe. *)

		проц ClearHalt*() : булево;
		нач СТОП(301); возврат ложь; кон ClearHalt; (* abstract *)

		(** Set timeout for transfers for this pipe;  0 = NonBlocking, n = n milliseconds *)

		проц SetTimeout*(timeout : цел32);
		нач СТОП(301); кон SetTimeout; (* abstract *)

		(** Specifiy the completion handler that is called when the USB transfer is processed. *)

		проц SetCompletionHandler*(handler: CompletionHandler);
		нач СТОП(301); кон SetCompletionHandler;

		(** Update and return the status of the current USB transfer.
		 	@param actLen: Number of bytes that have been sent/receive (only valid if status * ResInProgress = {})
		 	@return: Status of the USB transfer *)

		проц GetStatus*(перем actLen : цел32) : Status;
		нач СТОП(301); возврат Error; кон GetStatus; (* abstract *)

		(** Return the actual number of bytes transfered. *)

		проц GetActLen*() : цел32;
		нач СТОП(301); возврат 0; кон GetActLen; (* abstract *)

		(**
		 * Cancel a transaction (transfer or request) that was already scheduled in the pipe.
		 * Fails if the transfer is completed, in progress or under some HCD-specific constraints.
		 * Available only if the pipe is non-blocking.
		 *)
		проц CancelTransaction * (id: цел32): булево;
		нач СТОП(301); возврат ложь
		кон CancelTransaction;

		(**
		 * Set the policy of a periodic (isochronous or interrupt) pipe.
		 * The policy is given as an interval and a size.
		 * The interval is the time between 2 samples in 0.1 microsecond units.
		 * The size is the maximum sample size in bytes.
		 * This procedure returns FALSE if there is not enough bandwidth to accomodate
		 * the specified policy and TRUE otherwise.
		 *)
		проц SetPolicy * (interval, size: цел32): булево;
		нач СТОП(301); возврат ложь
		кон SetPolicy;

		(** Show debug information for this pipe. In detailed mode, the scheduling data structures related to
			this pipe will be shown (QH and TD list) *)

		проц Show*(detailed : булево);
		нач СТОП(301); кон Show; (* abstract *)

	кон Pipe;

тип

	(** The USB driver will call the Probe procedure for each interface of the device *)
	ProbeProc* = проц {делегат} (device : UsbDevice; interface : InterfaceDescriptor) : Driver;

	(** This object manages USB device drivers. All device drivers registered at the driver manager are automatically mapped to
		appropriate USB functions. *)
	DriverManager* = окласс

		(** Add a USB device driver to the internal registry. Driver names have to be unique and no longer than 30 characters (incl. Null-String) *)

		проц Add*(probe : ProbeProc; конст name: Plugins.Name; конст desc: Plugins.Description; priority : цел32);
		нач СТОП(301); кон Add; (* abstract *)

		(** Calls Disconnect of all instances of the driver. All instances are removed from the usbDrivers registry
			and the device driver is removed from the internal registry  *)

		проц Remove*(конст name : Plugins.Name);
		нач СТОП(301); кон Remove; (* abstract *)

	кон DriverManager;

перем
	(** Can be used if you need to pass an VAR parameter argument which is not used *)
	NoData* : (*ARRAY 1 OF CHAR;*)UsbBuffers.Buffer;

	(** Instantiated by USB driver; consider read-only *)
	drivers* : DriverManager;

нач
	нов(NoData, 1)
кон Usbdi.
