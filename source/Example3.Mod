(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Example3;	(* pjm *)

(*
Alarm clock.
Ref: C.A.R. Hoare, "Monitors: An Operating System Structuring Concept", CACM 17(10), 1974
*)

тип
	Clock* = окласс
		перем now: размерМЗ;

		проц Delay*(n: размерМЗ);
		перем then: размерМЗ;
		нач {единолично}
			then := now + n;
			дождись(then - now >= 0)
		кон Delay;

		проц Tick*;
		нач {единолично}
			увел(now)
		кон Tick;

		проц &Init*;
		нач
			now := 0
		кон Init;

	кон Clock;

кон Example3.
