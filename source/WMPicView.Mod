модуль WMPicView;	(** AUTHOR "tf"; PURPOSE "Open a window with a picture..."; *)

использует
	Commands, Потоки, Strings, Codecs, Raster, Files,
	WMRectangles, WMGraphics, WM := WMWindowManager;

проц Open*(context : Commands.Context); (** filename ~ *)
перем
	fn, name : Files.FileName;
	pw : WM.BufferWindow;
	res: целМЗ; w, h : размерМЗ; x : цел32;
	dec : Codecs.ImageDecoder;
	rdr : Потоки.Чтец;
	ext : массив 16 из симв8;
	icon : WMGraphics.Image;
	canvas : WMGraphics.BufferCanvas;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fn);
	Strings.Trim(fn, '"');
	Strings.GetExtension(fn, name, ext);
	Strings.UpperCase(ext);
	dec := Codecs.GetImageDecoder(ext);
	если dec = НУЛЬ то
		context.error.пСтроку8("WMPicView: No decoder found for "); context.error.пСтроку8(ext); context.error.пВК_ПС;
		возврат
	всё;
	rdr := Codecs.OpenInputStream(fn);
	если rdr # НУЛЬ то
		dec.Open(rdr, res);
		если res = 0 то
			dec.GetImageInfo(w, h, x, x);
			нов(pw, w, h, истина);
			dec.Render(pw.img);
			нов(icon);
			pw.SetTitle(Strings.NewString(fn));
			Raster.Create(icon, 64, 64, Raster.BGRA8888);
			нов(canvas, icon);
			canvas.ScaleImage(pw.img,
				WMRectangles.MakeRect(0, 0, pw.img.width, pw.img.height),
				WMRectangles.MakeRect(0, 0, 64, 64),
				WMGraphics.ModeCopy, WMGraphics.ScaleBilinear);
			pw.SetIcon(icon);
			WM.DefaultAddWindow(pw);
		иначе
			context.error.пСтроку8("WMPicView: Could not open decoder for file "); context.error.пСтроку8(fn); context.error.пВК_ПС;
		всё;
	иначе
		context.error.пСтроку8("WMPicView: Could not open inputstream for file "); context.error.пСтроку8(fn); context.error.пВК_ПС;
	всё;
кон Open;

проц Convert*(context : Commands.Context);
перем fni, fno,
	inName, outName : Files.FileName;
	res : целМЗ;
	dec : Codecs.ImageDecoder;
	enc : Codecs.ImageEncoder;
	img : Raster.Image;
	rdr : Потоки.Чтец;
	outX, inX : массив 16 из симв8;
	f : Files.File;
	w : Files.Writer;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fni);
	Strings.GetExtension(fni, inName, inX);
	Strings.UpperCase(inX);

	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(fno);
	Strings.GetExtension(fno, outName, outX);
	Strings.UpperCase(outX);

	dec := Codecs.GetImageDecoder(inX);
	если dec = НУЛЬ то
		context.error.пСтроку8("No decoder found for "); context.error.пСтроку8(inX); context.error.пВК_ПС;
		возврат
	всё;

	enc := Codecs.GetImageEncoder(outX);
	если enc = НУЛЬ то
		context.error.пСтроку8("No encoder found for "); context.error.пСтроку8(outX); context.error.пВК_ПС;
		возврат
	всё;

	rdr := Codecs.OpenInputStream(fni);
	если rdr # НУЛЬ то
		dec.Open(rdr, res);
		если res = 0 то
			dec.GetNativeImage(img);

			f := Files.New(fno);
			Files.OpenWriter(w, f, 0);
			enc.Open(w);
			enc.WriteImage(img, res);
			если res = 0 то
				Files.Register(f);
				context.out.пСтроку8("Done");
			иначе
				context.out.пСтроку8("Not converted");
			всё
		всё
	всё;
кон Convert;

кон WMPicView.

System.Free WMPicView ~
WMPicView.Open  	errorpos.png ~
WMPicView.Convert BluebottlePic0.png temp.gif ~
