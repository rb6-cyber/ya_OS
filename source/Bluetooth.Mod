модуль Bluetooth;	(** AUTHOR "be"; PURPOSE "Core Bluetooth types/functions"; *)

использует
	Objects, Потоки;

(**---- general Bluetooth types ----*)

конст
	(** Result codes. res > 0: command specific error *)
	Ok* = 0;
	ErrTimeout* = -1;
	ErrInvalidPacket* = -2;
	ErrInvalidEvent* = -3;
	ErrInvalidParameters* = -4;
	ErrSendError* = -5;

	BDAddrLen* = 6;	(** length of Bluetooth device address *)
	DeviceClassLen* = 3;	(** length of Bluetooth class of device *)

тип
	BDAddr* = массив BDAddrLen из симв8;	(** Bluetooth device address *)
	DeviceClass* = массив DeviceClassLen из симв8;	(** Bluetooth class of device *)

(**---- HCI packet queue ----*)
конст
	(** queue types *)
	Default* = 0;	(** default queue *)
	Command* = 1;	(** command queue *)
	ACL* = 2;	(** ACL data packet queue *)
	SCO* = 3;	(** SCO data packet queue *)
	Event* = 4;	(** HCI event queue *)
	Error* = 5;	(** error queue *)
	Negotiation* = 6;	(** negotiation queue *)
	NumQueues = 7;

	MaxACLDataLen* = 256;
	MaxSCODataLen* = 256;
	MaxEventParamLen* = 256;
	MaxUnknownDataLen* = 256;
	MaxLen* = 256;

тип
	Packet* = укль на запись	(** generic packet type *)
		next: Packet
	кон;

	ACLPacket* = укль на запись(Packet)	(** ACL packet, see specs chapter 4.4.3 *)
		handle*,	(** connection handle *)
		PB*, 	(** packet boundary flag *)
		BC*, 	(** broadcast flag *)
		len*: цел32;	(** length of data, in bytes *)
		data*: массив MaxACLDataLen из симв8	(** data *)
	кон;

	SCOPacket* = укль на запись(Packet)	(** SCO packet, see specs chapter 4.4.3 *)
		handle*,	(** connection handle *)
		len*: цел32;	(** length of data, in bytes *)
		data*: массив MaxSCODataLen из симв8	(** data *)
	кон;

	EventPacket* = укль на запись(Packet)	(** HCI event packet, see specs chapter 4.4.2 *)
		code*: симв8;	(** event code *)
		paramLen*: цел32;	(** parameter length, in bytes *)
		params*: массив MaxEventParamLen из симв8	(** parameter values *)
	кон;

	UnknownPacket* = укль на запись(Packet) (** unknown packet...should not happen ;-) *)
		len*: цел32;	(** length of data, in bytes *)
		data*: массив MaxUnknownDataLen из симв8	(** data *)
	кон;

	(** packet filter/notifier: the filter is called first and should return quickly. If it returns TRUE the
		correspoding notifier will be called *)
	PacketFilter* = проц{делегат} (packet: Packet): булево;
	PacketNotify* = проц{делегат} (packet: Packet);

	Filter = укль на запись
		filter: PacketFilter;
		notify: PacketNotify;
		next: Filter
	кон;

	(** used if we need to know which timer has expired *)
	IDTimer* = окласс
		перем
			t: Objects.Timer;
			handler: IDTimerHandler;

		проц &Init*(handler: IDTimerHandler; timeout: цел32);
		нач
			сам.handler := handler; нов(t);
			Objects.SetTimeout(t, TimeoutHandler, timeout)
		кон Init;

		проц Cancel*;
		нач {единолично} Objects.CancelTimeout(t)
		кон Cancel;

		проц TimeoutHandler;
		нач {единолично} handler(сам)
		кон TimeoutHandler;
	кон IDTimer;

	IDTimerHandler* = проц {делегат} (sender: IDTimer);

	(** packet queue *)
	Queue* = окласс
		перем
			head, tail: Packet;
			filters: Filter;
			dead: булево;
			expired: IDTimer;
			getNext: Packet;
			inGetNext: цел32;

		проц &Init*;
		нач
			inGetNext := 0; dead := ложь;
			нов(filters)	(* dummy head *)
		кон Init;

		(** closes a queue and aborts any pending 'Get' requests *)
		проц Close*;
		нач {единолично}
			dead := истина
		кон Close;

		(** clears the queue *)
		проц Clear*;
		нач {единолично}
			head := НУЛЬ; tail := НУЛЬ
		кон Clear;

		(** add a packet to the queue *)
		проц Add*(packet: Packet);
		нач
			если ~CheckPacketFilters(packet) то	(* packet filters are priorized *)
				нач {единолично}
					если (tail # НУЛЬ) то tail.next := packet; tail := packet
					иначе head := packet; tail := packet
					всё
				кон
			всё
		кон Add;

		проц HandleTimeout(sender: IDTimer);
		нач {единолично} expired := sender
		кон HandleTimeout;

		(** blocks until a HCI packet is available or a timeout occurs. Packet filters are priorized over the Get request *)
		проц Get*(перем p: Packet; timeout: цел32; перем res: целМЗ);
		перем timer: IDTimer;
		нач {единолично}
			если (head = НУЛЬ) то
				нов(timer, HandleTimeout, timeout);
				дождись((head # НУЛЬ) или (expired = timer) или dead);
				если (expired # timer) то timer.Cancel всё
			всё;
			если (head # НУЛЬ) то
				p := head; head := head.next;
				если (head = НУЛЬ) то tail := НУЛЬ всё;
				p.next := НУЛЬ; res := 0
			иначе
				p := НУЛЬ; res := ErrTimeout
			всё
		кон Get;

		(** blocks until the next HCI packet is available or a timeout occurs. Packet filters are priorized over the
			GetNext request.
		*)
		проц GetNextFilter(p: Packet): булево;
		нач
			возврат истина
		кон GetNextFilter;

		проц GetNextHandler(p: Packet);
		нач
			getNext := p
		кон GetNextHandler;

		(* naaa...won't work. besser: filter rein, der alles frisst, dann wieder rausnehmen *)
		проц GetNext*(перем p: Packet; timeout: цел32; перем res: целМЗ);
		перем f: Filter; timer: IDTimer;
		нач {единолично}
			(* lock *)
			дождись(inGetNext = 0); увел(inGetNext);

			getNext := НУЛЬ;

			(* plug-in greedy filter *)
			нов(f); f.filter := GetNextFilter; f.notify := GetNextHandler;
			f.next := filters.next; filters.next := f;

			нов(timer, HandleTimeout, timeout);
			дождись((getNext # НУЛЬ) или (expired = timer) или dead);

			(* remove greedy filter *)
			filters.next := f.next;

			если (getNext # НУЛЬ) то p := getNext; res := 0
			иначе p := НУЛЬ; res := ErrTimeout
			всё;

			(* unlock *)
			умень(inGetNext)
		кон GetNext;

		(** registers a packet filter/handler. Multiple filters/handlers may be registered *)
		проц RegisterPacketFilter*(filter: PacketFilter; notify: PacketNotify);
		перем f: Filter;
		нач {единолично}
			нов(f); f.filter := filter; f.notify := notify;
			f.next := filters.next; filters.next := f
		кон RegisterPacketFilter;

		(** removes a registered filter/handler. *)
		проц UnregisterPacketFilter*(notify: PacketNotify);
		перем p,q: Filter;
		нач {единолично}
			q := filters.next; p := filters;
			нцПока (q # НУЛЬ) делай
				если (q.notify = notify) то
					p.next := q.next
				всё;
				q := q.next
			кц
		кон UnregisterPacketFilter;

		(* checks if a packet filter/handler wants to handle the packet *)
		проц CheckPacketFilters(packet: Packet): булево;
		перем f: Filter; notify: PacketNotify; res: булево;
		нач
			res := ложь;
			нач {единолично}
				notify := НУЛЬ;
				f := filters.next;
				нцПока (f # НУЛЬ) делай
					если f.filter(packet) то res := истина; notify := f.notify; f := НУЛЬ
					иначе f := f.next
					всё
				кц
			кон;
			если (notify # НУЛЬ) то notify(packet) всё;
			возврат res
		кон CheckPacketFilters;
	кон Queue;

	(**---- abstract transport layer ----*)
	TransportLayer* = окласс
		перем
			name-: массив 32 из симв8;
			out*: Потоки.Писарь;
			in*: Потоки.Чтец;
			sink-: массив NumQueues из Queue;

		проц &Init*(name: массив из симв8; sender: Потоки.Делегат˛реализующийЗаписьВПоток; receiver: Потоки.Делегат˛реализующийЧтениеИзПотока);
		перем q: Queue;
		нач
			копируйСтрокуДо0(name, сам.name);
			нов(q); sink[Default] := q	(* install default queue *)
		кон Init;

		(** close the transport layer *)
		проц Close*;
		кон Close;

		(** install a queue for certain HCI packet types *)
		проц SetSink*(type: цел32; queue: Queue);
		нач {единолично}
			sink[type] := queue
		кон SetSink;

		(** get the queue for certain HCI packet types *)
		проц GetSink*(type: цел32): Queue;
		нач {единолично}
			возврат sink[type]
		кон GetSink;

		(** send a HCI packet *)
		проц Send*(type: цел32; перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		нач
			СТОП(301)
		кон Send;

		проц Send1H*(type: цел32; перем hdr: массив из симв8; hdrlen: цел32; перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		нач
			СТОП(301)
		кон Send1H;

		проц Send2H*(type: цел32; перем hdr1: массив из симв8; hdr1len: цел32;
								перем hdr2: массив из симв8; hdr2len: цел32;
								перем data: массив из симв8; ofs, len: цел32; перем res: целМЗ);
		нач
			СТОП(301)
		кон Send2H;
	кон TransportLayer;

(** transforms 'character string' into an array of char.
	string = char { " " char } 0X.
	char = hexdigit hexdigit.
	hexdigit = "0"|..|"9"|"A"|..|"F".
*)
проц StringToParam*(string: массив из симв8; перем param: массив из симв8; перем len: цел32);
перем i, h, l: цел32; error: булево;

	проц Value(c: симв8): цел32;
	нач
		если ("0" <= c) и (c <= "9") то возврат кодСимв8(c)-кодСимв8("0")
		иначе
			c := ASCII_вЗаглавную(c);
			если ("A" <= c) и (c <= "F") то возврат кодСимв8(c)-кодСимв8("A")+10 всё
		всё;
		возврат -1
	кон Value;

нач
	i := 0; len := 0; error := ложь;
	нцПока ~error и (string[i] # 0X) делай
		h := Value(string[i]); l := Value(string[i+1]);
		если (h # -1) и (l # -1) то
			param[len] := симв8ИзКода(h*10H+l); увел(len);
			увел(i, 2);
			если (string[i] # 0X) то
				если (string[i] = " ") то увел(i)
				иначе error := истина; len := 0
				всё
			всё
		иначе error := истина; len := 0
		всё
	кц;
	param[len] := 0X
кон StringToParam;


проц CharArrayToString*(buf: массив из симв8; ofs, len: цел32; перем string: массив из симв8);
перем i, pos, maxLen: размерМЗ; c: симв8;

	проц Char(v: цел32): симв8;
	нач
		утв((0 <= v) и (v < 10H));
		если (v < 10) то возврат симв8ИзКода(кодСимв8("0") + v)
		иначе возврат симв8ИзКода(кодСимв8("A") + v - 10)
		всё
	кон Char;

нач
	i := 0; pos := 0; maxLen := длинаМассива(string)-1-3;
	нцПока (i < len) и (pos < maxLen) делай
		c := buf[ofs+i];
		string[pos] := Char(кодСимв8(c) DIV 10H); увел(pos);
		string[pos] := Char(кодСимв8(c) остОтДеленияНа 10H); увел(pos);
		string[pos] := " "; увел(pos);
		увел(i)
	кц;
	string[pos] := 0X
кон CharArrayToString;


кон Bluetooth.
