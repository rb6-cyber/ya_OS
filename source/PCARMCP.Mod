модуль PCARMCP;	(** be  **)

использует НИЗКОУР, PCO := PCOARM, PCM, PCBT, ЛогЯдра;

конст
	Trace = ложь;

	ErrInternalError* = 100;
	ErrConstantNotRegistered* = 101;
	ErrAddressNotRegistered* = 102;

	FlushThreshold = 80H;

тип
	UseList = окласс
		перем
			pc: цел32;	(* where the element is used *)
			next: UseList;

		проц &Init*(pc: цел32);
		нач сам.pc := pc
		кон Init;
	кон UseList;

	Element = окласс
		перем
			pc: цел32;	(* where this element is located in the code, -1 if unknown *)
			firstUse:цел32;	(* where this element's first use is located in the code. *)
			next: Element;
			uses: UseList;

		проц &InitElement*;
		нач pc := -1
		кон InitElement;
	кон Element;

	Constant = окласс(Element)
		перем
			value: цел32;

		проц &Init*(value: цел32);
		нач InitElement; сам.value := value
		кон Init;
	кон Constant;

	Address = окласс(Element)
		перем
			adr: PCM.Attribute;

		проц &Init*(adr: PCM.Attribute);
		нач InitElement; сам.adr := adr
		кон Init;
	кон Address;

	ConstantPool* = окласс
		перем items: Element;
			limitPC: цел32;	(* constant pool must be flushed the latest at this pc *)

		(* Init - constructor *)
		проц &Init*;
		нач PCO.SetConstantPoolBarrierCallback(FlushCallback); limitPC := -1
		кон Init;

		(* Insert - inserts the element 'i' at the correct position in the linked list *)
		проц Insert(i: Element);
		перем p,c: Element;
		нач
			c := items; p := НУЛЬ;
			нцПока (c # НУЛЬ) и (c.firstUse < i.firstUse) делай p := c; c := c.next кц;
			если (p = НУЛЬ) то
				i.next := c; items := i
			иначе
				i.next := p.next; p.next := i
			всё
		кон Insert;

		проц AddConstant*(pc, c: цел32): цел32;
		перем i, p: Element; cnst: Constant; use: UseList;
		нач { единолично }
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Adding constant "); ЛогЯдра.пЦел64(c, 0); ЛогЯдра.пСтроку8(" @ "); ЛогЯдра.пЦел64(pc, 0);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			i := items; p := НУЛЬ;
			нцПока (i # НУЛЬ) и (~(i суть Constant) или (i(Constant).value # c)) делай p := i; i := i.next кц;
			если (i = НУЛЬ) то
				нов(cnst, c); i := cnst; i.firstUse := pc;
				Insert(i)
				(*
				IF (last = NIL) THEN items := cnst; last := cnst
				ELSE last.next := cnst; last := cnst
				END
				*)
			аесли (i.firstUse > pc) то
				i.firstUse := pc;
				если ((p # НУЛЬ) и (p.firstUse > i.firstUse)) или ((i.next # НУЛЬ) и (i.next.firstUse < i.firstUse)) то
					если (p # НУЛЬ) то p.next := i.next
					иначе items := i.next
					всё;
					Insert(i)
				всё
			всё;
			если (i.pc # -1) то (* already stored somewhere *)
				если (pc + 8 - i.pc < 1000H) то возврат i.pc - pc - 8
				иначе i.pc := -1 (* we need a new location *)
				всё
			всё;
			нов(use, pc); use.next := i.uses; i.uses := use;
			если (limitPC = -1) то
				limitPC := pc + 1000H - 2*PCO.InstructionSize - FlushThreshold; (* FFFh is max offset, aligned word access at FFCh minus one branch *)
				PCO.SetConstantPoolBarrier(limitPC)
			всё;
			возврат 0
		кон AddConstant;

		проц AddAddress*(pc: цел32; adr: PCM.Attribute): цел32;
		перем i, p: Element; address: Address; use: UseList;
		нач { единолично }
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование;
				ЛогЯдра.пСтроку8("Adding address "); ЛогЯдра.п16ричное(адресОт(adr^), 8); ЛогЯдра.пСтроку8(" @ "); ЛогЯдра.пЦел64(pc, 0);
				ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			i := items;
			нцПока (i # НУЛЬ) и (~(i суть Address) или (i(Address).adr # adr)) делай p := i; i := i.next кц;
			если (i = НУЛЬ) то
				нов(address, adr); i := address; i.firstUse := pc;
				Insert(i)
			аесли (i.firstUse > pc) то
				i.firstUse := pc;
				если ((p # НУЛЬ) и (p.firstUse > i.firstUse)) или ((i.next # НУЛЬ) и (i.next.firstUse < i.firstUse)) то
					если (p # НУЛЬ) то p.next := i.next
					иначе items := i.next
					всё;
					Insert(i)
				всё
			всё;
			если (i.pc # -1) то (* already stored somewhere *)
				если (pc + 8 - i.pc < 1000H) то возврат i.pc - pc - 8
				иначе i.pc := -1 (* we need a new location *)
				всё
			всё;
			(* 08.05.02: interface changed *)
			если (adr суть PCBT.GlobalVariable) то
				(*
				WITH adr: PCBT.GlobalVariable DO
					l := adr.link;
					IF (l = NIL) OR (l.offset # -1) THEN (* only add an entry to the fixup list if needed *)
						(* adr.link is a linked list that contains all locations where the address of this global variable needs to be
							fixed by the loader. If adr.link = NIL, this is the first time the address of the global is used and we have
							to add it to the list. If adr.link.offset # -1, then the address is already stored somewhere, but we can't
							use it (because it's too far away from the load instruction); i.e. we'll get a new location and consequently
							need a new fixup link.
							cf. Flush
						*)
						NEW(l); l.offset := -1; l.next := adr.link; adr.link := l
					END;
				END
				*)
			аесли (adr суть PCBT.Procedure) то
				(* nothing to do *)
				(*IF adr.imported THEN adr.fixlist := -1 (* TODO (= adr of constant in code) *)
				ELSE PCBT.context.syscalls[PCBT.procaddr] := 0 (* TODO *)
				END;
				HALT(ErrInternalError) (* TODO *)*)
			иначе Error(pc, "AddAddress: unknown 'adr' type")
			всё;
			нов(use, pc); use.next := i.uses; i.uses := use;
			если (limitPC = -1) то
				limitPC := pc + 1000H - 2*PCO.InstructionSize - FlushThreshold; (* FFFh is max offset, aligned word access at FFCh minus one branch *)
				PCO.SetConstantPoolBarrier(limitPC)
			всё;
			возврат 0
		кон AddAddress;

		проц Flush*(pc: цел32);
		перем i: Element; u: UseList; adr: PCM.Attribute; cnt: цел32;
		нач
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.пСтроку8("Flushing Constant Pool..."); ЛогЯдра.пВК_ПС
			всё;
			i := items;
			нцПока (i # НУЛЬ) делай
				i.firstUse := матМаксимум(цел32); (* reset firstUse field *)
				если (i.uses # НУЛЬ) и (i.pc = -1) то
					увел(cnt);
					если Trace то
						если (i суть Constant) то ЛогЯдра.пСтроку8("  constant (value = "); ЛогЯдра.пЦел64(i(Constant).value, 0)
						иначе ЛогЯдра.пСтроку8("  address (id = "); ЛогЯдра.п16ричное(адресОт(i(Address).adr^), 8)
						всё;
						ЛогЯдра.пСтроку8(");  pc = ")
					всё;
					i.pc := PCO.GetCodePos();
					если (i суть Constant) то PCO.DCD(i(Constant).value)
					иначе
						adr := i(Address).adr;
						если (adr суть PCBT.GlobalVariable) то	(* fix offsets in adr.link structure *)
							просейТип adr: PCBT.GlobalVariable делай
								PCO.DCD(adr.offset);
								(* 08.05.02: interface changed
								IF (adr.link = NIL) THEN Error(pc, "Flush: 'adr.link' is NIL") END;
								IF (adr.link.offset = -1) THEN adr.link.offset := i.pc DIV 4 END
								*)
								PCBT.context.UseVariable(adr, i.pc DIV 4)
							всё
						аесли (adr суть PCBT.Procedure) то
							просейТип adr: PCBT.Procedure делай
								(*
								IF adr.imported THEN
									PCO.DCD(adr.fixlist*10000H);
									adr.fixlist := i.pc DIV 4
								ELSE
									(* local procedure variables: fixup-list located in the code *)
									IF (adr.next = NIL) & (PCBT.context.lastEntry # adr) THEN PCBT.context.NewEntry(adr) END;
									PCO.DCD(PCBT.context.syscalls[PCBT.procaddr]*10000H + adr.entryNr);
									PCBT.context.syscalls[PCBT.procaddr] := i.pc DIV 4
								END
								*)
								PCO.DCD(0);
								PCBT.context.UseProcedure(adr, i.pc DIV 4)
							всё
						иначе Error(pc, "Flush: unknown 'adr' type")
						всё;
					всё;
					если Trace то
						ЛогЯдра.п16ричное(i.pc, 8); ЛогЯдра.пВК_ПС;
						ЛогЯдра.пСтроку8("  fixing references at pos: ")
					всё;
					u := i.uses;
					нцПока (u # НУЛЬ) делай
						если Trace то ЛогЯдра.пЦел64(u.pc, 5) всё;
						PCO.FixLoad(u.pc, i.pc - (u.pc + 8));
						u := u.next
					кц;
					если Trace то ЛогЯдра.пВК_ПС всё;
					i.uses := НУЛЬ
				всё;
				i := i.next
			кц;
			limitPC := -1;
			PCO.SetConstantPoolBarrier(limitPC);
			если Trace то ЛогЯдра.пСтроку8(" # of addresses/constants flushed: "); ЛогЯдра.пЦел64(cnt, 0); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования всё
		кон Flush;

		(* FlushCallback - callback handler called by PCOARM *)
		проц FlushCallback(pc: цел32);
		нач
			если Trace то
				ЛогЯдра.ЗахватВЕдиноличноеПользование; ЛогЯдра.п16ричное(pc, 8); ЛогЯдра.пСтроку8(": Constant Pool: Flush callback called"); ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования
			всё;
			PCO.B(PCO.AL, 0); (* branch target still unknown, will be fixed after the Flush *)
			Flush(pc);
			PCO.FixJump(pc, (PCO.GetCodePos() - (pc + 8)) DIV 4)
		кон FlushCallback;

		проц Error(pc: цел32; конст msg: массив из симв8);
		нач
			ЛогЯдра.ЗахватВЕдиноличноеПользование;
			ЛогЯдра.пСтроку8("ConstantPool Error @ pc = "); ЛогЯдра.п16ричное(pc, 8); ЛогЯдра.пСтроку8("h: ");
			ЛогЯдра.пСтроку8(msg);
			ЛогЯдра.ОсвобождениеИзЕдиноличногоПользования;
			СТОП(ErrInternalError);
		кон Error;
	кон ConstantPool;

кон PCARMCP.
