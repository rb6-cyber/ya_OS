(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль WebHTTPServerTools;	(** AUTHOR "be"; PURPOSE "User interface to HTTPServer"; *)

использует Commands, WebHTTPServer;

конст
	ModuleName = "WebHTTPServerTools";

проц Start*(context : Commands.Context); (** ["\r:" default root directory] ["\l:" log file] *)
перем c, opt: симв8; str, root, log, tls: массив 1024 из симв8; msg : массив 128 из симв8; res : целМЗ;
нач
	root := ""; log := "";
	context.arg.ПропустиБелоеПоле;
	нц
		c := context.arg.чИДайСимв8();
		если (c # "\") то прервиЦикл всё;

		opt := ASCII_вЗаглавную(context.arg.чИДайСимв8());
		c := context.arg.чИДайСимв8();
		если (c # ":") то прервиЦикл всё;

		context.arg.ПропустиБелоеПоле;
		context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках( str);
		context.arg.ПропустиБелоеПоле;

		просей opt из
		| "R": копируйСтрокуДо0(str, root)
		| "L": копируйСтрокуДо0(str, log)
		| "S": копируйСтрокуДо0(str, tls)
		иначе прервиЦикл
		всё
	кц;

	если tls = "on"  то
		WebHTTPServer.StartHTTPS(root, log, msg, res);
		если (res # WebHTTPServer.Ok) то
			context.error.пСтроку8("Could not start HTTPS server, res: "); context.error.пЦел64(res, 0);
			context.error.пСтроку8(" ("); context.error.пСтроку8(msg); context.error.пСтроку8(")"); context.error.пВК_ПС;
		всё;
	всё;
	WebHTTPServer.StartHTTP(root, log, msg, res);
	если (res # WebHTTPServer.Ok) то
		context.error.пСтроку8("Could not start HTTP server, res: "); context.error.пЦел64(res, 0);
		context.error.пСтроку8(" ("); context.error.пСтроку8(msg); context.error.пСтроку8(")"); context.error.пВК_ПС;
	иначе
		context.out.пСтроку8("HTTP server started. Default root directory = '"); context.out.пСтроку8(root); context.out.пСтроку8("'; logging ");
		если (log = "") то context.out.пСтроку8("disabled")
		иначе context.out.пСтроку8("to '"); context.out.пСтроку8(log); context.out.пСимв8("'")
		всё;
		context.out.пВК_ПС;
	всё;
кон Start;

проц Stop*(context : Commands.Context);
перем msg : массив 128 из симв8; res : целМЗ; stopped : булево;
нач
	WebHTTPServer.StopHTTP(msg, res); stopped := (res = WebHTTPServer.Ok);
	WebHTTPServer.StopHTTPS(msg, res); stopped := stopped или (res = WebHTTPServer.Ok);
	если stopped то
		context.out.пСтроку8("HTTP/HTTPS Server stopped."); context.out.пВК_ПС;
	иначе
		context.out.пСтроку8("HTTP/HTTPS server is not running."); context.out.пВК_ПС;
	всё;
кон Stop;

проц AddHost*(context : Commands.Context); (** host ["\r:" root directory] ["\d:" default file] ["\e:" error file] *)
перем c, opt: симв8; str: массив 256 из симв8; host: WebHTTPServer.Host;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str) то
		если (WebHTTPServer.FindHosts(str) = НУЛЬ) то
			нов(host, str);
			нц
				context.arg.ПропустиБелоеПоле;
				c := context.arg.чИДайСимв8();
				если (c # "\") то прервиЦикл всё;

				opt := ASCII_вЗаглавную(context.arg.чИДайСимв8());
				c := context.arg.чИДайСимв8();
				если (c # ":") то прервиЦикл всё;

				context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(str);

				просей opt из
				| "R": host.SetPrefix(str)
				| "D": host.SetDefault(str)
				| "E": host.SetError(str)
				иначе прервиЦикл
				всё
			кц;

			WebHTTPServer.AddHost(host);
			context.out.пСтроку8("Added host '"); context.out.пСтроку8(host.name); context.out.пСтроку8("'; root = '"); context.out.пСтроку8(host.prefix);
			context.out.пСтроку8("'; default = '"); context.out.пСтроку8(host.default); context.out.пСтроку8("'; error = '"); context.out.пСтроку8(host.error); context.out.пСимв8("'")
		иначе
			context.out.пСтроку8(ModuleName); context.out.пСтроку8(".AddHost: host '"); context.out.пСтроку8(str); context.out.пСтроку8("' already present.")
		всё
	иначе
		context.error.пСтроку8(ModuleName); context.error.пСтроку8('.AddHost: expected parameters: host ["\r:" root directory] ["\d:" default file] ["\e:" error file]');
	всё;
	context.error.пВК_ПС;
кон AddHost;

проц RemoveHost*(context : Commands.Context); (** host *)
перем hostname: массив 256 из симв8; res : целМЗ;
нач
	если context.arg.ПропустиБелоеПолеИЧитайЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(hostname) то
		WebHTTPServer.RemoveHost(hostname, res);
		если (res = WebHTTPServer.Ok) то
			context.out.пСтроку8("Host '"); context.out.пСтроку8(hostname); context.out.пСтроку8("' removed."); context.out.пВК_ПС;
		иначе
			context.error.пСтроку8("Host '"); context.error.пСтроку8(hostname); context.error.пСтроку8("' not found."); context.error.пВК_ПС;
		всё;
	иначе
		context.error.пСтроку8(ModuleName); context.error.пСтроку8(".RemoveHost: expected parameters: host");
		context.error.пВК_ПС;
	всё;
кон RemoveHost;

проц ListHosts*(context : Commands.Context);
нач
	context.out.пСтроку8("Virtual hosts:"); context.out.пВК_ПС;
	WebHTTPServer.ShowHosts(context.out);
кон ListHosts;

кон WebHTTPServerTools.

System.Free WebHTTPServerTools~
System.FreeDownTo TCP ~

WebHTTPServerTools.Start \r:E:/Test/ ~

WebHTTPServerTools.Start \r:httproot \l:HTTP.Log ~
WebHTTPServerTools.Start \r:httproot \l:HTTP.Log \s:on ~
WebHTTPServerTools.Stop

WebHTTPServerTools.AddHost livepc \r:FAT:/httproot/test~
WebHTTPServerTools.RemoveHost livepc.inf.ethz.ch~
WebHTTPServerTools.ListHosts

WebSSMPPlugin.Install ~
WebDefaultSSMP.Install ~
