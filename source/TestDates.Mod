модуль TestDates; (** AUTHOR "staubesv"; PURPOSE "Test for function of Dates.Mod"; *)

использует
	Modules, Потоки, Commands, Dates, Strings, Random;

перем
	stop : булево;
	nofRunningTests : цел32;

проц AddYear(перем time : Dates.DateTime);
нач
	увел(time.year);
кон AddYear;

проц AddMonth(перем time : Dates.DateTime);
нач
	если (time.month = 12) то
		time.month := 1; AddYear(time);
	иначе
		увел(time.month);
	всё;
кон AddMonth;

проц AddDay(перем time : Dates.DateTime);
нач
	если (time.day = Dates.NofDays(time.year, time.month)) то
		time.day := 1; AddMonth(time);
	иначе
		увел(time.day);
	всё;
кон AddDay;

проц AddHour(перем time : Dates.DateTime);
нач
	если (time.hour = 23) то
		time.hour := 0; AddDay(time);
	иначе
		увел(time.hour);
	всё;
кон AddHour;

проц AddMinute(перем time : Dates.DateTime);
нач
	если (time.minute = 59) то
		time.minute := 0; AddHour(time);
	иначе
		увел(time.minute);
	всё;
кон AddMinute;

проц AddSecond(перем time : Dates.DateTime);
нач
	если (time.second = 59) то
		time.second := 0; AddMinute(time);
	иначе
		увел(time.second);
	всё;
кон AddSecond;

проц ToSeconds(days, hours, minutes, seconds : цел32) : цел32;
нач
	возврат days * 86400 + hours * 3600 + minutes * 60 + seconds;
кон ToSeconds;

проц Show(time : Dates.DateTime; out : Потоки.Писарь);
перем string : массив 256 из симв8;
нач
	Strings.DateToStr(time, string); out.пСтроку8(string); out.пСтроку8("  ");
	Strings.TimeToStr(time, string); out.пСтроку8(string);
кон Show;

проц TestTimeDifference*(context : Commands.Context);
перем t1, t2 : Dates.DateTime; diff, days, hours, minutes, seconds, i : цел32;
нач
	IncNofRunningTests;
	t1 := Dates.Now();
	t2 := t1;
	diff := 0;
	нцДля i := 0 до матМаксимум(цел32)-1 делай
		если (i остОтДеленияНа 10000000 = 0) то
			context.out.пЦел64(округлиВниз(100 *  (i / матМаксимум(цел32)) ), 0); context.out.пСтроку8("%, delta = ");
			Strings.ShowTimeDifference(t1, t2, context.out);
			context.out.пСтроку8(" (T1="); Show(t1, context.out); context.out.пСтроку8(", T2="); Show(t2, context.out); context.out.пСтроку8(")");
			context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		всё;
		Dates.TimeDifference(t1, t2, days, hours, minutes, seconds);
		diff := days * 86400 + hours * 3600 + minutes * 60 + seconds;
		если (diff # i) то
			context.out.пСтроку8("ERROR: Should: "); context.out.пЦел64(i, 0);
			context.out.пСтроку8(", but is: "); context.out.пЦел64(diff, 0);
			context.out.пСтроку8(" ( T1 = "); Show(t1, context.out);
			context.out.пСтроку8(", T2 = "); Show(t2, context.out); context.out.пСтроку8(" )");
			context.out.пВК_ПС;
			возврат;
		всё;
		если stop то возврат всё;
		AddSecond(t2);
		утв(Dates.ValidDateTime(t2));
	кц;
	DecNofRunningTests;
кон TestTimeDifference;

проц TestAddX*(context : Commands.Context);
конст MaxDelta = 1000;
перем
	dtRef, dt : Dates.DateTime;
	days, hours, minutes, seconds : цел32;
	random : Random.Generator;
	expectedDifference : цел32;
	value, i : цел32;
нач
	IncNofRunningTests;
	dtRef := Dates.Now();
	нов(random);
	random.InitSeed(dt.second + 100 * dt.minute);
	i := 0;
	нц
		dt := dtRef;
		value := random.Dice(2 * MaxDelta) - MaxDelta;
		expectedDifference := ToSeconds(value, value, value, value);
		если (expectedDifference < 0) то expectedDifference := -expectedDifference; всё;

		Dates.AddSeconds(dt, value);
		Dates.AddMinutes(dt, value);
		Dates.AddHours(dt, value);
		Dates.AddDays(dt, value);

		Dates.TimeDifference(dt, dtRef, days, hours, minutes, seconds);

		если (expectedDifference # ToSeconds(days, hours, minutes, seconds)) то
			context.out.пСтроку8("ERROR: Difference between ");
			Show(dtRef, context.out); context.out.пСтроку8(" and "); Show(dt, context.out);
			context.out.пСтроку8(" is expected as ");
			context.out.пЦел64(expectedDifference, 0); context.out.пСтроку8(", but result is ");
			context.out.пЦел64(ToSeconds(days, hours, minutes, seconds), 0);
			context.out.пВК_ПС;
			возврат;
		всё;

		если (i остОтДеленияНа 1000000 = 0) то
			context.out.пЦел64(i, 0); context.out.пСтроку8(" TestAddX tests performed.");
			context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
		всё;
		увел(i);
		если (i = матМаксимум(цел32)) или stop то прервиЦикл; всё;
	кц;
	DecNofRunningTests;
кон TestAddX;

проц TestToday*(context:Commands.Context);
перем dt: Dates.DateTime;
	year, week, weekday:цел32;
	dayTable: массив 25 из симв8;
нач
	dt:=Dates.Now();
	Dates.WeekDate(dt, year, week, weekday);
	context.out.пЦел64(year,6); context.out.пЦел64(week,6); context.out.пЦел64(weekday,6); context.out.пВК_ПС; context.out.пВК_ПС;
	dayTable := "SunMonTueWedThuFriSatSun";
	context.out.пСимв8(dayTable[weekday*3]); context.out.пСимв8(dayTable[weekday*3+1]); context.out.пСимв8(dayTable[weekday*3+2]); context.out.пВК_ПС; context.out.ПротолкниБуферВПоток;
кон TestToday;


проц IncNofRunningTests;
нач {единолично}
	увел(nofRunningTests);
кон IncNofRunningTests;

проц DecNofRunningTests;
нач {единолично}
	умень(nofRunningTests);
кон DecNofRunningTests;

проц Cleanup;
нач {единолично}
	stop := истина;
	дождись(nofRunningTests = 0);
кон Cleanup;

нач
	nofRunningTests := 0;
	stop := ложь;
	Modules.InstallTermHandler(Cleanup);
кон TestDates.

System.Free TestDates ~

TestDates.TestTimeDifference ~

TestDates.TestAddX ~
TestDates.TestToday ~
