модуль WMTerminator;  (** AUTHOR "GF"; PURPOSE "terminate A2"; *)

использует Modules, WMDialogs, Commands, Kernel;

конст
	Ok = WMDialogs.ResOk;
	Abort = WMDialogs.ResAbort;

проц Shutdown*;
перем res: целМЗ;  msg: массив 128 из симв8;
	t: Kernel.Timer;
нач
	нов( t );
	если  WMDialogs.Message( 1, "A2 terminator", "  Are you sure you want to stop A2 execution ?", {Ok, Abort} ) = Ok то
		Commands.Call( "WMTerminator.Do", {}, res, msg );

		(* in case the termination handler (Modules.Shutdown) hangs, retry!*)
		t.Sleep( 1000 );
		Commands.Call( "WMTerminator.Do", {}, res, msg );
	всё;
кон Shutdown;

проц Do*;
нач
	Modules.Shutdown( Modules.PowerDown );
кон Do;

кон WMTerminator.
