(* Runtime environment for Unix *)
(* Copyright (C) Florian Negele *)

модуль Environment;

использует НИЗКОУР, Activities, Counters, Unix, Processors, Queues, Timer, Трассировка;

конст IsNative* = ложь;

конст Running* = 0; ShuttingDown* = 1; Rebooting* = 2;

перем status* := 0: целМЗ;
перем clock: Timer.Counter;
перем milliseconds: Timer.Counter;
перем sleepingQueue: Queues.Queue;
перем activity: Unix.Key_t;
перем timer: Unix.Thread_t;

проц {NORETURN} Abort-;
нач {безКооперации, безОбычныхДинПроверок}
	если Activities.GetCurrentActivity () # НУЛЬ то Activities.TerminateCurrentActivity всё;
	Exit (1);
кон Abort;

проц Shutdown*;
нач {безКооперации, безОбычныхДинПроверок}
	если сравнениеСОбменом (status, Running, ShuttingDown) # Running то возврат всё;
	Трассировка.StringLn ("system: shutting down...");
кон Shutdown;

проц Reboot*;
нач {безКооперации, безОбычныхДинПроверок}
	Shutdown;
	утв (сравнениеСОбменом (status, ShuttingDown, Rebooting) = ShuttingDown);
кон Reboot;

проц {NORETURN} Exit- (status: целМЗ);
нач {безКооперации, безОбычныхДинПроверок}
	Трассировка.пСтроку8 ("system: exiting to Unix"); Трассировка.пВК_ПС;
	Unix.exit (status);
кон Exit;

проц Clock- (): цел32;
нач {безКооперации, безОбычныхДинПроверок}
	возврат цел32 ((Timer.GetCounter () - clock) / milliseconds);
кон Clock;

проц Sleep- (milliseconds: цел32);
перем nextActivity: Activities.Activity;
нач {безКооперации, безОбычныхДинПроверок}
	увел (milliseconds, Clock ());
	Counters.Inc (Activities.awaiting);
	нцПока (status = Running) и (Clock () - milliseconds < 0) делай
		если Activities.Select (nextActivity, Activities.IdlePriority) то
			Activities.SwitchTo (nextActivity, Enqueue, НУЛЬ);
			Activities.FinalizeSwitch;
		всё;
	кц;
	Counters.Dec (Activities.awaiting);
кон Sleep;

проц Enqueue (previous {неОтслСборщиком}: Activities.Activity; argument: адресВПамяти);
перем item: Queues.Item;
нач {безКооперации, безОбычныхДинПроверок}
	Queues.Enqueue (previous, sleepingQueue);
	если status # Running то
		нцПока Queues.Dequeue (item, sleepingQueue) делай Activities.Resume (item(Activities.Activity)) кц;
	всё;
кон Enqueue;

проц {C} TimerThread (): адресВПамяти;
нач {безКооперации, безОбычныхДинПроверок}
	Activities.CallVirtual (TickLoop, НУЛЬ, Activities.CreateVirtualProcessor ());
	возврат НУЛЬ;
кон TimerThread;

проц TickLoop (argument: адресВПамяти);
перем item: Queues.Item;
нач
	нцПока status = Running делай
		Unix.ThrSleep (1);
		нцПока Queues.Dequeue (item, sleepingQueue) делай
			Activities.Resume (item(Activities.Activity));
		кц;
	кц;
	нцПока Queues.Dequeue (item, sleepingQueue) делай
		Activities.Resume (item(Activities.Activity));
	кц;
кон TickLoop;

проц Allocate- (size: размерМЗ): адресВПамяти;
перем result, address: адресВПамяти;
нач {безКооперации, безОбычныхДинПроверок}
	result := Unix.malloc (size);
	если result = НУЛЬ то возврат НУЛЬ всё;
	нцДля address := result до result + size - 1 делай НИЗКОУР.запиши8битПоАдресу (address, 0) кц;
	возврат result;
кон Allocate;

проц Deallocate- (address: адресВПамяти);
нач {безКооперации, безОбычныхДинПроверок}
	Unix.free (address);
кон Deallocate;

проц GetInit- (n: размерМЗ; перем val: цел32);
нач val := 0;
кон GetInit;

проц GetString- (конст name: массив из симв8; перем result: массив из симв8);
нач {безКооперации, безОбычныхДинПроверок}
	result := "";
кон GetString;

проц StoreActivity-;
нач {безКооперации, безОбычныхДинПроверок}
	Unix.WriteKey (activity, НИЗКОУР.GetActivity ());
кон StoreActivity;

проц RestoreActivity-;
нач {безКооперации, безОбычныхДинПроверок}
	НИЗКОУР.SetActivity(НИЗКОУР.подмениТипЗначения(Activities.Activity,Unix.ReadKey (activity)));
кон RestoreActivity;

проц TrapHandler ( sig: цел32; mc: Unix.Mcontext );
проц Trap отКомпоновщика "Runtime.Trap" (number: размерМЗ);
нач
	RestoreActivity;
	Trap (sig);
кон TrapHandler;

проц Initialize-;
нач {безКооперации, безОбычныхДинПроверок}
	clock := Timer.GetCounter ();
	milliseconds := Timer.GetFrequency () DIV 1000;
	activity := Unix.NewKey ();
	Unix.HandleSignal(Unix.SIGSEGV);
	Unix.InstallTrap (TrapHandler);
кон Initialize;

проц Terminate-;
нач {безКооперации, безОбычныхДинПроверок}
кон Terminate;

нач
	Трассировка.пСтроку8 ("Build "); Трассировка.пСтроку8 (НИЗКОУР.Date); Трассировка.пСтроку8 (" (");
	Трассировка.пСтроку8 (Unix.Version); Трассировка.пСтроку8 (", GC, ");
	Трассировка.пЦел64 (Processors.count, 0); Трассировка.пСтроку8 (" CPU");
	если Processors.count > 1 то Трассировка.пСимв8 ('s') всё; Трассировка.пСтроку8 (", ");
	Трассировка.пЦел64 (размерОт адресВПамяти * 8, 0); Трассировка.пСтроку8 ("-bit)"); Трассировка.пВК_ПС;
	утв (Unix.pthread_create (операцияАдресОт timer, НУЛЬ, TimerThread, НУЛЬ) = 0);
кон Environment.
