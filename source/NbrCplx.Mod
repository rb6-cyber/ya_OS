(* CAPO - Computational Analysis Platform for Oberon - by Alan Freed and Felix Friedrich. *)
(* Version 1, Update 2 *)

модуль NbrCplx;   (** AUTHOR "adf"; PURPOSE "Defines a base Complex type for scientific computing."; *)

(** Cartesian componets:  z = x + i y  where  x, y N !.
	Polar components:  z = r exp(if)  where  r, f N !; r 3 0.
	Imaginary unit:  i = V-1. *)

использует Потоки, NbrInt, NbrRat, NbrRe;

тип
	Complex* = запись
		re, im: NbrRe.Real
	кон;

перем
	zero, one: NbrRe.Real;
	(**  The imaginary unit:  i = V-1. *)
	I-: Complex;

	(* Local procedure *)
	проц Reciprocal( x: Complex ): Complex;
	перем cplx: Complex;  denom, ratio: NbrRe.Real;
	нач
		если NbrRe.Abs( x.re ) > NbrRe.Abs( x.im ) то
			ratio := x.im / x.re;  denom := x.re + x.im * ratio;  cplx.re := 1 / denom;  cplx.im := -ratio / denom
		иначе ratio := x.re / x.im;  denom := x.re * ratio + x.im;  cplx.re := ratio / denom;  cplx.im := -1 / denom
		всё;
		возврат cplx
	кон Reciprocal;

	(** Monadic Operators *)
(** Negative, i.e., -z = -x - i y *)
	операция "-"*( x: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := -x.re;  cplx.im := -x.im;  возврат cplx
	кон "-";

(** Complex conjugate, i.e., ~z = x - i y  *)
	операция "~"*( x: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := x.re;  cplx.im := -x.im;  возврат cplx
	кон "~";

	(** Dyadic Operators *)
(** Type Conversions *)
	операция ":="*( перем l: Complex;  r: NbrInt.Integer );
	нач
		l.re := r;  l.im := 0
	кон ":=";

	операция ":="*( перем l: Complex;  r: NbrRat.Rational );
	нач
		l.re := r;  l.im := 0
	кон ":=";

	операция ":="*( перем l: Complex;  r: NbrRe.Real );
	нач
		l.re := r;  l.im := 0
	кон ":=";

(** Comparison Operators *)
	операция "="*( l, r: Complex ): булево;
	нач
		возврат (l.re = r.re) и (l.im = r.im)
	кон "=";

	операция "="*( l: Complex;  r: NbrRe.Real ): булево;
	нач
		возврат (l.re = r) и (l.im = 0)
	кон "=";

	операция "="*( l: Complex;  r: NbrRat.Rational ): булево;
	нач
		возврат (l.re = r) и (l.im = 0)
	кон "=";

	операция "="*( l: Complex;  r: NbrInt.Integer ): булево;
	нач
		возврат (l.re = r) и (l.im = 0)
	кон "=";

	операция "="*( l: NbrRe.Real;  r: Complex ): булево;
	нач
		возврат (l = r.re) и (r.im = 0)
	кон "=";

	операция "="*( l: NbrRat.Rational;  r: Complex ): булево;
	нач
		возврат (l = r.re) и (r.im = 0)
	кон "=";

	операция "="*( l: NbrInt.Integer;  r: Complex ): булево;
	нач
		возврат (l = r.re) и (r.im = 0)
	кон "=";

	операция "#"*( l, r: Complex ): булево;
	нач
		возврат (l.re # r.re) или (l.im # r.im)
	кон "#";

	операция "#"*( l: Complex;  r: NbrRe.Real ): булево;
	нач
		возврат (l.re # r) или (l.im # 0)
	кон "#";

	операция "#"*( l: Complex;  r: NbrRat.Rational ): булево;
	нач
		возврат (l.re # r) или (l.im # 0)
	кон "#";

	операция "#"*( l: Complex;  r: NbrInt.Integer ): булево;
	нач
		возврат (l.re # r) или (l.im # 0)
	кон "#";

	операция "#"*( l: NbrRe.Real;  r: Complex ): булево;
	нач
		возврат (l # r.re) или (r.im # 0)
	кон "#";

	операция "#"*( l: NbrRat.Rational;  r: Complex ): булево;
	нач
		возврат (l # r.re) или (r.im # 0)
	кон "#";

	операция "#"*( l: NbrInt.Integer;  r: Complex ): булево;
	нач
		возврат (l # r.re) или (r.im # 0)
	кон "#";

(** Arithmetic *)
	операция "+"*( l, r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re + r.re;  cplx.im := l.im + r.im;  возврат cplx
	кон "+";

	операция "+"*( l: Complex;  r: NbrRe.Real ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re + r;  cplx.im := l.im;  возврат cplx
	кон "+";

	операция "+"*( l: Complex;  r: NbrRat.Rational ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re + r;  cplx.im := l.im;  возврат cplx
	кон "+";

	операция "+"*( l: Complex;  r: NbrInt.Integer ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re + r;  cplx.im := l.im;  возврат cplx
	кон "+";

	операция "+"*( l: NbrRe.Real;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l + r.re;  cplx.im := r.im;  возврат cplx
	кон "+";

	операция "+"*( l: NbrRat.Rational;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l + r.re;  cplx.im := r.im;  возврат cplx
	кон "+";

	операция "+"*( l: NbrInt.Integer;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l + r.re;  cplx.im := r.im;  возврат cplx
	кон "+";

	операция "-"*( l, r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re - r.re;  cplx.im := l.im - r.im;  возврат cplx
	кон "-";

	операция "-"*( l: Complex;  r: NbrRe.Real ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re - r;  cplx.im := l.im;  возврат cplx
	кон "-";

	операция "-"*( l: Complex;  r: NbrRat.Rational ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re - r;  cplx.im := l.im;  возврат cplx
	кон "-";

	операция "-"*( l: Complex;  r: NbrInt.Integer ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re - r;  cplx.im := l.im;  возврат cplx
	кон "-";

	операция "-"*( l: NbrRe.Real;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l - r.re;  cplx.im := -r.im;  возврат cplx
	кон "-";

	операция "-"*( l: NbrRat.Rational;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l - r.re;  cplx.im := -r.im;  возврат cplx
	кон "-";

	операция "-"*( l: NbrInt.Integer;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l - r.re;  cplx.im := -r.im;  возврат cplx
	кон "-";

	операция "*"*( l, r: Complex ): Complex;
	перем cplx: Complex;  left, right: NbrRe.Real;
	нач
		left := l.re * r.re;  right := l.im * r.im;  cplx.re := left - right;  left := l.re * r.im;  right := l.im * r.re;
		cplx.im := left + right;  возврат cplx
	кон "*";

	операция "*"*( l: Complex;  r: NbrRe.Real ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re * r;  cplx.im := l.im * r;  возврат cplx
	кон "*";

	операция "*"*( l: Complex;  r: NbrRat.Rational ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re * r;  cplx.im := l.im * r;  возврат cplx
	кон "*";

	операция "*"*( l: Complex;  r: NbrInt.Integer ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l.re * r;  cplx.im := l.im * r;  возврат cplx
	кон "*";

	операция "*"*( l: NbrRe.Real;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l * r.re;  cplx.im := l * r.im;  возврат cplx
	кон "*";

	операция "*"*( l: NbrRat.Rational;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l * r.re;  cplx.im := l * r.im;  возврат cplx
	кон "*";

	операция "*"*( l: NbrInt.Integer;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx.re := l * r.re;  cplx.im := l * r.im;  возврат cplx
	кон "*";

	операция "/"*( l, r: Complex ): Complex;
	(* Algorithm can be found in Press et al. *)
	перем cplx: Complex;  denom, ratio: NbrRe.Real;
	нач
		если NbrRe.Abs( r.re ) > NbrRe.Abs( r.im ) то
			ratio := r.im / r.re;  denom := r.re + r.im * ratio;  cplx.re := (l.re + l.im * ratio) / denom;
			cplx.im := (l.im - l.re * ratio) / denom
		иначе
			ratio := r.re / r.im;  denom := r.re * ratio + r.im;  cplx.re := (l.re * ratio + l.im) / denom;
			cplx.im := (l.im * ratio - l.re) / denom
		всё;
		возврат cplx
	кон "/";

	операция "/"*( l: Complex;  r: NbrRe.Real ): Complex;
	перем cplx: Complex;
	нач
		cplx := l * (1 / r);  возврат cplx
	кон "/";

	операция "/"*( l: Complex;  r: NbrRat.Rational ): Complex;
	перем cplx: Complex;
	нач
		cplx := l * (1 / r);  возврат cplx
	кон "/";

	операция "/"*( l: Complex;  r: NbrInt.Integer ): Complex;
	перем re: NbrRe.Real;  cplx: Complex;
	нач
		re := r;  cplx := l * (1 / re);  возврат cplx
	кон "/";

	операция "/"*( l: NbrRe.Real;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx := l * Reciprocal( r );  возврат cplx
	кон "/";

	операция "/"*( l: NbrRat.Rational;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx := l * Reciprocal( r );  возврат cplx
	кон "/";

	операция "/"*( l: NbrInt.Integer;  r: Complex ): Complex;
	перем cplx: Complex;
	нач
		cplx := l * Reciprocal( r );  возврат cplx
	кон "/";

	(** Basic functions *)
(** x  in  z = x + i y *)
	проц Re*( x: Complex ): NbrRe.Real;
	нач
		возврат x.re
	кон Re;

(** y  in  z = x + i y *)
	проц Im*( x: Complex ): NbrRe.Real;
	нач
		возврат x.im
	кон Im;

(** r  in  z = r exp(if) *)
	проц Abs*( x: Complex ): NbrRe.Real;
	перем abs, absRe, absIm, ratio: NbrRe.Real;
	нач
		absRe := NbrRe.Abs( x.re );  absIm := NbrRe.Abs( x.im );
		если absRe > absIm то ratio := absIm / absRe;  abs := absRe * NbrRe.Sqrt( 1 + ratio * ratio )
		аесли absIm = 0 то abs := 0
		иначе ratio := absRe / absIm;  abs := absIm * NbrRe.Sqrt( 1 + ratio * ratio )
		всё;
		возврат abs
	кон Abs;

(** f  in  z = r exp(if) *)
	проц Arg*( x: Complex ): NbrRe.Real;
	перем arg: NbrRe.Real;

		проц ArcTan( xn, xd: NbrRe.Real ): NbrRe.Real;
		перем sn, sd: NbrInt.Integer;  atan, ratio: NbrRe.Real;
		нач
			если xn < 0 то sn := -1
			аесли xn = 0 то sn := 0
			иначе sn := 1
			всё;
			если xd < 0 то sd := -1
			аесли xd = 0 то sd := 0
			иначе sd := 1
			всё;
			если xd = 0 то atan := sn * NbrRe.Pi / 2
			аесли xn = 0 то atan := (1 - sd) * NbrRe.Pi / 2
			иначе ratio := xn / xd;  atan := NbrRe.ArcTan( ratio ) + sn * (1 - sd) * NbrRe.Pi / 2
			всё;
			возврат atan
		кон ArcTan;

	нач
		если x = 0 то arg := 0 иначе arg := ArcTan( x.im, x.re ) всё;
		возврат arg
	кон Arg;

(** re = x  and  im = y  in  z = x + i y. *)
	проц Get*( x: Complex;  перем re, im: NbrRe.Real );
	нач
		re := x.re;  im := x.im
	кон Get;

	проц Set*( re, im: NbrRe.Real;  перем x: Complex );
	нач
		x.re := re;  x.im := im
	кон Set;

(** abs = r  and  arg = f  in  z = r exp(if). *)
	проц GetPolar*( x: Complex;  перем abs, arg: NbrRe.Real );
	нач
		abs := Abs( x );  arg := Arg( x )
	кон GetPolar;

	проц SetPolar*( abs, arg: NbrRe.Real;  перем x: Complex );
	перем cos, sin: NbrRe.Real;
	нач
		cos := NbrRe.Cos( arg );  sin := NbrRe.Sin( arg );  x.re := abs * cos;  x.im := abs * sin
	кон SetPolar;

	(** String conversions. *)
(** Admissible characters: {" ", "+", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "E", "i", ",", "."}. *)
	проц StringToCplx*( string: массив из симв8;  перем x: Complex );
	перем negative: булево;  i, j: NbrInt.Integer;
		reString, imString: массив 32 из симв8;
	нач
		i := 0;
		нцПока (string[i] # 0X) и (string[i] # "i") делай reString[i] := string[i];  NbrInt.Inc( i ) кц;
		j := i;
		нц
			NbrInt.Dec( j );
			если j < 1 то x.re := 0;  прервиЦикл всё;
			(* Pass over any tailing white space and a plus or minus sign. *)
			если (reString[j] # симв8ИзКода( 20H )) и ((reString[j] # симв8ИзКода( 2BH )) или (reString[j] = симв8ИзКода( 2DH ))) то
				reString[j + 1] := 0X;  NbrRe.StringToRe( reString, x.re );  прервиЦикл
			всё
		кц;
		если string[i] = "i" то
			j := i;
			нц
				NbrInt.Dec( j );
				(* Determine the sign of the imaginary part. *)
				если j < 1 то negative := ложь;  прервиЦикл всё;
				если string[j] = симв8ИзКода( 2BH ) то negative := ложь;  прервиЦикл всё;
				если string[j] = симв8ИзКода( 2DH ) то negative := истина;  прервиЦикл всё
			кц;
			NbrInt.Inc( i );  j := 0;
			нцДо imString[j] := string[i];  NbrInt.Inc( i );  NbrInt.Inc( j ) кцПри string[i] = 0X;
			NbrRe.StringToRe( imString, x.im );
			если negative то x.im := -x.im всё
		иначе x.im := 0
		всё
	кон StringToCplx;

(** LEN(string) >= 2*significantFigures+18 *)
	проц CplxToString*( x: Complex;  significantFigures: NbrInt.Integer;  перем string: массив из симв8 );
	перем i, j: NbrInt.Integer;
		reString, imString: массив 32 из симв8;
	нач
		NbrRe.ReToString( x.re, significantFigures, reString );  i := 0;
		нцДо string[i] := reString[i];  NbrInt.Inc( i ) кцПри reString[i] = 0X;
		если x.im > 0 то
			string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  string[i] := "+";  NbrInt.Inc( i );  string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  string[i] := "i";
			NbrInt.Inc( i );  string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  NbrRe.ReToString( x.im, significantFigures, imString );  j := 0;
			нцДо string[i] := imString[j];  NbrInt.Inc( i );  NbrInt.Inc( j ) кцПри imString[j] = 0X
		аесли x.im = 0 то  (* write nothing *)
		иначе
			string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  string[i] := "-";  NbrInt.Inc( i );  string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  string[i] := "i";
			NbrInt.Inc( i );  string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  NbrRe.ReToString( -x.im, significantFigures, imString );  j := 0;
			нцДо string[i] := imString[j];  NbrInt.Inc( i );  NbrInt.Inc( j ) кцПри imString[j] = 0X
		всё;
		string[i] := 0X
	кон CplxToString;

(** Admissible characters: {" ", "+", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "E", "exp", "(", ")", "i", ",", "."}. *)
	проц PolarStringToCplx*( string: массив из симв8;  перем x: Complex );
	перем negative: булево;  i, j: NbrInt.Integer;  abs, arg: NbrRe.Real;
		absString, argString: массив 32 из симв8;
	нач
		i := 0;
		нцПока (string[i] # 0X) и (string[i] # "e") делай absString[i] := string[i];  NbrInt.Inc( i ) кц;
		если i = 0 то x.re := 0;  x.im := 0;  возврат всё;
		absString[i] := 0X;  NbrRe.StringToRe( absString, abs );
		если string[i] = "e" то
			нцДо NbrInt.Inc( i ) кцПри string[i] = "(";
			NbrInt.Inc( i );
			(* Pass over leading white space. *)
			нцПока string[i] = симв8ИзКода( 20H ) делай NbrInt.Inc( i ) кц;
			(* Determine the sign. *)
			если string[i] = симв8ИзКода( 2DH ) то negative := истина;  NbrInt.Inc( i ) иначе negative := ложь всё;
			(* Move to the first digit. *)
			нцПока (string[i] < симв8ИзКода( 30H )) или (string[i] > симв8ИзКода( 39H )) делай NbrInt.Inc( i ) кц;
			j := 0;
			нцПока string[i] # ")" делай argString[j] := string[i];  NbrInt.Inc( i );  NbrInt.Inc( j ) кц;
			argString[j] := 0X;  NbrRe.StringToRe( argString, arg );
			если negative то arg := -arg всё
		иначе arg := 0
		всё;
		SetPolar( abs, arg, x )
	кон PolarStringToCplx;

(** LEN(string) >= 2*significantFigures+22 *)
	проц CplxToPolarString*( x: Complex;  significantFigures: NbrInt.Integer;  перем string: массив из симв8 );
	перем i, j: NbrInt.Integer;  abs, arg: NbrRe.Real;
		absString, argString: массив 32 из симв8;
	нач
		GetPolar( x, abs, arg );  NbrRe.ReToString( abs, significantFigures, absString );  i := 0;
		нцДо string[i] := absString[i];  NbrInt.Inc( i ) кцПри absString[i] = 0X;
		если arg > 0 то
			string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  string[i] := "e";  NbrInt.Inc( i );  string[i] := "x";  NbrInt.Inc( i );  string[i] := "p";
			NbrInt.Inc( i );  string[i] := "(";  NbrInt.Inc( i );  string[i] := "i";  NbrInt.Inc( i );  string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );
			NbrRe.ReToString( arg, significantFigures, argString );  j := 0;
			нцДо string[i] := argString[j];  NbrInt.Inc( i );  NbrInt.Inc( j ) кцПри argString[j] = 0X;
			string[i] := ")";  NbrInt.Inc( i )
		аесли arg = 0 то
			(* write nothing *)
		иначе
			string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  string[i] := "e";  NbrInt.Inc( i );  string[i] := "x";  NbrInt.Inc( i );  string[i] := "p";
			NbrInt.Inc( i );  string[i] := "(";  NbrInt.Inc( i );  string[i] := "-";  NbrInt.Inc( i );  string[i] := "i";  NbrInt.Inc( i );
			string[i] := симв8ИзКода( 20H );  NbrInt.Inc( i );  NbrRe.ReToString( -arg, significantFigures, argString );  j := 0;
			нцДо string[i] := argString[j];  NbrInt.Inc( i );  NbrInt.Inc( j ) кцПри argString[j] = 0X;
			string[i] := ")";  NbrInt.Inc( i )
		всё;
		string[i] := 0X
	кон CplxToPolarString;

(** Persistence: file IO *)
	проц Load*( R: Потоки.Чтец;  перем x: Complex );
	нач
		NbrRe.Load( R, x.re );  NbrRe.Load( R, x.im )
	кон Load;

	проц Store*( W: Потоки.Писарь;  x: Complex );
	нач
		NbrRe.Store( W, x.re );  NbrRe.Store( W, x.im )
	кон Store;

нач
	zero := 0;  one := 1;  Set( zero, one, I )
кон NbrCplx.
