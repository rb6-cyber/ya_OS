# Постройка образа для RPiC

# Сборка
СборщикВыпускаЯОС.Build --lis --path=../../RPiCHDD/jaos/ --build RPiC ~
Linker.Link --path=../../RPiCHDD/jaos/ --fileName=../../RPicHDD/jaos/kernel.img --extension=Gof --displacement=8000H Builtins Trace CPU Runtime Counters 
Processors Queues BaseTypes Timer Activities ExclusiveBlocks HeapManager Interrupts Environment Mutexes Machine Heaps 
Modules GarbageCollector Objects Kernel ~

# Подготовка диска
System.DoCommands

System.Timer start ~
FSTools.DeleteFiles -i ../../RPiCHDD/disk/jaos-rpi.img ~

VirtualDisks.Create ../../RPiCHDD/disk/jaos-rpi.img 320000 512 ~
VirtualDisks.Install -b=512 VDISK0 ../../RPiCHDD/disk/jaos-rpi.img ~

Partitions.WriteMBR VDISK0#0 OBEMBR.BIN ~

Partitions.Create VDISK0#1 12 150 ~
Partitions.Format VDISK0#1 FatFS ~
FSTools.Mount TEMP FatFS VDISK0#1 ~
FSTools.CopyFiles ../../RPiCHDD/RPi-kernel/kernel7.img => TEMP:kernel7.img ~
FSTools.CopyFiles ../../RPiCHDD/RPi-kernel/bcm2709-rpi-2-b.dtb => TEMP:bcm2709-rpi-2-b.dtb ~
FSTools.CopyFiles ../../RPiCHDD/jaos/kernel.img => TEMP:kernel.img ~

FSTools.Watch TEMP ~
FSTools.Unmount TEMP ~

VirtualDisks.Uninstall VDISK0 ~

System.Show HDD image build time: ~ System.Timer elapsed ~

FSTools.CloseFiles ../../RPiCHDD/disk/jaos-rpi.img ~

~