модуль Attributes; (** AUTHOR "staubesv"; PURPOSE "Associate attributes with objects"; *)

конст

	Invalid = -1;
	InitialAttributeArraySize = 16;

тип

	(** Attributes associated with <object> *)
	Attribute* = запись
		object : динамическиТипизированныйУкль;
		flags- : мнвоНаБитахМЗ;
		data- : динамическиТипизированныйУкль;
	кон;

	AttributeArray = укль на массив из Attribute;

тип

	(**
		Manage relationship between objects and attributes. Objects are only
		stored if there are flags or data associated to it.

		Notes:
			- not thread-safe
			- no trick implementation only suited for small number of objects
			- if needed, objects can be pinned be the use of a flag dedicated to this purpose
	*)
	Attributes* = окласс
	перем
		attributes : AttributeArray;
		nofAttributes : цел32;

		проц &Init*; (* private *)
		нач
			attributes := НУЛЬ;
			nofAttributes := 0;
		кон Init;

		проц ResizeArrayIfNecessary; (* private *)
		перем newAttributes : AttributeArray; i : цел32;
		нач
			если (attributes = НУЛЬ) то
				утв(nofAttributes = 0);
				нов(attributes, InitialAttributeArraySize);
			аесли (nofAttributes >= длинаМассива(attributes)) то
				нов(newAttributes, 2 * длинаМассива(attributes));
				нцДля i := 0 до nofAttributes - 1 делай newAttributes[i] := attributes[i]; кц;
			всё;
		кон ResizeArrayIfNecessary;

		(* Add <object> to array *)
		проц AddObject(object : динамическиТипизированныйУкль; flags : мнвоНаБитахМЗ; data : динамическиТипизированныйУкль); (* private *)
		нач
			(* caller must ensure to not add duplicate nodes! *)
			утв((object # НУЛЬ) и ((flags # {}) или (data # НУЛЬ)));
			ResizeArrayIfNecessary;
			attributes[nofAttributes].object := object;
			attributes[nofAttributes].flags := flags;
			attributes[nofAttributes].data := data;
			увел(nofAttributes);
		кон AddObject;

		(*	Remove <object> from array. If hint is not <Invalid>, hint is the index of the object.
			<object> must be contained in array! *)
		проц RemoveObject(object : динамическиТипизированныйУкль; hint : цел32); (* private *)
		перем index, i : цел32;
		нач
			утв((object # НУЛЬ) и ((hint = Invalid) или (attributes[hint].object = object)));
			index := hint;
			если (index = Invalid) то index := GetIndexOf(object); всё;
			утв(index # Invalid);
			если (index = nofAttributes - 1) то
				attributes[index].object := НУЛЬ;
				attributes[index].flags := {};
				attributes[index].data := НУЛЬ;
			иначе
				утв(attributes[nofAttributes-1].object = НУЛЬ);
				нцДля i := index до nofAttributes-2 делай
					attributes[i] := attributes[i + 1];
				кц;
			всё;
			умень(nofAttributes);
		кон RemoveObject;

		(* Return index of node, <Invalid> if not found *)
		проц GetIndexOf(object : динамическиТипизированныйУкль) : цел32; (* private *)
		перем index : цел32;
		нач
			утв(object # НУЛЬ);
			index := 0;
			нцПока (index < nofAttributes) и (attributes[index].object # object) делай увел(index); кц;
			если (index = nofAttributes) то index := Invalid; всё;
			утв((index = Invalid) или ((0 <= index) и (index < nofAttributes) и (attributes[index].object = object)));
			возврат index;
		кон GetIndexOf;

		(** Get flags and data associated to object <object> *)
		проц Get*(object : динамическиТипизированныйУкль) : Attribute;
		перем attribute : Attribute; index : цел32;
		нач
			утв(object # НУЛЬ);
			index := GetIndexOf(object);
			если (index # Invalid) то
				attribute := attributes[index];
			иначе
				attribute.flags := {};
				attribute.data := НУЛЬ;
			всё;
			возврат attribute;
		кон Get;

		(** Set flags and data associated to object <object> *)
		проц Set*(object : динамическиТипизированныйУкль; flags : мнвоНаБитахМЗ; data : динамическиТипизированныйУкль);
		перем index : цел32;
		нач
			утв(object # НУЛЬ);
			index := GetIndexOf(object);
			если (index # Invalid) то
				если (flags # {}) или (data # НУЛЬ) то
					attributes[index].flags := flags;
					attributes[index].data := data;
				иначе
					RemoveObject(object, index);
				всё;
			аесли (flags # {}) или (data # НУЛЬ) то
				AddObject(object, flags, data);
			всё;
		кон Set;

		(** Get flags associated to object <object> *)
		проц GetFlags*(object : динамическиТипизированныйУкль) : мнвоНаБитахМЗ;
		перем flags : мнвоНаБитахМЗ; index : цел32;
		нач
			утв(object # НУЛЬ);
			flags := {};
			index := GetIndexOf(object);
			если (index # Invalid) то flags := attributes[index].flags; всё;
			возврат flags;
		кон GetFlags;

		(** Set flags associated to object <object> *)
		проц SetFlags*(object : динамическиТипизированныйУкль; flags : мнвоНаБитахМЗ);
		перем index : цел32;
		нач
			утв(object # НУЛЬ);
			index := GetIndexOf(object);
			если (index # Invalid) то
				если (flags # {}) то
					attributes[index].flags := flags;
				аесли (attributes[index].data = НУЛЬ) то
					RemoveObject(object, index);
				всё;
			аесли (flags # {}) то
				AddObject(object, flags, НУЛЬ);
			всё;
		кон SetFlags;

		(** Include a flag associated to object <object> *)
		проц Include*(object : динамическиТипизированныйУкль; flag : цел32);
		перем index : цел32;
		нач
			утв((object # НУЛЬ) и (0 <= flag) и (flag <= матМаксимум(мнвоНаБитахМЗ)));
			index := GetIndexOf(object);
			если (index # Invalid) то
				включиВоМнвоНаБитах(attributes[index].flags, flag);
			иначе
				AddObject(object, мнвоНаБитахМЗ({flag}), НУЛЬ);
			всё;
		кон Include;

		(** Exclude a flag associated to <object> *)
		проц Exclude*(object : динамическиТипизированныйУкль; flag : цел32);
		перем index : цел32;
		нач
			утв((object # НУЛЬ) и (0 <= flag) и (flag <= матМаксимум(мнвоНаБитахМЗ)));
			index := GetIndexOf(object);
			если (index # Invalid) то
				исключиИзМнваНаБитах(attributes[index].flags, flag);
				если (attributes[index].flags = {}) то RemoveObject(object, index); всё;
			всё;
		кон Exclude;

		(** Get data associated to object <object> *)
		проц GetData*(object : динамическиТипизированныйУкль) : динамическиТипизированныйУкль;
		перем data : динамическиТипизированныйУкль; index : цел32;
		нач
			утв(object # НУЛЬ);
			data := НУЛЬ;
			index := GetIndexOf(object);
			если (index # Invalid) то data := attributes[index].data; всё;
			возврат data;
		кон GetData;

		(** Set data associated to object <object> *)
		проц SetData*(object, data : динамическиТипизированныйУкль);
		перем index : цел32;
		нач
			утв(object # НУЛЬ);
			index := GetIndexOf(object);
			если (index # Invalid) то
				если (data # НУЛЬ) то
					attributes[index].data := data;
				аесли (attributes[index].flags = {}) то
					RemoveObject(object, index);
				всё;
			иначе
				AddObject(object, {}, data);
			всё;
		кон SetData;

		(** Clear all flags / data *)
		проц Clear*;
		нач
			attributes := НУЛЬ;
			nofAttributes := 0;
		кон Clear;

	кон Attributes;

кон Attributes.
