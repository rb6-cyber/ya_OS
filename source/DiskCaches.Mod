модуль DiskCaches; (* Simple (disk) cache, fof 2018 *)
использует НИЗКОУР, Disks;

	тип TransferProcedure = проц {делегат} (op,block,num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);

	конст BlockSize = 512;
	конст NumberCacheLines = 128;

	тип
		CacheLine = запись
			globalIndex: размерМЗ;
			data: укль на массив из симв8;
		кон;

	тип

	(* Yet very simple disk cache.
		- synchronous
		- One-way associative
		- Write-through
	*)
	Cache* = окласс
	перем
		lines: массив NumberCacheLines из CacheLine;
		blocksPerCacheLine: цел32;
		transfer: TransferProcedure;

		проц &Init*(transfer: TransferProcedure; blocksPerCacheLine = 32: цел32);
		перем i: размерМЗ;
		нач
			сам.transfer := transfer;
			сам.blocksPerCacheLine := blocksPerCacheLine;
			нцДля i := 0 до длинаМассива(lines)-1 делай
				lines[i].globalIndex := -1;
				нов(lines[i].data,blocksPerCacheLine * BlockSize );
			кц;
		кон Init;

		(* LONGINTs for compatibility -- should be largely replaced by SIZEs ! *)
		проц Transfer* (op: цел32; block, num: цел32; перем data: массив из симв8; ofs: размерМЗ; перем res: целМЗ);
		перем globalIndex, lineIndex, lineOfs, lineFirstBlock, lineBlocks: цел32;
		нач
			нцПока num > 0 делай
				globalIndex := block DIV blocksPerCacheLine; (* global index *)
				lineIndex := globalIndex остОтДеленияНа NumberCacheLines; (* (local) line index *)
				lineOfs := block остОтДеленияНа blocksPerCacheLine; (* line offset in blocks *)
				lineFirstBlock := block - lineOfs;
				lineBlocks := матМинимум(num, blocksPerCacheLine - lineOfs); (* chunk size in bytes *)

				если (lines[lineIndex].globalIndex # globalIndex) то (* cacheline present *)
					если op = Disks.Read то (* read to cache *)
						transfer(op, lineFirstBlock, blocksPerCacheLine, lines[lineIndex].data^, 0, res); (* assumes that lineFirstBlock is a valid block *)
						lines[lineIndex].globalIndex := globalIndex;
					всё;
				аесли op = Disks.Write то (* write to present cacheline *)
					НИЗКОУР.копируйПамять(операцияАдресОт data[ofs], операцияАдресОт lines[lineIndex].data[lineOfs*BlockSize], lineBlocks*BlockSize);
				всё;

				если op = Disks.Write то (* always write through*)
					transfer(op, block, lineBlocks, data, ofs, res);
				иначе (* read from cache *)
					НИЗКОУР.копируйПамять(операцияАдресОт lines[lineIndex].data[lineOfs*BlockSize], операцияАдресОт data[ofs], lineBlocks*BlockSize);
				всё;
				умень(num, lineBlocks);
				увел(block, lineBlocks);
				увел(ofs, lineBlocks*BlockSize);
			кц;

		кон Transfer;

	кон Cache;

кон DiskCaches.

(** USAGE PATTERN:

	VirtualDisk = OBJECT(Disks.Device)
	VAR
		...
		cache: DiskCaches.Cache;

		PROCEDURE TransferX*(op, block, num: SIGNED32; VAR data: ARRAY OF CHAR; ofs: SIGNED32; VAR res: INTEGER);
		BEGIN
			.... OLD TRANSFER CODE
		END TransferX;

		PROCEDURE Transfer(op, block, num: SIGNED32; VAR data: ARRAY OF CHAR; ofs: SIGNED32; VAR res: INTEGER);
		BEGIN{EXCLUSIVE}
			cache.Transfer(op,block,num,data,ofs,res)
		END Transfer;

		....

		PROCEDURE &Init(CONST name : ARRAY OF CHAR; blockSize, cyls, hds, spt : SIGNED32);
		BEGIN
			NEW(cache, TransferX);
			....
		END Init;

	END VirtualDisk;

**)
