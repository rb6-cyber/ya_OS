модуль CDRecordUtils;

использует
	НИЗКОУР, Codecs, Strings, Потоки, Files, SoundDevices, ЛогЯдра, Disks, DiskVolumes, FATVolumes, ISO9660Volumes, ATADisks,
	WMWindowManager, WMComponents, WMStandardComponents, WMFileManager, WMSystemComponents, WMGrids, WMStringGrids, WMDialogs,
	WMEvents, WMTabComponents, WMProperties, WMGraphics, WMRectangles;

конст
	MaxLen = 256;
	ResOk=0; ResErr=1;

	(* mpeg *)
	MPEGVer1 = 3;
	MPEGVer2 =  2;
	MPEGVer25 = 0;

	(* wave compression *)
	CUnknown* = 0H;
	CPCM* = 1H;
	CMsADPCM* = 2H;

	(* Errors *)
	ErrFileNotFound* = 100;
	ErrNoISOImage* = 101;

	White = 0FFFFFFFFH;
	Black = 0000000FFH;
	LightGray = (0C8C8C8FFH);
	DarkGray = (0A0A0A0FFH);

тип
	String = Strings.String;

	Status* = окласс кон Status;

	ConvertingStatus* = окласс(Status)
	перем
		bytesEncoded*: размерМЗ;
	кон ConvertingStatus;

	StatusProc* = проц {делегат} (status: Status);

	WAVInfo* = окласс
		перем
			compression*, size*, nofchannels*, samplerate*, encoding* : цел32;

		проц Open*(filename : Strings.String): цел32;
		перем
			bytesRead: размерМЗ;
			file: Files.File;
			r: Files.Reader;
			buf: массив 8 из симв8;
		нач
			file := Files.Old(filename^);
			если file = НУЛЬ то
				возврат 1;
			всё;
			Files.OpenReader(r, file, 0);
			(* read first chunk and check if file is a wav file *)
			buf[4] := 0X;
			r.чБайты(buf, 0, 4, bytesRead);
			если buf # "RIFF" то
				возврат ResErr;
			всё;
			r.чБайты(buf, 0, 4, bytesRead);
			size := ConvertLE32Int(buf);
			(* Bluebottle's Wave Encoder only sets the header size
			IF size # file.Length() - 8 THEN
				RETURN ResErr;
			END;
			*)
			если size < 1024 то size := (file.Length() - 8)(цел32); всё;

			r.чБайты(buf, 0, 4, bytesRead);
			если buf # "WAVE" то
				возврат ResErr;
			всё;

			(* format chunk *)
			r.чБайты(buf, 0, 4, bytesRead);
			если buf # "fmt " то
				возврат ResErr;
			всё;

			r.чБайты(buf, 0, 4, bytesRead);

			r.чБайты(buf, 0, 2, bytesRead);
			compression := ConvertLE16Int(buf);

			r.чБайты(buf, 0, 2, bytesRead);
			nofchannels := ConvertLE16Int(buf);

			r.чБайты(buf, 0, 4, bytesRead);
			samplerate := ConvertLE32Int(buf);

			r.чБайты(buf, 0, 6, bytesRead);
			r.чБайты(buf, 0, 2, bytesRead);
			encoding := ConvertLE16Int(buf);

			возврат ResOk;
		кон Open;

	кон WAVInfo;

	Frame = запись
		mpegver, bitrate, samplerate, padding, layer, size : цел32
	кон;

	ID3v1* = окласс
	перем
		Title*, Artist*, Album: массив 31 из симв8;
	кон ID3v1;

	MP3Info* = окласс
	перем
		nofframes*, playtime* : цел32;
		id3v1*: ID3v1;
		bitrateTable: массив 6, 16 из цел32;

		проц &New*;
		нач
			InitTable(bitrateTable);
			id3v1 := НУЛЬ;
		кон New;

		проц GetNextFrame(r: Files.Reader; перем frame: Frame): цел32;
		перем
			n, start, tmp: цел32;
			bytesRead: размерМЗ;
			chr: симв8;
			buf : массив 4 из симв8;

		нач
			start := -1; n := 0;
			(* find first frame in first 10k by comparing sync pattern *)
			нцДо
				r.чБайты(buf, 0, 1, bytesRead);
				если (bytesRead = 1) и (buf[0] = 0FFX) то
					chr := r.ПодглядиСимв8();
					если арифмСдвиг(кодСимв8(chr), -5) = 7 то
						start := n;
						r.чБайты(buf, 1, 3, bytesRead);
					всё;
				всё;
				увел(n, 1);
			кцПри (n > 10000) или (bytesRead < 1) или (start # -1);

			если start = -1 то
				возврат ResErr;
			всё;

			(* MPEG version *)
			tmp := арифмСдвиг(кодСимв8(buf[1]), -3) остОтДеленияНа 4;
			просей tmp из
				  0: frame.mpegver := MPEGVer25; (* 2.5 *)
				| 2: frame.mpegver := MPEGVer2;
				| 3: frame.mpegver := MPEGVer1;
				иначе возврат ResErr;
			всё;

			(* MPEG Layer *)
			tmp := арифмСдвиг(кодСимв8(buf[1]), -1) остОтДеленияНа 4;
			просей tmp из
				  1: frame.layer := 3;
				| 2: frame.layer := 2;
				| 3: frame.layer := 1;
				иначе возврат ResErr;
			всё;

			(* Bitrate *)
			tmp := арифмСдвиг(кодСимв8(buf[2]), -4);
			если frame.mpegver = MPEGVer1 то
				просей frame.layer из
					  1: frame.bitrate := bitrateTable[0][tmp];
					| 2: frame.bitrate := bitrateTable[1][tmp];
					| 3: frame.bitrate := bitrateTable[2][tmp];
					иначе возврат ResErr;
				всё;
			иначе
				просей frame.layer из
					  1: frame.bitrate := bitrateTable[3][tmp];
					| 2: frame.bitrate := bitrateTable[4][tmp];
					| 3: frame.bitrate := bitrateTable[5][tmp];
					иначе возврат ResErr;
				всё;
			всё;

			(* samplerate *)
			tmp := арифмСдвиг(кодСимв8(buf[2]), -2) остОтДеленияНа 4;
			если frame.mpegver = MPEGVer1 то
				просей tmp из
					  0: frame.samplerate := 44100;
					| 1: frame.samplerate := 48000;
					| 2: frame.samplerate := 32000;
					иначе возврат ResErr;
				всё;
			аесли frame.mpegver = MPEGVer2 то
				просей tmp из
					  0: frame.samplerate := 22050;
					| 1: frame.samplerate := 24000;
					| 2: frame.samplerate := 16000;
					иначе возврат ResErr;
				всё;
			аесли frame.mpegver = MPEGVer25 то
				просей tmp из
					  0: frame.samplerate := 11025;
					| 1: frame.samplerate := 12000;
					| 2: frame.samplerate := 8000;
					иначе возврат ResErr;
				всё;
			всё;

			frame.padding := арифмСдвиг(кодСимв8(buf[2]), -1) остОтДеленияНа 2;

			если frame.mpegver = MPEGVer1 то
				если frame.layer = 1 то
					tmp := 48000;
				иначе
					tmp := 144000;
				всё;
			иначе
				если frame.layer = 1 то
					tmp := 24000
				иначе
					tmp := 72000;
				всё;
			всё;

			frame.size := tmp*frame.bitrate DIV frame.samplerate + frame.padding;
			возврат ResOk;
		кон GetNextFrame;

		проц Open*(filename: String): цел32;
		перем
			file : Files.File;
			r: Files.Reader;
			frame: Frame;

		нач
			nofframes := 0; playtime := 0;
			file := Files.Old(filename^);
			если file = НУЛЬ то
				возврат ResErr;
			всё;

			Files.OpenReader(r, file, 0);
			нцПока GetNextFrame(r, frame) = ResOk делай
				увел(nofframes);
				r.ПропустиБайты(frame.size-4);
				увел(playtime, (8*frame.size) DIV frame.bitrate); (* in ms *)
			кц;

			если nofframes < 1 то возврат ResErr всё;
			playtime := (playtime+1000-1) DIV 1000;

			нов(id3v1);
			если ReadID3v1(filename, id3v1) # ResOk то
				id3v1 := НУЛЬ;
			всё;

			возврат ResOk;
		кон Open;

		проц InitTable(перем table: массив из массив из цел32);
		нач
			table[0][0] := 0;			table[1][0] := 0;			table[2][0] := 0; 		table[3][0] := 0;			table[4][0] := 0; 		table[5][0] := 0;
			table[0][1] :=32;		table[1][1] := 32;		table[2][1] := 32; 		table[3][1] := 32;		table[4][1] := 8; 		table[5][1] := 8;
			table[0][2] :=64;		table[1][2] := 48; 		table[2][2] := 40; 		table[3][2] := 48; 		table[4][2] := 16; 		table[5][2] := 16;
			table[0][3] :=96;		table[1][3] := 56; 		table[2][3] := 48; 		table[3][3] := 56; 		table[4][3] := 24; 		table[5][3] := 24;
			table[0][4] :=128;		table[1][4] := 64;		table[2][4] := 56; 		table[3][4] := 64; 		table[4][4] := 32; 		table[5][4] := 32;
			table[0][5] :=160;		table[1][5] := 80; 		table[2][5] := 64; 		table[3][5] := 80;		table[4][5] := 40; 		table[5][5] := 64;
			table[0][6] :=192;		table[1][6] := 96; 		table[2][6] := 80; 		table[3][6] := 96; 		table[4][6] := 48 ;		table[5][6] := 80;
			table[0][7] :=224;		table[1][7] := 112;		table[2][7] := 96; 		table[3][7] := 112; 		table[4][7] := 56;  		table[5][7] := 56;
			table[0][8] :=256;		table[1][8] := 128; 		table[2][8] := 112; 		table[3][8] := 128; 		table[4][8] := 64; 		table[5][8] := 64;
			table[0][9] :=288;		table[1][9] := 160; 		table[2][9] := 128; 		table[3][9] := 144; 		table[4][9] := 80; 		table[5][9] := 128;
			table[0][10] := 320; 	table[1][10] := 192; 	table[2][10] := 160; 	table[3][10] := 160;	table[4][10] := 96; 		table[5][10] := 160;
			table[0][11] := 352; 	table[1][11] := 224; 	table[2][11] := 192; 	table[3][11] := 176;	table[4][11] := 112; 	table[5][11] := 112;
			table[0][12] := 384; 	table[1][12] := 256; 	table[2][12] := 224; 	table[3][12] := 192;	table[4][12] := 128; 	table[5][12] := 128;
			table[0][13] := 416; 	table[1][13] := 320; 	table[2][13] := 256; 	table[3][13] := 224;	table[4][13] := 144; 	table[5][13] := 256;
			table[0][14] := 448; 	table[1][14] := 384; 	table[2][14] := 320;	table[3][14] := 256; 	table[4][14] := 160; 	table[5][14] := 320;
			table[0][15] := -1; 	table[1][15] := -1; 	table[2][15] := -1;		table[3][15] := -1; 	table[4][15] := -1;  	table[5][15] := -1;
		кон InitTable;

	кон MP3Info;

	(* Window Components *)

	StandardDialog* = окласс(WMDialogs.Dialog);
	перем
		width*, height*: цел32;
		ok*, abort*: WMStandardComponents.Button;
		content*: WMComponents.VisualComponent;
		buttonPanel*: WMStandardComponents.Panel;

		проц &New*(title: String; bounds: WMRectangles.Rectangle; width, height: цел32);
		нач
			x := bounds.l + ((bounds.r - bounds.l - width) DIV 2); если x < 0 то x := 0 всё;
			y := bounds.t + ((bounds.b - bounds.t - height) DIV 2); если y < 20 то y := 20 всё;
			сам.width := width; сам.height := height;
			SetTitle(title);
			errors := ложь;
			CreateDialog;
			WireDialog;
			Init(content.bounds.GetWidth(), content.bounds.GetHeight(), ложь);
			SetContent(content);
		кон New;

		проц {перекрыта}Show*;
		нач
			result := -1;
			manager := WMWindowManager.GetDefaultManager();
			manager.Add(x, y, сам, {WMWindowManager.FlagFrame, WMWindowManager.FlagStayOnTop});
			manager.SetFocus(сам);
			content.Reset(НУЛЬ, НУЛЬ);
			нач {единолично}
				дождись(result >= 0)
			кон;
			manager.Remove(сам)
		кон Show;

		проц ShowNonModal*;
		нач
			result := -1;
			manager := WMWindowManager.GetDefaultManager();
			manager.Add(x, y, сам, {WMWindowManager.FlagFrame, WMWindowManager.FlagStayOnTop});
			manager.SetFocus(сам);
		кон ShowNonModal;

		проц CreateDialog*;
		перем
			panel: WMStandardComponents.Panel;
			manager: WMWindowManager.WindowManager;
			windowStyle: WMWindowManager.WindowStyle;
		нач
			manager := WMWindowManager.GetDefaultManager();
			windowStyle := manager.GetStyle();
			нов(panel); panel.bounds.SetExtents(width, height);
			panel.fillColor.Set(windowStyle.bgColor);
			panel.takesFocus.Set(ложь);

			нов(buttonPanel); buttonPanel.bounds.SetHeight(30); buttonPanel.alignment.Set(WMComponents.AlignBottom);
			panel.AddContent(buttonPanel);

			нов(abort);
			abort.bounds.SetExtents(60,30);
			abort.alignment.Set(WMComponents.AlignRight);
			abort.caption.SetAOC("Abort");
			buttonPanel.AddContent(abort);

			нов(ok);
			ok.bounds.SetExtents(60,30);
			ok.alignment.Set(WMComponents.AlignRight);
			ok.caption.SetAOC("Ok");
			buttonPanel.AddContent(ok);

			content := panel
		кон CreateDialog;

		проц WireDialog;
		нач
			ok.onClick.Add(Ok);
			abort.onClick.Add(Abort);
		кон WireDialog;

	кон StandardDialog;

	FileDialog* = окласс(StandardDialog);
		перем
			path*: WMProperties.StringProperty;
			explorer: ExplorerPanel;

		проц {перекрыта}CreateDialog*;
		перем
			bearing: WMRectangles.Rectangle;
		нач
			CreateDialog^;
			нов(path, НУЛЬ, НУЛЬ, НУЛЬ);
			path.Set(Strings.NewString(""));
			bearing := WMRectangles.MakeRect(3, 3, 3, 3);
			нов(explorer);
			explorer.alignment.Set(WMComponents.AlignClient);
			content.AddContent(explorer);
		кон CreateDialog;

		проц {перекрыта}Ok(sender, data: динамическиТипизированныйУкль);
		перем
			dirEntries: WMSystemComponents.DirEntries;
			str: массив MaxLen из симв8;
		нач
			dirEntries := explorer.list.list.GetSelection();
			если (длинаМассива(dirEntries) > 0) и (dirEntries[0] # НУЛЬ) и  (dirEntries[0].name # НУЛЬ) и (dirEntries[0].path # НУЛЬ) то
				копируйСтрокуДо0(dirEntries[0].path^, str);
				Strings.Append(str, "/");
				Strings.Append(str, dirEntries[0].name^);
				path.Set(Strings.NewString(str));
			всё;
			Ok^(sender, data);
		кон Ok;
	кон FileDialog;

	PropertyPage* = окласс(WMComponents.VisualComponent);
	перем
		tab: WMTabComponents.Tab;
		owner*: PropertySheet;

		проц UpdateData*(save: булево);
		кон UpdateData;
	кон PropertyPage;

	PropertySheet* = окласс(StandardDialog);
	перем
		tabs: WMTabComponents.Tabs;
		curPage: PropertyPage;

		проц {перекрыта}CreateDialog*;
		перем
			topPanel: WMStandardComponents.Panel;
		нач
			CreateDialog^;
			нов(topPanel);
			topPanel.bounds.SetHeight(20); topPanel.alignment.Set(WMComponents.AlignTop); topPanel.fillColor.Set(LightGray);
			content.AddContent(topPanel);
			нов(tabs);
			tabs.alignment.Set(WMComponents.AlignTop); tabs.bounds.SetExtents(width,20);
			tabs.onSelectTab.Add(TabSelected);
			topPanel.AddContent(tabs);
		кон CreateDialog;

		проц TabSelected(sender, data: динамическиТипизированныйУкль);
		перем
			tab: WMTabComponents.Tab;

		нач
			если (data # НУЛЬ) то
				tab := data(WMTabComponents.Tab);
				если tab.data # НУЛЬ то
					если curPage # НУЛЬ то
						content.RemoveContent(curPage);
					всё;
					curPage := tab.data(PropertyPage);
					content.AddContent(curPage);
					curPage.Reset(НУЛЬ, НУЛЬ);
					curPage.Invalidate();
					curPage.Resized;
				всё;
			всё;
		кон TabSelected;

		проц AddPage*(page: PropertyPage; name: String);
		перем
			tab: WMTabComponents.Tab;
		нач
			page.owner := сам;
			tab := tabs.NewTab();
			tabs.SetTabCaption(tab, name);
			tabs.SetTabData(tab, page);
			page.tab := tab;
			tabs.AddTab(tab);
		кон AddPage;

		проц SelectPage*(page: PropertyPage);
		нач
			tabs.Select(page.tab);
			curPage := page;
			если curPage # НУЛЬ то
				content.RemoveContent(page);
			всё;
			content.AddContent(page);
		кон SelectPage;

	кон PropertySheet;


	ProgressBar* = окласс(WMComponents.VisualComponent)
	перем
		start*, end*, cur*: WMProperties.IntegerProperty;
		color*: WMProperties.ColorProperty;
		borderColor*: WMProperties.ColorProperty;

		проц &{перекрыта}Init*;
		перем
		нач
			Init^();
			нов(color, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(borderColor, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(start, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(end, НУЛЬ, НУЛЬ, НУЛЬ);
			нов(cur, НУЛЬ, НУЛЬ, НУЛЬ);
		кон Init;

		проц {перекрыта}SetPos*(pos: цел64);
		нач
			если pos < start.Get() то
				pos := start.Get();
			аесли pos > end.Get() то
				pos := end.Get();
			всё;
			cur.Set(pos);
			Invalidate();
		кон SetPos;

		проц {перекрыта}GetPos*(): цел64;
		нач
			возврат cur.Get();
		кон GetPos;

		проц SetRange*(start, end: цел64);
		нач
			сам.start.Set(start);
			сам.end.Set(end);
			cur.Set(start);
		кон SetRange;

		проц StepIt*;
		нач
			если cur.Get() < end.Get() то
				cur.Set(cur.Get() + 1);
				Invalidate;
			всё;
		кон StepIt;

		проц {перекрыта}DrawBackground*(canvas: WMGraphics.Canvas);
		перем
			rect: WMRectangles.Rectangle;
			width: цел32;
			pt: массив 4 из WMGraphics.Point2d;
		нач
			если end.Get() > start.Get() то
				width := цел32((cur.Get()-start.Get()) * bounds.GetWidth() DIV (end.Get()-start.Get()));
			всё;
			rect := WMRectangles.MakeRect(0, 0, width, bounds.GetHeight());

			canvas.Fill(rect, color.Get(), WMGraphics.ModeCopy);

			rect := GetClientRect();
			rect.l := width;
			canvas.Fill(rect, fillColor.Get(), WMGraphics.ModeCopy);

			pt[0].x := 0; pt[0].y := 0;
			pt[1].x := bounds.GetWidth()-1; pt[1].y := 0;
			pt[2].x := bounds.GetWidth()-1; pt[2].y := bounds.GetHeight()-1;
			pt[3].x := 0; pt[3].y := bounds.GetHeight()-1;
			canvas.PolyLine(pt, 4, истина, borderColor.Get(), WMGraphics.ModeCopy);
		кон DrawBackground;
	кон ProgressBar;

	ListBox* =  окласс(WMComponents.VisualComponent);
	перем
		grid: WMStringGrids.StringGrid;
		nofRows: цел32;
		caption*: WMProperties.StringProperty;
		label: WMStandardComponents.Label;
		selected*: WMProperties.SizeProperty;
		onSelectionChanged* : WMEvents.EventSource;

		проц &{перекрыта}Init*;
		перем
			leftPanel, rightPanel: WMStandardComponents.Panel;
		нач
			Init^();
			нов(caption, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(caption);
			нов(selected, НУЛЬ, НУЛЬ, НУЛЬ); properties.Add(selected);
			нов(onSelectionChanged, сам, НУЛЬ, НУЛЬ, НУЛЬ);

			нов(leftPanel); leftPanel.bounds.SetWidth(80); leftPanel.alignment.Set(WMComponents.AlignLeft);
			AddContent(leftPanel);

			нов(label); label.bounds.SetHeight(14); label.alignment.Set(WMComponents.AlignTop); label.textColor.Set(Black);
			leftPanel.AddContent(label);

			нов(rightPanel); rightPanel.alignment.Set(WMComponents.AlignClient);
			AddContent(rightPanel);

			нов(grid);
			grid.alignment.Set(WMComponents.AlignClient);
			rightPanel.AddContent(grid);
			grid.onSelect.Add(SelectionHandler);
			grid.model.Acquire;
			grid.model.SetNofCols(1);
			grid.SetSelectionMode(WMGrids.GridSelectSingleRow);
			grid.model.Release;
		кон Init;

		проц Update*;
		нач
			Reset(НУЛЬ, НУЛЬ);
			Invalidate();
			Resized;
		кон Update;

		проц Clear*;
		нач
			nofRows := 0;
			grid.model.Acquire;
			grid.model.SetNofRows(nofRows);
			grid.model.Release;
		кон Clear;

		проц SelectionHandler*(sender, data: динамическиТипизированныйУкль);
		перем
			scol, srow, ecol, erow: размерМЗ;
		нач
			grid. GetSelection(scol, srow, ecol, erow );
			selected.Set(srow);
		кон SelectionHandler;

		проц {перекрыта}RecacheProperties;
		нач
			label.caption.Set(caption.Get());
			grid.SetSelection(0, selected.Get(), 0, selected.Get());
			onSelectionChanged.Call(selected);
			RecacheProperties^;
		кон RecacheProperties;

		проц {перекрыта}PropertyChanged(sender, property: динамическиТипизированныйУкль);
		нач
			если property = caption то
				label.caption.Set(caption.Get());
			аесли property = selected то
				grid.SetSelection(0, selected.Get(), 0, selected.Get());
				onSelectionChanged.Call(selected);
			иначе
				PropertyChanged^(sender, property);
			всё;
		кон PropertyChanged;

		проц Add*(name: String; data: динамическиТипизированныйУкль);
		нач
			grid.model.Acquire;
			увел(nofRows);
			grid.model.SetNofRows(nofRows);
			grid.model.SetCellText(0, nofRows-1, name);
			grid.model.SetCellData(0, nofRows-1, data);
			grid.model.Release;
		кон Add;
	кон ListBox;

	ExplorerPanel* = окласс(WMComponents.VisualComponent);
	перем
		tree*: WMSystemComponents.DirectoryTree;
		list: WMFileManager.FileListPanel;

		проц &{перекрыта}Init*;
		перем
			panel, sidePanel: WMStandardComponents.Panel;
			resizer: WMStandardComponents.Resizer;
		нач
			Init^();
			нов(panel);
			panel.alignment.Set(WMComponents.AlignClient);
			panel.fillColor.Set(White);
			AddContent(panel);

			нов(sidePanel);
			sidePanel.alignment.Set(WMComponents.AlignLeft);
			sidePanel.bounds.SetWidth(200);

			нов(resizer);
			resizer.alignment.Set(WMComponents.AlignRight);
			resizer.bounds.SetWidth(4);
			sidePanel.AddContent(resizer);

			нов(tree);
			tree.alignment.Set(WMComponents.AlignClient);
			sidePanel.AddContent(tree);
			panel.AddContent(sidePanel);
			tree.onPathChanged.Add(PathChanged);

			нов(list);
			list.alignment.Set(WMComponents.AlignClient);
			panel.AddContent(list);
		кон Init;

		проц PathChanged(sender, data: динамическиТипизированныйУкль);
		нач
			list.pathProp.Set(tree.currentPath.Get());
		кон PathChanged;
	кон ExplorerPanel;


проц ReadID3v1(перем filename: String; перем id3v1: ID3v1): цел32;
перем
	file: Files.File;
	r: Files.Reader;
	id, buf: массив 128 из симв8;
	len: Files.Size; bytesRead: размерМЗ;
нач
	file := Files.Old(filename^);
	если file = НУЛЬ то
		возврат ResErr;
	всё;
	Files.OpenReader(r, file, 0);
	len := file.Length();
	r.ПропустиБайты(len-128);
	r.чБайты(buf, 0, 128, bytesRead);
	Strings.Copy(buf, 0, 3, id);
	если id # "TAG" то возврат ResErr; всё;
	Strings.Copy(buf, 3, 30, id3v1.Title);
	Strings.Copy(buf, 33, 30, id3v1.Artist);
	Strings.Copy(buf, 63, 30, id3v1.Album);
	возврат ResOk;

кон ReadID3v1;


проц ConvertLE16Int*(конст buf : массив из симв8): цел32;
нач
	возврат арифмСдвиг(кодСимв8(buf[1]), 8) + кодСимв8(buf[0]);
кон ConvertLE16Int;

проц ConvertLE32Int*(конст buf : массив из симв8): цел32;
нач
	возврат арифмСдвиг(кодСимв8(buf[3]), 24) + арифмСдвиг(кодСимв8(buf[2]), 16) + арифмСдвиг(кодСимв8(buf[1]), 8) + кодСимв8(buf[0]);
кон ConvertLE32Int;

проц ConvertBE16Int*(конст buf : массив из симв8): цел32;
нач
	возврат арифмСдвиг(кодСимв8(buf[0]), 8) + кодСимв8(buf[1]);
кон ConvertBE16Int;

проц ConvertBE32Int*(конст buf : массив из симв8): цел32;
нач
	возврат арифмСдвиг(кодСимв8(buf[0]), 24) + арифмСдвиг(кодСимв8(buf[1]), 16) + арифмСдвиг(кодСимв8(buf[2]), 8) + кодСимв8(buf[3]);
кон ConvertBE32Int;


проц SetLE16*(x: цел16; перем dst : массив из симв8);
нач
	dst[0] := симв8ИзКода(x остОтДеленияНа 100H);
	dst[1] := симв8ИзКода(x DIV 100H остОтДеленияНа 100H);
кон SetLE16;

проц SetLE32*(x: цел32; перем dst: массив из симв8);
нач
	dst[0] := симв8ИзКода(x остОтДеленияНа 100H);
	dst[1] := симв8ИзКода(x DIV 100H остОтДеленияНа 100H);
	dst[2] := симв8ИзКода(x DIV 10000H остОтДеленияНа 100H);
	dst[3] := симв8ИзКода(x DIV 1000000H остОтДеленияНа 100H);
кон SetLE32;

проц SetBE16*(x: цел16; перем dst : массив из симв8);
нач
	dst[1] := симв8ИзКода(x остОтДеленияНа 100H);
	dst[0] := симв8ИзКода(x DIV 100H остОтДеленияНа 100H);
кон SetBE16;

проц SetBE32*(x: цел32; перем dst: массив из симв8);
нач
	dst[3] := симв8ИзКода(x остОтДеленияНа 100H);
	dst[2] := симв8ИзКода(x DIV 100H остОтДеленияНа 100H);
	dst[1] := симв8ИзКода(x DIV 10000H остОтДеленияНа 100H);
	dst[0] := симв8ИзКода(x DIV 1000000H остОтДеленияНа 100H);
кон SetBE32;

проц Mp3ToWave*(srcFileName, destFileName : Strings.String; onConvertStatusChanged: StatusProc) : целМЗ;
перем
	res: целМЗ;
	srcFile, destFile : Files.File;
	decoder: Codecs.AudioDecoder;
	encoder: Codecs.AudioEncoder;
	in : Потоки.Чтец;
	out : Files.Writer;
	buffer : SoundDevices.Buffer;
	convertStatus: ConvertingStatus;
	bytesEncoded: размерМЗ;
нач
	нов(convertStatus);
	decoder := Codecs.GetAudioDecoder("MP3");
	если decoder = НУЛЬ то
		ЛогЯдра.пСтроку8("Could not open MP3DEcoder");
		возврат ResErr;
	всё;

	encoder := Codecs.GetAudioEncoder("WAV");
	если encoder = НУЛЬ то
		ЛогЯдра.пСтроку8("Could not open WAV Encoder");
		возврат ResErr;
	всё;

	srcFile := Files.Old(srcFileName^);
	destFile := Files.New(destFileName^);
	если destFile = НУЛЬ то
		возврат ResErr;
	всё;
	Files.Register(destFile);
	Files.OpenWriter(out, destFile, 0);
	in := Codecs.OpenInputStream(srcFileName^);
	если (in = НУЛЬ) или (out = НУЛЬ) то
		возврат ResErr;
	всё;

	decoder.Open(in, res);
	decoder.SetStreamLength(srcFile.Length()(цел32));
	нов(buffer);
	buffer.len := 4096;
	нов(buffer.data, 4096);

	encoder.Open(out, 44100, 16, 2, res);
	нцПока decoder.HasMoreData() и (res = ResOk) делай
		decoder.FillBuffer(buffer);
		encoder.Write(buffer, res);
		увел(bytesEncoded, buffer.len);
		если onConvertStatusChanged # НУЛЬ то
			convertStatus.bytesEncoded := bytesEncoded;
			onConvertStatusChanged(convertStatus);
		всё;
	кц;
	encoder.Close(res);
	возврат res;

кон Mp3ToWave;

проц GetFreeSpace*(конст destination: массив из симв8; перем freeSpace: цел32): целМЗ;
перем
	fs: Files.FileSystem;
	prefix, name: массив MaxLen из симв8;
	res: целМЗ;
нач
	res := ResErr;
	Files.SplitName(destination, prefix, name);
	fs := Files.This(prefix);
	если fs # НУЛЬ то
		freeSpace := (fs.vol.Available() DIV 1024) * fs.vol.blockSize;
		res := ResOk;
	всё;
	возврат res;
кон GetFreeSpace;

проц IsReadOnly*(конст destination: массив из симв8; перем readOnly: булево): целМЗ;
перем
	fs: Files.FileSystem;
	prefix, name: массив MaxLen из симв8;
	res: целМЗ;
нач
	res := ResErr;
	Files.SplitName(destination, prefix, name);
	fs := Files.This(prefix);
	если (fs # НУЛЬ) и (fs.vol # НУЛЬ) то
		readOnly := Files.ReadOnly в fs.vol.flags;
		res := ResOk;
	всё;
	возврат res;
кон IsReadOnly;

проц GetDevice*(file: Files.File; перем device: Disks.Device): целМЗ;
перем
	fs: Files.FileSystem;
	res: целМЗ;
нач
	res := ResErr;
	fs := file.fs;
	если (fs # НУЛЬ) и (fs.vol # НУЛЬ) то
		если fs.vol суть DiskVolumes.Volume то
			device := fs.vol(DiskVolumes.Volume).dev;
			res := ResOk;
		аесли fs.vol суть FATVolumes.Volume то
			device := fs.vol(FATVolumes.Volume).dev;
			res := ResOk;
		аесли fs.vol суть ISO9660Volumes.Volume то
			device := fs.vol(FATVolumes.Volume).dev;
			res := ResOk;
		всё;
	всё;
	возврат res;
кон GetDevice;

проц IsOnSameController*(device1, device2: Disks.Device): булево;
нач
	если (device1 суть ATADisks.Device) и (device2 суть ATADisks.Device) то
		возврат device1(ATADisks.Device).controller = device2(ATADisks.Device).controller;
	всё;
	возврат ложь;
кон IsOnSameController;

проц ClearBuffer*(перем buf: массив из симв8; ofs, len: размерМЗ);
перем
	adr: адресВПамяти;
	rem: размерМЗ;
нач
	утв((ofs+len) <= длинаМассива(buf));
	adr := адресОт(buf);
	увел(adr, ofs);
	rem := adr остОтДеленияНа 4;
	нцПока rem > 0 делай
		НИЗКОУР.запиши8битПоАдресу(adr, 0X);
		умень(rem); увел(adr); умень(len);
	кц;
	нцПока len >= 4 делай
		НИЗКОУР.запиши32битаПоАдресу(adr, 0H);
		увел(adr, 4); умень(len, 4);
	кц;
	нцПока len > 0 делай
		НИЗКОУР.запиши8битПоАдресу(adr, 0X);
		увел(adr); умень(len);
	кц;
кон ClearBuffer;

кон CDRecordUtils.
