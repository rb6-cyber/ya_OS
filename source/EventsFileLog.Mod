модуль EventsFileLog; (** AUTHOR "staubesv"; PURPOSE "Log system events to files"; *)
(**
 * Log system events to files
 *
 * History:
 *
 *	07.03.2007	First release (staubesv)
 *	31.07.2007	Added log file size limitation (staubesv)
 *)

использует
	Modules, Commands, Files, Events, EventsUtils;

конст

	DefaultLogFile = "AosEventLog.log";

	MaxLogFileSize = 1024 * 1024; (* Bytes, 1024 bytes space reserved *)

	Verbose = истина;

	(* System event classification *)
	Class = 1; 		(* Events *)
	Subclass = 1;	(* Logging *)

	ModuleName = "EventsFileLog";

тип

	EventLogger = окласс(Events.Sink);
	перем
		file : Files.File;
		w : Files.Writer;
		currentFileSize, maxFileSize : Files.Size;
		warned : булево;

		проц {перекрыта}Handle*(event : Events.Event);
		нач
			если currentFileSize < maxFileSize - 1024 то
				EventsUtils.ToStream(w, event);
				currentFileSize := file.Length();
			аесли ~warned то
				warned := истина;
				Events.AddEvent(ModuleName, Events.Warning, Class, Subclass, 0, "Event log file is full. Stop logging.", истина);
			всё;
		кон Handle;

		проц &Init*(file : Files.File; append : булево; maxFileSize : Files.Size);
		перем ofs : Files.Position;
		нач
			утв(file # НУЛЬ);
			name := ModuleName;
			сам.file := file;
			сам.maxFileSize := maxFileSize;
			если append то ofs := file.Length(); иначе ofs := 0; всё;
			currentFileSize := ofs;
			warned := ложь;
			Files.OpenWriter(w, file, ofs);
		кон Init;

	кон EventLogger;

перем
	eventLogger- : EventLogger;

проц OpenFile(конст filename : массив из симв8; append : булево) : Files.File;
перем file : Files.File;
нач
	если append то
		file := Files.Old(filename);
	иначе
		file := Files.New(filename);
		Files.Register(file);
	всё;
	возврат file;
кон OpenFile;

проц Start*(context : Commands.Context); (** [-append] [-max] [filename] ~ *)
перем
	filename : массив 256 из симв8; append : булево;
	file : Files.File;
нач {единолично}
	context.arg.ПропустиБелоеПоле;
	context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
	если filename[0] = "-" то
		если filename = "-append" то
			append := истина;
			context.arg.ПропустиБелоеПоле;
			context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(filename);
		иначе
			context.out.пСтроку8("Unexpected parameter"); context.out.пВК_ПС;
			возврат;
		всё;
	всё;
	если eventLogger = НУЛЬ то
		если filename = "" то filename := DefaultLogFile; всё;
		file := OpenFile(filename, append);
		если file # НУЛЬ то
			нов(eventLogger, file, append, MaxLogFileSize);
			Events.Register(eventLogger);
			Events.AddEvent(ModuleName, Events.Information, Class, Subclass, 0, "Started file log event logger", Verbose);
			context.out.пСтроку8("Logging system events to file "); context.out.пСтроку8(filename); context.out.пВК_ПС;
		иначе
		всё;
	иначе
		context.out.пСтроку8("Logger is already running."); context.out.пВК_ПС;
	всё;
кон Start;

проц Stop*(context : Commands.Context); (** ~ *)
нач {единолично}
	если eventLogger # НУЛЬ то
		Events.Unregister(eventLogger); eventLogger := НУЛЬ;
		Events.AddEvent(ModuleName, Events.Information, Class, Subclass, 0, "Stopped file log event logger", Verbose);
		context.out.пСтроку8("Logger stopped."); context.out.пВК_ПС;
	иначе
		context.out.пСтроку8("Logger is not running."); context.out.пВК_ПС;
	всё;
кон Stop;

проц Cleanup;
нач
	если eventLogger # НУЛЬ то
		Events.AddEvent(ModuleName, Events.Information, Class, Subclass, 0, "File log event logger shut down", Verbose);
		Events.Unregister(eventLogger); eventLogger := НУЛЬ;
	всё;
кон Cleanup;

нач
	Modules.InstallTermHandler(Cleanup);
кон EventsFileLog.

EventsFileLog.Start ~
EventsFileLog.Stop ~

EventsFileLog.Start -append ~

System.Free EventsFileLog ~
