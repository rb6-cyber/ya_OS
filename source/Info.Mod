(* Aos, Copyright 2001, Pieter Muller, ETH Zurich *)

модуль Info; (** AUTHOR "pjm/staubesv"; PURPOSE "System information"; *)

использует
	НИЗКОУР, Machine, Heaps, Objects, Потоки, Reflection, Modules, Commands, Options, Strings, D := Debugging, Kernel;

конст
	AddressSize = размер16_от(адресВПамяти);

	RecordBlock = 1;
	ProtRecBlock = 2;
	ArrayBlock = 3;
	SystemBlock = 4;

	MaxNofTypes = 2048;

	(* Analyzer.Sort *)
	SortNone = 0;
	SortByCount = 1;
	SortBySize = 2;
	SortByTotalSize = 3; (* whereas TotalSize = Count * Size *)
	SortByName = 4;
	AllocatorHistorySize = 4096; (* recent history of allocators to be able to trace memory wasting sources *)

тип

	Type = запись
		ptag : адресВПамяти;
		count : цел32;
		size : размерМЗ;
		type : цел8;
		pc: адресВПамяти;
	кон;

	Analyzer = окласс
	перем
		types : укль на массив из Type;
		nofElements : цел32;

		(* global statistics *)
		nofHeapBlocks, nofFreeBlocks, nofSystemBlocks, nofRecordBlocks, nofProtRecBlocks, nofArrayBlocks: цел32;
		sizeHeapBlocks, sizeFreeBlocks, sizeSystemBlocks, sizeRecordBlocks, sizeProtRecBlocks, sizeArrayBlocks: размерМЗ;

		проц &Init(size : цел32);
		нач
			утв(size > 0);
			нов(types, size);
			Reset;
		кон Init;

		проц Reset;
		перем i : размерМЗ;
		нач
			nofElements := 0;
			если (types # НУЛЬ) то
				нцДля i := 0 до длинаМассива(types)-1 делай
					types[i].ptag := Heaps.NilVal;
					types[i].count := 0;
					types[i].size := 0;
				кц;
			всё;
			nofHeapBlocks := 0; sizeHeapBlocks := 0;
			nofFreeBlocks := 0; sizeFreeBlocks := 0;
			nofSystemBlocks := 0; sizeSystemBlocks := 0;
			nofRecordBlocks := 0; sizeRecordBlocks := 0;
			nofProtRecBlocks := 0; sizeProtRecBlocks := 0;
			nofArrayBlocks := 0; sizeArrayBlocks := 0;
		кон Reset;

		проц SortBy(mode : цел32);
		перем i, j : цел32; temp : Type;

			проц IsGreaterThan(конст entry1, entry2 : Type; mode : цел32) : булево;
			перем name1, name2: массив 256 из симв8; count1,count2, size1, size2: размерМЗ;
			нач
				если mode = SortByName то
					GetName(entry1.ptag,name1);
					GetName(entry2.ptag,name2);
					возврат name1 > name2;
				иначе

					count1 := entry1.count;
					size1 := entry1.size DIV count1;

					count2 := entry2.count;
					size2 := entry2.size DIV count2;

				возврат
					((mode = SortByCount) и (count1 > count2)) или
					((mode = SortBySize) и (size1 > size2)) или
					((mode = SortByTotalSize) и (size1*count1 > size2 * count2))
					;
				всё;

			кон IsGreaterThan;

		нач
			утв((mode = SortByCount) или (mode = SortBySize) или (mode = SortByTotalSize) или (mode=SortByName));
			(* sort descending... *)
			нцДля i := 0 до nofElements-1 делай
				нцДля j := 1 до nofElements-1 делай
					если IsGreaterThan(types[j], types[j-1], mode) то
						temp := types[j-1];
						types[j-1] := types[j];
						types[j] := temp;
					всё;
				кц;
			кц;
		кон SortBy;

		проц Add(конст block : Heaps.HeapBlock; byPC: булево);
		перем type: цел8;


			проц AddByType(type: цел8);
			перем tag: адресВПамяти; i: цел32;
			нач
				НИЗКОУР.прочтиОбъектПоАдресу(block.dataAdr + Heaps.TypeDescOffset, tag);
				i := 0; нцПока (i < длинаМассива(types)) и (i < nofElements) и (types[i].ptag # tag) делай увел(i) кц;
				если (i < nofElements) то
					увел(types[i].count);
					увел(types[i].size, block.size);
				аесли (i = nofElements) и (i < длинаМассива(types)) то
					types[i].ptag := tag;
					types[i].count := 1;
					types[i].size := block.size;
					types[i].type := type;
					types[i].pc := 0;
					увел(nofElements)
				всё;
			кон AddByType;

			проц AddByPC(type: цел8);
			перем pc: адресВПамяти; i: цел32;
			нач
				НИЗКОУР.прочтиОбъектПоАдресу(block.dataAdr + Heaps.HeapBlockOffset, pc);
				НИЗКОУР.прочтиОбъектПоАдресу(pc + Heaps.HeapBlockOffset, pc);
				если pc # 0 то
					i := 0; нцПока (i < длинаМассива(types)) и (i < nofElements) и (types[i].pc # pc) делай увел(i) кц;
					если (i < nofElements) то
						увел(types[i].count);
						увел(types[i].size, block.size);
					аесли (i = nofElements) и (i < длинаМассива(types)) то
						types[i].ptag := 0;
						types[i].count := 1;
						types[i].size := block.size;
						types[i].type := type;
						types[i].pc := pc;
						увел(nofElements)
					всё;
				всё;
			кон AddByPC;



		нач
			увел(nofHeapBlocks); увел(sizeHeapBlocks, block.size);
			если (block суть Heaps.RecordBlock) или (block суть Heaps.ProtRecBlock) или (block суть Heaps.ArrayBlock) то
				если (block суть Heaps.ProtRecBlock) то
					type := ProtRecBlock;
					увел(nofProtRecBlocks); увел(sizeProtRecBlocks, block.size);
				аесли (block суть Heaps.RecordBlock) то
					type := RecordBlock;
					увел(nofRecordBlocks); увел(sizeRecordBlocks, block.size);
				аесли (block суть Heaps.ArrayBlock) то
					type := ArrayBlock;
					увел(nofArrayBlocks); увел(sizeArrayBlocks, block.size);
				иначе
					СТОП(99);
				всё;
				если byPC то
					AddByPC(type)
				иначе
					(* all these heap blocks have a type tag *)
					AddByType(type)
				всё;
			аесли (block суть Heaps.SystemBlock) то
				увел(nofSystemBlocks); увел(sizeSystemBlocks, block.size);
				(* system blocks do not have a type tag *)
				AddByPC(SystemBlock);
			аесли (block суть Heaps.FreeBlock) то
				увел(nofFreeBlocks); увел(sizeFreeBlocks, block.size);
			всё;
		кон Add;

		проц ShowBlocks(конст mask : массив из симв8; out : Потоки.Писарь);
		перем
			module : Modules.Module; typedesc : Modules.TypeDesc;
			size, totalSize: размерМЗ;
			startpc: адресВПамяти;
			i, selected, total : цел32;
			string : массив 256 из симв8; copy: массив 256 из симв8;
		нач
			утв(out # НУЛЬ);
			size := 0; totalSize := 0;
			selected := 0; total := 0;
			нцДля i := 0 до nofElements-1 делай
				увел(total, types[i].count);
				module := НУЛЬ;
				если (types[i].pc # 0) то
					module := Modules.ThisModuleByAdr(types[i].pc);
				аесли (types[i].ptag # 0) то
					Modules.ThisTypeByAdr(types[i].ptag, module, typedesc);
				всё;
				если (module # НУЛЬ)  то
					если (types[i].ptag # 0) то
						string := "";
						копируйСтрокуДо0(module.name,copy);
						Strings.AppendX(string, copy);
						Strings.AppendX(string, ".");
						копируйСтрокуДо0(typedesc.name,copy);
						Strings.AppendX(string, copy);
					иначе
						string := "";
						копируйСтрокуДо0(module.name,copy);
						Strings.AppendX(string, copy);
						Strings.AppendX(string, ".");
						Reflection.GetProcedureName(types[i].pc, copy,startpc);
						Strings.AppendX(string, copy);
						Strings.Append(string,":");
						Strings.IntToStr(цел32(types[i].pc - startpc), copy);
						Strings.Append(string,copy);
					всё;

					если Strings.Match(mask, string) то
						просей types[i].type из
							|RecordBlock: out.пСтроку8("R ");
							|ProtRecBlock: out.пСтроку8("P ");
							|ArrayBlock: out.пСтроку8("A ");
							|SystemBlock: out.пСтроку8("S ");
						иначе
							out.пСтроку8("U ");
						всё;
						увел(selected, types[i].count);
						out.пЦел64(types[i].count, 8); out.пСимв8(" ");
						увел(size, types[i].size);
						out.пЦел64(types[i].size DIV types[i].count, 6); out.пСтроку8("B ");
						out.пЦел64(types[i].size, 10); out.пСтроку8("B ");
						out.пСтроку8(string);
						out.пСтроку8(" (total ");
						WriteB(types[i].size, out); out.пСтроку8(")"); out.пВК_ПС
					всё;
				всё;
			кц;
			out.пВК_ПС;
			если (selected # total) то
				out.пСтроку8("Selected "); out.пЦел64(selected, 1); out.пСтроку8(" of ");
				out.пЦел64(total, 1); out.пСтроку8(" dynamic records of ");
				out.пЦел64(nofElements, 1); out.пСтроку8(" unique types (total size : ");
				WriteB(size, out); out.пСтроку8(" of "); WriteB(totalSize, out); out.пСтроку8(")");
				out.пВК_ПС;
			иначе
				out.пЦел64(total, 1); out.пСтроку8(" dynamic records of ");
				out.пЦел64(nofElements, 1); out.пСтроку8(" unique types found");
				out.пСтроку8(" (total size : "); WriteB(sizeHeapBlocks, out); out.пСтроку8(")");
				out.пВК_ПС;
			всё;
		кон ShowBlocks;

		проц Show(out : Потоки.Писарь; конст mask : массив из симв8; sortMode : цел32; byPC: булево);
		перем nofUsedBlocks, sizeUsedBlocks :размерМЗ;

			проц ShowBlock(конст name : массив из симв8; nofBlocks: размерМЗ; size: размерМЗ; totalNofBlocks: размерМЗ; totalSize : размерМЗ; out : Потоки.Писарь);
			нач
				out.пЦел64(nofBlocks, 8); out.пСимв8(" "); ShowPercent(nofBlocks, totalNofBlocks, out); out.пСимв8(" ");
				out.пСтроку8(name);
				out.пСтроку8(" ("); WriteB(size, out); out.пСтроку8(", "); ShowPercent(size, totalSize, out); out.пСтроку8(")");
				out.пВК_ПС;
			кон ShowBlock;

			проц ShowPercent(cur, max : размерМЗ; out : Потоки.Писарь);
			перем percent : цел32;
			нач
				если (max > 0) то
					percent := округлиВниз(100 * (cur / max) + 0.5);
				иначе
					percent := 0;
				всё;
				если (percent < 10) то out.пСтроку8("  ");
				аесли (percent < 100) то out.пСимв8(" ");
				всё;
				out.пЦел64(percent, 0); out.пСимв8("%");
			кон ShowPercent;

		нач
			утв(out # НУЛЬ);
			nofUsedBlocks := nofHeapBlocks - nofFreeBlocks;
			sizeUsedBlocks := sizeHeapBlocks - sizeFreeBlocks;
			out.пСимв8(0EX); (* non-proportional font *)
			ShowBlock("HeapBlocks", nofHeapBlocks, sizeHeapBlocks, nofHeapBlocks, sizeHeapBlocks, out);
			ShowBlock("UsedBlocks", nofUsedBlocks, sizeUsedBlocks, nofHeapBlocks, sizeHeapBlocks, out);
			ShowBlock("FreeBlocks", nofFreeBlocks, sizeFreeBlocks, nofHeapBlocks, sizeHeapBlocks, out);
			out.пВК_ПС;
			ShowBlock("UsedBlocks", nofUsedBlocks, sizeUsedBlocks, nofUsedBlocks, sizeUsedBlocks, out);
			ShowBlock("SystemBlocks", nofSystemBlocks, sizeSystemBlocks, nofUsedBlocks, sizeUsedBlocks, out);
			ShowBlock("RecordBlocks", nofRecordBlocks, sizeRecordBlocks, nofUsedBlocks, sizeUsedBlocks, out);
			ShowBlock("ProtRectBlocks", nofProtRecBlocks, sizeProtRecBlocks, nofUsedBlocks, sizeUsedBlocks, out);
			ShowBlock("ArrayBlocks", nofArrayBlocks, sizeArrayBlocks, nofUsedBlocks, sizeUsedBlocks, out);

			если (mask # "") то
				out.пВК_ПС;
				если (sortMode = SortByCount) или (sortMode = SortBySize) или (sortMode = SortByTotalSize) или (sortMode = SortByName) то
					SortBy(sortMode);
				всё;
				ShowBlocks(mask, out);
			всё;
			out.пСимв8(0FX); (* proportional font *)
		кон Show;

	кон Analyzer;

перем
	currentMarkValueAddress : адресВПамяти;
	recentAllocators*: массив AllocatorHistorySize из запись pc*: массив 3 из адресВПамяти; time*: цел64 кон;
	numRecentAllocators*: цел32;

проц LogAlloc(p: динамическиТипизированныйУкль);
перем time: цел64; bp: адресВПамяти; i: цел32;pc: адресВПамяти;
нач
	time := Machine.GetTimer();

	bp := НИЗКОУР.GetFramePointer();

	НИЗКОУР.прочтиОбъектПоАдресу(bp+размер16_от(адресВПамяти),pc);

	нцДля i := 0 до длинаМассива(recentAllocators[numRecentAllocators].pc)-1 делай
		recentAllocators[numRecentAllocators].pc[i] := pc;
		если bp # 0 то
			НИЗКОУР.прочтиОбъектПоАдресу(bp, bp);
			НИЗКОУР.прочтиОбъектПоАдресу(bp+размер16_от(адресВПамяти),pc);
		всё;
	кц;

	recentAllocators[numRecentAllocators].time := time;
	увел(numRecentAllocators); numRecentAllocators := numRecentAllocators остОтДеленияНа длинаМассива(recentAllocators);
кон LogAlloc;


проц WriteB(b : размерМЗ; out : Потоки.Писарь);
перем shift : цел32; suffix : массив 2 из симв8;
нач
	если b < 100*1024 то suffix := ""; shift := 0
	аесли b < 100*1024*1024 то suffix := "K"; shift := -10
	иначе suffix := "M"; shift := -20
	всё;
	если b # арифмСдвиг(арифмСдвиг(b, shift), -shift) то out.пСимв8("~") всё;
	out.пЦел64(арифмСдвиг(b, shift), 1);
	если истина то
		out.пСтроку8(suffix); out.пСимв8("B")
	иначе
		out.пСимв8(" ");
		out.пСтроку8(suffix); out.пСтроку8("byte");
		если b # 1 то out.пСимв8("s") всё
	всё
кон WriteB;

(** Show the details of the specified module. *)

проц ModuleDetails*(context : Commands.Context); (** [Options] module ~ *)
перем
	m : Modules.Module; i: размерМЗ; j, k: цел32;
	p, procAdr: адресВПамяти;
	adr : адресВПамяти;
	modn : массив 33 из симв8;
	options : Options.Options;
нач
	нов(options);
	options.Add("d", "details", Options.Flag);
	если options.Parse(context.arg, context.error) то
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modn);
		m := Modules.root;
		нцПока (m # НУЛЬ) и (m.name # modn) делай m := m.next кц;
		если m # НУЛЬ то
			context.out.пСтроку8(m.name);
			context.out.пСтроку8(" refcnt = "); context.out.пЦел64(m.refcnt, 1);
			context.out.пСтроку8(" sb ="); context.out.п16ричное(m.sb, 9);
			context.out.пСтроку8(" dataSize = "); context.out.пЦел64(длинаМассива(m.data), 1);
			context.out.пСтроку8(" codeSize = "); context.out.пЦел64(длинаМассива(m.code), 1);
			context.out.пСтроку8(" refSize = "); context.out.пЦел64(длинаМассива(m.refs), 1);
			context.out.пСтроку8(" entries = "); context.out.пЦел64(длинаМассива(m.entry), 1);
			context.out.пСтроку8(" commands = "); context.out.пЦел64(длинаМассива(m.command), 1);
			context.out.пСтроку8(" modules = "); context.out.пЦел64(длинаМассива(m.module), 1);
			context.out.пСтроку8(" types = "); context.out.пЦел64(длинаМассива(m.typeInfo), 1);
			context.out.пСтроку8(" pointers = "); context.out.пЦел64(длинаМассива(m.ptrAdr), 1);
			context.out.пВК_ПС; context.out.пСтроку8("  ptrAdr:");
			нцДля i := 0 до длинаМассива(m.ptrAdr)-1 делай
				context.out.пСимв8(" "); context.out.пЦел64(m.ptrAdr[i]-m.sb, 1)
			кц;
			context.out.пВК_ПС;
			если options.GetFlag("details") то
				context.out.пСтроку8("Pointer Details: ");
				если (m.ptrAdr # НУЛЬ) то
					context.out.пВК_ПС;
					нцДля i := 0 до длинаМассива(m.ptrAdr) - 1 делай
						context.out.пЦел64(i, 0); context.out.пСтроку8(": ");
						context.out.пАдресВПамяти(m.ptrAdr[i]); context.out.пСтроку8(" -> ");
						НИЗКОУР.прочтиОбъектПоАдресу(m.ptrAdr[i], adr);
						context.out.пАдресВПамяти(adr);
						context.out.пВК_ПС;
					кц;
				иначе
					context.out.пСтроку8("none"); context.out.пВК_ПС;
				всё;
			всё;
			нцДля i := 0 до длинаМассива(m.typeInfo) - 1 делай
				context.out.пВК_ПС; context.out.пСтроку8("  type:");
				context.out.п16ричное(m.typeInfo[i].tag, 9);
				context.out.пСимв8(" "); context.out.пСтроку8(m.typeInfo[i].name);
				context.out.п16ричное(размерМЗ(m.typeInfo[i].flags), 9);
					(* type descriptor info *)
				context.out.пВК_ПС; context.out.пСтроку8("  typedesc1:");
				p := m.typeInfo[i].tag; (* address of static type descriptor *)
				нцДо
					НИЗКОУР.прочтиОбъектПоАдресу(p, k);
					если матМодуль(k) <= 4096 то context.out.пСимв8(" "); context.out.пЦел64(k, 1)
					иначе context.out.п16ричное(k, 9)
					всё;
					увел(p, AddressSize)
				кцПри k < -40000000H;
					(* methods *)
				context.out.пВК_ПС; context.out.пСтроку8("  typedescmths:");
				p := НИЗКОУР.подмениТипЗначения(адресВПамяти, m.typeInfo[i].tag) + Modules.Mth0Ofs;
				j := 0;
				НИЗКОУР.прочтиОбъектПоАдресу(p, procAdr);
				нцПока procAdr # Heaps.MethodEndMarker делай
					context.out.пВК_ПС; context.out.пЦел64(j, 3); context.out.пСимв8(" ");
					Reflection.WriteProc(context.out, procAdr);
					умень(p, AddressSize);
					НИЗКОУР.прочтиОбъектПоАдресу(p, procAdr);
					увел(j)
				кц
			кц;
			context.out.пВК_ПС
		всё;
	всё;
кон ModuleDetails;

(** Find a procedure, given the absolute PC address. *)

проц ModulePC*(context : Commands.Context);	(** pc *)
перем pc : цел32;
нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦел32(pc, ложь);
	если Modules.ThisModuleByAdr(pc) # НУЛЬ то
		Reflection.WriteProc(context.out, pc);
	иначе
		context.out.п16ричное(pc, 8); context.out.пСтроку8(" not found")
	всё;
	context.out.пВК_ПС;
кон ModulePC;

проц AllObjects*(context : Commands.Context); (** [Options] mask ~ *)
перем
	options : Options.Options; sortMode : цел32;
	analyzer : Analyzer;
	memBlock {неОтслСборщиком}: Machine.MemoryBlock;
	heapBlock : Heaps.HeapBlock;
	p : адресВПамяти;
	mask : массив 128 из симв8;
нач
	нов(options);
	options.Add("s", "sort", Options.Integer);
	options.Add(0X, "pc", Options.Flag);
	options.Add(0X, "gc", Options.Flag);
	если options.Parse(context.arg, context.error) то
		если ~options.GetInteger("sort", sortMode) то sortMode := SortNone; всё;
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(mask);
		нов(analyzer, MaxNofTypes);
		если options.GetFlag("gc") то Heaps.LazySweepGC всё; (* slight inaccuracy here: other processes can kick in now *)
		Machine.Acquire(Machine.Heaps);
		Heaps.FullSweep(); (* the heap might contain wrong pointers in the freed part *)
		memBlock := Machine.memBlockHead;
		нцПока memBlock # НУЛЬ делай
			p := memBlock.beginBlockAdr;
			нцПока p # memBlock.endBlockAdr делай
				heapBlock := НИЗКОУР.подмениТипЗначения(Heaps.HeapBlock, p + Heaps.BlockHeaderSize); (* get heap block *)
				analyzer.Add(heapBlock, options.GetFlag("pc"));
				p := p + heapBlock.size
			кц;
			memBlock := memBlock.next
		кц;
		Machine.Release(Machine.Heaps);
		analyzer.Show(context.out, mask, sortMode, options.GetFlag("pc"));
	всё;
кон AllObjects;

проц ShowRecentAllocators*(out: Потоки.Писарь; scale: цел64);
перем
	i,from,to,num, pcs: цел32;
	pc,startpc: адресВПамяти;
	module: Modules.Module; name: массив 256 из симв8;
	time: цел64;
	timer: Kernel.MilliTimer;
нач
	time := Machine.GetTimer();
	если scale <= 0 то
	   Kernel.SetTimer( timer, 100 );  scale := Machine.GetTimer();
	   нцПока ~Kernel.Expired( timer ) делай кц;
        scale := (Machine.GetTimer() - scale) DIV 100; (* 1 ms resolution *)
	всё;
	out.пСтроку8("----------- recent allocators, t = ");
	out.п16ричное(recentAllocators[i].time, -16);
	out.пСтроку8(" ---------------"); out.пВК_ПС;
	Machine.Acquire(Machine.Heaps);
	i := numRecentAllocators;
	умень(i); i := i остОтДеленияНа длинаМассива(recentAllocators);
	from := i;
	pc := recentAllocators[i].pc[0];
	нцПока (i # numRecentAllocators) и (pc # 0) делай
		умень(i); i := i остОтДеленияНа длинаМассива(recentAllocators);
		pc := recentAllocators[i].pc[0];
	кц;
	to := i;
	Machine.Release(Machine.Heaps);
	i := from; num := 0;
	нцПока i # to делай

		out.пЦел64(num,1); out.пСтроку8(": ");
		out.п16ричное(recentAllocators[i].time, -16);

		out.пСтроку8("(");
		out.пЦел64( устарПреобразуйКБолееУзкомуЦел((recentAllocators[i].time-time) DIV scale), 1);
		out.пСтроку8(")");

		out.пСтроку8(": ");

		нцДля pcs := 0 до длинаМассива(recentAllocators[i].pc)-1 делай
			pc := recentAllocators[i].pc[pcs];

			module := Modules.ThisModuleByAdr(pc);
			out.пСтроку8(module.name);
			out.пСтроку8(".");
			Reflection.GetProcedureName(pc, name,startpc);
			out.пСтроку8(name);
			out.пСтроку8(":");
			out.пЦел64(pc-startpc,1);
			out.пСтроку8(" ");
		кц;
		out.пВК_ПС;
		умень(i); i := i остОтДеленияНа длинаМассива(recentAllocators);
		увел(num);
	кц;
кон ShowRecentAllocators;

проц ClearRecentAllocators*;
перем i: цел32;
нач
	Machine.Acquire(Machine.Heaps);
	i := (numRecentAllocators - 1) остОтДеленияНа длинаМассива(recentAllocators);
	recentAllocators[i].pc[0] := 0;
	Machine.Release(Machine.Heaps);
кон ClearRecentAllocators;

проц AddAllocatorLogger*;
нач
	Heaps.SetAllocationLogger(LogAlloc);
кон AddAllocatorLogger;


проц RecentAllocators*(context : Commands.Context); (** [Options] mask ~ *)
перем
	options : Options.Options;
	scale: цел64;
	num: цел32;
нач
	нов(options);
	options.Add("c", "clear", Options.Flag);
	options.Add("s", "scale", Options.Integer);
	если options.Parse(context.arg, context.error) то
		если options.GetInteger("scale", num) и (num > 0 ) то
			scale := num
		иначе (* autoscale to ms *)
			scale := 0;
	     всё;
	     ShowRecentAllocators(context.out, scale);
		если options.GetFlag("clear") то ClearRecentAllocators всё;
	всё;
кон RecentAllocators;

проц TraceModule*(context : Commands.Context); (** moduleName mask ~ *)
перем
	options : Options.Options; sortMode : цел32;
	analyzer : Analyzer;
	mask : массив 128 из симв8;
	moduleName : Modules.Name; module : Modules.Module;
нач
	нов(options);
	options.Add("s", "sort", Options.Integer);
	если options.Parse(context.arg, context.error) то
		если ~options.GetInteger("sort", sortMode) то sortMode := SortNone; всё;
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(moduleName);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(mask);
		module := Modules.ModuleByName(moduleName);
		если (module # НУЛЬ) то
			нов(analyzer, MaxNofTypes);
			Machine.Acquire(Machine.Heaps);
			IncrementCurrentMarkValue;
			module.FindRoots;
			AnalyzeMarkedBlocks(analyzer);
			Machine.Release(Machine.Heaps);
			context.out.пСтроку8("Heap block referenced by module "); context.out.пСтроку8(moduleName); context.out.пСимв8(":");
			context.out.пВК_ПС;
			analyzer.Show(context.out, mask, sortMode, ложь);
		иначе
			context.error.пСтроку8("Module "); context.error.пСтроку8(moduleName); context.error.пСтроку8(" is not loaded."); context.error.пВК_ПС;
		всё;
	всё;
кон TraceModule;

проц TraceReference*(context : Commands.Context); (** ModuleName.VariableName mask ~ *)
перем
	options : Options.Options; sortMode : цел32;
	analyzer : Analyzer; address : адресВПамяти;
	module : Modules.Module; variable : Reflection.Variable;
	mask, modVar : массив 256 из симв8; array : Strings.StringArray;
	varName : массив 64 из симв8;
нач
	нов(options);
	options.Add("s", "sort", Options.Integer);
	если options.Parse(context.arg, context.error) то
		если ~options.GetInteger("sort", sortMode) то sortMode := SortNone; всё;
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(modVar);
		context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(mask);
		array := Strings.Split(modVar, ".");
		если (длинаМассива(array) = 2) то
			module := Modules.ModuleByName(array[0]^);
			если (module # НУЛЬ) то
				копируйСтрокуДо0(array[1]^, varName);
				если Reflection.FindVar(module, varName, variable) то
					если (variable.type = 13) или (variable.type = 29) то
						нов(analyzer, MaxNofTypes);
						context.out.пСтроку8("Heap blocks reference by variable "); context.out.пСтроку8(modVar);
						context.out.пСимв8(":"); context.out.пВК_ПС;
						если (variable.adr # 0) то
							НИЗКОУР.прочтиОбъектПоАдресу(variable.adr, address);
							MarkReference(analyzer, НИЗКОУР.подмениТипЗначения(динамическиТипизированныйУкль, address));
							analyzer.Show(context.out, mask, sortMode, ложь);
						всё;
					иначе
						context.error.пСтроку8("Variable is not a pointer"); context.error.пВК_ПС;
					всё;
				иначе
					context.error.пСтроку8("Variable "); context.error.пСтроку8(array[1]^); context.error.пСтроку8(" not found");
					context.error.пВК_ПС;
				всё;
			иначе
				context.error.пСтроку8("Module "); context.error.пСтроку8(array[0]^); context.error.пСтроку8(" not found");
				context.error.пВК_ПС;
			всё;
		иначе
			context.error.пСтроку8("Expected ModuleName.VariableName parameter"); context.error.пВК_ПС;
		всё;
	всё;
кон TraceReference;

проц MarkReference(analyzer : Analyzer; ref : динамическиТипизированныйУкль);
нач
	утв(analyzer # НУЛЬ);
	Machine.Acquire(Machine.Heaps);
	IncrementCurrentMarkValue;
	Heaps.Mark(ref);
	AnalyzeMarkedBlocks(analyzer);
	Machine.Release(Machine.Heaps);
кон MarkReference;

проц TraceProcessID*(context : Commands.Context); (** ProcessID mask ~ *)
перем
	options : Options.Options; sortMode : цел32;
	analyzer : Analyzer;
	process : Objects.Process;
	processID : цел32; mask : массив 256 из симв8;
нач
	нов(options);
	options.Add("s", "sort", Options.Integer);
	если options.Parse(context.arg, context.error) то
		если ~options.GetInteger("sort", sortMode) то sortMode := SortNone; всё;
		если context.arg.ПропустиБелоеПолеИЧитайЦел32(processID, ложь) то
			context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках(mask);
			process := FindProcessByID(processID);
			если (process # НУЛЬ) то
				нов(analyzer, MaxNofTypes);
				Machine.Acquire(Machine.Heaps);
				IncrementCurrentMarkValue;
				process.FindRoots;
				Heaps.CheckCandidates;
				AnalyzeMarkedBlocks(analyzer);
				Machine.Release(Machine.Heaps);
				context.out.пСтроку8("Heap blocks referenced by process ID = "); context.out.пЦел64(processID, 0); context.out.пСимв8(":");
				context.out.пВК_ПС;
				analyzer.Show(context.out, mask, sortMode, ложь);
			иначе
				context.error.пСтроку8("Process ID = "); context.error.пЦел64(processID, 0); context.error.пСтроку8(" not found");
				context.error.пВК_ПС;
			всё;
		иначе
			context.error.пСтроку8("Expected ProcessID parameter"); context.error.пВК_ПС;
		всё;
	всё;
кон TraceProcessID;

проц FindProcessByID(id : цел32) : Objects.Process;
перем
	memBlock {неОтслСборщиком}: Machine.MemoryBlock;
	heapBlock {неОтслСборщиком}: Heaps.HeapBlock;
	blockAdr, tag : адресВПамяти;
	process : Objects.Process;
	i : цел32;
нач
	i := 0;
	Machine.Acquire(Machine.Heaps);
	process := НУЛЬ;
	memBlock := Machine.memBlockHead;
	нцПока (memBlock # НУЛЬ) и (process = НУЛЬ) делай
		blockAdr := memBlock.beginBlockAdr;
		нцПока (blockAdr # memBlock.endBlockAdr) и (process = НУЛЬ) делай
			heapBlock := НИЗКОУР.подмениТипЗначения(Heaps.HeapBlock, blockAdr + Heaps.BlockHeaderSize);
			если (heapBlock суть Heaps.RecordBlock) то
				НИЗКОУР.прочтиОбъектПоАдресу(heapBlock.dataAdr + Heaps.TypeDescOffset, tag);
				если (tag = НИЗКОУР.дайМетаданныеВремениВыполненияДляТипа(Objects.Process)) то
					process := НИЗКОУР.подмениТипЗначения(Objects.Process, heapBlock.dataAdr);
					если (process.id # id) то process := НУЛЬ; всё;
				всё;
			всё;
			blockAdr := blockAdr + heapBlock.size
		кц;
		memBlock := memBlock.next
	кц;
	Machine.Release(Machine.Heaps);
	возврат process;
кон FindProcessByID;

(* Caller MUST hold Machine.Heaps lock!! *)
проц AnalyzeMarkedBlocks(analyzer : Analyzer);
перем
	memBlock {неОтслСборщиком}: Machine.MemoryBlock;
	heapBlock : Heaps.HeapBlock;
	currentMarkValue : цел32;
	blockAdr : адресВПамяти;
	mark : цел32;
нач
	утв(analyzer # НУЛЬ);
	currentMarkValue := GetCurrentMarkValue();
	memBlock := Machine.memBlockHead;
	нцПока memBlock # НУЛЬ делай
		blockAdr := memBlock.beginBlockAdr;
		нцПока blockAdr # memBlock.endBlockAdr делай
			heapBlock := НИЗКОУР.подмениТипЗначения(Heaps.HeapBlock, blockAdr + Heaps.BlockHeaderSize); (* get heap block *)
			mark := НИЗКОУР.прочти32битаПоАдресу(blockAdr + Heaps.BlockHeaderSize); (* access to private field heapBlock.mark *)
			если (mark = currentMarkValue) то
				analyzer.Add(heapBlock, ложь);
				НИЗКОУР.запиши32битаПоАдресу(blockAdr + Heaps.BlockHeaderSize, currentMarkValue - 1);
			всё;
			blockAdr := blockAdr + heapBlock.size
		кц;
		memBlock := memBlock.next
	кц;
	SetCurrentMarkValue(currentMarkValue - 1); (* restore Heaps.currentMarkValue *)
кон AnalyzeMarkedBlocks;

проц WriteType(adr : цел32; out : Потоки.Писарь);
перем m : Modules.Module;  t : Modules.TypeDesc; name: массив 256 из симв8;
нач
	Modules.ThisTypeByAdr(adr, m, t);
	если m # НУЛЬ то
		out.пСтроку8(m.name);  out.пСимв8(".");
		если (t # НУЛЬ) то
			если t.name = "" то out.пСтроку8("TYPE") иначе
				копируйСтрокуДо0(t.name,name);
			out.пСтроку8(name) всё
		иначе
			out.пСтроку8("NOTYPEDESC");
		всё;
	иначе
		out.пСтроку8("NIL")
	всё
кон WriteType;

проц GetName(adr: адресВПамяти; перем name: массив из симв8);
перем  m : Modules.Module;  t : Modules.TypeDesc;
нач
	Modules.ThisTypeByAdr(adr, m, t);
	name := "";
	если m # НУЛЬ то
		копируйСтрокуДо0(m.name,name);
		если (t # НУЛЬ) то
			Strings.Append(name,".");
			Strings.Append(name,t.name);
		всё;
	всё;
кон GetName;

(* Access to private field Heaps.currentMarkValue *)

проц GetCurrentMarkValue() : цел32;
нач
	возврат НИЗКОУР.прочти32битаПоАдресу(currentMarkValueAddress);
кон GetCurrentMarkValue;

проц SetCurrentMarkValue(value : цел32);
нач
	НИЗКОУР.запиши32битаПоАдресу(currentMarkValueAddress, value);
кон SetCurrentMarkValue;

проц IncrementCurrentMarkValue;
нач
	SetCurrentMarkValue(GetCurrentMarkValue() + 1);
кон IncrementCurrentMarkValue;

проц GetCurrentMarkValueAddress() : адресВПамяти;
перем address : адресВПамяти; module : Modules.Module; variable : Reflection.Variable;
нач
	address := Heaps.NilVal;
	module := Modules.ModuleByName("Heaps");
	утв(module # НУЛЬ);
	если (module # НУЛЬ) то
		если Reflection.FindVar(module, "currentMarkValue", variable) то
			(*
			ASSERT(variable.n = 1); (* currentMarkValue is not an array *)
			ASSERT(variable.type = 6); (*? type is SIGNED32, currently no support for 64-bit addresses *)
			*)
			address := variable.adr;
		иначе СТОП(100);
		всё;
	всё;
	возврат address;
кон GetCurrentMarkValueAddress;

проц Terminate;
нач
	если Heaps.allocationLogger = LogAlloc то Heaps.SetAllocationLogger(НУЛЬ) всё;
кон Terminate;

нач
	currentMarkValueAddress := GetCurrentMarkValueAddress();
	утв(currentMarkValueAddress # Heaps.NilVal);
	Modules.InstallTermHandler(Terminate);
кон Info.

System.Free Info ~

Debugging.DisableGC
Debugging.EnableGC
Compiler.Compile -p=Win32 FoxIntermediateBackend.Mod ~
Info.AllObjects ~
Info.AllObjects * ~

(* view by type *)
Info.AllObjects --sort=0 * ~ sort by none
Info.AllObjects --sort=1 * ~ sort by count
Info.AllObjects --sort=2 * ~ sort by size
Info.AllObjects --sort=3 * ~ sort by total size
Info.AllObjects --sort=4 * ~ sort by name

(* view by allocation pc *)
Info.AllObjects --pc --sort=0 * ~ sort by none
Info.AllObjects --pc --sort=1 * ~ sort by count
Info.AllObjects --pc --sort=2 * ~ sort by size
Info.AllObjects --pc --sort=3 * ~ sort by total size
Info.AllObjects --pc --sort=4 * ~ sort by name




Info.TraceModule PET ~

Info.TraceModule Info ~
Info.TraceModule FoxIntermediateBackend * ~

Info.TraceReference HotKeys.hotkeys ~
Info.TraceReference HotKeys.hotkeys * ~

Info.TraceProcessID 7180 * ~

Info.ModuleDetails -d Modules  ~

System.CollectGarbage ~


Compiler.Compile --symbolFilePrefix=/temp/objEO/ --objectFilePrefix=/temp/objEO/  Info.Mod ~

Info.AddAllocatorLogger
Info.RecentAllocators --clear   --scale=2400000 ~
Info.RecentAllocators    --scale=2400000 ~
Info.RecentAllocators --clear ~
Info.ClearRecentAllocators  ~

