модуль FileHandlers;	(** AUTHOR "negelef"; PURPOSE "Open files according to file handlers"; *)

использует
	Files, Configuration, Strings, Commands, Потоки;

(** Opens the file path; corresponding file handlers are specified in Configuration.XML *)
проц OpenFile* (конст path : массив из симв8; err: Потоки.Писарь; caller: окласс);
перем config,filehandler: массив 128 из симв8; name,filename, ext : Files.FileName; index: размерМЗ; res : целМЗ;
	context: Commands.Context; arg: Потоки.ЧтецИзСтроки; in: Потоки.Чтец; out: Потоки.Писарь;
нач
	Files.SplitExtension(path, name, ext);
	index := Strings.Find (ext, 0, '@');
	если index >= 0 то Strings.Truncate (ext, index); всё;
	Strings.LowerCase(ext);
	config := "Filehandlers.";
	(* get the right handler *)
	Strings.Append(config, ext);
	Strings.Append(config, ".Open");
	Configuration.Get(config, filehandler, res);

	если (res # Configuration.Ok) то
		если err # НУЛЬ то err.пСтроку8 ("Opening '"); err.пСтроку8 (ext); err.пСтроку8 ("' files not specified in Configuration.XML."); err.пВК_ПС всё;
		возврат
	всё;

	(* construct the context *)
	context := Commands.GetContext ();
	если context = НУЛЬ то
		in := НУЛЬ; out := НУЛЬ
	иначе
		in := context.in; out := context.out
	всё;
	filename := '"';
	Strings.Append(filename,path);
	Strings.Append(filename,'"');
	если index >= 0 то ext[index] := '@'; Strings.Move (ext, index, Strings.Length (ext) - index, ext, 0); Strings.Append (filename, ext); всё;
	нов (arg, длинаМассива (filename)); arg.ПримиСтроку8ДляЧтения (filename);
	нов (context, in, arg, out, err, caller);

	(* call the command *)
	Commands.Activate (filehandler, context, {}, res, name);
	если res # Commands.Ok то
		если err # НУЛЬ то err.пСтроку8 (name); err.пВК_ПС всё
	всё;
кон OpenFile;

(* Open a file *)
проц Open* (context: Commands.Context);
перем path: массив Files.NameLength из симв8;
нач
	context.arg.чСтроку8ДоКонцаСтрокиТекстаВключительно (path);
	Strings.TrimWS (path);
	OpenFile (path, context.error, context.caller);
кон Open;

кон FileHandlers.

System.Free FileHandlers ~
