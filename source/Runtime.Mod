(* Generic runtime support *)
(* Copyright (C) Florian Negele *)

модуль Runtime;

использует НИЗКОУР, BaseTypes, Трассировка;

тип TrapHandler* = проц (number: размерМЗ);

перем trapHandler*: TrapHandler;

(** This procedure aborts the program and prints the number of the trap that caused the program to fail. *)
(** The compiler calls this procedure for HALT statements or for unsatisfied ASSERT statements. *)
проц {NORETURN} Trap- (number: размерМЗ);
проц Abort отКомпоновщика "Environment.Abort";
нач {безКооперации, безОбычныхДинПроверок}
	если trapHandler # НУЛЬ то
		trapHandler (number);
	иначе
		Трассировка.ПишиКрасным; Трассировка.пСтроку8 ("trap: "); Трассировка.пЦел64 (number, 0); Трассировка.пВК_ПС;
		Трассировка.ПишиЦветомПоУмолчанию; Трассировка.пКадрыСтека (1, 8, 20 * размерОт адресВПамяти);
	всё;
	Abort;
кон Trap;

(** This procedure acquires memory and returns the address to the first byte or NIL if the allocation fails. *)
(** The compiler implements all NEW statements with a call to this procedure. *)
проц New- (size: размерМЗ): адресВПамяти;
перем result: адресВПамяти; tries: размерМЗ;
проц CollectGarbage отКомпоновщика "GarbageCollector.Collect";
проц Allocate отКомпоновщика "Environment.Allocate" (size: размерМЗ): адресВПамяти;
нач {безКооперации, безОбычныхДинПроверок}
	нцДля tries := 1 до 10 делай
		result := Allocate (size);
		если result # НУЛЬ то возврат result всё;
		CollectGarbage;
	кц;
	возврат НУЛЬ;
кон New;

(** This procedure releases memory that was previously acquired by a call to the [[Runtime.New]] procedure. *)
(** The compiler implements DISPOSE statements on types marked as disposable with a call to this procedure. *)
проц Dispose- (перем pointer {неОтслСборщиком}: BaseTypes.Pointer);
проц Deallocate отКомпоновщика "Environment.Deallocate" (address: адресВПамяти);
проц Watch отКомпоновщика "GarbageCollector.Watch" (pointer {неОтслСборщиком}: BaseTypes.Pointer);
нач {безКооперации, безОбычныхДинПроверок}
	утв (pointer # НУЛЬ);
	если pointer суть BaseTypes.Object то pointer(BaseTypes.Object).Finalize всё;
	если (pointer суть BaseTypes.Pointer) и (pointer(BaseTypes.Pointer).nextWatched = НУЛЬ) то Watch (pointer(BaseTypes.Pointer));
	иначе Deallocate (pointer) всё; pointer := НУЛЬ;
кон Dispose;

кон Runtime.

Runtime.Obw
