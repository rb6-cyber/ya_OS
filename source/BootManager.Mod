(* ETH Oberon, Copyright 2001 ETH Zuerich Institut fuer Computersysteme, ETH Zentrum, CH-8092 Zuerich.
Refer to the "General ETH Oberon System Source License" contract available at: http://www.oberon.ethz.ch/ *)

модуль BootManager;
(** AUTHOR "afi"; PURPOSE "Split the boot manager object code BootManager.Bin"; *)

использует Commands, Files;

(** Split the boot manager object code BootManager.Bin, obtained by assembling BootManager.Asm,
	into a 512-byte MBR and the rest of it in bmsize sectors. *)
проц Split* (context: Commands.Context);
перем fileName: Files.FileName;
	fi, fo, fo2 : Files.File; r : Files.Reader; w : Files.Writer;
	count,i : цел32; ch : симв8;

нач
	context.arg.ПропустиБелоеПоле; context.arg.чЦепочкуСимволов8ДоБелогоПоляИлиВКавычках (fileName);
	fi := Files.Old(fileName);
	fo := Files.New("BootManagerMBR.Bin");
	fo2 := Files.New("BootManagerTail.Bin");
	если fi # НУЛЬ то
		Files.OpenReader(r, fi, 0);
		Files.OpenWriter(w, fo, 0);
		i := 0;
		нцДля count := 1 до 512 делай
			r.чСимв8(ch);
			w.пСимв8(ch);
			увел(i);
		кц;
		w.ПротолкниБуферВПоток;
		Files.Register(fo);

		Files.OpenWriter(w, fo2, 0);
		нцПока r.ЗапасиБайтыИзПотокаВБуфереИДайИхКоличество() > 0 делай
			r.чСимв8(ch);
			w.пСимв8(ch);
			увел(i);
		кц;
		нцПока i остОтДеленияНа 512 # 0 делай
			w.пСимв8(0X);
			увел(i);
		кц;
		w.ПротолкниБуферВПоток;
		Files.Register(fo2);
	всё;
кон Split;

нач
кон BootManager.

BootManager.Split BootManager.Bin ~
