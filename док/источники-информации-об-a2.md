Раньше у A2 был сайт http://www.ocp.inf.ethz.ch, но он уничтожен и зеркала я не видел. Это печально. 
Когда-то я снял зеркало, но оно кривовато получилось и не работает при неработающем форуме. Т.е. инфу там просматривать крайне неудобно. Вместо этого появился новый сайт: http://cas.inf.ethz.ch

Вот что имеется на сегодня:

* Форум оберонцоре: https://forum.oberoncore.ru/viewforum.php?f=22 (рус) 
* Новый форум, почти пустой: http://cas.inf.ethz.ch/projects/a2/boards - заметание следов налицо :)
* Списки документов, в т.ч. инструкций и диссертаций, от Ярослава Романченко
    * https://gitlab.com/YarRom/a2os/tree/mod
    * https://gitlab.com/YarRom/a2os/tree/mod/Docs
* Размеченные исходные тексты с переходом к определению 
    * [Мои - поновее](http://программирование-по-русски.рф/static/размеченные-исходники-A2/содержание.яргт),
    * [bbos.org - исконные, от автора инструмента](http://bbos.org/xref/)
* [Описание языка](http://cas.inf.ethz.ch/projects/a2/repository/raw/trunk/LanguageReport/OberonLanguageReport.pdf) (pdf, 2019, англ, черновик)
* [The Active Object System Design and Multiprocessor Implementation](http://oberon2005.oberoncore.ru/paper/eth14755.pdf) (pdf, 2002, англ, диссертация Мюллера, содержит определения "активного объекта" и "активности")
* [Система учёта ошибок A2](http://cas.inf.ethz.ch/projects/a2/issues)

Также есть группа в телеграм, ссылка для входа на неё есть где-то на oberoncore. 
